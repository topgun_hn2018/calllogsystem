﻿namespace CallLogSystem.Data.DataAccess.UoW
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using Repository;

    /// <summary>
    /// The UnitOfWork
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    /// <seealso cref="IUnitOfWork" />
    /// <seealso cref="System.IDisposable" />
    public class UnitOfWork<TContext> : IUnitOfWork, IDisposable where TContext : DbContext, new()
    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly TContext context;

        /// <summary>
        /// The repositories
        /// </summary>
        private readonly Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        /// <summary>
        /// The disposed
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork{TContext}"/> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        public UnitOfWork(TContext context)
        {
            this.context = context;
        }
        
        /// <summary>
        /// Gets generic repository
        /// </summary>
        /// <typeparam name="T">The generic type</typeparam>
        /// <returns>The generic repository</returns>
        public IGenericRepository<T> Repo<T>()
            where T : class, new()
        {
            if (this.repositories.Keys.Contains(typeof(T)))
            {
                return this.repositories[typeof(T)] as IGenericRepository<T>;
            }

            IGenericRepository<T> repo = new GenericRepository<T, TContext>(this.context);
            this.repositories.Add(typeof(T), repo);
            return repo;
        }

        /// <summary>
        /// Commits the asynchronous.
        /// </summary>
        /// <returns>
        /// The commit result
        /// </returns>
        public int CommitAsync()
        {
            return this.context.SaveChanges();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.context.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}