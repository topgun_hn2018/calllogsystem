﻿namespace CallLogSystem.Data.DataAccess.UoW
{
    using System.Threading.Tasks;
    using Repository;

    /// <summary>
    /// The IUnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Gets generic repository
        /// </summary>
        /// <typeparam name="T">The generic type</typeparam>
        /// <returns>The generic repository</returns>
        IGenericRepository<T> Repo<T>()
            where T : class, new();

        /// <summary>
        /// Commits the asynchronous.
        /// </summary>
        /// <returns>The commit result</returns>
        int CommitAsync();

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        void Dispose();
    }
}