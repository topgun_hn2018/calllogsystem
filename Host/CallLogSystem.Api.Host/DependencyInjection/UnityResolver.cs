﻿namespace CallLogSystem.Host.Api.DependencyInjection
{
    using System.Data.Entity;
    using BusinessLogic.Authentication;
    using BusinessLogic.RequestManagement.Components;
    using BusinessLogic.RequestManagement.Interfaces;
    using BusinessLogic.UploadDocument;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Microsoft.Practices.Unity;

    public class UnityResolver
    {
        public static void Register(UnityContainer container)
        {
            container.RegisterType<IAuthenticationBusinessLogic, AuthenticationBusinessLogic>();
            container.RegisterType<IUploadDocument, UploadDocument>();
            container.RegisterType<IAccountManagement, AccountManagement>();
            container.RegisterType<IRolePermission, RolePermission>();
            container.RegisterType<IServiceConfiguration, ServiceConfiguration>();
            container.RegisterType<IRequestConfiguration, RequestConfiguration>();
            container.RegisterType<IRequestGroupManagement, RequestGroupManagement>();
            container.RegisterType<IPartyManagement, PartyManagement>();
            container.RegisterType<IReport, Report>();
            container.RegisterType<IEmailManagement, EmailManagement>();
            container.RegisterType<IWorkingTimeManagement, WorkingTimeManagement>();
            container.RegisterType<ISystemConfiguration, SystemConfiguration>();
            container.RegisterType<ISecurity, Security>();
            container.RegisterType<DbContext, CallLogEntities>();
            container.RegisterType<IUnitOfWork, UnitOfWork<CallLogEntities>>();
        }
    }
}