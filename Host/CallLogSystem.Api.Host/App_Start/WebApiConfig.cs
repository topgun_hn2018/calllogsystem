﻿namespace CallLogSystem.Host.Api
{
    using System.Web.Http;
    using DependencyInjection;
    using FilterAttribute;
    using Microsoft.Practices.Unity;

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            UnityResolver.Register(container);
            config.DependencyResolver = new ServiceHostFactory(container);
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            
            config.Filters.Add(new AuthorizationFilterAttribute());
            config.Filters.Add(new ExceptionHandlingAttribute());
        }
    }
}
