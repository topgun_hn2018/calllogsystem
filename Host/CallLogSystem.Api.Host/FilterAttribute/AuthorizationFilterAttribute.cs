﻿namespace CallLogSystem.Host.Api.FilterAttribute
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using System.Web;
    using System.Web.Http.Controllers;
    using BusinessLogic.Authentication.Contract.Model;
    using BusinessLogic.Authentication.Jwt;
    using Common.Constant;
    using Common.Enum;
    using Common.Utilites;
    using log4net;

    public class AuthorizationFilterAttribute : System.Web.Http.Filters.AuthorizationFilterAttribute
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// On Authorization
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            //Get login url
            string loginUrl = ConfigUtilities.GetStringKey(ConfigurationConstant.LogOnUrl, "");
            //Get download url
            string downloadUrl = ConfigUtilities.GetStringKey(ConfigurationConstant.DownloadUrl, "");
            //Get url of sending email function for rating service 
            string sendEmailUrl = ConfigUtilities.GetStringKey(ConfigurationConstant.CloseRequestAutomatically, "");
            string uri = filterContext.Request.RequestUri.AbsoluteUri.ToLower();

            //Skip check Access Token if next url is login or download or sending email for rating service
            if (uri == loginUrl.ToLower() ||
                (uri.Contains("?") && uri.Substring(0, uri.IndexOf("?", StringComparison.Ordinal)) == downloadUrl.ToLower()) ||
                (uri.Contains("?") && uri.Substring(0, uri.IndexOf("?", StringComparison.Ordinal)) == sendEmailUrl.ToLower()))
            {
                return;
            }

            //Check access token
            var authorizationHeader = filterContext.Request.Headers.Authorization;
            if (authorizationHeader != null)
            {
                var accessToken = filterContext.Request.Headers.Authorization.Parameter;
                TokenValues tokenValues = null;
                JwtValidation jwtValidation = new JwtValidation();
                ValidationTokenResult result = jwtValidation.ValidateToken(accessToken, ref tokenValues);
                switch (result)
                {
                    //Session Timeout
                    case ValidationTokenResult.Expired:
                        filterContext.Response = filterContext.Request.CreateResponse((HttpStatusCode)440);
                        break;
                    //Access Denied
                    case ValidationTokenResult.InvalidSignature:
                    //Exception when validate token
                    case ValidationTokenResult.Invalid:
                        this._log.ErrorFormat("The access token {0} is invalid signature", accessToken);
                        filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                        break;
                    //Token is valid
                    case ValidationTokenResult.Valid:
                        //renew the Token, extend the expiration time
                        string extendedToken = JwtHelper.GenerateToken(new UserProfile
                        {
                            FullName = tokenValues.FullName,
                            UserId = tokenValues.UserId,
                            UserInfo = new List<UserInfo>
                            {
                                new UserInfo
                                {
                                    UserAliasId = tokenValues.UserAliasId,
                                    CompanyId = tokenValues.CompanyId,
                                    CompanyName = tokenValues.CompanyName,
                                    IsDefault = true
                                }
                            }
                        });
                        if (HttpContext.Current != null)
                        {
                            HttpContext.Current.Items["UserAliasId"] = tokenValues.UserAliasId;
                            HttpContext.Current.Items["UserId"] = tokenValues.UserId;
                            HttpContext.Current.Items["CompanyId"] = tokenValues.CompanyId;
                            HttpContext.Current.Items["AccessToken"] = extendedToken;
                        }
                        break;
                }
            }
            else
            {
                this._log.Error("The access token is missing");
                filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }
    }
}