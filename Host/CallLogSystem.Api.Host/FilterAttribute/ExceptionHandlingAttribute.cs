﻿namespace CallLogSystem.Host.Api.FilterAttribute
{
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.Filters;
    using log4net;

    /// <summary>
    /// Exception Handling Attribute
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ExceptionFilterAttribute" />
    public class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <param name="context">The context.</param>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent">An error occurred, please try again or contact the administrator.</exception>
        public override void OnException(HttpActionExecutedContext context)
        {
            //Log Critical errors]
            this._log.ErrorFormat("{0}", context.Exception.Message);

            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent("An error occurred, please try again or contact the administrator."),
                ReasonPhrase = "Critical Exception"
            });
        }
    }
}