﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class WorkingTimeController : ApiController
    {
         private readonly IWorkingTimeManagement _businessLogic;

         public WorkingTimeController(IWorkingTimeManagement businessLogic)
        {
            this._businessLogic = businessLogic;
        }
        [HttpPost]
        public SaveResponse SaveWorkingTime(SaveWorkingTimeRequest request)
        {
            var response = this._businessLogic.SaveWorkingTime(request);
            return response;
        }

        [HttpPost]
        public SaveResponse SaveHoliday(SaveHolidayRequest request)
        {
            var response = this._businessLogic.SaveHoliday(request);
            return response;
        }

        [HttpPost]
        public GetWorkingTimeResponse GetWorkingTime(GetWorkingTimeRequest request)
        {
            var response = this._businessLogic.GetWorkingTime(request);
            return response;
        }

        [HttpPost]
        public GetHolidayResponse GetHoliday(GetHolidayRequest request)
        {
            var response = this._businessLogic.GetHoliday(request);
            return response;
        }
    }
}