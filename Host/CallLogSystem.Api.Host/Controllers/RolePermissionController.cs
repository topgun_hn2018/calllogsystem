﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class RolePermissionController : ApiController
    {
         private readonly IRolePermission _businessLogic;

         public RolePermissionController(IRolePermission businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        public RolePermissionController()
        {
        }
        [HttpPost]
        public SaveRoleResponse SaveRole(SaveRoleRequest request)
        {
            //Implement code here
            var response = this._businessLogic.SaveRole(request);
            return response;
        }

        [HttpGet]
        public SearchPermissionResponse GetAllPermission()
        {
            var response = this._businessLogic.GetAllPermission();

            return response;
        }

        [HttpPost]
        public SearchRoleResponse SearchRole(SearchRoleRequest request)
        {
            var response = this._businessLogic.SearchRole(request);
            return response;
        }

        [HttpPost]
        public GetRoleByIdResponse GetRoleById(GetRoleByIdRequest request)
        {
            var response = this._businessLogic.GetRoleById(request);
            return response;
        }

        [HttpGet]
        public SearchRoleResponse GetAllRoles()
        {
            var request = new SearchRoleRequest
            {
                Name = "",
                IsActive = 1
            };
            var response = this._businessLogic.SearchRole(request);

            return response;
        }
        
    }
}