﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class AccountManagementController : ApiController
    {
         private readonly IAccountManagement _businessLogic;

         public AccountManagementController(IAccountManagement businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        public AccountManagementController()
        {
        }
        [HttpPost]
        public SaveAccountResponse SaveAccount(SaveAccountRequest request)
        {
            //Implement code here
            var response = this._businessLogic.SaveAccount(request);
            return response;
        }
        [HttpPost]
        public SaveAliasResponse SaveAlias(SaveAliasRequest request)
        {
            //Implement code here
            var response = this._businessLogic.SaveAlias(request);
            return response;
        }
        
        [HttpGet]
        public SearchAccountResponse SearchAccount([FromUri]SearchAccountRequest request)
        {
            var response = this._businessLogic.SearchAccount(request);
            return response;
        }

        [HttpPost]
        public GetAccountByIdResponse GetAccountById(GetAccountByIdRequest request)
        {
            var response = this._businessLogic.GetAccountById(request);
            return response;
        }
        [HttpPost]
        public GetAliasByIdResponse GetAliasById(GetAliasByIdRequest request)
        {
            var response = this._businessLogic.GetAliasById(request);
            return response;
        }
        [HttpPost]
        public DeleteAliasResponse DeleteAlias(DeleteAliasRequest request)
        {
            var response = this._businessLogic.DeleteAlias(request);
            return response;
        }
        

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public GetAllUserResponse GetAllUsers()
        {
            return this._businessLogic.GetAllUsers(new GetAllUserRequest());
        }
        
    }
}