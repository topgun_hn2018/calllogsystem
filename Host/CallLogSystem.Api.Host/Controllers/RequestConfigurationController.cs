﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class RequestConfigurationController:ApiController
    {
         private readonly IRequestConfiguration _businessLogic;

         public RequestConfigurationController(IRequestConfiguration businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        /// <summary>
        /// Gets all request types.
        /// </summary>
        /// <returns></returns>
        public GetAllRequestTypeResponse GetAllRequestTypes()
        {
            return this._businessLogic.GetAllRequestTypes();
        }

        [HttpPost]
        public SearchRequestTypeResponse SearchRequestType(SearchRequestTypeRequest request)
        {
            var response = this._businessLogic.SearchRequestType(request);
            return response;
        }

        [HttpPost]
        public GetRequestTypeByIdResponse GetRequestTypeById(GetByIdRequest request)
        {
            var response = this._businessLogic.GetRequestTypeById(request);
            return response;
        }
        [HttpPost]
        public SaveResponse SaveRequestType(SaveRequestTypeRequest request)
        {
            var response = this._businessLogic.SaveRequestType(request);
            return response;
        }
        
        [HttpGet]
        public GetAllRequestStatusResponse GetRequestStatus()
        {
            var response = this._businessLogic.GetAllRequestStatus();
            return response;
        }
    }
}