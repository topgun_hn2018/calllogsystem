﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web;
    using System.Web.Http;
    using BusinessLogic.Authentication;
    using BusinessLogic.Authentication.Contract.Request;
    using BusinessLogic.Authentication.Contract.Response;
    using BusinessLogic.Authentication.Jwt;
    using Common.Enum;

    public class AuthenticationController:ApiController
    {
        private readonly IAuthenticationBusinessLogic _businessLogic;

        public AuthenticationController(IAuthenticationBusinessLogic businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        /// <summary>
        /// LogOn
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public LoginResponse LogOn([FromBody]LoginRequest request)
        {
            if (request == null)
            {
                return new LoginResponse
                {
                    ErrorType = ErrorType.InvalidRequest
                };
            }
            LoginResponse response = this._businessLogic.LogOn(request);
            if (response.ErrorType == ErrorType.NoError)
            {
                HttpContext.Current.Items["AccessToken"] = JwtHelper.GenerateToken(response.UserProfile);
            }
            return response;
        }

         /// <summary>
         /// Switches the site.
         /// </summary>
         /// <param name="request">The request.</param>
         /// <returns></returns>
         [HttpPost]
         public SwitchSiteResponse SwitchSite(SwitchSiteRequest request)
         {
             if (request == null)
             {
                 return new SwitchSiteResponse
                 {
                     ErrorType = ErrorType.InvalidRequest
                 };
             }
             SwitchSiteResponse response = this._businessLogic.SwitchSite(request);
             if (response.ErrorType == ErrorType.NoError)
             {
                 HttpContext.Current.Items["AccessToken"] = JwtHelper.GenerateToken(response.UserProfile);
             }
             return response;
         }
    }
}