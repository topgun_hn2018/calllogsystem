﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class SysConfigurationController : ApiController
    {
        private readonly ISystemConfiguration _businessLogic;

        public SysConfigurationController(ISystemConfiguration businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        public SysConfigurationController()
        {
        }
        [HttpPost]
        public SaveSystemConfigurationResponse SaveSystemConfiguration(SaveSystemConfigurationRequest request)
        {
            //Implement code here
            var response = this._businessLogic.SaveSystemConfiguration(request);
            return response;
        }

        [HttpGet]
        public GetSystemConfigurationResponse GetSystemConfiguration()
        {
            var response = this._businessLogic.GetSystemConfiguration();

            return response;
        }
    }
}