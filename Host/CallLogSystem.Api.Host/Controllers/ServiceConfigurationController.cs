﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;
    using Common.Enum;

    public class ServiceConfigurationController:ApiController
    {
         private readonly IServiceConfiguration _businessLogic;

         public ServiceConfigurationController(IServiceConfiguration businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        /// <summary>
        /// Gets all services
        /// </summary>
        /// <returns></returns>
        public GetAllServiceResponse GetAllServices()
        {
            return this._businessLogic.GetAllServices(new GetAllServiceRequest());
        }

         /// <summary>
         /// Saves the service.
         /// </summary>
         /// <param name="request">The request.</param>
         /// <returns></returns>
         [HttpPost]
         public SaveServiceResponse SaveService(SaveServiceRequest request)
         {
             if (request == null)
             {
                 return new SaveServiceResponse
                 {
                     ErrorType = ErrorType.InvalidRequest
                 };
             }
             return this._businessLogic.SaveService(request);
         }

         /// <summary>
         /// Searches the service.
         /// </summary>
         /// <param name="request">The request.</param>
         /// <returns></returns>
         [HttpPost]
         public SearchServiceResponse SearchService(SearchServiceRequest request)
         {
             if (request == null)
             {
                 return new SearchServiceResponse
                 {
                     ErrorType = ErrorType.Error
                 };
             }
             return this._businessLogic.SearchService(request);
         }

         /// <summary>
         /// Gets the service by identifier.
         /// </summary>
         /// <param name="serviceId">The service identifier.</param>
         /// <returns></returns>
         [HttpGet]
         public GetServiceByIdResponse GetServiceById(int serviceId)
         {
             return this._businessLogic.GetServiceById(serviceId);
         }
    }
}