﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class SecurityController : ApiController
    {
         private readonly ISecurity _businessLogic;

         public SecurityController(ISecurity businessLogic)
        {
            this._businessLogic = businessLogic;
        }
        /// <summary>
        /// Gets the permission.
        /// </summary>
        /// <param name="functionKey">Name of the function.</param>
        /// <returns></returns>
        [HttpGet]
        public GetPermissionResponse GetPermission(string functionKey)
        {
            return this._businessLogic.GetPermissionByFunction(new GetPermissionRequest
            {
                FunctionKey = functionKey
            });
        }

        /// <summary>
        /// Gets the permission accessing.
        /// </summary>
        /// <returns></returns>
         [HttpGet]
         public GetPermissionAccessingResponse GetPermissionAccessing()
        {
            return this._businessLogic.GetPermissionAccessing(new GetPermissionAccessingRequest());
        }
    }
}