﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;
    using Common.ContractBase;

    public class EmailTemplateController : ApiController
    {
        private readonly IEmailManagement _businessLogic;

        public EmailTemplateController(IEmailManagement businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        public EmailTemplateController()
        {
        }

        [HttpPost]
        public SearchEmailTemplateResponse SearchEmailTemplate(SearchEmailTemplateRequest request)
        {
            var response = this._businessLogic.SearchEmailTemplate(request);
            return response;
        }
        [HttpPost]
        public SaveEmailTemplateResponse SaveEmailTemplate(SaveEmailTemplateRequest request)
        {
            //Implement code here
            var response = this._businessLogic.SaveEmailTemplate(request);
            return response;
        }

        [HttpPost]
        public GetEmailTemplateByIdResponse GetEmailTemplateById(GetEmailTemplateByIdRequest request)
        {
            var response = this._businessLogic.GetEmailTemplateById(request);
            return response;
        }
        [HttpPost]
        public PreSendEmailTemplateResponse PreSendEmailTemplate(PreSendEmailTemplateRequest request)
        {
            var response = this._businessLogic.PreSendEmailTemplate(request);
            return response;
        }

        [HttpGet]
        public BaseResponse SendEmailAfterCloseRequestAutomatically(int requestId)
        {

            var response = this._businessLogic.SendEmailAfterCloseRequestAutomatically(requestId);
            return response;
        }
    }
}