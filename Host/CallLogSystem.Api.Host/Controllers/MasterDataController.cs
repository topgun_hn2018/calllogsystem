﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Utilities;

    public class MasterDataController : ApiController
    {
        /// <summary>
        /// Gets the list key objects.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
         [HttpGet]
         public GetListKeyObjectsResponse GetListKeyObjects(string tableName)
        {
            var keyObjects = KeyObjectUtilities<int>.GetListKeyObjects(tableName);
             return new GetListKeyObjectsResponse
             {
                 KeyObjects = keyObjects
             };
        }

         /// <summary>
         /// Gets the key object.
         /// </summary>
         /// <param name="tableName">Name of the table.</param>
         /// <param name="key">The key.</param>
         /// <returns></returns>
         public int GetKeyObject(string tableName, string key)
         {
             return KeyObjectUtilities<int>.GetKeyObject(tableName, key);
         }
    }
}