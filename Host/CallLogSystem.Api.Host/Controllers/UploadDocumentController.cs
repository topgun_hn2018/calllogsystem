﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Web;
    using System.Web.Http;
    using BusinessLogic.UploadDocument;
    using BusinessLogic.UploadDocument.Contract.Request;
    using BusinessLogic.UploadDocument.Contract.Response;
    using Common.ContractBase;
    using Common.Enum;

    public class UploadDocumentController:ApiController
    {
        private readonly IUploadDocument _businessLogic;

        public UploadDocumentController(IUploadDocument businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        /// <summary>
        /// Downloads Document
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="uploadedDate"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get([FromUri] string fileName, DateTime uploadedDate, string fileType)
        {
            var response = this._businessLogic.DownloadDocument(new DownloadDocumentRequest
            {
                FileName = fileName,
                UploadedDate = uploadedDate,
                FileType = fileType
            });

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(response.ByteStream))
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        /// <summary>
        /// Uploads Document
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public UploadDocumentResponse Upload()
        {
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count != 0)
            {
                var postedFile = httpRequest.Files[0];
                var bytesInStream = new byte[postedFile.InputStream.Length];
                postedFile.InputStream.Read(bytesInStream, 0, bytesInStream.Length);
                var response = this._businessLogic.SaveDocument(new UploadDocumentRequest
                {
                    FileNameServer = postedFile.FileName,
                    FileType = postedFile.ContentType,
                    Length = postedFile.ContentLength,
                    ByteStream = bytesInStream,
                    FilePosition = (int)postedFile.InputStream.Position
                });
                if (response.IsSuccess)
                {
                    return new UploadDocumentResponse
                    {
                        DocumentId = response.DocumentId,
                        FileNameServer = response.FileNameServer,
                        Extension = response.Extension,
                        FileName = response.FileName,
                        FileType = response.FileType,
                        Length = response.Length,
                        UploadedBy = response.UploadedBy,
                        UploadedByName = response.UploadedByName,
                        UploadedDate = response.UploadedDate,
                        IsSuccess = true
                    };    
                }
                return response;
            }
            return new UploadDocumentResponse
            {
                ErrorType = ErrorType.Error,
                //TODO: define new error message
                //ErrorDescription = "MSG68"
            };
        }

        /// <summary>
        /// Uploads Category
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public UploadDocumentResponse UploadCate()
        {
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count != 0)
            {
                var postedFile = httpRequest.Files[0];
                var bytesInStream = new byte[postedFile.InputStream.Length];
                postedFile.InputStream.Read(bytesInStream, 0, bytesInStream.Length);
                var response = this._businessLogic.SaveDocument(new UploadDocumentRequest
                {
                    FileNameServer = postedFile.FileName,
                    FileType = postedFile.ContentType,
                    Length = postedFile.ContentLength,
                    ByteStream = bytesInStream,
                    FilePosition = (int)postedFile.InputStream.Position,
                    IsCategory = true
                });
                if (response.IsSuccess)
                {
                    return new UploadDocumentResponse
                    {
                        DocumentId = response.DocumentId,
                        FileNameServer = response.FileNameServer,
                        Extension = response.Extension,
                        FileName = response.FileName,
                        FileType = response.FileType,
                        Length = response.Length,
                        UploadedBy = response.UploadedBy,
                        UploadedByName = response.UploadedByName,
                        UploadedDate = response.UploadedDate,
                        IsSuccess = true
                    };
                }
                return response;
            }
            return new UploadDocumentResponse
            {
                ErrorType = ErrorType.Error,
                //TODO: define new error message
                //ErrorDescription = "MSG68"
            };
        }

        /// <summary>
        /// Deletes Document
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="uploadedDate"></param>
        /// <returns></returns>
        [HttpGet]
        public UploadBaseResponse Delete(string fileName, DateTime uploadedDate)
        {
            var response = this._businessLogic.RemoveDocument(new RemoveDocumentRequest
            {
                FileName = fileName, UploadedDate = uploadedDate
            });
            return new UploadBaseResponse
            {
                ErrorDescription = response.ErrorDescription,
                ErrorType = response.ErrorType
            };
        }

    }
}