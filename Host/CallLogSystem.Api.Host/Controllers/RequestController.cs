﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System;
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;

    public class RequestController : ApiController
    {
        /// <summary>
        /// Gets all related request with user
        /// </summary>
        /// <returns></returns>
        public GetRelatedRequestResponse GetRelatedRequest()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CreateRequestResponse CreateRequest(CreateRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Updates the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public UpdateRequestResponse UpdateRequest(UpdateRequestRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Searches the specified search criteria.
        /// </summary>
        /// <param name="searchCriteria">The search criteria.</param>
        /// <returns></returns>
        public SearchResponse Search(SearchRequest searchCriteria)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Searches the specified search criteria.
        /// </summary>
        /// <param name="searchCriteria">The search criteria.</param>
        /// <returns></returns>
        public SearchResponse SearchRequest(SearchRequest searchCriteria)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets request by id
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public GetRequestByIdResponse GetRequestById(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the request.
        /// </summary>
        /// <param name="deleteRequest">The delete request.</param>
        /// <returns></returns>
        public DeleteRequestResponse DeleteRequest(DeleteRequestRequest deleteRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates the comment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CreateCommentResponse CreateComment(CreateCommentRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the comments.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <returns></returns>
        public GetCommentResponse GetHistories(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Follows the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public FollowRequestResponse FollowRequest(FollowRequestRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets process info
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public GetProcessInfoResponse GetProcessInfo(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Confirms the result.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        public ConfirmResultResponse ConfirmResult(ConfirmResultRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the confirm result.
        /// </summary>
        /// <returns></returns>
        public GetConfirmResultResponse GetConfirmResult(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the approval levels.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public GetApprovalLevelResponse GetApprovalLevels(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the approval request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        public SearchResponse SearchApprovalRequest(SearchRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Approves the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        public ApproveRequestResponse ApproveRequest(ApproveRequestRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the approval.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public GetApprovalResponse GetApproval(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the assign request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SearchResponse SearchAssignRequests(SearchAssignRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the groups by request.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <returns></returns>
        public GetGroupByRequestResponse GetGroupsByRequest(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the users by request.
        /// </summary>
        /// <param name="requestId">The request identifier.</param>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        public GetUsersByRequestResponse GetUsersByRequest(int requestId, int groupId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Assigns the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssignRequestResponse AssignRequest(AssignRequestRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets request assign
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public GetRequestAssignResponse GetRequestAssign(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the request to process.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetProcessResponse GetProcessRequest(GetProcessRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the process request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ProcessRequestResponse ProcessRequest(ProcessRequestRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get child of the request.
        /// </summary>
        /// <param name="requestId">request.</param>
        /// <returns></returns>
        public GetChildRequestResponse GetChildRequest(int requestId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get request to process
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetRequestByIdResponse GetRequestToProcess(GetRequestToProcessRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get relate request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SearchRelateRequestResponse GetRelatedRequest(SearchRelateRequestRequest request)
        {
            throw new NotImplementedException();
        }
    }
}