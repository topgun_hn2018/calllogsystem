﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class RequestGroupManagementController : ApiController
    {
         private readonly IRequestGroupManagement _businessLogic;

         public RequestGroupManagementController(IRequestGroupManagement businessLogic)
        {
            this._businessLogic = businessLogic;
        }

        [HttpPost]
        public SearchCategoriesResponse SearchCategories(SearchCategoriesRequest request)
        {
            var response = this._businessLogic.SearchCategories(request);
            return response;
        }

        [HttpGet]
        public SearchCategoriesResponse GetAllCategoriesActive(SearchCategoriesRequest request)
        {
            var response = this._businessLogic.GetAllCategoryActive(request);
            return response;
        }

        [HttpPost]
        public GetCategoryByIdResponse GetCategoryById(GetByIdRequest request)
        {
            var response = this._businessLogic.GetCategoryById(request);
            return response;
        }
        [HttpGet]
        public SearchCategoriesResponse GetAllCategory()
        {
            var response = this._businessLogic.GetAllCategory();
            return response;
        }        

        [HttpPost]
        public SaveResponse SaveCategory(SaveCategoryRequest request)
        {
            var response = this._businessLogic.SaveCategory(request);
            return response;
        }
        
    }
}