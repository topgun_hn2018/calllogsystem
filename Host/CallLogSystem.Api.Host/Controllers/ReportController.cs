﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class ReportController:ApiController
    {
        private readonly IReport _businessLogic;

        public ReportController(IReport businessLogic)
        {
            this._businessLogic = businessLogic;
        }

         /// <summary>
         /// Get all active company
         /// </summary>
         /// <returns></returns>
        [HttpPost]
        public GetCompanyByUserResponse GetCompanyByUser(GetCompanyByUserRequest request)
        {
            return this._businessLogic.GetCompanyByUser(request);
        }

         /// <summary>
         /// Get department by company
         /// </summary>
         /// <param name="request"></param>
         /// <returns></returns>
        [HttpPost]
        public GetDepartmentByCompanyResponse GetDepartmentByCompany(GetDepartmentByCompanyRequest request)
        {
            return this._businessLogic.GetDepartmentByCompany(request);
        }

        /// <summary>
        /// Get group by department
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public GetGroupByDepartmentResponse GetGroupByDepartment(GetGroupByDepartmentRequest request)
        {
            return this._businessLogic.GetGroupByDepartment(request);
        }

        /// <summary>
        /// Get reports by admin
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public GetReportResponse GetReport(GetReportRequest request)
        {
            return this._businessLogic.GetReport(request);
        }

        /// <summary>
        /// Get reports by admin
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public GetReportResponse GetReportAssignee(GetReportRequest request)
        {
            return this._businessLogic.GetReportAssignee(request);
        }

        /// <summary>
        /// Get users report.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public GetUserReportResponse GetUserReport(GetAllUserRequest request)
        {
            return this._businessLogic.GetUserReport(request);
        }
    }
}