﻿namespace CallLogSystem.Host.Api.Controllers
{
    using System.Collections.Generic;
    using System.Web.Http;
    using BusinessLogic.RequestManagement.Contract.Request;
    using BusinessLogic.RequestManagement.Contract.Response;
    using BusinessLogic.RequestManagement.Interfaces;

    public class PartyConfigurationController:ApiController
    {
         private readonly IPartyManagement _businessLogic;

         public PartyConfigurationController(IPartyManagement businessLogic)
        {
            this._businessLogic = businessLogic;
        }


        [HttpPost]
        public SearchPartyResponse SearchParty(SearchPartyRequest request)
        {
            var response = this._businessLogic.SearchParty(request);
            return response;
        }
                
        [HttpPost]
        public SearchPartyResponse SearchDepartment(SearchDepartmentRequest request)
        {
            var response = this._businessLogic.SearchDepartment(request);
            return response;
        }
        [HttpPost]
        public SearchPartyResponse SearchGroup(SearchGroupRequest request)
        {
            var response = this._businessLogic.SearchGroup(request);
            return response;
        }
        
        [HttpPost]
        public GetPartyResponse GetPartyById(GetPartyRequest request)
        {
            var response = this._businessLogic.GetParty(request);
            return response;
        }

        [HttpGet]
        public GetAllCompanyResponse GetAllCompanies()
        {
            var response = this._businessLogic.GetAllCompanies();
            return response;
        }

        [HttpGet]
        public GetAllCompanyResponse GetActiveCompanies()
        {
            var response = this._businessLogic.GetActiveCompanies();
            return response;
        }

        [HttpGet]
        public GetAllUserResponse GetAllPartyUsers()
        {
            return this._businessLogic.GetAllPartyUsers();
        }

        [HttpGet]
        public GetAllUserResponse GetAllDepartmentUserManagers()
        {
            return this._businessLogic.GetAllDepartmentUserManagers();
        }

        [HttpGet]
        public GetAllUserResponse GetAllDepartmentUserReporters()
        {
            return this._businessLogic.GetAllDepartmentUserReporters();
        }

        [HttpGet]
        public GetAllUserResponse GetAllGroupUserLeaders()
        {
            return this._businessLogic.GetAllGroupUserLeaders();
        }

        [HttpGet]
        public GetAllUserResponse GetAllGroupUserMembers()
        {
            return this._businessLogic.GetAllGroupUserMembers();
        }

        [HttpPost]
        public GetDepartmentsResponse GetDepartments(GetDepartmentsRequest request)
        {
            return this._businessLogic.GetDepartments(request);
        }

        [HttpPost]
        public SaveResponse SaveParty(SavePartyRequest request)
        {
            var response = this._businessLogic.SaveParty(request);
            return response;
        }

        /// <summary>
        /// Gets the department by company.
        /// </summary>
        /// <param name="companyIds">The company ids.</param>
        /// <returns></returns>
        [HttpPost]
        public GetDepartmentByCompanyResponse GetDepartmentByCompany(List<int> companyIds)
        {
            return this._businessLogic.GetDepartmentByCompany(companyIds);
        }

        /// <summary>
        /// Gets the group by department.
        /// </summary>
        /// <param name="departmentIds">The department identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public GetGroupByDepartmentResponse GetGroupByDepartment(List<int> departmentIds)
        {
            return this._businessLogic.GetGroupByDepartment(departmentIds);
        }
    }
}