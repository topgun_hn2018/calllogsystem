﻿'use strict';
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require('webpack');
var path = require('path');
var APP = __dirname + '/app';
var MINIFY = process.env.NODE_ENV === 'Release';
module.exports = {
    context: APP,
    entry: {
        bundle: './bootstrap.js',
        'style.bundle': __dirname + '/content/css/style.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: 'js/[name].js'
    },
    externals: {
        angular: 'angular',
        jquery: 'jQuery',
        moment: 'moment'
    },
    resolve: {
        root: [path.join(__dirname, "libs")],
        alias: {
            jquery: __dirname + '/libs/jquery/jquery.min',
            angular: __dirname + '/libs/angular/angular.min',
            moment: __dirname + '/libs/moment/moment-with-locales.min',
            angularRoute: __dirname + '/libs/angular-route/angular-route.min',
            angularTranslate: __dirname + '/libs/angular-translate/angular-translate.min',
            angularTranslateStaticFileLoader: __dirname + '/libs/angular-translate-loader-static-files/angular-translate-loader-static-files.min',
            angularSpinner: __dirname + '/libs/angular-spinner/angular-spinner.min',
            angularSanitize: __dirname + '/libs/angular-sanitize/angular-sanitize.min',
            angularCookies: __dirname + '/libs/angular-cookies/angular-cookies.min',
            angularFileSaver: __dirname + '/libs/angular-file-saver/angular-file-saver.bundle.min',
            ngStorage: __dirname + '/libs/ngstorage/ngStorage.min',
            x2js: __dirname + '/libs/x2js/xml2json.min',
            spin: __dirname + '/libs/spin/spin.min',
            'spin.js': __dirname + '/libs/spin/spin.min'
        }
    },
    module: {
        loaders: [
          {
              test: /\.css$/,
              loader: ExtractTextPlugin.extract("style-loader", "css-loader")
          },
          {
              test: /\.less$/,
              loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
          },

          { test: /\.(png|gif|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
        ]
    },
    plugins: MINIFY ?
        [
            new webpack.ResolverPlugin(
                new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
            ),
            new ExtractTextPlugin('css/[name].css', {
                allChunks: true
            }),
            new webpack.optimize.UglifyJsPlugin({ minimize: true })
        ] :
        [
            new webpack.ResolverPlugin(
                new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
            ),
            new ExtractTextPlugin('css/[name].css', {
                allChunks: true
            })
        ]
}