﻿require('./bootstrap.min.css');


//Rating
require('./starr.min.css');

//Custom theme
require('./custom.min.css');

//Data table
require('./datatables/css/angular-datatables.min.css');
require('./datatables/css/jquery.dataTables.min.css');

//Multi selection
require('./select2.min.css');

//Popup
require('./pnotify.min.css');

//Loading indicator
require('./animate.min.css');
require('./jquery.loading-indicator.min.css');

require('./site.css');
require('./button.css');
require('./input.css');
require('./form.css');
