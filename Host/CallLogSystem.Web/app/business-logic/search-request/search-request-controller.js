﻿'use strict';

var ctrl = function searchrequest($routeParams, $scope, $q, $location, $translate, $filter, dataTableService, DTColumnDefBuilder, initialScriptService, requestDataService, serviceConfigurationDataService, uiDatetimePickerConfig, securityDataService, requestStatusConstant, masterDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    uiDatetimePickerConfig.enableTime = false;
    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [-1],
        ToDate: null,
        FromDate: null,
        CompletedStatusId: 1
    };

    vm.dtOptions = dataTableService.getDataTableOptions(3, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4)
    ];
    $.fn.dataTable.moment($scope.dateTimeMomment);
           
    $q.all([
       initialScriptService.set(),       
       securityDataService.getPermission('SearchRequest')      
    ]).then(function (results) {        
        vm.permission = results[1].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        
        vm.searchRequest();
        vm.loading = false;
    });
    vm.searchRequest = function (search) {
        
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            vm.searchCriteria = conditionSearchService.searchrequest;
        }
        requestDataService.searchRequest(vm.searchCriteria).success(function (response) {
            conditionSearchService.searchrequest=vm.searchCriteria ;
            vm.searchResult = response.Requests;                                
        });       
    };
    vm.getStatus = function (list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return parseInt(list[i].Value);
        }
        return false;
    };       
};

module.exports = ctrl;