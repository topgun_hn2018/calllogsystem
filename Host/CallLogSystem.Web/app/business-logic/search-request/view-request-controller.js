﻿'use strict';

var ctrl = function viewrequest($scope, $q, $routeParams, $translate, $location, $filter, initialScriptService, routerDataService, dataTableService, DTColumnDefBuilder, uploadDataService, requestDataService, FileSaver, masterDataService, requestStatusConstant, securityDataService) {
    var vm = this;
    vm.loading = true;    
    $.fn.dataTable.moment($scope.dateTimeMomment);    

    vm.requestId = $routeParams.requestid;

    $q.all([        
        securityDataService.getPermission('SearchRequest')
    ]).then(function (results) {
        vm.permission = results[0].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

        vm.loading = false;
    });

    vm.onDownload = function (item) {
        uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
            .success(function (response) {
                var file = new Blob([response], { type: item.FileType });
                FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
            });
    };    

    vm.back = function () {
        if ($routeParams.parentview) {
            $location.url('/' + $routeParams.parentview+'?keysearch=true');
        }
        else {
            $location.url('/searchrequest');
        }
    };    

    $('.stars').on('starrr:change', function (e, value) {       
        $('.stars-count').html(value);
    });
};

module.exports = ctrl;

