﻿'use strict';

var ctrl = function adddepartment($scope, $q, $translate, $location, $routeParams, $timeout, dataTableService, initialScriptService, partyConfigurationDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.partyType = "Dep";
    vm.partyId = $routeParams.PartyId;
    vm.department = { Id: null, PartyCode: "", PartyName: "", IsActive: true, IsBOD: false, CompanyId: "", SelectedManagements: [], SelectedReporters:[] };
    vm.Companies = [];
    vm.GroupLeaders = [];
    var data = { PartyTypeId: vm.partyType, PartyId: vm.partyId };
    vm.isAdd = false;
    if ($routeParams.Action == undefined) vm.isAdd = true;

    $q.all([
       partyConfigurationDataService.getActiveCompanies(),
       partyConfigurationDataService.getAllUsers(),
       securityDataService.getPermission('AddDepartment'),
       initialScriptService.set()
    ]).then(function (results) {
        vm.Companies = [];
        for (var i = 0; i < results[0].data.Companies.length; i++) {
            vm.Companies.push({ Id: results[0].data.Companies[i].Id, Name: results[0].data.Companies[i].Name });
        }
        if (!vm.isAdd) {
            vm.title = $translate.instant('ManageParty_EditDepartment');
            $q.all([
                partyConfigurationDataService.getPartyById(data)
            ]).then(function (subResults) {
                vm.isDepartment = true;
                vm.department = {};
                var selectedManagements = [];
                var selectedReporters = [];
                for (var i = 0; i < subResults[0].data.Department.DepartmentManagements.length; i++) {
                    selectedManagements.push(subResults[0].data.Department.DepartmentManagements[i].UserId);
                }
                for (var j = 0; j < subResults[0].data.Department.DepartmentReporters.length; j++) {
                    selectedReporters.push(subResults[0].data.Department.DepartmentReporters[j].UserId);
                }
                vm.department = subResults[0].data.Department;
                vm.department.SelectedManagements = selectedManagements;
                vm.department.SelectedReporters = selectedReporters;
                vm.department.SelectedManagements = initialScriptService.convertUsersArr(results[1].data.Users, vm.department.SelectedManagements);
                vm.department.SelectedReporters = vm.convertUsersArr(results[1].data.Users, vm.department.SelectedReporters);
                vm.rootObject = angular.copy(vm.department);
            });
        } else {
            initialScriptService.convertUsersArr(results[1].data.Users, vm.department.SelectedManagements);
            vm.convertUsersArr(results[1].data.Users, vm.department.SelectedReporters);
            vm.title = $translate.instant('ManageParty_AddDepartment');
            vm.rootObject = angular.copy(vm.department);
        }
        vm.permission = results[2].data.Actions;        
        vm.loading = false;
    });

    vm.backRequestPage = function() {
        if (JSON.stringify(vm.rootObject) !== JSON.stringify(vm.department)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };
    vm.back = function () {
        $location.url('/managedepartment?keysearch=true');
    };
    vm.Save = function () {
        if (!vm.department.CompanyId) {
            $('#company').val('');
        }
        if (initialScriptService.validateForm()) {
            var data = {
                PartyType: vm.partyType,
                Department: vm.department
            };
            partyConfigurationDataService.saveParty(data).success(function(response) {
                vm.back();
            });
        }
    };
    vm.changeCompany = function () {
        initialScriptService.removeValidateSelect('company');
    };

    vm.convertUsersArr = function (source, selectedValues) {
        var selectedStringValues = [];

        angular.forEach(selectedValues, function (item, index) {
            selectedStringValues.push(item.toString());
        });

        $('#users-ddl-2').select2({ data: source });
        $timeout(function() {
            $('#users-ddl-2').val(selectedStringValues).trigger('change');
        }, 100);
        return selectedStringValues;
    };
};

module.exports = ctrl;