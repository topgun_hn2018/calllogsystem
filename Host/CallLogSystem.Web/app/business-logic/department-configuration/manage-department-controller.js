﻿'use strict';

var ctrl = function managedepartment($scope, $q, $routeParams, $translate, $location, dataTableService, DTColumnDefBuilder, initialScriptService, partyConfigurationDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4).notSortable()
    ];


    $q.all([
      partyConfigurationDataService.getAllCompanies(),      
      securityDataService.getPermission('ManageDepartment'),
      securityDataService.getPermission('AddDepartment'),
      partyConfigurationDataService.getAllDepartmentUserManagers(),
      partyConfigurationDataService.getAllDepartmentUserReporters(),
      initialScriptService.set()
    ]).then(function (results) {
        vm.Companies = results[0].data.Companies;
        vm.permission = results[1].data.Actions;
        vm.permissionAddDepartment = results[2].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.DepartmentUserManagers = results[3].data.Users;
        vm.DepartmentUserReporters = results[4].data.Users;
        vm.searchManageParty();
        vm.loading = false;       
    });
    vm.searchManageParty = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            vm.selectedPartyName = conditionSearchService.managedepartment.PartyName;
            vm.CompanyId = conditionSearchService.managedepartment.CompanySearch;
            vm.selectedPartyCode = conditionSearchService.managedepartment.PartyCode;
            vm.selectedStatus = conditionSearchService.managedepartment.IsActive;
            vm.ManagerId = conditionSearchService.managedepartment.Manager;
            vm.ReporterId = conditionSearchService.managedepartment.Reporter;
        }
        var status;
        switch (vm.selectedStatus) {
        case "-1":
            status = null;
            break;
        case "1":
            status = true;
            break;
        case "0":
            status = false;
            break;
        default:
            status = null;
        }
        
        var data = {
            PartyName: vm.selectedPartyName,
            CompanySearch: vm.CompanyId,
            PartyCode: vm.selectedPartyCode,
            IsActive: status,
            Manager: vm.ManagerId,
            Reporter: vm.ReporterId
        }
        partyConfigurationDataService.SearchDepartment(data).success(function (response) {
            conditionSearchService.managedepartment.PartyName =vm.selectedPartyName;
            conditionSearchService.managedepartment.CompanySearch = vm.CompanyId;
            conditionSearchService.managedepartment.PartyCode = vm.selectedPartyCode;
            conditionSearchService.managedepartment.IsActive =vm.selectedStatus ;
            conditionSearchService.managedepartment.Manager =vm.ManagerId ;
            conditionSearchService.managedepartment.Reporter=vm.ReporterId;
            vm.searchResult = [];
            var predata = response.Parties;
            var len = predata.length;
            for (var i = 0; i < len; i += 1) {
                var party = {};
                party.PartyId = predata[i].PartyId;
                party.PartyName = predata[i].PartyName;
                party.PartyCode = predata[i].PartyCode;
                party.CompanyName = predata[i].CompanyName;
                party.IsActive = predata[i].IsActive === true ? $translate.instant('Manage_Using') : $translate.instant('Manage_NotUsing');
                vm.searchResult.push(party);
            }            
        });
    };
    vm.addNew = function() {
        $location.url('/adddepartment');
    };
    vm.edit = function(item) {
        $location.url('/adddepartment?PartyId=' + item.PartyId + '&Action=Edit');
    };

    vm.removeManagerId = function () {
        vm.ManagerId = 0;
    };

    vm.removeReporterId = function () {
        vm.ReporterId = 0;
    };
};

module.exports = ctrl;