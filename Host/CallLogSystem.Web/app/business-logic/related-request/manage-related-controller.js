﻿'use strict';

var ctrl = function request($routeParams, $scope, $q, $location, $translate, $filter, dataTableService, DTColumnDefBuilder, initialScriptService, requestDataService, serviceConfigurationDataService, uiDatetimePickerConfig, masterDataService, requestStatusConstant, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    uiDatetimePickerConfig.enableTime = false;
    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [-1],
        ToDate: null,
        FromDate: null,
        CompletedStatusId: 1,
        FilterBy: -1
    };
    vm.dtOptions = dataTableService.getDataTableOptions(4, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5)
    ];

    $.fn.dataTable.moment($scope.dateTimeMomment);

    vm.filterBy = [
        {
            Id: -1,
            Name: $translate.instant('ManageRelated_NotifyMe')
        },
        {
            Id: 0,
            Name: $translate.instant('ManageRelated_ReferDepart')
        },
        {
            Id: 1,
            Name: $translate.instant('ManageRelated_ReferCompany')
        }];

    vm.requestStatus = [];

    vm.services = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        }];

    vm.completedStatus = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        },
        {
            Id: 0,
            Name: $translate.instant('ManageRequest_Completed')
        },
        {
            Id: 1,
            Name: $translate.instant('ManageRequest_UnCompleted')
        }];
    vm.counter = {};
    vm.requestStatusConstant = requestStatusConstant;

    $q.all([
       initialScriptService.set(),
       serviceConfigurationDataService.getAllServices(),
        masterDataService.getKeyObjects('RequestStatus'),
        securityDataService.getPermission('ManageRelatedRequest'),
        securityDataService.getPermission('AddRequest'),
        securityDataService.getPermission('FollowRequest')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        vm.permissionAddRequest = results[4].data.Actions;
        vm.permissionFollowRequest = results[5].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if ($routeParams != null && $routeParams.keysearch == 'true' ) {
            vm.searchCriteria = conditionSearchService.managerelatedrequest;
        }
        vm.services = vm.services.concat(results[1].data.Services);
        vm.KeyObjects = results[2].data.KeyObjects;

        vm.requestStatus.push({ Id: -1, Name: $translate.instant('FilterAll') });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForApproval }),
            Name: $translate.instant('RequestStatus_WaitingForApproval')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.RejectedApproval }),
            Name: $translate.instant('RequestStatus_Rejected')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForAssigned }),
            Name: $translate.instant('RequestStatus_WaitingForAssigned')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForProcess }),
            Name: $translate.instant('RequestStatus_WaitingForProcess')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Processing }),
            Name: $translate.instant('RequestStatus_Processing')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.ProcessCompleted }),
            Name: $translate.instant('RequestStatus_Processed')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.RejectedProcess }),
            Name: $translate.instant('RequestStatus_Cancel')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Completed }),
            Name: $translate.instant('RequestStatus_Completed')
        });


        vm.searchRequest();
        vm.loading = false;
    });
    vm.getStatus = function (list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return list[i].Value;
        }
        return false;
    }

    vm.searchRequest = function (statusKey,search) {
        if (vm.checkValid() && initialScriptService.validatePastDate($scope.dateFormat)) {
            var temp = vm.requestStatus;
            if (statusKey!='' && statusKey!=undefined) {
                var requestStatusId = [vm.getStatus(vm.KeyObjects, { Key: statusKey })];
                vm.searchCriteria.RequestStatusId = requestStatusId;
                vm.requestStatus = [];
            }
            vm.invalidDateTo = false;
            requestDataService.searchRelatedRequest(vm.searchCriteria).success(function (response) {
                conditionSearchService.managerelatedrequest =vm.searchCriteria;
                vm.searchResult = response.Requests;
                vm.searchCriteria.RequestStatusId = requestStatusId == undefined ? vm.searchCriteria.RequestStatusId : requestStatusId;
                vm.filterSearch = response.Filters;
                vm.counter.waittingForApproval = $filter('filter')(vm.filterSearch, { RequestStatusId: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForApproval }) }).length;
                vm.counter.waitingForAssigned = $filter('filter')(vm.filterSearch, { RequestStatusId: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForAssigned }) }).length;
                vm.counter.waitingForProcess = $filter('filter')(vm.filterSearch, { RequestStatusId: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForProcess }) }).length;
                vm.counter.processing = $filter('filter')(vm.filterSearch, { RequestStatusId: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Processing }) }).length;
                vm.requestStatus = temp;                
            });
        }
    };
    vm.checkValid = function () {
        if (vm.searchCriteria.FromDate && vm.searchCriteria.ToDate &&
            vm.searchCriteria.FromDate > vm.searchCriteria.ToDate) {
            $('.date-picker-invalid').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid').removeClass('parsley-error');
        return true;
    };
    vm.addNewRequest = function () {
        $location.url("/addrequest");
    };
};

module.exports = ctrl;