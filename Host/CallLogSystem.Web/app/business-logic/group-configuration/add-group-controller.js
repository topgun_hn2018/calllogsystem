﻿'use strict';

var ctrl = function addgroup($scope, $q, $translate, $location, $routeParams, $timeout, $filter, dataTableService, initialScriptService, partyConfigurationDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.partyType = "Grp";
    vm.partyId = $routeParams.PartyId;
    vm.group = { Id: null, PartyCode: "", PartyName: "", IsActive: true, SelectedGroupLeaders:[], SelectedGroupUsers:[]};
    var data = { PartyTypeId: vm.partyType, PartyId: vm.partyId };
    vm.isAdd = false;
    if ($routeParams.Action == undefined) {
         vm.isAdd = true;
    }

    $q.all([
       partyConfigurationDataService.getActiveCompanies(),
       partyConfigurationDataService.getAllUsers(),
       securityDataService.getPermission('AddGroup'),
       initialScriptService.set()
    ]).then(function (results) {
        vm.Companies = results[0].data.Companies;
        vm.permission = results[2].data.Actions;
        if (!vm.isAdd) {
            vm.title = $translate.instant('ManageParty_EditGroup');
            $q.all([
                partyConfigurationDataService.getPartyById(data)
            ]).then(function (subResults) {
                vm.isGroup = true;
                for (var n = 0; n < vm.Companies.length; n++) {
                    if (vm.Companies[n].Id === subResults[0].data.Group.CompanyId) {
                        vm.Companies[n].IsShow = true;
                    }
                }
                vm.group = subResults[0].data.Group;
                vm.group.SelectedGroupLeaders = [];
                vm.group.SelectedGroupUsers = [];
                for (var j = 0; j < subResults[0].data.Group.GroupLeaders.length; j++) {
                    vm.group.SelectedGroupLeaders.push(subResults[0].data.Group.GroupLeaders[j].UserId);
                }
                subResults[0].data.Group.GroupUsers = $filter('orderBy')(subResults[0].data.Group.GroupUsers, 'Name');
                for (var k = 0; k < subResults[0].data.Group.GroupUsers.length; k++) {
                    vm.group.SelectedGroupUsers.push(subResults[0].data.Group.GroupUsers[k].UserId);
                }
                vm.group.SelectedGroupLeaders = initialScriptService.convertUsersArr(results[1].data.Users, vm.group.SelectedGroupLeaders);
                vm.group.SelectedGroupUsers = vm.convertUsersArr(results[1].data.Users, vm.group.SelectedGroupUsers);
                $q.all([
                    partyConfigurationDataService.getDepartments({ CompanyDepartmentId: [vm.group.CompanyId] })
                ]).then(function (subResult) {
                    vm.departments = subResult[0].data.Departments;                    
                    vm.loading = false;
                    vm.rootObject = angular.copy(vm.group);
                });
            });
        } else {
            vm.title = $translate.instant('ManageParty_AddGroup');
            initialScriptService.convertUsersArr(results[1].data.Users, vm.group.SelectedGroupLeaders);
            vm.convertUsersArr(results[1].data.Users, vm.group.SelectedGroupUsers);           
            vm.rootObject = angular.copy(vm.group);
            vm.loading = false;
        }
    });

    vm.backRequestPage = function () {
        if (JSON.stringify(vm.rootObject) !== JSON.stringify(vm.group)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/managegroup?keysearch=true');
    };

    vm.FilterDepartments = function() {
        partyConfigurationDataService.getDepartments({ CompanyDepartmentId: [vm.group.CompanyId] }).success(function(reponse) {
            vm.departments = reponse.Departments;
            initialScriptService.removeValidateSelect('company');
        });
    };

    vm.ChangeDepartment = function() {
        initialScriptService.removeValidateSelect('department');
    };

    vm.Save = function () {
        if (!vm.group.DepartmentId) {
            $('#department').val('');
            }
        if (!vm.group.CompanyId) {
            $('#company').val('');
        }
        if (initialScriptService.validateForm()) {
            var data = {
                PartyType: vm.partyType,
                Group: vm.group
            };
            partyConfigurationDataService.saveParty(data).success(function(response) {
                vm.back();
            });
        }
    };

    vm.convertUsersArr = function (source, selectedValues) {
        var selectedStringValues = [];

        angular.forEach(selectedValues, function (item, index) {
            selectedStringValues.push(item.toString());
        });

        $('#users-ddl-2').select2({ data: source });
        $timeout(function() {
            $('#users-ddl-2').val(selectedStringValues).trigger('change');
        }, 100);
        return selectedStringValues;
    };
};

module.exports = ctrl;