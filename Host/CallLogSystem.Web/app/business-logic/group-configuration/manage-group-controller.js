﻿'use strict';

var ctrl = function managegroup($scope, $window, $q, $routeParams, $translate, $location, dataTableService, DTColumnDefBuilder, initialScriptService, partyConfigurationDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];
    vm.DepartmentId = [-1];
    vm.CompanyId = [-1];
    vm.Companies = [{ Id: -1, Name: $translate.instant('FilterAll') }];
    vm.departments = [{ Id: -1, Name: $translate.instant('FilterAll') }];

    
    $q.all([
      partyConfigurationDataService.getAllCompanies(),
      partyConfigurationDataService.getDepartments({ CompanyDepartmentId: null }),
      securityDataService.getPermission('ManageGroup'),
      securityDataService.getPermission('AddGroup'),
      partyConfigurationDataService.getAllGroupUserLeaders(),
      partyConfigurationDataService.getAllGroupUserMembers(),
      initialScriptService.set()
    ]).then(function (results) {
        vm.Companies = vm.Companies.concat(results[0].data.Companies);
        vm.departments = vm.departments.concat(results[1].data.Departments);
        vm.permission = results[2].data.Actions;
        vm.permissionAddGroup = results[3].data.Actions;
        vm.groupUserLeaders = results[4].data.Users;
        vm.groupUserMembers = results[5].data.Users;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }        
        vm.searchManageParty(true);
        vm.loading = false;
    });
    vm.searchManageParty = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            vm.selectedPartyName = conditionSearchService.managegroup.PartyName;
            vm.selectedPartyCode = conditionSearchService.managegroup.PartyCode;
            vm.CompanyId = conditionSearchService.managegroup.CompanySearch;
            vm.DepartmentId = conditionSearchService.managegroup.DepartmentSearch;
            vm.selectedStatus = conditionSearchService.managegroup.IsActive;
            vm.GroupUserLeaderId = conditionSearchService.managegroup.GroupUserLeaderId;
            vm.GroupUserMemberId = conditionSearchService.managegroup.GroupUserMemberId;
        }
        var status;
        switch (vm.selectedStatus) {
        case "-1":
            status = null;
            break;
        case "1":
            status = true;
            break;
        case "0":
            status = false;
            break;
        default:
            status = null;
        }
        var data = {
            PartyName: vm.selectedPartyName,
            PartyCode: vm.selectedPartyCode,
            CompanySearch: vm.CompanyId,
            DepartmentSearch: vm.DepartmentId,
            IsActive: status,
            GroupUserLeaderId: vm.GroupUserLeaderId,
            GroupUserMemberId: vm.GroupUserMemberId
        };
        partyConfigurationDataService.SearchGroup(data).success(function (response) {
            conditionSearchService.managegroup.PartyName =vm.selectedPartyName ;
            conditionSearchService.managegroup.PartyCode =vm.selectedPartyCode;
            conditionSearchService.managegroup.CompanySearch= vm.CompanyId;
            conditionSearchService.managegroup.DepartmentSearch = vm.DepartmentId ;
            conditionSearchService.managegroup.IsActive =   vm.selectedStatus;
            conditionSearchService.managegroup.GroupUserLeaderId =vm.GroupUserLeaderId;
            conditionSearchService.managegroup.GroupUserMemberId =  vm.GroupUserMemberId;
            vm.searchResult = [];
            var predata = response.Parties;
            var len = predata.length;
            for (var i = 0; i < len; i += 1) {
                var party = {};
                party.PartyId = predata[i].PartyId;
                party.PartyName = predata[i].PartyName;
                party.PartyCode = predata[i].PartyCode;
                party.CompanyName = predata[i].CompanyName;
                party.DepartmentName = predata[i].DepartmentName;
                party.IsActive = predata[i].IsActive === true ? $translate.instant('Manage_Using') : $translate.instant('Manage_NotUsing');
                vm.searchResult.push(party);
            }            
        });
    };
    vm.FilterDepartments = function () {
        vm.departments = [];
        partyConfigurationDataService.getDepartments({ CompanyDepartmentId: vm.CompanyId }).success(function (response) {
            vm.departments = [{ Id: -1, Name: $translate.instant('FilterAll') }].concat(response.Departments);
            vm.DepartmentId = [-1];
        });
    };
    vm.addNew = function() {
        $location.url('/addgroup');
    };
    vm.edit = function(item) {
        $location.url('/addgroup?PartyId=' + item.PartyId + '&Action=Edit');       
        //$window.open($location.$$absUrl.substring(0, $location.$$absUrl.lastIndexOf('/')) + '/addgroup?PartyId=' + item.PartyId + '&Action=Edit');
    };

    vm.removeGroupUserLeaderId = function () {
        vm.GroupUserLeaderId = 0;
    };

    vm.removeGroupUserMemberId = function () {
        vm.GroupUserMemberId = 0;
    };
};

module.exports = ctrl;