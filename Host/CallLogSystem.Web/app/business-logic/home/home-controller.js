﻿'use strict';

var ctrl = function home($scope, $timeout, $q, $translate, $location, $filter, initialScriptService, requestGroupDataService, requestConfigurationDataService, requestDataService, securityDataService, masterDataService, requestStatusConstant) {
    var vm = this;
    vm.loading = true;

    //input data search
    vm.searchDataProcessWaiting = {
        ServiceId: ['-1'],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [],
        CreateDateTo: null,
        CreateDate: null,
        RequesterId: ['-1']
    };

    vm.searchDataApproveWaiting = {
        ServiceId: ['-1'],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [],
        RequesterId: '-1',
        ToDate: null,
        FromDate: null
    };

    vm.searchDataAssignWaiting = {
        ServiceId: ['-1'],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [],
        RequesterId: [-1],
        ToDate: null,
        FromDate: null
    };

    vm.searchDataProcessing = {
        ServiceId: ['-1'],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [],
        CreateDateTo: null,
        CreateDate: null,
        RequesterId: ['-1']
    };
    $q.all([
        requestGroupDataService.getAllCategoryActive(),
        requestConfigurationDataService.getAllRequestTypes(),
        initialScriptService.set(),
        securityDataService.getPermission('Home'),
        securityDataService.getPermission('ManageApprovalRequest'),
        securityDataService.getPermission('ManageAssignRequest'),
        securityDataService.getPermission('ManageProcess'),
        masterDataService.getKeyObjects('RequestStatus')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        console.log(vm.permission);
        vm.ApproveRole = results[4].data.Actions;
        vm.AssignRole = results[5].data.Actions;
        vm.ProcessRole = results[6].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

        vm.KeyObjects = results[7].data.KeyObjects;
        vm.searchDataProcessWaiting.RequestStatusId.push(parseInt(vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForProcess })));
        vm.searchDataApproveWaiting.RequestStatusId.push(parseInt(vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForApproval })));
        vm.searchDataAssignWaiting.RequestStatusId.push(parseInt(vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForAssigned })));
        vm.searchDataProcessing.RequestStatusId.push(parseInt(vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Processing })));

        vm.categories = results[0].data.Categories;        
        vm.requestTypes = results[1].data.RequestTypes;
        vm.statisticalData = {
            ApproveWaiting: 0, AssignWaiting: 0, ProcessWaiting: 0, Processing: 0
        }
        $q.all([
            requestDataService.getProcessRequestData(vm.searchDataProcessWaiting),
            requestDataService.searchApprovalRequest(vm.searchDataApproveWaiting),
            requestDataService.searchAssignRequest(vm.searchDataAssignWaiting),
            requestDataService.getProcessRequestData(vm.searchDataProcessing)
        ]).then(function (resultReq) {
            vm.statisticalData.ProcessWaiting = resultReq[0].data.Requests.length;
            vm.statisticalData.ApproveWaiting = resultReq[1].data.Requests.length;
            vm.statisticalData.AssignWaiting = resultReq[2].data.Requests.length;
            vm.statisticalData.Processing = resultReq[3].data.Requests.length;

            //create statistical chart
            vm.createStatisticalChart(vm.statisticalData);
            $(".cate-box").matchHeight();            
        });

        vm.loading = false;
    });

    vm.getStatus = function (list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return list[i].Value;
        }
        return false;
    }

    vm.arrangeCate = 5;

    vm.requestTypeChange = function () {
        if (vm.requesttypeid) {
            $location.url("/addrequest?requesttypeid=" + vm.requesttypeid);
        }
    };

    //Create statistical data chart
    vm.createStatisticalChart = function (statisticalChart) {
        Chart.defaults.global.legend = {
            enabled: false
        };
        
        if (statisticalChart.ApproveWaiting === 0 &&
            statisticalChart.AssignWaiting === 0 &&
            statisticalChart.ProcessWaiting === 0 &&
            statisticalChart.Processing === 0) {
            vm.datatitle = [$translate.instant('HomePage_NodataChart')];
            vm.data = [1];
            vm.backgroundColor = ["#CCCCCC"];
        } else {
            //create list title of chart
            vm.datatitle = [];
            vm.datatitle.push($translate.instant('HomePage_AproveWaiting'));
            vm.datatitle.push($translate.instant('HomePage_AssignWaiting'));
            vm.datatitle.push($translate.instant('HomePage_ProcessWaiting'));
            vm.datatitle.push($translate.instant('HomePage_Processing'));

            //create data statistical chart
            vm.data = [];
            vm.data.push(statisticalChart.ApproveWaiting);
            vm.data.push(statisticalChart.AssignWaiting);
            vm.data.push(statisticalChart.ProcessWaiting);
            vm.data.push(statisticalChart.Processing);

            vm.backgroundColor = ["#3498DB", "#26B99A", "#9B59B6", "#E74C3C"];
        }
        

        // Pie chart
        var ctx = document.getElementById("pieChart");
        var data = {
            datasets: [{
                data: vm.data,
                backgroundColor: vm.backgroundColor,
                label: 'My dataset' // for legend
            }],
            labels: vm.datatitle
        };

        var pieChart = new Chart(ctx, {
            data: data,
            type: 'pie',
            otpions: {
                responsive: false,
                legend: false
            }
        });

        vm.chartdata = [
            { Name: $translate.instant('HomePage_AproveWaiting'), Link: '#manageapprovalrequest', Value: statisticalChart.ApproveWaiting, Color: 'blue' },
            { Name: $translate.instant('HomePage_AssignWaiting'), Link: '#manageassignrequest', Value: statisticalChart.AssignWaiting, Color: 'green' },
            { Name: $translate.instant('HomePage_ProcessWaiting'), Link: '#manageprocess?requeststatusid=' + parseInt(vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForProcess })), Value: statisticalChart.ProcessWaiting, Color: 'purple' },
            { Name: $translate.instant('HomePage_Processing'), Link: '#manageprocess?requeststatusid=' + parseInt(vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Processing })), Value: statisticalChart.Processing, Color: 'red' }
        ];
    }
    
};

module.exports = ctrl;