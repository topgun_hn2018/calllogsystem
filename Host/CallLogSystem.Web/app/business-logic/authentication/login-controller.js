﻿'use strict';

var ctrl = function login($location, $translate, authenticationDataService, cookieService, jwtService) {
    var vm = this;
    vm.isShowMsg = false;
    cookieService.clearCookie();
    var homeLoader = $('body').loadingIndicator({
        useImage: false
    }).data("loadingIndicator");
    homeLoader.hide();
    vm.login = function () {
        if (vm.user === undefined ||
            vm.user.LoginId === '' || vm.user.LoginId.trim() === "" ||
            vm.user.Password === '' || vm.user.Password.trim() === "") {
            vm.errorMsg = $translate.instant('LoginRequired');
            vm.isShowMsg = true;
        }
        else {
            authenticationDataService.login(vm.user).success(function (response) {
                if (response.IsAuthenticated) {
                    cookieService.createObjectCookie('UserProfile', response.UserProfile);
                    $location.url('/home');
                } else {
                    if (response.ErrorType === 2) {
                        vm.errorMsg = $translate.instant('Login_NotFoundLocalUser');
                    } else if (response.ErrorType === 5) {
                        vm.errorMsg = $translate.instant(response.ErrorDescription);
                    }
                    else {
                        vm.errorMsg = $translate.instant('Login_Fail');
                        $("#user").focus();
                    }
                    vm.isShowMsg = true;
                }
            });
        }
    };

    $("body").css("background", "url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAMAAADz0U65AAAACVBMVEX4%2BPjs7Oz5%2Bfl%2BU6S3AAAAG0lEQVQImWNggAEmEGBkZEJjAAESAyiMxoACAAzwAGnXyNREAAAAAElFTkSuQmCC\") repeat scroll 0 0 transparent");
    $("#user").focus();
};

module.exports = ctrl;