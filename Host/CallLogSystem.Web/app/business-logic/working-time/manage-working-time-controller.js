﻿'use strict';

var ctrl = function manageworkingtime($scope, $q, $translate, $location, workingTimeConstant, partyConfigurationDataService, workingTimeDataService, initialScriptService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.month = new Date();
    vm.selectedMonth = new Date().getMonth();
    vm.selectedYear = new Date().getFullYear();
    vm.allDays = vm.morningDays = vm.afternoonDays = [];
    vm.options = {
        minDate: $scope.minDate,
        maxDate: $scope.maxDate,
        maxMode: 'day',
        showWeeks: false
    }
    vm.workingTime = {
        CompanyId: null
    };
    vm.rootObject = angular.copy(vm.workingTime);

    $q.all([
        partyConfigurationDataService.getAllCompanies(),
        securityDataService.getPermission('ManageWorkingTime')
    ]).then(function(results) {
        vm.Companies = results[0].data.Companies;
        vm.permission = results[1].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.years = [];
        vm.times = [];
        for (var i = 2010; i < 2051; i++) {
            vm.years.push(i);
        }
        vm.workingTime.Year = new Date().getFullYear();
        vm.rootObject = angular.copy(vm.workingTime);
        for (var j = 6; j < 21; j++) {
            for (var k = 0; k < 4; k++) {
                var id = j * 60+k*15;
                var name =  String(j)+'h';
                if (k === 0) name = name + "00";
                else name=name+ String(k * 15);
                vm.times.push({ Id: id, Name: name });
            }
        }        
    });

    vm.getDayClass = function (date, mode) {
      if (mode === 'day') {
            var dateToCheck = new Date(date);
            var i;
            for (i = 0; i < vm.allDays.length ; i++) {
                if (vm.allDays[i].setHours(0, 0, 0, 0) === dateToCheck.setHours(0, 0, 0, 0)) {
                    return workingTimeConstant.OffAllDay;
                }
            }
            for ( i = 0; i < vm.morningDays.length ; i++) {
                if (vm.morningDays[i].setHours(0, 0, 0, 0) === dateToCheck.setHours(0, 0, 0, 0)) {
                    return workingTimeConstant.OffMorning;
                }
            }
            for (i = 0; i < vm.afternoonDays.length ; i++) {
                if (vm.afternoonDays[i].setHours(0, 0, 0, 0) === dateToCheck.setHours(0, 0, 0, 0)) {
                   return workingTimeConstant.OffAfternoon;
                }
            }
          if (dateToCheck.getDate() === 20) {
              vm.selectedMonth = dateToCheck.getMonth();
              vm.selectedYear = dateToCheck.getFullYear();
          }
        } else {
            return '';
        }
        return '';
    };
    vm.commentdata = [
            { Name: $translate.instant('Manage_OffAllDay'), Color: 'all-day' },
            { Name: $translate.instant('Manage_OffMorning'), Color: 'morning' },
            { Name: $translate.instant('Manage_OffAfternoon'), Color: 'afternoon' }
    ];

    vm.filterCompanyYear = function () {
        workingTimeDataService.getWorkingTime({ SelectCompanyId: vm.workingTime.CompanyId, Year: vm.workingTime.Year }).success(function (reponse) {
            if (reponse.WorkingTime) {
                var companyId = reponse.WorkingTime.CompanyId;
                var time1 = reponse.WorkingTime.Time1;
                var time2 = reponse.WorkingTime.Time2;
                var time3 = reponse.WorkingTime.Time3;
                var time4 = reponse.WorkingTime.Time4;
                var hourPerDay = reponse.WorkingTime.HourPerDay;
                var year = reponse.WorkingTime.Year;
                vm.workingTime = { CompanyId: companyId, Time1: time1, Time2: time2, Time3: time3, Time4: time4, HourPerDay: hourPerDay, Year: year };
                vm.isShowEdit = true;
            } else {
                vm.workingTime = { CompanyId: vm.workingTime.CompanyId, Time1: null, Time2: null, Time3: null, Time4: null, HourPerDay: null, Year: vm.workingTime.Year };
                vm.isShowEdit = false;
            }
            if (vm.workingTime.Year === new Date().getFullYear())
                vm.month = new Date(vm.workingTime.Year, new Date().getMonth(), 1);
            else vm.month = new Date(vm.workingTime.Year, 0, 1);
            vm.selectedYear = parseInt(vm.workingTime.Year);
            vm.rootObject = angular.copy(vm.workingTime);
            vm.warningHourPerDay = String.empty;
        });
           
        workingTimeDataService.getHoliday({ SelectCompanyId: vm.workingTime.CompanyId, Year: vm.workingTime.Year }).success(function (reponse) {
            if (reponse.Holiday) {
                 vm.allDays = [];
                 vm.morningDays = [];
                 vm.afternoonDays = [];
                for (var i = 0; i < reponse.Holiday.length ; i++) {
                    if (reponse.Holiday[i].HolidayTypeId === 1) {
                        vm.allDays.push(new Date(reponse.Holiday[i].Date));
                    }
                    if (reponse.Holiday[i].HolidayTypeId === 2) {
                        vm.morningDays.push(new Date(reponse.Holiday[i].Date));
                    }
                    if (reponse.Holiday[i].HolidayTypeId === 3) {
                        vm.afternoonDays.push(new Date(reponse.Holiday[i].Date));
                    }
                }
                vm.month = new Date(vm.month + 1);
            } 
        });
    };

    vm.changeTime = function() {
        if (vm.workingTime.Time1 && vm.workingTime.Time2 && vm.workingTime.Time3 && vm.workingTime.Time4) {
            vm.workingTime.HourPerDay = (parseInt(vm.workingTime.Time2) - parseInt(vm.workingTime.Time1) + parseInt(vm.workingTime.Time4) - parseInt(vm.workingTime.Time3)) / 60;
            if (vm.workingTime.HourPerDay !== 8) {
                vm.warningHourPerDay = $translate.instant('Manage_WarningHourPerDay');
            } else {
                vm.warningHourPerDay = String.empty;
            }
        }
    };

    vm.Save = function() {
        if (initialScriptService.validateForm()) {
            var data = {
                WorkingTime: vm.workingTime
            };
            workingTimeDataService.saveWorkingTime(data).success(function (response) {
                if (response.ErrorType === 0) {
                    vm.msg = $translate.instant('Manage_UpdatedWorkingTimeMsg');
                    vm.isShowEdit = true;
                }
            });
        }
    };

    vm.backRequestPage = function() {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.workingTime)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/home');
    };

    vm.Edit = function() {
        if (vm.workingTime.CompanyId !== undefined && vm.workingTime.Year !== undefined) {
            $location.url('/editworkingtime?CompanyId=' + vm.workingTime.CompanyId + '&Year=' + vm.selectedYear + '&Month=' + vm.selectedMonth);
        }
    };

    vm.filterGreater1 = function(time) {
        return time.Id > vm.workingTime.Time1;
    };
    vm.filterGreater2 = function (time) {
        return time.Id > vm.workingTime.Time2;
    };
    vm.filterGreater3 = function (time) {
        return time.Id > vm.workingTime.Time3;
    };

    $("body").on("DOMSubtreeModified", '.uib-title strong', function () {
        var tittleYear = $('.uib-title strong').text().split(' ');
        var selectedYear = parseInt(tittleYear[tittleYear.length - 1]);
        if (selectedYear !== vm.workingTime.Year) {
            vm.workingTime.Year = selectedYear;
            vm.filterCompanyYear();
        }
    });

    vm.loading = false;
};

module.exports = ctrl;