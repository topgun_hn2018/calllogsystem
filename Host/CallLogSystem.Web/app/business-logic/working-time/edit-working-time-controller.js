﻿'use strict';

var ctrl = function editworkingtime($scope, $q, $translate, $location, $routeParams,workingTimeConstant, dataTableService, DTColumnDefBuilder, initialScriptService, workingTimeDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.companyId = $routeParams.CompanyId;
    vm.currentMonth = parseInt($routeParams.Month);
    vm.currentYear = parseInt($routeParams.Year);
    vm.selectedDate = new Date(vm.currentYear, vm.currentMonth, 1);
    vm.holidays = [];
    vm.rootObject = angular.copy(vm.holidays);
    vm.selectedDates = [];
    $q.all([
        workingTimeDataService.getHoliday({ SelectCompanyId: vm.companyId, Year: vm.currentYear, Month: vm.currentMonth +1}),
        securityDataService.getPermission('ManageWorkingTime')
    ]).then(function(results) {
        var holiday = results[0].data.Holiday;
        vm.permission = results[1].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        for (var i = 0; i < holiday.length; i++) {
            var date = new Date(holiday[i].Date);
            var strDate = vm.padNumber(date.getDate()) + "/" + vm.padNumber(date.getMonth() + 1) + "/" + date.getFullYear();
            vm.holidays.push({ StrDate: strDate, HolidayTypeId: String(holiday[i].HolidayTypeId), Note: holiday[i].Note });
            vm.rootObject = angular.copy(vm.holidays);
            vm.selectedDates.push(date);
        }
       vm.permission = results[1].data.Actions;
       vm.selectedDate = null;       
    });
    $scope.$watch('vm.selectedDate', function (newDate) {
        if (newDate) {
            var strDate = vm.padNumber(newDate.getDate()) + "/" + vm.padNumber(newDate.getMonth() + 1) + "/" + newDate.getFullYear();
            var existHoliday = false;
            var index1 = 0;
            var index2 = 0;
            var existSelected = false;
            for (var i = 0; i < vm.holidays.length; i++) {
                if (vm.holidays[i].StrDate === strDate)
                {
                    existHoliday = true;
                    index1 = i;
                }
            }
            for (var j = 0; j < vm.selectedDates.length; j++) {
                var date = new Date(vm.selectedDates[j]);
                var strDate2 = vm.padNumber(date.getDate()) + "/" + vm.padNumber(date.getMonth() + 1) + "/" + date.getFullYear();
                if (strDate2 === strDate){ existSelected = true;
                    index2 = j;
                }
            }
            if (existHoliday === false && existSelected === true) {
                vm.holidays.push({ StrDate: strDate, HolidayTypeId: '1', Note: '' });
                vm.selectedDate = null;
            }
            if (existHoliday === true) {
                vm.holidays.splice(index1, 1);
                if (existSelected === true) {
                    vm.selectedDates.splice(index2, 1);
                }
                vm.selectedDate = null;
            }
        }
    });
    vm.minDate = new Date(vm.currentYear, vm.currentMonth, 1);
    vm.maxDate = new Date(vm.currentYear, vm.currentMonth + 1, 1);
    vm.maxDate = new Date(vm.maxDate - 1);

    vm.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var strDate = vm.padNumber(date.getDate()) + "/" + vm.padNumber(date.getMonth() + 1) + "/" + date.getFullYear();
            var i;
            for (i = 0; i < vm.holidays.length ; i++) {
                if (vm.holidays[i].StrDate === strDate) {
                    if (vm.holidays[i].HolidayTypeId === "1") {
                        return workingTimeConstant.OffAllDay;
                    }
                    if (vm.holidays[i].HolidayTypeId === "2") {
                        return workingTimeConstant.OffMorning;
                    }
                    if (vm.holidays[i].HolidayTypeId === "3") {
                        return workingTimeConstant.OffAfternoon;
                    }
                    
                }
            }
        } else {
            return '';
        }
        return '';
    };
    vm.dtOptions = dataTableService.getDataTableScrollOptions(2, 'desc','246');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2)
    ];
    vm.holidayTypes = [{ Id: '1', Name: $translate.instant('Manage_OffAllDay') },{ Id: '2', Name: $translate.instant('Manage_OffMorning')} ,{ Id: '3', Name: $translate.instant('Manage_OffAfternoon')} ];

    vm.backRequestPage = function () {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.holidays)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    }
    vm.back = function () {
        $location.url('/manageworkingtime');
    };
    vm.Save = function () {
        if (initialScriptService.validateForm()) {
        var data = {
                SelectedCompanyId: vm.companyId,
                Month: vm.currentMonth +1,
                Year:vm.currentYear,
                Holydays: vm.holidays
            };
        workingTimeDataService.saveHoliday(data).success(function (response) {
            if (response.ErrorType === 0) {
                vm.msg = $translate.instant('Manage_UpdatedMsg');
                vm.selectedDate = null;
                vm.errorMsg = '';
                vm.rootObject = angular.copy(vm.holidays);
            } else {
                vm.errorMsg = response.ErrorDescription;
                vm.msg = '';
            }
        });
        }
    }
    vm.FilterHolidayType = function () {
        vm.refressdatepicker();
    }
    vm.refressdatepicker= function() {
        if ( vm.selectedDate === undefined) {
            vm.selectedDate = null;
        } else {
            vm.selectedDate = undefined;
        }
    }
    vm.padNumber= function (number) {
        var ret = new String(number);
        if (ret.length === 1)
            ret = "0" + ret;
        return ret;
    }
    vm.loading = false;
};

module.exports = ctrl;