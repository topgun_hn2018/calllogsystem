﻿'use strict';

var ctrl = function addrequesttype($scope, $q, $location,$translate, $routeParams, dataTableService, initialScriptService, requestTypeDataService, requestGroupDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.requestTypes = [];  
    vm.ProcessBySite = false;
    vm.IsActive = true;
    vm.isShowMsg = false;
    vm.RequestTypeId = $routeParams.RequestTypeId;
    vm.requestType = { Id: null, Name: "", CategoryId: "", ProcessBySite:"", IsActive: true,TAT:null, Order: ""};
    vm.rootObject = angular.copy(vm.requestType);

    $q.all([
        requestGroupDataService.searchCategories({ Name: '', IsActive: true }),
        securityDataService.getPermission('AddRequestType'),
        initialScriptService.set()
    ]).then(function (results) {
        if ($routeParams.action != "Edit") {
            vm.Title = $translate.instant('AddRequest_AddRequestType');
        }
        else
        {
            vm.Title = $translate.instant('AddRequest_EditRequestType');
        }
        vm.groups = results[0].data.Categories;
        if (vm.RequestTypeId && vm.RequestTypeId !== '') {
            requestTypeDataService.getRequestTypeById({ Id: vm.RequestTypeId }).success(function (response) {
                var requestType={};
                requestType.Name = response.RequestType.Name;
                requestType.IsActive = response.RequestType.IsActive;
                requestType.CategoryId = String(response.RequestType.CategoryId);
                requestType.ProcessBySite = response.RequestType.ProcessBySite;
                requestType.TAT = response.RequestType.TAT;
                requestType.Id = response.RequestType.Id;
                requestType.Order = response.RequestType.Order;
                vm.requestType = requestType;
                vm.rootObject = angular.copy(vm.requestType);
            });
        }
        vm.permission = results[1].data.Actions;        
        vm.loading = false;
    });

    vm.backRequestPage = function () {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.requestType)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    }
    vm.back = function () {
        $location.url('/managerequesttype?keysearch=true');
    };
    vm.Save = function() {
        if (initialScriptService.validateForm()) {
            var data = {
                RequestType: vm.requestType
            };
            requestTypeDataService.saveRequestType(data).success(function (response) {
                if (response.ErrorType != 0) {
                    vm.isShowMsg = true;
                    vm.errorMessage = $translate.instant('RequestType_Exist');
                }
                else{
                    vm.back();
                }
            });
        }
    };

};

module.exports = ctrl;