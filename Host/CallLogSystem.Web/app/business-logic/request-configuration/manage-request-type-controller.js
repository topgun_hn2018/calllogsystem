﻿'use strict';

var ctrl = function requestmanage($routeParams,$scope, $q, $translate, $location, dataTableService, DTColumnDefBuilder, initialScriptService, requestTypeDataService, requestGroupDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;
    vm.selectedStatus = "-1";
    vm.selectedGroup = "01";
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];
    vm.searchResult = [];
    $q.all([
       requestGroupDataService.searchCategories({ Name: '', IsActive: null }),
       securityDataService.getPermission('ManageRequestType'),
       securityDataService.getPermission('AddRequestType'),
       initialScriptService.set()
    ]).then(function (results) {
        vm.groups = results[0].data.Categories;
        vm.permission = results[1].data.Actions;
        vm.permissionAddRequestType = results[2].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.searchRequestType();
        vm.loading = false;
    });
    vm.searchRequestType = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            vm.selectedGroup = conditionSearchService.managerequesttype.CategoryId;
            vm.selectedStatus = conditionSearchService.managerequesttype.Status; 
            vm.selectedRequestType = conditionSearchService.managerequesttype.RequestTypeName;
        }
        var status = null;
        switch (vm.selectedStatus) {
        case "-1":
            status = null;
            break;
        case "1":
            status = true;
            break;
        case "0":
            status = false;
            break;
        }
        var data = {
            CategoryId: vm.selectedGroup,
            Status: status,
            RequestTypeName: vm.selectedRequestType
        }
        requestTypeDataService.searchRequestType(data).success(function (response) {
            conditionSearchService.managerequesttype.CategoryId= vm.selectedGroup ;
            conditionSearchService.managerequesttype.Status =vm.selectedStatus;
            conditionSearchService.managerequesttype.RequestTypeName =vm.selectedRequestType;
            vm.searchResult = [];
            for (var i = 0; i < response.RequestTypes.length; i++) {
                var id = response.RequestTypes[i].Id;
                var name = response.RequestTypes[i].Name;
                var categoryName = response.RequestTypes[i].CategoryName;
                var isActive = response.RequestTypes[i].IsActive ? $translate.instant('SearchFrom_RoleStatus_On') : $translate.instant('SearchFrom_RoleStatus_Off');
                var isRegion = response.RequestTypes[i].ProcessBySite ? $translate.instant('CreateFrom_Yes') : $translate.instant('CreateFrom_No');
                var tat = response.RequestTypes[i].TAT;
                vm.searchResult.push({ Id: id, Name: name, CategoryName: categoryName, IsActive: isActive, IsRegion: isRegion, TAT: tat });
            }            
        });
    };
    vm.addNew = function() {
        $location.url("/addrequesttype");
    };

    vm.edit = function(item) {
        $location.url('/addrequesttype?RequestTypeId=' + item.Id + '&action=Edit');
    };

};

module.exports = ctrl;