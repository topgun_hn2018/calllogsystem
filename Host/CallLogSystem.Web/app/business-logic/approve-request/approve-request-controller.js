﻿'use strict';

var ctrl = function approverequest($scope, $timeout, $q, $translate, $routeParams, $location, FileSaver, uploadDataService, DTColumnDefBuilder, dataTableService, requestDataService, securityDataService) {
    var vm = this;
    vm.loading = false;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3)
    ];
    vm.documents = [];
    vm.requestId = $routeParams.requestid;
    if (!vm.requestId) {
        $location.url('/notfound');
    }
    vm.approvalRequest = {
        Note: '',
        Files: [],
        RequestId: vm.requestId
    };
    vm.isApproved = false;

    $q.all([
        requestDataService.getApproval(vm.requestId),
        securityDataService.getPermission('ApproveRequest')
    ]).then(function (results) {
        vm.permission = results[1].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if (results[0].data.ErrorType === 0) {
            if (results[0].data.IsApproved) {
                vm.isApproved = true;
                vm.approvalRequest.Note = results[0].data.Note;
                vm.documents = results[0].data.Files;
            }
        } else {
            $location.url('/notfound');
        }        
    });

    $scope.uploadFile = function (element) {
        $scope.$apply(function ($scope) {
            if (element.files[0].size > $scope.maxLength) {
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MaxSize') + $scope.maxSize + 'MB';
                vm.invalidExtension = true;
            } else if (element.files[0].size === 0) {
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MinSize');
                vm.invalidExtension = true;
            } else {
                vm.invalidExtension = false;
                var fileUpload = element.files[0];
                var data = new FormData();
                data.append("File", fileUpload);
                vm.uploadFile = data;
            }

        });
    };

    vm.uploadDocument = function () {
        if (vm.uploadFile) {
            uploadDataService.uploadDocument(vm.uploadFile).success(function (response) {
                if (response.IsSuccess) {
                    vm.invalidExtension = false;
                    delete vm.uploadFile;
                    $('#file').val('');
                    vm.documents.push(response);
                } else {
                    if (response.ErrorType === 4) {
                        vm.errorMsgUpload = $translate.instant('AddRequest_Upload_InvalidType');
                        vm.invalidExtension = true;
                    }
                }
            });
        } else {
            vm.errorMsgUpload = $translate.instant('AddRequest_Upload_Required');
            vm.invalidExtension = true;
        }
    };

    vm.onDownload = function (item) {
        uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
            .success(function (response) {
                var file = new Blob([response], { type: item.FileType });
                FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
            });
    };

    vm.delete = function () {
        uploadDataService.deleteDocument({ FileName: vm.deleteFile.FileNameServer, UploadedDate: vm.deleteFile.UploadedDate })
            .success(function (response) {
                if (response.ErrorType === 0) {
                    var index = vm.documents.indexOf(vm.deleteFile);
                    vm.documents.splice(index, 1);
                }
            });
    };

    vm.onTextareChange = function () {
        $('#approvalNote').removeClass('parsley-error');
        vm.isShowMsg = false;
    };

    vm.approveRequest = function() {
        if (!vm.isApprove && (vm.approvalRequest.Note === '' || vm.approvalRequest.Note === null)) {

            $('#approvalNote').addClass('parsley-error');
            $timeout(function() {
                $('#approvalNote').focus();
            }, 500);
            vm.isShowMsg = true;
        } else {
            for (var i = 0; i < vm.documents.length; i++) {
                vm.approvalRequest.Files.push({
                    FileName: vm.documents[i].FileName,
                    FileNameServer: vm.documents[i].FileNameServer,
                    Extension: vm.documents[i].Extension,
                    FileType: vm.documents[i].FileType,
                    Length: vm.documents[i].Length,
                    UploadedDate: vm.documents[i].UploadedDate
                });
            }
            vm.approvalRequest.IsApproval = vm.isApprove;
            requestDataService.approveRequest(vm.approvalRequest).success(function (response) {
                if (response.ErrorType === 0) {
                    $location.url('/manageapprovalrequest');
                } else {
                    
                }
            });
        }
    };

    vm.back = function() {
        $location.url('/manageapprovalrequest?keysearch=true');
    }
};

module.exports = ctrl;