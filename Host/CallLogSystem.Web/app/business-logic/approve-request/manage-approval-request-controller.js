﻿'use strict';

var ctrl = function manageapprovalrequest($routeParams,$scope, $q, $location, $translate, dataTableService, DTColumnDefBuilder, initialScriptService, serviceConfigurationDataService, accountDataService, requestDataService, uiDatetimePickerConfig, securityDataService, masterDataService, requestStatusConstant, conditionSearchService) {
    conditionSearchService.a = 1;
    var vm = this;
    vm.loading = true;

    uiDatetimePickerConfig.enableTime = false;
    vm.dtOptions = dataTableService.getDataTableOptions(4, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5)
    ];
    $.fn.dataTable.moment($scope.dateTimeMomment);
    vm.services = [
    {
        Id: -1,
        Name: $translate.instant('FilterAll')
    }];
    vm.users = [
    {
        id: -1,
        text: $translate.instant('FilterAll')
    }];
    var defaultRequestStatusId = $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForApproval);
   
    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [defaultRequestStatusId],
        RequesterId: -1,
        ToDate: null,
        FromDate: null
    };
    if ($routeParams != null && $routeParams.keysearch == 'true') {
        vm.searchCriteria.RequestNo = conditionSearchService.manageapprovalrequest.RequestNo;
        vm.searchCriteria.RequestName = conditionSearchService.manageapprovalrequest.RequestName;
        vm.searchCriteria.RequestStatusId = conditionSearchService.manageapprovalrequest.RequestStatusId;
        vm.searchCriteria.RequesterId = conditionSearchService.manageapprovalrequest.RequesterId;
        vm.searchCriteria.ToDate = conditionSearchService.manageapprovalrequest.ToDate;
        vm.searchCriteria.FromDate = conditionSearchService.manageapprovalrequest.FromDate;
    }
    vm.requestStatus = [
        { Id: -1, Name: $translate.instant('FilterAll') },
        { Id: defaultRequestStatusId, Name: $translate.instant('RequestStatus_WaitingForApproval') },
        { Id: $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForAssigned), Name: $translate.instant('RequestStatus_Approved') },
        { Id: $scope.filter($scope.keyObjects, requestStatusConstant.RejectedApproval), Name: $translate.instant('RequestStatus_Rejected') }
    ];


    $q.all([
        initialScriptService.set(),
        serviceConfigurationDataService.getAllServices(),
        accountDataService.getAllUsers(),
        securityDataService.getPermission('ManageApprovalRequest'),
        securityDataService.getPermission('ApproveRequest')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if ($routeParams != null && $routeParams.keysearch == 'true') {
            vm.searchCriteria.ServiceId = conditionSearchService.manageapprovalrequest.ServiceId;
        }
        vm.permissionApprove = results[4].data.Actions;
        vm.services = vm.services.concat(results[1].data.Services);
        vm.users = vm.users.concat(results[2].data.Users);
        initialScriptService.convertUsersArr(vm.users, vm.searchCriteria.RequesterId);
        vm.searchRequest();
        vm.loading = false;
    });
    vm.searchRequest = function (search) {
        //`
        if (vm.checkValid() && initialScriptService.validatePastDate($scope.dateFormat)) {
            vm.invalidDateTo = false;
            requestDataService.searchApprovalRequest(vm.searchCriteria).success(function (response) {
                conditionSearchService.manageapprovalrequest = vm.searchCriteria;
                vm.searchResult = response.Requests;                
            });
        }
    };
    vm.checkValid = function () {
        if (vm.searchCriteria.FromDate && vm.searchCriteria.ToDate &&
            vm.searchCriteria.FromDate > vm.searchCriteria.ToDate) {
            $('.date-picker-invalid').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid').removeClass('parsley-error');
        return true;
    };
};

module.exports = ctrl;