﻿'use strict';

var ctrl = function manageassignrequest($routeParams, $scope, $q, $location, $translate, dataTableService, DTColumnDefBuilder, initialScriptService, requestDataService, serviceConfigurationDataService, userManagementDataFactory, uiDatetimePickerConfig, securityDataService, requestStatusConstant, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    uiDatetimePickerConfig.enableTime = false;
    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [$scope.filter($scope.keyObjects, requestStatusConstant.WaitingForAssigned)],
        RequesterId: [-1],
        ToDate: null,
        FromDate: null
    };
    if ($routeParams != null && $routeParams.keysearch == 'true') {
        vm.searchCriteria.RequestNo = conditionSearchService.manageapprovalrequest.RequestNo;
        vm.searchCriteria.RequestName = conditionSearchService.manageapprovalrequest.RequestName;
        vm.searchCriteria.RequestStatusId = conditionSearchService.manageapprovalrequest.RequestStatusId;
        vm.searchCriteria.RequesterId = conditionSearchService.manageapprovalrequest.RequesterId;
        vm.searchCriteria.ToDate = conditionSearchService.manageapprovalrequest.ToDate;
        vm.searchCriteria.FromDate = conditionSearchService.manageapprovalrequest.FromDate;
    }
    vm.requestStatus = [
        { Id: "-1", Name: $translate.instant('FilterAll') },
        { Id: $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForAssigned), Name: $translate.instant('RequestStatus_NotAssigned') },
        { Id: $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForProcess), Name: $translate.instant('RequestStatus_Assigned') },
        { Id: $scope.filter($scope.keyObjects, requestStatusConstant.RejectedProcess), Name: $translate.instant('RequestStatus_Cancel') }
    ];
    vm.dtOptions = dataTableService.getDataTableOptions(4, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5)
    ];
    $.fn.dataTable.moment($scope.dateTimeMomment);
    vm.services = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        }
    ];
    vm.users = [
        {
            id: -1,
            text: $translate.instant('FilterAll')
        }
    ];
  
   
    $q.all([
        initialScriptService.set(),
        serviceConfigurationDataService.getAllServices(),
        userManagementDataFactory.getAllUsers(),
        securityDataService.getPermission('ManageAssignRequest'),
        securityDataService.getPermission('AssignRequest')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if ($routeParams != null && $routeParams.keysearch == 'true') {
            vm.searchCriteria.ServiceId = conditionSearchService.manageapprovalrequest.ServiceId;
        }
        vm.permissionAssign = results[3].data.Actions;
        vm.services = vm.services.concat(results[1].data.Services);
        vm.users = vm.users.concat(results[2].data.Users);
        initialScriptService.convertUsersArr(vm.users, vm.searchCriteria.RequesterId);
        vm.searchRequest();
        vm.loading = false;
    });
    vm.searchRequest = function (search) {
        //if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
        //    vm.searchCriteria = conditionSearchService.manageapprovalrequest;
        //}

        if (vm.checkValid() && initialScriptService.validatePastDate($scope.dateFormat)) {
            conditionSearchService.manageapprovalrequest = vm.searchCriteria;
            vm.invalidDateTo = false;
            requestDataService.searchAssignRequest(vm.searchCriteria).success(function (response) {
                vm.searchResult = response.Requests;                
            });
        }
    };
    vm.checkValid = function () {
        if (vm.searchCriteria.FromDate && vm.searchCriteria.ToDate &&
            vm.searchCriteria.FromDate > vm.searchCriteria.ToDate) {
            $('.date-picker-invalid').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid').removeClass('parsley-error');
        return true;
    };
}
module.exports = ctrl;