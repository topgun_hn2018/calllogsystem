﻿'use strict';

var ctrl = function assignrequest($scope, $q, $location, $routeParams, $translate, requestDataService, initialScriptService, securityDataService) {
    var vm = this;
    vm.loading = false;
    vm.requestId = $routeParams.requestid;
    if (!vm.requestId) {
        $location.url('/notfound');
    }
    vm.request = {
        Description: '',
        Note: ''
    }
    $q.all([
        initialScriptService.set(),
        requestDataService.getRequestAssign(vm.requestId),
       requestDataService.getGroupsByRequest(vm.requestId),
       securityDataService.getPermission('AssignRequest')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if (results[1].data.ErrorType === 2) {
            $location.url('/notfound');
        }
        vm.requestAssign = results[1].data;
        if (vm.requestAssign.Note != null && vm.requestAssign.ProcessDescription != null) {
            vm.request = {
                Description: vm.requestAssign.ProcessDescription,
                Note: vm.requestAssign.Note
            }
        }
        
        vm.groups = results[2].data.Groups;
        if (vm.groups.length === 1) {
            vm.request.GroupId = vm.groups[0].GroupId;
            vm.onGroupChange();
        }
        angular.forEach(vm.groups, function(item, index) {
            if (item.IsProcessing) {
                vm.request.GroupId = item.GroupId;
                requestDataService.getUsersByRequest(vm.requestId, vm.request.GroupId).success(function (response) {
                    vm.users = response.Users;
                    angular.forEach(vm.users, function (user, index) {
                        if (user.IsProcessing) {
                            vm.request.AssigneeId = user.UserId;
                            vm.rootObject = angular.copy(vm.request);
                        }
                    });
                });
            }
        });
        if (!vm.request.AssigneeId) {
            vm.rootObject = angular.copy(vm.request);
        }
                
        vm.loading = false;
    });

    vm.onGroupChange = function () {
        if (vm.request.GroupId) {
            vm.invalidGroup = false;
            $('.assigned-group').removeClass('parsley-error');
        }
        requestDataService.getUsersByRequest(vm.requestId, vm.request.GroupId).success(function(response) {
            vm.users = response.Users;
            if (vm.users.length === 1) {
                vm.request.AssigneeId = vm.users[0].UserId;
                vm.rootObject = angular.copy(vm.request);
            }
        });
    };

    vm.onMemberChange = function () {
        if (vm.request.AssigneeId) {
            vm.invalidUser = false;
            $('.assignee').removeClass('parsley-error');
        }
    };

    vm.assignRequest = function () {        
        if (!vm.request.Description === '' || !vm.request.Description == undefined) {
            vm.request.Description = initialScriptService.removeHiddenText(vm.request.Description);
        }
        vm.request.RequestId = vm.requestId;
        if (!vm.request.isCancel) {
            vm.requiredNote = false;
            $('.assign-note').removeClass('parsley-error');
            if (!vm.request.GroupId) {
                vm.invalidGroup = true;
                $('.assigned-group').focus();
                $('.assigned-group').addClass('parsley-error');
                if (!vm.request.AssigneeId) {
                    vm.invalidUser = true;
                    $('.assignee').focus();
                    $('.assignee').addClass('parsley-error');
                }
                return;
            }
            if (!vm.request.AssigneeId) {
                vm.invalidUser = true;
                $('.assignee').focus();
                $('.assignee').addClass('parsley-error');
                return;
            }
            requestDataService.assignRequest(vm.request).success(function (response) {
                if (response.ErrorType === 0) {
                    $location.url('/manageassignrequest');
                } else {
                    vm.message = $translate.instant(response.ErrorDescription);
                }
            });
        } else {
            $('.assignee').removeClass('parsley-error');
            $('.assigned-group').removeClass('parsley-error');
            vm.invalidUser = false;
            vm.invalidGroup = false;
            if (vm.request.Note === '' || vm.request.Note == undefined) {
                $('.assign-note').focus();
                $('.assign-note').addClass('parsley-error');
                vm.requiredNote = true;
                return;
            }
            requestDataService.assignRequest(vm.request).success(function (response) {
                if (response.ErrorType === 0) {
                    $location.url('/manageassignrequest');
                } else {
                    vm.message = $translate.instant(response.ErrorDescription);
                }
            });
        }
    };

    vm.changeNote = function() {
        if (vm.request.Note !== '' && vm.request.Note != undefined) {
            vm.requiredNote = false;
            $('.assign-note').focus();
            $('.assign-note').removeClass('parsley-error');
        }
    };

    vm.confirmBack = function () {
        //vm.request.Description = initialScriptService.removeHiddenText($('#editor').html());
        if (vm.requestAssign.IsCompleted) {
            vm.back();
        }
        if (JSON.stringify(vm.rootObject) !== JSON.stringify(vm.request)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function() {
        $location.url('/manageassignrequest?keysearch=true');
    };

};

module.exports = ctrl;

