﻿'use strict';

var ctrl = function managecompany($routeParams ,$scope, $q, $translate, $location, dataTableService, DTColumnDefBuilder, initialScriptService, partyConfigurationDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];
    $q.all([
      securityDataService.getPermission('ManageCompany'),
      securityDataService.getPermission('AddCompany'),
      initialScriptService.set()
    ]).then(function (results) {
        vm.permission = results[0].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.permissionAddCompany = results[1].data.Actions;
        vm.party_types = [{ Id: "Com", Name: $translate.instant('ManageParty_Company') }, { Id: "Dep", Name: $translate.instant('ManageParty_Department') }, { Id: "Grp", Name: $translate.instant('ManageParty_Group') }];
        var allOject = { Id: 'All', Name: $translate.instant('FilterAll') };
        vm.party_types.unshift(allOject);
        vm.selectedPartyType = vm.party_types[0].Id;
        vm.selectedStatus = "-1";
        vm.searchManageParty();
        vm.loading = false;
    });
    vm.searchManageParty = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            vm.selectedPartyName = conditionSearchService.managecompany.PartyName;
            vm.selectedPartyCode = conditionSearchService.managecompany.PartyCode;
            vm.selectedStatus = conditionSearchService.managecompany.status;
        }
        var status;
        switch (vm.selectedStatus) {
        case "-1":
            status = null;
            break;
        case "1":
            status = true;
            break;
        case "0":
            status = false;
            break;
        default:
            status = null;
        }
        var data = {
            PartyName: vm.selectedPartyName,
            PartyTypeId: 'Com',
            PartyCode: vm.selectedPartyCode,
            IsActive: status
        }
        partyConfigurationDataService.getManageParties(data).success(function (response) {
            conditionSearchService.managecompany.PartyName =vm.selectedPartyName ;
            conditionSearchService.managecompany.PartyCode =vm.selectedPartyCode ;
            conditionSearchService.managecompany.status = vm.selectedStatus;
            vm.searchResult = [];
            var predata = response.Parties;
            var len = predata.length;
            for (var i = 0; i < len; i += 1) {
                var party = {};
                party.PartyId = predata[i].PartyId;
                party.PartyName = predata[i].PartyName;
                party.PartyCode = predata[i].PartyCode;
                party.IsActive = predata[i].IsActive === true ? $translate.instant('Manage_Using') : $translate.instant('Manage_NotUsing');
                vm.searchResult.push(party);
            }            
        });
    };
    vm.addNew = function() {
        $location.url('/addcompany');
    };
    vm.edit = function(item) {
        $location.url('/addcompany?PartyId=' + item.PartyId + '&Action=Edit');
    };
};

module.exports = ctrl;