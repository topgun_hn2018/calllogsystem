﻿'use strict';

var ctrl = function addcompany($scope, $q, $translate, $location, $routeParams, dataTableService, initialScriptService, partyConfigurationDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.PartyType = "Com";
    vm.partyId = $routeParams.PartyId;
    vm.company = { Id: null, PartyCode: "", PartyName: "", IsActive: true };
    var data = { PartyTypeId: vm.PartyType, PartyId: vm.partyId };
    vm.isAdd = false;
    if ($routeParams.Action ==undefined) vm.isAdd = true;
    vm.rootObject = angular.copy(vm.company);
    $q.all([
       securityDataService.getPermission('AddCompany'),
       initialScriptService.set()
    ]).then(function (results) {
        vm.permission = results[0].data.Actions;        
        vm.loading = false;
    });

    vm.backRequestPage = function () {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.company)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    }
    vm.back = function () {
        $location.url('/managecompany?keysearch=true');
    };
    if (!vm.isAdd) {
        vm.title = $translate.instant('ManageParty_AddCompany');
        partyConfigurationDataService.getPartyById(data).success(function (response) {
                    vm.isCompany = true;
                    vm.isCompany = {};
                    vm.company = response.Company;
                    vm.rootObject = angular.copy(vm.company);
        });
    } else {
        vm.title = $translate.instant('ManageParty_EditCompany');
    }
    vm.FilterDepartments=function() {
        partyConfigurationDataService.getDepartments({ CompanyDepartmentId: vm.group.CompanyId}).success(function (reponse) {
            vm.departments = reponse.Departments;
        });
    }

    vm.Save = function() {
        if (initialScriptService.validateForm()) {
            var data = {
                PartyType: vm.PartyType,
                Company: vm.company
            };
            partyConfigurationDataService.saveParty(data).success(function(response) {
                vm.back();
            });
        }
    }
};

module.exports = ctrl;