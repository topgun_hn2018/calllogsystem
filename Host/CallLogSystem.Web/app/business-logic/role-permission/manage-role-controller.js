﻿'use strict';

var ctrl = function managerole($routeParams,$scope, $q, $location, $translate, dataTableService, DTColumnDefBuilder, initialScriptService, roleDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;
    vm.Role = {
        Name: "",
        IsActive: "-1"
    };
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2).notSortable()
    ];
    vm.searchResult = [{ Id: 0, Name: "", Status: "" }];

    $q.all([
       initialScriptService.set(),
       securityDataService.getPermission('ManageRole'),
       securityDataService.getPermission('AddRole')
    ]).then(function (results) {
        vm.permission = results[1].data.Actions;
        vm.permissionAddRole = results[2].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

        vm.searchRole();

    });
    vm.searchRole = function (key) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && key != 'true') {
            vm.Role.Name = conditionSearchService.Role.Name;
            vm.Role.IsActive = conditionSearchService.Role.IsActive;
        }
        roleDataService.searchRole(vm.Role).success(function (response) {
            conditionSearchService.Role.Name =vm.Role.Name;
            conditionSearchService.Role.IsActive = vm.Role.IsActive;
            vm.searchResult = [];
            //vm.searchResult = [{ Id: 1, Name: "Test 1", IsActive: true }];
            for (var i = 0; i < response.Roles.length; i++) {
                var id = response.Roles[i].Id;
                var name = response.Roles[i].Name;
                var status = response.Roles[i].IsActive ? $translate.instant('ManageRole_Active') : $translate.instant('ManageRole_UnActive');
                vm.searchResult.push({ Id: id, Name: name, Status: status });
            }            
        });
    };
    vm.addNew = function() {
        $location.url("/addrole");
    };
    vm.view = function(item) {
        $location.url('/addrole?RoleId=' + item.Id + '&Action=View');
    };

    vm.edit = function(item) {
        $location.url('/addrole?RoleId=' + item.Id + '&Action=Edit');
    };
    
    vm.loading = false;
};

module.exports = ctrl;