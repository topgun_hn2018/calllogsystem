﻿'use strict';

var ctrl = function addrole($scope, $q, $location, $translate, $routeParams, initialScriptService, roleDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.Permissions = [];
    vm.selectedPermissions = [];
    vm.notSelectedPermissions = [];
    vm.isRegion = false;
    vm.isShowMsg = false;
    vm.isEffect = true;
    vm.RoleId = $routeParams.RoleId;
    vm.IsSellectAll = false;
    vm.IsUnSellectAll = false;
    vm.Role = { Id: null, Name: "", IsActive: true, Permissions: [] };
    $q.all([
       roleDataService.getAllPermission(),
       initialScriptService.set(),
       securityDataService.getPermission('AddRole')
    ]).then(function (results) {
        if ($routeParams.Action == "Edit") {
            vm.Title = $translate.instant('AddRole_EditRole');
        }
        else {
            vm.Title = $translate.instant('ManageRole_AddRole');
        }

        vm.permission = results[2].data.Actions;
        vm.Permissions = results[0].data.Permissions;
        for (var i = 0; i < vm.Permissions.length; i++) {
            vm.notSelectedPermissions.push({ Id: vm.Permissions[i].FunctionActionId, Name: vm.Permissions[i].Name, Selected: false });
        }
        for (var i = 0; i < vm.selectedPermissions.length; i++) {
            vm.Role.Permissions.push({ FunctionActionId: vm.selectedPermissions[i].Id });
        }
        vm.rootObject = angular.copy(vm.Role);
        if (vm.RoleId && vm.RoleId != null && vm.RoleId !== '') {
            roleDataService.getRoleById(vm.RoleId).success(function (response) {
                vm.Role.Id = response.Role.Id;
                vm.Role.Name = response.Role.Name;
                vm.Role.IsActive = response.Role.IsActive;
                for (var i = 0; i < response.Role.Permissions.length; i++) {
                    vm.selectedPermissions.push({ Id: response.Role.Permissions[i].FunctionActionId, Name: response.Role.Permissions[i].Name, Selected: false });
                    for (var j = 0; j < vm.notSelectedPermissions.length; j++) {
                        if (vm.notSelectedPermissions[j].Id === response.Role.Permissions[i].FunctionActionId) {
                            vm.notSelectedPermissions.splice(j, 1);
                        }
                    }
                }
                for (var i = 0; i < vm.selectedPermissions.length; i++) {
                    vm.Role.Permissions.push({ FunctionActionId: vm.selectedPermissions[i].Id });
                }
                vm.rootObject = angular.copy(vm.Role);

            });
        }        
    });


    vm.confirmBack = function () {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.Role)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/managerole?keysearch=true');
    };

    vm.addSelected = function () {
        for (var i = vm.notSelectedPermissions.length - 1; i >= 0; i--) {
            if (vm.notSelectedPermissions[i].Selected) {
                vm.selectedPermissions.push({ Id: vm.notSelectedPermissions[i].Id, Name: vm.notSelectedPermissions[i].Name, Selected: false });
                vm.notSelectedPermissions.splice(i, 1);
            }
        }
        for (var i = 0; i < vm.selectedPermissions.length; i++) {
            vm.Role.Permissions.push({ FunctionActionId: vm.selectedPermissions[i].Id });
        }
    }

    vm.removeSelected=function() {
        for (var i = vm.selectedPermissions.length - 1; i >= 0; i--) {
            if (vm.selectedPermissions[i].Selected) {
                vm.notSelectedPermissions.push({ Id: vm.selectedPermissions[i].Id, Name: vm.selectedPermissions[i].Name, Selected: false });
                vm.selectedPermissions.splice(i, 1);
            }
        }
        for (var i = 0; i < vm.selectedPermissions.length; i++) {
            vm.Role.Permissions.push({ FunctionActionId: vm.selectedPermissions[i].Id });
        }
    }
    vm.Save = function () {
        if (initialScriptService.validateForm()) {
            vm.Role.Permissions = [];
            for (var i = 0; i < vm.selectedPermissions.length; i++) {
                vm.Role.Permissions.push({ FunctionActionId: vm.selectedPermissions[i].Id });
            }
            roleDataService.saveRole(vm.Role).success(function (response) {
                if (response.ErrorType != 0) {
                    vm.isShowMsg = true;
                    vm.errorMessage = $translate.instant('Role_Exist');
                }
                else {
                    vm.back();
                }
            });
        }
    }

    vm.SellectAll = function (sellect) {
        if (sellect === 1) {
            vm.IsSellectAll = !vm.IsSellectAll;
        }
        for (var i = 0; i < vm.notSelectedPermissions.length; i++) {
            vm.notSelectedPermissions[i].Selected = vm.IsSellectAll;
        }
    }
    vm.UnSellectAll = function (sellect) {
        if (sellect === 1) {
            vm.IsUnSellectAll = !vm.IsUnSellectAll;
        }
        for (var i = 0; i < vm.selectedPermissions.length; i++) {
            vm.selectedPermissions[i].Selected = vm.IsUnSellectAll;
        }
    }

    vm.loading = false;
};

module.exports = ctrl;