﻿'use strict';

var ctrl = function addrequestgroup($scope, $q, $translate, $location, $routeParams, uploadDataService, initialScriptService, requestGroupDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.IsActive = true;
    vm.RequestGroupId = $routeParams.RequestGroupId;
    vm.requestGroup = { Id: null, Name: "", AvatarSource: "", OrderNum: "", IsActive: true};
    vm.rootObject = angular.copy(vm.requestGroup);
    $q.all([
        securityDataService.getPermission('AddRequestGroup'),
        initialScriptService.set()
    ]).then(function (results) {
        vm.permission = results[0].data.Actions;
        if (vm.RequestGroupId && vm.RequestGroupId !== '') {
            vm.title = $translate.instant('RequestGroup_EditRequestGroup');
            requestGroupDataService.getCategoryById({ Id: vm.RequestGroupId }).success(function (response) {
                var requestGroup = {};
                requestGroup.Id = response.Category.Id;
                requestGroup.Name = response.Category.Name;
                requestGroup.IsActive = response.Category.IsActive;
                requestGroup.AvatarSource = response.Category.AvatarSource;
                requestGroup.OrderNum = response.Category.OrderNum;
                vm.requestGroup = requestGroup;
                vm.rootObject = angular.copy(vm.requestGroup);
            });
        } else {
            vm.title = $translate.instant('RequestGroup_AddRequestGroup');
        }        
        vm.loading = false;
    });
    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            if (element.files[0].size > $scope.maxLength) {
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MaxSize') + $scope.maxSize + 'MB';
                vm.invalidExtension = true;
            } else if (element.files[0].size === 0) {
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MinSize');
                vm.invalidExtension = true;
            } else {
                vm.invalidExtension = false;
                var fileUpload = element.files[0];
                var data = new FormData();
                data.append("File", fileUpload);
                vm.uploadFile = data;
            }
        });
    }
    vm.Save = function() {
        if (initialScriptService.validateForm()) {
            if ((vm.RequestGroupId == undefined || vm.RequestGroupId === '') && (vm.uploadFile === undefined)) {
                vm.invalidExtension = true;
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_Required');
                return;
            }
            var data = {
                Category: vm.requestGroup
            }
            if (vm.uploadFile) {
                uploadDataService.uploadCategory(vm.uploadFile).success(function (response) {
                    if (response.IsSuccess) {
                        data.Category.AvatarSource = response.FileNameServer;
                        requestGroupDataService.saveCategory(data).success(function(response) {
                            vm.back();
                        });
                    }
                });
            } else {
                requestGroupDataService.saveCategory(data).success(function(response) {
                    vm.back();
                });
            }
        }
    }
    vm.backRequestPage = function () {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.requestGroup) || vm.uploadFile) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    }
    vm.back = function () {
        $location.url('/managerequestgroup?keysearch=true');
    }
}
    
module.exports = ctrl;