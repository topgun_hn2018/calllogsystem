﻿'use strict';

var ctrl = function requestgroup($routeParams, $scope, $q, $translate, $location, dataTableService, DTColumnDefBuilder, initialScriptService, requestGroupDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];
    vm.selectedStatus = '-1';
    vm.categories = [];
    $q.all([
       securityDataService.getPermission('ManageRequestGroup'),
       securityDataService.getPermission('AddRequestGroup'),       
       initialScriptService.set()
    ]).then(function (results) {
        vm.permission = results[0].data.Actions;
        vm.permissionAddRequestGroup = results[1].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.searchCategories();        
        vm.loading = false;
    });
    vm.searchCategories = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            vm.Name = conditionSearchService.managerequestgroup.Name;
            vm.selectedStatus = conditionSearchService.managerequestgroup.IsActive;
        }
        var status;
        switch (vm.selectedStatus) {
            case "-1":
                status = null;
                break;
            case "1":
                status = true;
                break;
            case "0":
                status = false;
                break;
            default:
                status = null;
        }
        var data = {
            Name: vm.Name,
            IsActive: status
        }
        requestGroupDataService.searchCategories(data).success(function (response) {
            // insert value search in service
            conditionSearchService.managerequestgroup.Name =vm.Name ;
            conditionSearchService.managerequestgroup.IsActive =vm.selectedStatus;
            vm.categories = [];
            for (var i = 0; i < response.Categories.length; i++) {
                var id = response.Categories[i].Id;
                var name = response.Categories[i].Name;
                var orderNum = response.Categories[i].OrderNum;
                var isActive = response.Categories[i].IsActive ? $translate.instant('SearchFrom_RoleStatus_On') : $translate.instant('SearchFrom_RoleStatus_Off');
                vm.categories.push({ Id: id, Name: name, OrderNum: orderNum, IsActive: isActive });
            }            
        });
    };
    vm.addNew = function () {
        $location.url("/addrequestgroup");
    }
    vm.edit = function (item) {
        $location.url('/addrequestgroup?RequestGroupId=' + item.Id + '&action=Edit');
    }
};

module.exports = ctrl;