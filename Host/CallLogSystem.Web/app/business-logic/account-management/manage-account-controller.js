﻿'use strict';

var ctrl = function manageaccount($scope, $q, $location, $translate, $routeParams, dataTableService, DTColumnDefBuilder, initialScriptService, accountDataService, securityDataService, DTColumnBuilder, DTOptionsBuilder, $compile, conditionSearchService) {
    //conditionSearchService.status = 1;
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;    
    vm.Account = {
        UserName: "",
        FullName: "",
        IsActive: "-1"
    };   
    vm.status = '-1';

    function createdRow(row, data, dataIndex) {
        // Recompiling so we can bind Angular directive to the DT
        $compile(angular.element(row).contents())($scope);
    }

    vm.dtOptions = DTOptionsBuilder.fromFnPromise(function (key) {
        var defer = $q.defer();
        if ($routeParams != null && $routeParams.keysearch == 'true' && $scope.search!='true') {
            vm.Account.UserName = conditionSearchService.Account.UserName;
            vm.Account.FullName = conditionSearchService.Account.FullName;
            vm.Account.status = conditionSearchService.Account.status;
        }
        accountDataService.searchAccount(vm.Account).success(function (response) {
            conditionSearchService.Account.UserName = vm.Account.UserName;
            conditionSearchService.Account.FullName = vm.Account.FullName;
            conditionSearchService.Account.status = vm.Account.status;
            defer.resolve(response.Accounts);            
            vm.loading = false;
        });
        return defer.promise;
    }).withOption('order', [0, 'asc'])
        .withOption('sScrollX', '100%')
        .withOption('scrollX', true)
        .withOption('bLengthChange', false)
        .withOption('searching', false)
        .withOption('pageLength', $scope.pageSize)
        .withOption('createdRow', createdRow);
    vm.dtColumns = [
        DTColumnBuilder.newColumn('FullName').withTitle($translate.instant('SearchFrom_AccountFullNameLabel')),
        DTColumnBuilder.newColumn('UserName').withTitle($translate.instant('SearchFrom_AccountUserNameLabel')),
        DTColumnBuilder.newColumn('WorkArea').withTitle($translate.instant('SearchFrom_AccountWorkAreaLabel')),
        DTColumnBuilder.newColumn('Status').withTitle($translate.instant('SearchFrom_AccountStatusLabel')).renderWith(function (data, type, full, meta) {
            return $translate.instant(data);
        }),
        DTColumnBuilder.newColumn(null).notSortable().renderWith(function (data, type, full, meta) {
            return '<button type="button" ng-click="vm.edit(' + data.UserId + ')" ng-show="vm.permissionAddAccount.Edit" class="btn btn-success"><i class="fa fa-pencil"></i> '
                + $translate.instant('CreateFrom_Edit') + '</button>';
        })
    ];
    vm.dtInstance = {};

    vm.searchAccount = function (key) {       
        switch (vm.status) {
            case "-1":
                vm.Account.IsActive = null;
                break;
            case "1":
                vm.Account.IsActive = true;
                break;
            case "0":
                vm.Account.IsActive = false;
                break;
        }
        $scope.search = 'true';
        //conditionSearchService.Account = vm.Account;
        window.filterAcc = vm.Account;
        vm.dtInstance.reloadData();
    };

    $q.all([
        initialScriptService.set(),
        securityDataService.getPermission('ManageAccount'),
        securityDataService.getPermission('AddAccount')
    ]).then(function (results) {
        vm.permission = results[1].data.Actions;
        vm.permissionAddAccount = results[2].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

    });

    vm.addNew = function () {
        $location.url("/addaccount");
    };

    vm.edit = function (item) {
        $location.url('/addaccount?AccountId=' + item);
    };
};

module.exports = ctrl;