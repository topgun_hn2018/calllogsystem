﻿'use strict';

var ctrl = function addroleaccount($scope, $q, $location, $routeParams, initialScriptService, accountDataService, roleDataService, partyConfigurationDataService, securityDataService, $translate) {
    var vm = this;
    vm.loading = true;
    vm.Permissions = [];
    vm.selectedPermissions = [];
    vm.Roles = [];
    vm.selectedRoles = [];
    vm.isRegion = false;
    vm.isEffect = true;
    vm.UserId = $routeParams.UserId;
    vm.AliasId = $routeParams.AliasId;
    vm.Alias = { UserId: null, Id: null, CompanyId: null, Roles: [], Addons: [] ,IsDefault: false};
    $q.all([
       roleDataService.getAllRoles(),
       roleDataService.getAllPermission(),
       partyConfigurationDataService.getActiveCompanies(),
       initialScriptService.set(),
       securityDataService.getPermission('AddRoleAccount')
    ]).then(function (results) {
        if ($routeParams.AliasId != "" && $routeParams.AliasId != null) {
            vm.Title = $translate.instant('AddRoleAccount_Edit');
        }
        else {
            vm.Title = $translate.instant('AddRoleAccount_Add');
        }
        vm.permission = results[4].data.Actions;
        vm.Companies = results[2].data.Companies;
        if (vm.AliasId && vm.AliasId != null && vm.AliasId != '') {
            $q.all([
                accountDataService.getAliasById(vm.AliasId)
            ]).then(function (subResults) {
                vm.Alias.Roles = [];
                vm.Alias.Addons = [];
                var data = subResults[0].data.Alias;
                vm.Alias.UserId = vm.UserId;
                vm.Alias.Id = vm.AliasId;
                vm.Alias.IsDefault = data.IsDefault;
                vm.Alias.CompanyId = data.CompanyId;
                vm.selectedRoles = [];
                var i;
                for (i = 0; i < data.Roles.length; i++) {
                    vm.selectedRoles.push(data.Roles[i].Id);
                    vm.Alias.Roles.push({ Id: data.Roles[i].Id });
                }
                vm.selectedPermissions = [];
                for (i = 0; i < data.Addons.length; i++) {
                    vm.selectedPermissions.push(data.Addons[i].PermissionId);
                    vm.Alias.Addons.push({ PermissionId: data.Addons[i].PermissionId });
                }
                vm.Roles = results[0].data.Roles;
                vm.Permissions = results[1].data.Permissions;
                vm.rootObject = angular.copy(vm.Alias);
            });
        } else {
            vm.Roles = results[0].data.Roles;
            vm.Permissions = results[1].data.Permissions;
            vm.rootObject = angular.copy(vm.Alias);
        }

        if ($routeParams.Action === 'View') {
            vm.isEdit = false;
        }
        else {
            vm.isEdit = true;
        }        
    });

    vm.confirmBack = function () {
        var i;
        vm.Alias.Roles = [];
        for (i = 0; i < vm.selectedRoles.length; i++) {
            vm.Alias.Roles.push({ Id: vm.selectedRoles[i] });
        }
        vm.Alias.Addons = [];
        for (i = 0; i < vm.selectedPermissions.length; i++) {
            vm.Alias.Addons.push({ PermissionId: vm.selectedPermissions[i] });
        }

        if (JSON.stringify(vm.rootObject) !== JSON.stringify(vm.Alias)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/addaccount?AccountId=' + vm.UserId);
    };

    vm.Save = function () {
        if (initialScriptService.validateForm()) {
            var i;
            vm.Alias.Roles = [];
            for (i = 0; i < vm.selectedRoles.length; i++) {
                vm.Alias.Roles.push({ Id: vm.selectedRoles[i] });
            }
            vm.Alias.Addons = [];
            for (i = 0; i < vm.selectedPermissions.length; i++) {
                vm.Alias.Addons.push({ PermissionId: vm.selectedPermissions[i] });
            }
            vm.Alias.UserId = vm.UserId;
            vm.Alias.Id = vm.AliasId;
            accountDataService.saveAlias(vm.Alias).success(function (response) {
                if (response.ErrorType == 0) {
                    vm.back();
                } else {

                }

            });
        }
    }

    vm.addNewRole = function () {
        $location.url('/addroleaccount?UserId=' + vm.Account.UserId);
    }
    vm.loading = false;
};

module.exports = ctrl;