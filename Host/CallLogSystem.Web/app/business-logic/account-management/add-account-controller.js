﻿'use strict';
var ctrl = function addaccount($scope, $timeout, $q, $location, $translate, $routeParams, dataTableService, DTColumnDefBuilder, initialScriptService, accountDataService, securityDataService) {
    // test
    var vm = this;
    vm.loading = true;
    vm.Permissions = [];
    vm.selectedPermissions = [];
    vm.isRegion = false;
    vm.isEffect = true;
    vm.AccountId = $routeParams.AccountId;
    vm.isShowMsg = false;
    vm.Account = {
        UserId: null,
        UserName: "",
        FullName: "",
        Password: "",
        ConfirmPassword: "",
        Mobile: "",
        Email: "",
        RoleId: null,
        DefaultAliasId: null,
        IsActive: true,
        Roles: [],
        Aliases: []
    };
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];
    $q.all([
       //accountDataService.getAllRoles(),
       initialScriptService.set(),
        securityDataService.getPermission('AddAccount'),
        securityDataService.getPermission('AddRoleAccount')
    ]).then(function (results) {
        if ($routeParams.AccountId) {
            vm.Title = $translate.instant('AddAccount_EditAccount');
        }
        else {
            vm.Title = $translate.instant('ManageAccount_AddAccount');
        }
        vm.permission = results[1].data.Actions;
        vm.permissionAddRoleAccount = results[2].data.Actions;
        vm.loading = false;
    });
    vm.rootObject = angular.copy(vm.Account);

    if (vm.AccountId && vm.AccountId != null && vm.AccountId != '') {
        accountDataService.getAccountById(vm.AccountId).success(function (response) {
            vm.Account.UserId = response.Account.UserId;
            vm.Account.UserName = response.Account.UserName;
            vm.Account.FullName = response.Account.FullName;
            vm.Account.IsActive = response.Account.IsActive;
            vm.Account.Mobile = response.Account.Mobile;
            vm.Account.Email = response.Account.Email;
            vm.Account.RoleId = response.Account.RoleId;
            vm.Account.DefaultAliasId = response.Account.DefaultAliasId;
            vm.Account.Roles = [];
            for (var i = 0; i < response.Account.Aliases.length; i++) {
                var rolenames = "";
                for (var j = 0; j < response.Account.Aliases[i].Roles.length; j++) {
                    rolenames += response.Account.Aliases[i].Roles[j].Name + ", ";
                }
                if (rolenames.length > 2) rolenames = rolenames.trim().substr(0, rolenames.length - 2);
                var alias = {
                    Id: response.Account.Aliases[i].Id,
                    CompanyName: response.Account.Aliases[i].CompanyName,
                    RoleNames: rolenames
                };
                vm.Account.Aliases.push(alias);
            }

            vm.rootObject = angular.copy(vm.Account);
        });
    };

    vm.confirmBack = function () {
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.Account)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/manageaccount?keysearch=true');
    };

    vm.Save = function() {
        if (vm.validateForm()) {
            accountDataService.saveAccount(vm.Account).success(function(response) {
                if (response.ErrorType === 0) {
                    vm.Account.UserId = response.UserId;
                    if ($routeParams.AccountId) {
                        vm.msg = $translate.instant('AddAccount_EditMsg');
                    } else {
                        vm.msg = $translate.instant('AddAccount_AddMsg');
                    }
                    vm.rootObject = angular.copy(vm.Account);
                } else {
                    $('#acc_pass2').focus();
                    $('#acc_pass2').addClass('parsley-error');
                    vm.isShowMsg = true;
                    vm.errorMessage = $translate.instant(response.ErrorDescription);
                }

            });
            vm.rootObject = angular.copy(vm.Account);
        }
    };

    vm.validateForm = function() {
        vm.isShowMsg = false;
        var valid = initialScriptService.validateForm();
        if (!valid) return valid;
        var passPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
        var phonePattern = /^(?:0|\(?\+84\)?\s?)[1-79](?:[\.\-\s]?\d\d){4}$/;
        var emailPAttern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        //if (vm.Account.Password !='' && !passPattern.test(vm.Account.Password)) {
        //    $('#acc_pass').addClass('parsley-error');
        //    vm.errorMessage = $translate.instant('ManageAccount_PasswordFormat');
        //    vm.isShowMsg = true;
        //    return false;
        //}
        if (vm.Account.Email !== '' && !emailPAttern.test(vm.Account.Email)) {
            $('#acc_pass').focus();
            $('#acc_pass').addClass('parsley-error');
            vm.errorMessage = $translate.instant('ManageAccount_EmailFormat');
            vm.isShowMsg = true;
            return false;
        }
        if (vm.Account.Password != vm.Account.ConfirmPassword) {
            $timeout(function() {
                $('#acc_pass2').focus();
                $('#acc_pass2').addClass('parsley-error');
            }, 100);
            vm.errorMessage = $translate.instant('ManageAccount_PasswordNotMatch');
            vm.isShowMsg = true;
            return false;
        }
        if (vm.Account.Mobile != null && vm.Account.Mobile != '' && !phonePattern.test(vm.Account.Mobile)) {
            $('#acc_pass').focus();
            $('#acc_pass').addClass('parsley-error');
            vm.errorMessage = $translate.instant('ManageAccount_PhoneFormat');
            vm.isShowMsg = true;
            return false;
        }
        return true;
    };

    vm.addNewRole = function() {
        $location.url('/addroleaccount?UserId=' + vm.Account.UserId + '&AliasId=');
    };

    vm.editAlias = function(aliasId) {
        $location.url('/addroleaccount?UserId=' + vm.Account.UserId + '&AliasId=' + aliasId);
    };

    vm.deleteAlias = function() {
        accountDataService.deleteAlias(vm.Account.UserId, vm.deletedId).success(function (response) {
            if (response.ErrorType == 0) {
                vm.Account.Aliases = [];
                for (var i = 0; i < response.Aliases.length; i++) {
                    var rolenames = "";
                    for (var j = 0; j < response.Aliases[i].Roles.length; j++) {
                        rolenames += response.Aliases[i].Roles[j].Name + ", ";
                    }
                    var alias = {
                        Id: response.Aliases[i].Id,
                        CompanyName: response.Aliases[i].CompanyName,
                        RoleNames: rolenames.trim().substr(0, rolenames.length - 1)
                    };
                    vm.Account.Aliases.push(alias);
                }
            } else {
                $('#acc_pass').focus();
                $('#acc_pass').addClass('parsley-error');

                vm.isShowMsg = true;
                vm.errorMessage = $translate.instant(response.ErrorDescription);
            }
        });
    };

    vm.setDeleteAlias = function(id) {
        vm.deletedId = id;
    };
};

module.exports = ctrl;