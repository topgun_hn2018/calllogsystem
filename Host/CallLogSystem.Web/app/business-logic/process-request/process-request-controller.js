﻿'use strict';

var ctrl = function processrequest($scope, $timeout, $q, $filter, $routeParams, $translate, $location, FileSaver, DTColumnDefBuilder, uploadDataService, dataTableService, requestDataService, initialScriptService, masterDataService, requestStatusConstant, eventTypeConstant, securityDataService) {
    var vm = this;
    vm.loading = false;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable(),
        DTColumnDefBuilder.newColumnDef(2).notSortable(),
        DTColumnDefBuilder.newColumnDef(3).notSortable(),
        DTColumnDefBuilder.newColumnDef(4).notSortable(),
        DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];
    vm.documents = [];
    vm.requestId = $routeParams.requestid;

    if (!vm.requestId) {
        $location.url('/notfound');
    }
    vm.request = {
        Description: '',
        Note: '',
        StatusId: '',
        RequestId: vm.requestId
    };

    vm.getRequest = {
        RequestId: vm.requestId
    };

    //download file excel report from server
    vm.onDownload = function (item) {
        uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
            .success(function (response) {
                var file = new Blob([response], { type: item.FileType });
                FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
            });
    };



    $q.all([
       requestDataService.getChildRequest(vm.requestId),
       requestDataService.getHistories(vm.requestId),
       requestDataService.getRequestToProcess(vm.getRequest),
       initialScriptService.set(),
        masterDataService.getKeyObjects('EventTypes'),
        masterDataService.getKeyObjects('RequestStatus'),
        securityDataService.getPermission('ProcessRequest'),
        securityDataService.getPermission('FollowRequest')
    ]).then(function (results) {
        vm.permission = results[6].data.Actions;
        vm.permissionFollowRequest = results[7].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if (results[2].data.ErrorType !== 0) {
            $location.url('/notfound');
        }

        vm.childRequests = results[0].data.Requests;
        vm.KeyObjects = results[4].data.KeyObjects;
        vm.ListRequestStatus = results[5].data.KeyObjects;
        vm.descriptions = [];
        //[2, 14, 15, 16, 17, 20]
        var arrFilter = [
            parseInt(vm.getStatus(vm.KeyObjects, { Key: eventTypeConstant.UpdateProcess })),
            parseInt(vm.getStatus(vm.KeyObjects, { Key: eventTypeConstant.AssignRequest })),
            parseInt(vm.getStatus(vm.KeyObjects, { Key: eventTypeConstant.StartProcess })),
            parseInt(vm.getStatus(vm.KeyObjects, { Key: eventTypeConstant.CompletedProcess })),
            parseInt(vm.getStatus(vm.KeyObjects, { Key: eventTypeConstant.RejectRequest })),
            parseInt(vm.getStatus(vm.KeyObjects, { Key: eventTypeConstant.WaitingForProcess }))
        ];
        angular.forEach(results[1].data.Histories, function (value, key) {
            if ((arrFilter.indexOf(value.EventType) !== -1)) {
                vm.descriptions.push(value);
            }
        });

        var orderBy = $filter('orderBy');
        if (results[2].data.Request != null) {
            vm.RequestStatusId = results[2].data.Request.RequestStatusId;
        }
        vm.descriptions = orderBy(vm.descriptions, '-HistoryId', false);
        if (vm.descriptions.length > 0) {
            vm.request.Description = vm.descriptions[0].Description;
            vm.request.Note = vm.descriptions[0].Content;
        }

        vm.request.StatusId = vm.RequestStatusId;
        

        vm.showBtnStart = vm.RequestStatusId === parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.WaitingForProcess }));
        vm.showBtnRelateCreate = vm.RequestStatusId === parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.Processing }));
        vm.showBtnReject = vm.RequestStatusId === parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.WaitingForProcess })) ||
            vm.RequestStatusId === parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.Processing }));
        var checkChildRequest = vm.checkRelateRequest(vm.childRequests);
        vm.showBtnFinish = (vm.RequestStatusId ===
            parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.Processing })) &&
            checkChildRequest);

        vm.rootObject = angular.copy(vm.request);        
        vm.loading = false;
    });

    vm.getStatus = function (list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return list[i].Value;
        }
        return false;
    }

    vm.checkRelateRequest = function (requests) {
        if (requests == undefined || requests.length === 0) return true;
        var a = parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.ProcessCompleted }));
        var b =
            parseInt(vm.getStatus(vm.ListRequestStatus, { Key: requestStatusConstant.Completed }));
        for (var i = 0; i < requests.length; i++) {
            if (requests[i].RequestStatusId !== a && requests[i].RequestStatusId !== b) {
                return false;
            }
        }
        return true;
    }
    vm.onTextareChange = function () {
        $('#processNote').removeClass('parsley-error');
        vm.isShowMsg = false;
    };

    vm.createRequest = function () {
        $location.url('/addrequest?parentid=' + vm.requestId);
    };

    vm.confirmBack = function () {
        //vm.request.Description = initialScriptService.removeHiddenText($('#editor').html());
        if (JSON.stringify(vm.rootObject) !== JSON.stringify(vm.request)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/manageprocess?keysearch=true');
    };


    vm.processRequest = function (typeRequest) {
        //vm.request.Description = initialScriptService.removeHiddenText($('#editor').html());
        if (!vm.request.Description === '' || !vm.request.Description == undefined) {
            vm.request.Description = initialScriptService.removeHiddenText(vm.request.Description);
        }
        if (typeRequest === null || typeRequest === '') {
            $location.url('/error');
        }
        if ((vm.request.Note === '' || vm.request.Note === null) && typeRequest === 3) {
            $timeout(function () {
                $('#processNote').addClass('parsley-error');
                $('#processNote').focus();
            }, 500);
            vm.isShowMsg = true;
        } else {
            vm.request.StatusId = typeRequest;
            requestDataService.processRequest(vm.request).success(function (response) {
                if (typeRequest === 0) {
                    $scope.isReloadProgressBar = true;
                }else if (typeRequest === 1) {
                    $scope.isReloadProgressBar = true;
                    vm.showBtnStart = false;
                    vm.showBtnFinish = true;
                }
                else if (response.ErrorType === 0) {
                    $location.url('/manageprocess');
                }
            });

        }

    };
};

module.exports = ctrl;

