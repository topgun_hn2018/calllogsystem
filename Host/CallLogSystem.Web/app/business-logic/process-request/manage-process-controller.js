﻿'use strict';

var ctrl = function request($scope, $q, $filter, $translate, $routeParams, $location, dataTableService, DTColumnDefBuilder, initialScriptService, serviceConfigurationDataService, accountDataService, requestDataService, uiDatetimePickerConfig, masterDataService, requestStatusConstant, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    uiDatetimePickerConfig.enableTime = false;
    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [],
        CreateDateTo: null,
        CreateDate: null,
        RequesterId: [-1]
    };
    
    vm.dtOptions = dataTableService.getDataTableOptions(4, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5)
    ];
    $.fn.dataTable.moment($scope.dateTimeMomment);

    vm.requestStatus = [];
    vm.users = [
    {
        id: -1,
        text: $translate.instant('FilterAll')
    }];
    vm.services = [
    {
        Id: -1,
        Name: $translate.instant('FilterAll')
    }];
    vm.counter = {};
    vm.requestStatusConstant = requestStatusConstant;
    $q.all([
       initialScriptService.set(),
       serviceConfigurationDataService.getAllServices(),
       accountDataService.getAllUsers(),
        masterDataService.getKeyObjects('RequestStatus'),
        securityDataService.getPermission('ManageProcess'),
        securityDataService.getPermission('ProcessRequest')
    ]).then(function (results) {
        vm.permission = results[4].data.Actions;
        vm.permissionProcess = results[5].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.requestStatusKeys = results[3].data.KeyObjects;
        if ($routeParams != null && $routeParams.keysearch == 'true') {
            vm.searchCriteria =conditionSearchService.manageprocess;
        }
        else {
            vm.searchCriteria.RequestStatusId.push(vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForProcess }));
        }
        vm.services = vm.services.concat(results[1].data.Services);
        vm.users = vm.users.concat(results[2].data.Users);
        initialScriptService.convertUsersArr(vm.users, vm.searchCriteria.RequesterId);
        
        if (!$routeParams.requeststatusid) {
            vm.searchCriteria.RequestStatusId.push(vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.Processing }));
            vm.searchCriteria.RequestStatusId.push(vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForProcess }));
        } else {
            vm.searchCriteria.RequestStatusId = [parseInt($routeParams.requeststatusid)];
        }
        vm.requestStatus.push({ Id: -1, Name: $translate.instant('FilterAll') });
        vm.requestStatus.push({ Id: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForProcess }), Name: $translate.instant('RequestStatus_WaitingForProcess') });
        vm.requestStatus.push({ Id: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.Processing }), Name: $translate.instant('RequestStatus_Processing') });
        vm.requestStatus.push({ Id: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.ProcessCompleted }), Name: $translate.instant('RequestStatus_Processed') });
        vm.temp = angular.copy(vm.requestStatus);
        vm.searchRequest();
        vm.loading = false;
    });
    vm.searchRequest = function (statusKey, search) {
        if (vm.checkValid() && initialScriptService.validatePastDate($scope.dateFormat)) {
            var temp = vm.requestStatus;
            if (statusKey != "" && statusKey != undefined) {
                var requestStatusId = [vm.getStatus(vm.requestStatusKeys, { Key: statusKey })];
                vm.searchCriteria.RequestStatusId = requestStatusId;
                vm.requestStatus = [];
            }
            vm.invalidDateTo = false;
            requestDataService.getProcessRequestData(vm.searchCriteria).success(function (response) {
                conditionSearchService.manageprocess = vm.searchCriteria;
                vm.searchResult = response.Requests;
                vm.searchCriteria.RequestStatusId = requestStatusId == undefined ? vm.searchCriteria.RequestStatusId : requestStatusId;
                vm.counter.waitingForProcess = vm.counter.waitingForProcess ?
                            vm.counter.waitingForProcess : $filter('filter')(vm.searchResult, { RequestStatusId: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForProcess }) }).length;
                vm.counter.processing = vm.counter.processing ?
                            vm.counter.processing : $filter('filter')(vm.searchResult, { RequestStatusId: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.Processing }) }).length;
                vm.requestStatus = angular.copy(temp);                
            });
        }
    };

    vm.getStatus = function (list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return parseInt(list[i].Value);
        }
        return false;
    }

    vm.checkValid = function () {
        if (vm.searchCriteria.CreateDate && vm.searchCriteria.CreateDateTo &&
            vm.searchCriteria.CreateDate > vm.searchCriteria.CreateDateTo) {
            $('.date-picker-invalid').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid').removeClass('parsley-error');
        return true;
    };
};

module.exports = ctrl;