﻿'use strict';

var ctrl = function request($scope, $q, $filter, $translate, $location, requestDataService, dataTableService, serviceConfigurationDataService, initialScriptService, accountDataService, reportDataService, uploadDataService, FileSaver, masterDataService, requestStatusConstant, securityDataService, uiDatetimePickerConfig, DTColumnBuilder, DTOptionsBuilder, $compile) {
    var vm = this;
    vm.loading = true;

    function createdRow(row, data, dataIndex) {
        // Recompiling so we can bind Angular directive to the DT
        $compile(angular.element(row).contents())($scope);
    }
    vm.isShowGrid = false;

    uiDatetimePickerConfig.enableTime = false;
    $.fn.dataTable.moment($scope.dateTimeMomment);
    vm.requestStatus = [];

    vm.services = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        }];

    vm.completedStatus = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        },
        {
            Id: 0,
            Name: $translate.instant('ManageRequest_Completed')
        },
        {
            Id: 1,
            Name: $translate.instant('ManageRequest_UnCompleted')
        }];

    vm.companies = [
        {
            CompanyId: -1,
            CompanyName: $translate.instant('FilterAll')
        }];

    vm.departments = [
        {
            DepartmentId: -1,
            DepartmentName: $translate.instant('FilterAll')
        }];

    vm.groups = [
        {
            GroupId: -1,
            GroupName: $translate.instant('FilterAll')
        }];

    vm.requesters = [
    {
        UserId: -1,
        Account: $translate.instant('FilterAll')
    }];

    vm.approvers = [
    {
        UserId: -1,
        Account: $translate.instant('FilterAll')
    }];

    vm.assigners = [
    {
        UserId: -1,
        Account: $translate.instant('FilterAll')
    }];

    vm.handlers = [
    {
        UserId: -1,
        Account: $translate.instant('FilterAll')
    }];

    vm.isAdmin = false;

    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [-1],
        CompanyIds: [-1],
        DepartmentIds: [-1],
        GroupIds: [-1],
        RequesterIds: [-1],
        ApproverIds: [-1],
        AssignerIds: [-1],
        HandlerIds: [-1],
        RequestFromDate: null,
        RequestToDate: null,
        ApproveFromDate: null,
        ApproveToDate: null,
        AssignFromDate: null,
        AssignToDate: null,
        HandleFromDate: null,
        HandleToDate: null,
        CompletedStatusId: -1,
        IsDownload: false
    };

    vm.companyFilter = {
        IsAdmin: vm.isAdmin
    }

    vm.departmentFilter = {
        CompaniesId: [-1]
    }

    vm.groupFilter = {
        CompaniesId: [-1],
        DepartmentIds: [-1]
    }

    $q.all([
       initialScriptService.set(),
       serviceConfigurationDataService.getAllServices(),
       reportDataService.GetUserReport(vm.companyFilter),
       reportDataService.GetCompanyByUser(vm.companyFilter),
       reportDataService.GetDepartmentByCompany(vm.departmentFilter),
       reportDataService.GetGroupByDepartment(vm.groupFilter),
        masterDataService.getKeyObjects('RequestStatus'),
        securityDataService.getPermission('Report')
    ]).then(function (results) {
        vm.permission = results[7].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

        vm.services = vm.services.concat(results[1].data.Services);
        vm.requesters = vm.requesters.concat(initialScriptService.getAccount(results[2].data.RequestUsers));
        vm.approvers = vm.approvers.concat(initialScriptService.getAccount(results[2].data.ApproveUsers));
        vm.assigners = vm.assigners.concat(initialScriptService.getAccount(results[2].data.AssignUsers));
        vm.handlers = vm.handlers.concat(initialScriptService.getAccount(results[2].data.HandleUsers));
        vm.companies = vm.companies.concat(results[3].data.Companies);
        vm.departmentFilter.AllCompanies = angular.copy(vm.companies);
        vm.departments = vm.departments.concat(results[4].data.Departments);
        vm.groupFilter.AllCompanies = angular.copy(vm.companies);
        vm.groupFilter.AllDepartments = angular.copy(vm.departments);
        vm.groups = vm.groups.concat(results[5].data.Groups);
        vm.KeyObjects = results[6].data.KeyObjects;

        vm.requestStatus.push({ Id: -1, Name: $translate.instant('FilterAll') });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForApproval }),
            Name: $translate.instant('RequestStatus_WaitingForApproval')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.RejectedApproval }),
            Name: $translate.instant('RequestStatus_Rejected')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForAssigned }),
            Name: $translate.instant('RequestStatus_WaitingForAssigned')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.WaitingForProcess }),
            Name: $translate.instant('RequestStatus_WaitingForProcess')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Processing }),
            Name: $translate.instant('RequestStatus_Processing')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.ProcessCompleted }),
            Name: $translate.instant('RequestStatus_Processed')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.RejectedProcess }),
            Name: $translate.instant('RequestStatus_Cancel')
        });
        vm.requestStatus.push({
            Id: vm.getStatus(vm.KeyObjects, { Key: requestStatusConstant.Completed }),
            Name: $translate.instant('RequestStatus_Completed')
        });

        //vm.getReport();        
        vm.loading = false;
    });

    vm.getReportAssignee = function () {
        if (vm.checkValid1() &&
            vm.checkValid2() &&
            vm.checkValid3() &&
            vm.checkValid4() &&
            initialScriptService.validatePastDate($scope.dateFormat)) {
            vm.searchCriteria.IsDownload = false;
            if (vm.isShowGrid) {
                vm.dtInstance.reloadData();
            } else {
                vm.isShowGrid = true;
                vm.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                    var defer = $q.defer();
                    reportDataService.GetReportAssignee(vm.searchCriteria).success(function (response) {
                        defer.resolve(response.Reports);
                        vm.loading = false;
                    });
                    return defer.promise;
                }).withOption('order', [1, 'asc'])
                    .withOption('sScrollX', '100%')
                    .withOption('scrollX', true)
                    .withOption('scrollY', 700)
                    .withOption('bLengthChange', false)
                    .withOption('searching', false)
                    .withOption('pageLength', $scope.pageSize)
                    .withOption('createdRow', createdRow);
                vm.dtColumns = [
                    DTColumnBuilder.newColumn('Index').withTitle($translate.instant('ReportView_STT')),
                    DTColumnBuilder.newColumn('RequestNo').withTitle($translate.instant('ReportView_RequestNo')),
                    DTColumnBuilder.newColumn('ServiceName').withTitle($translate.instant('ReportView_RequestType')),
                    DTColumnBuilder.newColumn('RequestName').withTitle($translate.instant('ReportView_RequestName')),
                    DTColumnBuilder.newColumn('RequestStatus').withTitle($translate.instant('ReportView_RequestStatus')),
                    DTColumnBuilder.newColumn('RequesterFullName').withTitle($translate.instant('ReportView_Requester')),
                    DTColumnBuilder.newColumn('RequesterCompanyCode').withTitle($translate.instant('ReportView_Company')),
                    DTColumnBuilder.newColumn('RequesterDepartmentCode').withTitle($translate.instant('ReportView_Department')),
                    DTColumnBuilder.newColumn('CreateRequestTime').withTitle($translate.instant('ReportView_CreateDatetime')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ParentRequestNo').withTitle($translate.instant('ReportView_RelateRequest')),
                    DTColumnBuilder.newColumn('RequestChildList').withTitle($translate.instant('ReportView_RelateRequestChild')),
                    DTColumnBuilder.newColumn('RequestNote').withTitle($translate.instant('ReportView_RequestNote')),
                    DTColumnBuilder.newColumn('ExpectedCompleteDate').withTitle($translate.instant('ReportView_ExpectedTime')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('IsCompleted').withTitle($translate.instant('ReportView_IsCompleted')),
                    DTColumnBuilder.newColumn('ApproverLv1').withTitle($translate.instant('ReportView_Approver1')),
                    DTColumnBuilder.newColumn('ApprovedDate1').withTitle($translate.instant('ReportView_ApproveTime1')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ApproveStatus1').withTitle($translate.instant('ReportView_ApproveStatus1')),
                    DTColumnBuilder.newColumn('ApproveNote1').withTitle($translate.instant('ReportView_ApproveNote1')),
                    DTColumnBuilder.newColumn('ApproverLv2').withTitle($translate.instant('ReportView_Approver2')),
                    DTColumnBuilder.newColumn('ApprovedDate2').withTitle($translate.instant('ReportView_ApproveTime2')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ApproveStatus2').withTitle($translate.instant('ReportView_ApproveStatus2')),
                    DTColumnBuilder.newColumn('ApproveNote2').withTitle($translate.instant('ReportView_ApproveNote2')),
                    DTColumnBuilder.newColumn('ApproverLv3').withTitle($translate.instant('ReportView_Approver3')),
                    DTColumnBuilder.newColumn('ApprovedDate3').withTitle($translate.instant('ReportView_ApproveTime3')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ApproveStatus3').withTitle($translate.instant('ReportView_ApproveStatus3')),
                    DTColumnBuilder.newColumn('ApproveNote3').withTitle($translate.instant('ReportView_ApproveNote3')),
                    DTColumnBuilder.newColumn('ApproverLv4').withTitle($translate.instant('ReportView_Approver4')),
                    DTColumnBuilder.newColumn('ApprovedDate4').withTitle($translate.instant('ReportView_ApproveTime4')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ApproveStatus4').withTitle($translate.instant('ReportView_ApproveStatus4')),
                    DTColumnBuilder.newColumn('ApproveNote4').withTitle($translate.instant('ReportView_ApproveNote4')),
                    DTColumnBuilder.newColumn('ApproverLv5').withTitle($translate.instant('ReportView_Approver5')),
                    DTColumnBuilder.newColumn('ApprovedDate5').withTitle($translate.instant('ReportView_ApproveTime5')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ApproveStatus5').withTitle($translate.instant('ReportView_ApproveStatus5')),
                    DTColumnBuilder.newColumn('ApproveNote5').withTitle($translate.instant('ReportView_ApproveNote5')),
                    DTColumnBuilder.newColumn('GroupHandleCode').withTitle($translate.instant('ReportView_GroupNo')),
                    DTColumnBuilder.newColumn('AssignerName').withTitle($translate.instant('ReportView_Assigner')),
                    DTColumnBuilder.newColumn('AssignDate').withTitle($translate.instant('ReportView_AssignDate')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('AssignNote').withTitle($translate.instant('ReportView_AssignNote')),
                    DTColumnBuilder.newColumn('ProcessStartTime').withTitle($translate.instant('ReportView_ProcessStartTime')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('ProcessEndTime').withTitle($translate.instant('ReportView_ProcessEndTime')).renderWith(function (data, type, full, meta) {
                        return data != null ? moment(data).format($scope.dateTimeMomment) : "";
                    }),
                    DTColumnBuilder.newColumn('HandlerName').withTitle($translate.instant('ReportView_Handler')),
                    DTColumnBuilder.newColumn('HandleDescription').withTitle($translate.instant('ReportView_HandleDescription')),
                    DTColumnBuilder.newColumn('HandleNote').withTitle($translate.instant('ReportView_HandleNote')),
                    DTColumnBuilder.newColumn('HandleConfirmStatus').withTitle($translate.instant('ReportView_StatusConfirmComplete')),
                    DTColumnBuilder.newColumn('Rating').withTitle($translate.instant('ReportView_Rating')),
                    DTColumnBuilder.newColumn('NoteIncomplete').withTitle($translate.instant('ReportView_NoteIncomplete')),
                    DTColumnBuilder.newColumn('TATDefault').withTitle($translate.instant('ReportView_DefaultTAT')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('TAT').withTitle($translate.instant('ReportView_TAT')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('Expired').withTitle($translate.instant('ReportView_Expired')),
                    DTColumnBuilder.newColumn('Duration1').withTitle($translate.instant('ReportView_ApproveRealDuration1')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('RealDuration1').withTitle($translate.instant('ReportView_ApproveDuration1')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('Duration2').withTitle($translate.instant('ReportView_ApproveRealDuration2')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('RealDuration2').withTitle($translate.instant('ReportView_ApproveDuration2')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('Duration3').withTitle($translate.instant('ReportView_ApproveRealDuration3')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('RealDuration3').withTitle($translate.instant('ReportView_ApproveDuration3')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('Duration4').withTitle($translate.instant('ReportView_ApproveRealDuration4')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('RealDuration4').withTitle($translate.instant('ReportView_ApproveDuration4')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('Duration5').withTitle($translate.instant('ReportView_ApproveRealDuration5')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('RealDuration5').withTitle($translate.instant('ReportView_ApproveDuration5')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('AssignDuration').withTitle($translate.instant('ReportView_AssignRealDuration')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('AssignRealDuration').withTitle($translate.instant('ReportView_AssignDuration')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('HandleDuration').withTitle($translate.instant('ReportView_HandleRealDuration')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('HandleRealDuration').withTitle($translate.instant('ReportView_HandleDuration')).renderWith(function (data) {
                        return parseFloat(data).toFixed(2);
                    }),
                    DTColumnBuilder.newColumn('SurveyContent').withTitle($translate.instant('ReportView_SurveyContent'))
                ];
                vm.dtInstance = {};
            }
        }
    }

    vm.getStatus = function (list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return parseInt(list[i].Value);
        }
        return false;
    }

    vm.downloadReport = function () {
        if (vm.checkValid1() &&
            vm.checkValid2() &&
            vm.checkValid3() &&
            vm.checkValid4() &&
            initialScriptService.validatePastDate($scope.dateFormat)) {
            vm.searchCriteria.IsDownload = true;
            reportDataService.GetReportAssignee(vm.searchCriteria)
                .success(function (response) {
                    vm.reportResponse = response;
                    vm.reports1 = response.Reports;
                    if (vm.reports1 != null) {
                        uploadDataService.downloadDocument({ FileName: (vm.reportResponse.FileNameServer + "." + vm.reportResponse.FileType), UploadedDate: vm.reportResponse.UploadedDate, fileType: vm.reportResponse.FileType })
                        .success(function (responsedata) {
                            var file = new Blob([responsedata], { type: vm.reportResponse.FileType });
                            FileSaver.saveAs(file, vm.reportResponse.FileNameServer + "." + vm.reportResponse.FileType);
                            uploadDataService.deleteDocument({ FileName: vm.reportResponse.FileNameServer + "." + vm.reportResponse.FileType, UploadedDate: vm.reportResponse.UploadedDate }).success(function (responsedelete) {
                            });
                        });
                    }
                });
        }
    }

    vm.onCompanyChange = function () {
        vm.departments = [];
        vm.departmentFilter.CompaniesId = vm.searchCriteria.CompanyIds;
        reportDataService.GetDepartmentByCompany(vm.departmentFilter).success(function (response) {
            vm.departments = [{ DepartmentId: -1, DepartmentName: $translate.instant('FilterAll') }].concat(response.Departments);
            vm.searchCriteria.DepartmentIds = [-1];
        });
        vm.onDepartmentChange();
    }

    vm.onDepartmentChange = function () {
        vm.groupFilter.DepartmentIds = vm.searchCriteria.DepartmentIds;
        vm.groupFilter.CompaniesId = vm.searchCriteria.CompanyIds;
        vm.groups = [];
        reportDataService.GetGroupByDepartment(vm.groupFilter).success(function (response) {
            vm.groups = [{ GroupId: -1, GroupName: $translate.instant('FilterAll') }].concat(response.Groups);
            vm.searchCriteria.GroupIds = [-1];
        });
    }

    vm.checkValid1 = function () {
        if (vm.searchCriteria.RequestFromDate && vm.searchCriteria.RequestToDate &&
            vm.searchCriteria.RequestFromDate > vm.searchCriteria.RequestToDate) {
            $('.date-picker-invalid1').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid1').removeClass('parsley-error');
        return true;
    };
    vm.checkValid2 = function () {
        if (vm.searchCriteria.ApproveFromDate && vm.searchCriteria.ApproveToDate &&
            vm.searchCriteria.ApproveFromDate > vm.searchCriteria.ApproveToDate) {
            $('.date-picker-invalid2').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid2').removeClass('parsley-error');
        return true;
    };
    vm.checkValid3 = function () {
        if (vm.searchCriteria.AssignFromDate && vm.searchCriteria.AssignToDate &&
            vm.searchCriteria.AssignFromDate > vm.searchCriteria.AssignToDate) {
            $('.date-picker-invalid3').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid3').removeClass('parsley-error');
        return true;
    };
    vm.checkValid4 = function () {
        if (vm.searchCriteria.HandleFromDate && vm.searchCriteria.HandleToDate &&
            vm.searchCriteria.HandleFromDate > vm.searchCriteria.HandleToDate) {
            $('.date-picker-invalid4').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid4').removeClass('parsley-error');
        return true;
    };
};

module.exports = ctrl;