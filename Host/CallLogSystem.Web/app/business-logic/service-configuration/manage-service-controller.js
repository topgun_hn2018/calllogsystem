﻿'use strict';

var ctrl = function service($scope, $q, $routeParams, $location, $translate, dataTableService, DTColumnDefBuilder, initialScriptService, serviceConfigurationDataService, requestConfigurationDataService, partyConfigurationDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];

    vm.requestTypes = [
    {
        Id: -1,
        Name: $translate.instant('FilterAll')
    }];

    vm.companies = [
    {
        Id: -1,
        Name: $translate.instant('FilterAll')
    }];

    vm.departments = [
    {
        DepartmentId: -1,
        DepartmentName: $translate.instant('FilterAll')
    }];

    vm.groups = [
    {
        GroupId: -1,
        GroupName: $translate.instant('FilterAll')
    }];

    vm.status = [
    {
        Id: -1,
        Name: $translate.instant('FilterAll')
    },
    {
        Id: 0,
        Name: $translate.instant('Active')
    },
    {
        Id: 1,
        Name: $translate.instant('InActive')
    }];

    vm.searchCriteria = {
        RequestTypeId: -1,
        CompanyId: -1,
        DepartmentId: -1,
        GroupId: -1,
        IsActive: -1
    }
    

    $q.all([
       initialScriptService.set(),
       requestConfigurationDataService.getAllRequestTypes(),
       partyConfigurationDataService.getAllCompanies(),
       securityDataService.getPermission('ManageService'),
       securityDataService.getPermission('AddService')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.permissionAddService = results[4].data.Actions;
        vm.requestTypes = vm.requestTypes.concat(results[1].data.RequestTypes);
        vm.companies = vm.companies.concat(results[2].data.Companies);

        vm.search();
        vm.loading = false;
    });

    vm.onCompanyChange = function () {
        partyConfigurationDataService.getDepartmentByCompany([vm.searchCriteria.CompanyId]).success(function (response) {
            vm.searchCriteria.DepartmentId = -1;
            vm.searchCriteria.GroupId = -1;
            vm.departments = [
            {
                DepartmentId: -1,
                DepartmentName: $translate.instant('FilterAll')
            }];
            vm.departments = vm.departments.concat(response.Departments);
            vm.groups = [
            {
                GroupId: -1,
                GroupName: $translate.instant('FilterAll')
            }];
        });
    };

    vm.onDepartmentChange = function () {
        partyConfigurationDataService.getGroupByDepartment([vm.searchCriteria.DepartmentId]).success(function (response) {
            vm.searchCriteria.GroupId = -1;
            vm.groups = [
            {
                GroupId: -1,
                GroupName: $translate.instant('FilterAll')
            }];
            vm.groups = vm.groups.concat(response.Groups);
        });
    };

    vm.search = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            //vm.selectedGroup = conditionSearchService.manageservice.CategoryId;
            //vm.selectedStatus = conditionSearchService.manageservice.Status;
            //vm.selectedRequestType = conditionSearchService.manageservice.RequestTypeName;
            vm.searchCriteria = conditionSearchService.manageservice;
        }
        serviceConfigurationDataService.searchService(vm.searchCriteria).success(function (response) {
            // gán value in service
            conditionSearchService.manageservice = vm.searchCriteria;
            vm.searchResult = response.Services;
            angular.forEach(vm.searchResult, function(value, index) {
                if (value.IsActive) {
                    value.Status = $translate.instant('Active');
                } else {
                    value.Status = $translate.instant('InActive');
                }
            });            
        });
    };

    vm.addNew = function() {
        $location.url('/addservice');
    };

    vm.edit = function (serviceId) {
        $location.url('/addservice?serviceid=' + serviceId);
    }
};

module.exports = ctrl;