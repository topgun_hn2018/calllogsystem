﻿'use strict';

var ctrl = function addservice($scope, $q, $location, $routeParams, $filter, $translate, initialScriptService, manageServiceDataService, requestConfigurationDataService, partyConfigurationDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.request = {
        IsApprovedByManager: true,
        ApprovalLevel2: [],
        ApprovalLevel3: [],
        ApprovalLevel4: [],
        ApprovalLevel5: [],
        IsActive: true
    };
    vm.serviceId = $routeParams.serviceid;

    $q.all([
       initialScriptService.set(),
       requestConfigurationDataService.getAllRequestTypes(),
       partyConfigurationDataService.getActiveCompanies(),
       securityDataService.getPermission('AddService')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        vm.requestTypes = results[1].data.RequestTypes;
        
        if ($routeParams.serviceid) {
            vm.tittle = $translate.instant('AddService_EditService');
            $q.all([
                manageServiceDataService.getServiceById($routeParams.serviceid)
            ]).then(function (subResult) {
                vm.request = subResult[0].data;
                vm.companies = results[2].data.Companies;
                partyConfigurationDataService.getDepartmentByCompany(vm.request.CompanyIds).success(function (response) {
                    vm.departments = response.Departments;
                });
                partyConfigurationDataService.getGroupByDepartment(vm.request.DepartmentIds).success(function(response) {
                    vm.groups = response.Groups;
                });
                vm.request.DepartmentIds = vm.toStringArray(vm.request.DepartmentIds);
                vm.request.GroupIds = vm.toStringArray(vm.request.GroupIds);
                vm.request.ApprovalLevel2 = vm.toStringArray(vm.request.ApprovalLevel2);
                vm.request.ApprovalLevel3 = vm.toStringArray(vm.request.ApprovalLevel3);
                vm.request.ApprovalLevel4 = vm.toStringArray(vm.request.ApprovalLevel4);
                vm.request.ApprovalLevel5 = vm.toStringArray(vm.request.ApprovalLevel5);
                vm.rootObject = angular.copy(vm.request);
            });
        } else {
            vm.tittle = $translate.instant('AddService_AddService');
            vm.companies = results[2].data.Companies;
        }        
        vm.loading = false;
    });

    vm.back = function() {
        $location.url('/manageservice?keysearch=true');
    };

    vm.backRequestPage = function() {        
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.request)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.save = function () {
        if (initialScriptService.validateForm() && vm.request.RequestTypeId && vm.request.GroupIds.length > 0) {
            if (vm.request.CompanyIds.length > vm.request.DepartmentIds.length
                || vm.request.DepartmentIds.length > vm.request.GroupIds.length) {
                vm.isError = true;
                vm.message = $translate.instant('AddService_MissingGroup');
                return;
            }                     
            vm.request.ServiceId = $routeParams.serviceid;
            manageServiceDataService.saveService(vm.request).success(function (response) {
                if (response.ErrorType !== 0) {
                    vm.isError = true;
                    vm.message = $translate.instant(response.ErrorDescription);
                } else {
                    vm.back();
                }
            });
        } else {
            return;
        }
    };

    vm.onCompanyChange = function () {
        if (angular.isDefined(vm.request.CompanyIds) && vm.request.CompanyIds.length > 0) {
            $('#company').parent().find('.select2-selection--multiple').removeClass('parsley-error');
            $('#parsley-id-multiple-company').remove();
            partyConfigurationDataService.getDepartmentByCompany(vm.request.CompanyIds).success(function (response) {
                vm.departments = response.Departments;
                angular.forEach(vm.request.DepartmentIds, function(value, key) {
                    var selectedService = $filter('filter')(response.Departments, { DepartmentId: value });
                    if (selectedService.length === 0) {
                        vm.request.DepartmentIds.splice(key, 1);
                    }
                });
                vm.onDepartmentChange();
                vm.request.ApprovalLevel2 = vm.onApprovalLevelChange(vm.request.ApprovalLevel2, vm.departments);
                vm.request.ApprovalLevel3 = vm.onApprovalLevelChange(vm.request.ApprovalLevel3, vm.departments);
                vm.request.ApprovalLevel4 = vm.onApprovalLevelChange(vm.request.ApprovalLevel4, vm.departments);
                vm.request.ApprovalLevel5 = vm.onApprovalLevelChange(vm.request.ApprovalLevel5, vm.departments);
            });
        } else {
            vm.departments = [];
            vm.groups = [];
        }
    };

    vm.onDepartmentChange = function () {
        if (angular.isDefined(vm.request.CompanyIds) && vm.request.CompanyIds.length > 0) {
            $('#department').parent().find('.select2-selection--multiple').removeClass('parsley-error');
            $('#parsley-id-multiple-department').remove();
            partyConfigurationDataService.getGroupByDepartment(vm.request.DepartmentIds).success(function (response) {
                vm.groups = response.Groups;
                angular.forEach(vm.request.GroupIds, function (value, key) {
                    var selectedService = $filter('filter')(vm.groups, { GroupId: value });
                    if (selectedService.length === 0) {
                        vm.request.GroupIds.splice(key, 1);
                    }
                });
            });
        } else {
            vm.groups = [];
        }
    };

    vm.onGroupChange = function() {
        if (vm.request.CompanyIds.length > 0) {
            $('#group').parent().find('.select2-selection--multiple').removeClass('parsley-error');
            $('#parsley-id-multiple-group').remove();
        }
    };

    function anyDepartment(arr, val) {
        return arr.some(function(arrVal) {
            return val === arrVal.DepartmentId.toString();
        });
    };

    vm.onApprovalLevelChange = function(source, targetSource) {
        angular.forEach(source, function(value, key) {
            if (!anyDepartment(targetSource, value)) {
                source.splice(key, 1);
            }
        });
        return source;
    };

    vm.onRequestTypeChange = function() {
        initialScriptService.onChangeSingleInstantSearch();
        angular.forEach(vm.requestTypes, function(item, index) {
            if (item.Id === vm.request.RequestTypeId) {
                vm.request.TAT = item.TAT;
            }
        });
    };

    vm.onApprovalLevel2Change = function () {
        if (vm.request.ApprovalLevel2.length === 0) {
            var temp = angular.copy(vm.departments);
            vm.request.ApprovalLevel3 = [];
            vm.request.ApprovalLevel4 = [];
            vm.request.ApprovalLevel5 = [];
            vm.departments = angular.copy(temp);
        }
    };

    vm.onApprovalLevel3Change = function () {
        if (vm.request.ApprovalLevel3.length === 0) {
            var temp = angular.copy(vm.departments);
            vm.request.ApprovalLevel4 = [];
            vm.request.ApprovalLevel5 = [];
            vm.departments = angular.copy(temp);
        }
    };

    vm.onApprovalLevel4Change = function () {
        if (vm.request.ApprovalLevel4.length === 0) {
            var temp = angular.copy(vm.departments);
            vm.request.ApprovalLevel5 = [];
            vm.departments = angular.copy(temp);
        }
    };

    vm.toStringArray = function(source) {
        var temp =[];
        angular.forEach(source, function(value, index) {
            temp.push(value.toString());
        });
        return temp;
    };

};

module.exports = ctrl;