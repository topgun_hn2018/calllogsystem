﻿'use strict';

var ctrl = function manageemailtemplate($routeParams,$scope, $q, $location, $translate, dataTableService, DTColumnDefBuilder, initialScriptService, emailDataService, requestConfigurationDataService, securityDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    vm.isShowMsg = false;
    vm.EmailTemplate = {
        Name: "",
        Subject: "",
        IsActive: "-1",
        StatusToId: "-1"
    };
    vm.SearchResult = [];
    vm.RequestStatus = [];
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4).notSortable()
    ];
    vm.SearchResult = [{ Id: 0, Name: "", Subject: "", StatusToName: "", Status: "" }];

    $q.all([
        requestConfigurationDataService.getRequestStatus(),
       initialScriptService.set(),
       securityDataService.getPermission('ManageEmailTemplate'),
       securityDataService.getPermission('AddEmailTemplate')
    ]).then(function (results) {
        vm.permission = results[2].data.Actions;
        vm.permissionAddEmail = results[3].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.RequestStatus = results[0].data.RequestStatus;
        vm.RequestStatus.splice(0, 0, { Id: -1, Name: $translate.instant('SearchFrom_EmailTemplateStatus_All') });
        vm.searchEmailTemplate();
    });

    vm.searchEmailTemplate = function (search) {
        if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
            //vm.EmailTemplate.Name = conditionSearchService.manageemailtemplate.Name;
            //vm.EmailTemplate.Subject = conditionSearchService.manageemailtemplate.Subject;
            //vm.EmailTemplate.IsActive = conditionSearchService.manageemailtemplate.IsActive;
            //vm.EmailTemplate.StatusToId = conditionSearchService.manageemailtemplate.StatusToId;
            vm.EmailTemplate = conditionSearchService.manageemailtemplate;
        }
        emailDataService.searchEmailTemplate(vm.EmailTemplate).success(function (response) {
            conditionSearchService.manageemailtemplate=vm.EmailTemplate ;
            vm.SearchResult = [];
            for (var i = 0; i < response.EmailTemplates.length; i++) {
                var id = response.EmailTemplates[i].Id;
                var name = response.EmailTemplates[i].Name;
                var subject = response.EmailTemplates[i].Subject;
                var statusToName = response.EmailTemplates[i].StatusToName;
                var status = response.EmailTemplates[i].IsActive ?
                    $translate.instant('SearchFrom_EmailTemplateStatus_On') : $translate.instant('SearchFrom_EmailTemplateStatus_Off');
                vm.SearchResult.push({ Id: id, Name: name, Subject: subject, StatusToName: statusToName, Status: status });                
            }

        });
    };
    vm.addNewEmailTemplate = function() {
        $location.url("/addemailtemplate");
    };
    vm.view = function(item) {
        $location.url('/addemailtemplate?EmailTemplateId=' + item.Id + '&Action=View');
    };

    vm.edit = function(item) {
        $location.url('/addemailtemplate?EmailTemplateId=' + item.Id + '&Action=Edit');
    };
    vm.loading = false;
};

module.exports = ctrl;