﻿'use strict';

var ctrl = function addemailtemplate($scope, $q, $location, $translate, $routeParams, initialScriptService, emailDataService, requestConfigurationDataService, userManagementDataFactory, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.RequestStatus = [];
    vm.users = [];
    vm.selectedUsers = [];
    vm.isRegion = false;
    vm.isEffect = true;
    vm.isShowErrorMsg = false;
    vm.isShowSuccessMsg = false;
    vm.TrySendEmail = "";
    vm.emailTemplateId = $routeParams.EmailTemplateId;
    vm.emailTemplate = {
        Id: null,
        Name: "",
        Subject: "",
        IsActive: false,
        Content: "",
        EmailSendTo: "",
        EmailCCs: []
    };
    $q.all([
       requestConfigurationDataService.getRequestStatus(),
       userManagementDataFactory.getAllUsers(),
       initialScriptService.set(),
       securityDataService.getPermission('AddEmailTemplate')
    ]).then(function (results) {
        vm.permission = results[3].data.Actions;
        vm.RequestStatus = results[0].data.RequestStatus;
        if (vm.emailTemplateId && vm.emailTemplateId != null && vm.emailTemplateId !== '') {
            vm.Title = $translate.instant('Menu_AddEmailTemplate');
            $q.all([
                emailDataService.getEmailTemplateById(vm.emailTemplateId)
            ]).then(function(subResults) {
                vm.emailTemplate = subResults[0].data.EmailTemplate;
                for (var i = 0; i < subResults[0].data.EmailTemplate.EmailCCs.length; i++) {
                    vm.selectedUsers.push(subResults[0].data.EmailTemplate.EmailCCs[i].UserId);
                }
                initialScriptService.convertUsersArr(results[1].data.Users, vm.selectedUsers);
                vm.setCC();
                vm.rootObject = angular.copy(vm.emailTemplate);                
                vm.loading = false;
            });
        } else {
            vm.Title = $translate.instant('Menu_EditEmailTemplate');
            initialScriptService.convertUsersArr(results[1].data.Users, vm.selectedUsers);
            
            vm.loading = false;
        }
    });

    vm.confirmBack = function () {        
        vm.setCC();
        if (angular.toJson(vm.rootObject) !== angular.toJson(vm.emailTemplate) && vm.emailTemplateId) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };
    
    vm.back = function () {
        $location.url('/manageemailtemplate?keysearch=true');
    };

    vm.Save = function() {
        if (vm.validateForm()) {                        
            vm.setCC();
            emailDataService.saveEmailTemplate(vm.emailTemplate).success(function(response) {
                if (response.ErrorType === 0) {
                    vm.back();
                } else {
                    vm.errorMessage = response.ErrorDescription;
                    vm.isShowErrorMsg = true;
                }

            });
        }
    };

    vm.validateForm = function() {
        var valid = initialScriptService.validateForm(true, true);
        if (!valid) return valid;
        
        if (vm.emailTemplate.Content === '' || vm.emailTemplate.Content == undefined) {
            $('.mce-panel').addClass('parsley-error');
            $('.mce-content-body').addClass('parsley-error');
            $('#parsley-id-13').remove();
            $('#tiny').append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('Required') + "</li></ul>");
            vm.invalidDate = true;
            return false;
        } else {
            var regex = /(<([^>]+)>)/ig;
            var result = vm.emailTemplate.Content.replace(regex, "");
            if (result.replace(/&nbsp;/g, '').length > 4000) {
                $('.mce-panel').addClass('parsley-error');
                $('.mce-content-body').addClass('parsley-error');
                $('#parsley-id-13').remove();
                $('#tiny').append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('MaxLength') + "</li></ul>");                
                return false;
            }
        }
        return true;
    };

    vm.preSend = function() {
        var emailPattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        if (vm.emailTemplate.EmailSendTo != '' && !emailPattern.test(vm.emailTemplate.EmailSendTo)) {
            $('#email_pre_send').addClass('parsley-error');
            vm.errorMessage = $translate.instant('ManageAccount_EmailFormat');
            vm.isShowErrorMsg = true;
            $(document).scrollTop(0);
            return false;
        } else {
            vm.emailTemplate.Content = $('#email-editor').html();
            emailDataService.preSendEmail(vm.emailTemplate).success(function(response) {
                if (response.ErrorType == '1') {
                    vm.errorMessage = $translate.instant(response.ErrorDescription);
                    vm.isShowErrorMsg = true;
                } else {
                    vm.succesMessage = $translate.instant('EmailTemplate_PreSendEmail_Success');
                    vm.isShowSuccessMsg = true;
                }
            });
            $(document).scrollTop(0);
            return true;
        }
    };

    vm.hideMessages = function() {
        vm.isShowSuccessMsg = false;
        vm.isShowErrorMsg = false;
    };
    vm.setCC = function() {
        vm.emailTemplate.EmailCCs = [];
        angular.forEach(vm.selectedUsers, function(item, value) {
            vm.emailTemplate.EmailCCs.push({ UserId: item.toString() });
        });
    };
};

module.exports = ctrl;