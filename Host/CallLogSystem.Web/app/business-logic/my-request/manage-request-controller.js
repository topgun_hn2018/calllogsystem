﻿'use strict';

var ctrl = function request($routeParams,$scope, $q, $location, $translate, $filter, dataTableService, DTColumnDefBuilder, initialScriptService, requestDataService, serviceConfigurationDataService, uiDatetimePickerConfig, securityDataService, requestStatusConstant, masterDataService, conditionSearchService) {
    var vm = this;
    vm.loading = true;
    uiDatetimePickerConfig.enableTime = false;
    vm.searchCriteria = {
        ServiceId: [-1],
        RequestNo: "",
        RequestName: "",
        RequestStatusId: [-1],
        ToDate: null,
        FromDate: null,
        CompletedStatusId: 1
    };
    
    vm.dtOptions = dataTableService.getDataTableOptions(3, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4)
    ];
    $.fn.dataTable.moment($scope.dateTimeMomment);
    vm.requestStatus = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.Draft),
            Name: $translate.instant('RequestStatus_Draft')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForApproval),
            Name: $translate.instant('RequestStatus_WaitingForApproval')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.RejectedApproval),
            Name: $translate.instant('RequestStatus_Rejected')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForAssigned),
            Name: $translate.instant('RequestStatus_WaitingForAssigned')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForProcess),
            Name: $translate.instant('RequestStatus_WaitingForProcess')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.Processing),
            Name: $translate.instant('RequestStatus_Processing')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.ProcessCompleted),
            Name: $translate.instant('RequestStatus_Processed')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.RejectedProcess),
            Name: $translate.instant('RequestStatus_Cancel')
        },
        {
            Id: $scope.filter($scope.keyObjects, requestStatusConstant.Completed),
            Name: $translate.instant('RequestStatus_Completed')
        }];

    vm.services = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        }];

    vm.completedStatus = [
        {
            Id: -1,
            Name: $translate.instant('FilterAll')
        },
        {
            Id: 0,
            Name: $translate.instant('ManageRequest_Completed')
        },
        {
            Id: 1,
            Name: $translate.instant('ManageRequest_UnCompleted')
        }];
    vm.counter = {};
    vm.requestStatusConstant = requestStatusConstant;

    $q.all([
       initialScriptService.set(),
       serviceConfigurationDataService.getAllServices(),
       securityDataService.getPermission('ManageRequest'),
       securityDataService.getPermission('AddRequest'),
       securityDataService.getPermission('FollowRequest'),
       masterDataService.getKeyObjects('RequestStatus')
    ]).then(function (results) {
        vm.services = vm.services.concat(results[1].data.Services);
        vm.permission = results[2].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        vm.permissionAddRequest = results[3].data.Actions;
        vm.permissionFollow = results[4].data.Actions;
        vm.requestStatusKeys = results[5].data.KeyObjects;
        vm.searchRequest();
        vm.loading = false;
    });
    vm.searchRequest = function (statusKey,search) {
        if (vm.checkValid() && initialScriptService.validatePastDate($scope.dateFormat)) {
            var temp = vm.requestStatus;
            if (statusKey != '' && statusKey !=undefined) {
                var requestStatusId = [vm.getStatus(vm.requestStatusKeys, { Key: statusKey })];
                vm.searchCriteria.RequestStatusId = requestStatusId;
                vm.requestStatus = [];
            }
            vm.invalidDateTo = false;
            // push value in back page
            if ($routeParams != null && $routeParams.keysearch == 'true' && search != 'true') {
                vm.searchCriteria = conditionSearchService.managerequest;
            }

            requestDataService.search(vm.searchCriteria).success(function (response) {
                conditionSearchService.managerequest = vm.searchCriteria;


                vm.searchResult = response.Requests;
                vm.searchCriteria.RequestStatusId = requestStatusId == undefined ? vm.searchCriteria.RequestStatusId : requestStatusId;
                vm.counter.waittingForApproval = vm.counter.waittingForApproval ?
                                    vm.counter.waittingForApproval : $filter('filter')(vm.searchResult, { RequestStatusId: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForApproval }) }).length;
                vm.counter.waitingForAssigned = vm.counter.waitingForAssigned ?
                                    vm.counter.waitingForAssigned : $filter('filter')(vm.searchResult, { RequestStatusId: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForAssigned }) }).length;
                vm.counter.waitingForProcess = vm.counter.waitingForProcess ?
                                    vm.counter.waitingForProcess : $filter('filter')(vm.searchResult, { RequestStatusId: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.WaitingForProcess }) }).length;
                vm.counter.processing = vm.counter.processing ?
                                    vm.counter.processing : $filter('filter')(vm.searchResult, { RequestStatusId: vm.getStatus(vm.requestStatusKeys, { Key: requestStatusConstant.Processing }) }).length;
                vm.requestStatus = temp;                
            });
        }
    };
    vm.getStatus = function(list, obj) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Key === obj.Key) return parseInt(list[i].Value);
        }
        return false;
    };

    vm.checkValid = function () {
        if (vm.searchCriteria.FromDate && vm.searchCriteria.ToDate &&
            vm.searchCriteria.FromDate > vm.searchCriteria.ToDate) {
            $('.date-picker-invalid').addClass('parsley-error');
            return false;
        }
        $('.date-picker-invalid').removeClass('parsley-error');
        return true;
    };
    vm.addNewRequest = function () {
        $location.url("/addrequest");
    };
};

module.exports = ctrl;