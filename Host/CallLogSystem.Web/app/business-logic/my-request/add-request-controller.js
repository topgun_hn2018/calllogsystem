﻿'use strict';

var ctrl = function addrequest($scope, $q, $translate, $filter, $routeParams, $location, dataTableService, DTColumnDefBuilder, uploadDataService, initialScriptService, FileSaver, serviceConfigurationDataService, accountDataService, requestDataService, routerDataService, uiDatetimePickerConfig, securityDataService, workingTimeDataService) {
    var vm = this;
    vm.loading = true;
    vm.invalidExtension = false;
    vm.message = '';
    vm.services = [];
    vm.documents = [];
    vm.request = {};
    var today = new Date(new Date());
    uiDatetimePickerConfig.enableTime = true;
    vm.timepickerOptions = { showMeridian: false };
    uiDatetimePickerConfig.showMeridian = false;
    vm.action = $routeParams.action;
    if ($routeParams.action === 'edit') {
        vm.title = $translate.instant('AddRequest_EditHeader');
        vm.requestId = $routeParams.requestid;
    }
    else if ($routeParams.action === 'copy') {
        vm.title = $translate.instant('AddRequest_CopyHeader');
        vm.requestId = $routeParams.requestid;
        vm.isCopy = true;
    }
    else if ($routeParams.parentid) {
        vm.request = {
            ParentId: parseInt($routeParams.parentid)
        };
        vm.title = $translate.instant('AddRequest_Header');
    }
    else {
        vm.title = $translate.instant('AddRequest_Header');
    }
    vm.dtOptions = dataTableService.getDataTableOptions();
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];

    $q.all([
        serviceConfigurationDataService.getAllServices(),
        accountDataService.getAllUsers(),
        requestDataService.getRelatedRequest(),
        securityDataService.getPermission('AddRequest'),
        workingTimeDataService.getWorkingTime({ SelectCompanyId: $scope.defaultSite, Year: today.getFullYear() }),
        initialScriptService.set()
    ]).then(function (results) {
        vm.services = results[0].data.Services;
        if ($routeParams.requesttypeid) {
            vm.requestTypeId = parseInt($routeParams.requesttypeid);
            angular.forEach(vm.services, function (value, key) {
                if (value.RequestTypeId === vm.requestTypeId) {
                    vm.serviceId = value.Id;
                    vm.processDescription = value.Description;
                    vm.request.RequestName = value.Name.concat(' - ', $scope.fullName, ' - ');
                }
            });
            vm.serviceChange();
        }
        if (vm.serviceId === 0) {
            vm.message = $translate.instant('AddRequest_InvalidService');
        }
        vm.relatedRequest = results[2].data.Requests;
        vm.permission = results[3].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }
        if (vm.request.ExpectedCompleteDate == null || vm.request.ExpectedCompleteDate == undefined) {
            if (results[4].data.WorkingTime == null) {
                vm.request.ExpectedCompleteDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 17, 30, 0, 0);
            } else {
                vm.time4 = results[4].data.WorkingTime.Time4;
                var hour = parseInt(vm.time4 / 60);
                var min = vm.time4 % 60;
                vm.request.ExpectedCompleteDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), hour, min, 0, 0);
            }
        }
        if (vm.requestId) {
            $q.all([
                requestDataService.getRequestById(vm.requestId)
            ]).then(function (subResult) {
                if (subResult[0].data.ErrorType === 2) {
                    $location.url("/notfound");
                } else {
                    vm.request = subResult[0].data.Request;
                    vm.requestTypeId = vm.request.RequestTypeId;
                    var offset = new Date().getTimezoneOffset();
                    vm.request.ExpectedCompleteDate = new Date(new Date(vm.request.ExpectedCompleteDate).getTime() + 60000 * offset);
                    vm.rootObject = angular.copy(vm.request);
                    vm.documents = vm.request.Files == null ? [] : vm.request.Files;
                    initialScriptService.convertUsersArr(results[1].data.Users, vm.request.InformTos);
                }                
                vm.loading = false;
            });
        } else {
            initialScriptService.convertUsersArr(results[1].data.Users, vm.request.InformTos);
            vm.loading = false;
        }

    });

    $scope.uploadedFile = function (element) {
        $scope.$apply(function ($scope) {
            if (element.files[0].size > $scope.maxLength) {
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MaxSize') + $scope.maxSize + 'MB';
                vm.invalidExtension = true;
            }
            else if (element.files[0].size === 0) {
                vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MinSize');
                vm.invalidExtension = true;
            } else {
                vm.invalidExtension = false;
                var fileUpload = element.files[0];
                var data = new FormData();
                data.append("File", fileUpload);
                vm.uploadFile = data;
            }

        });
    };

    vm.uploadDocument = function () {
        if (vm.uploadFile) {
            uploadDataService.uploadDocument(vm.uploadFile).success(function (response) {
                if (response.IsSuccess) {
                    vm.invalidExtension = false;
                    $('#file').val('');
                    delete vm.uploadFile;
                    vm.documents.push(response);
                } else {
                    if (response.ErrorType === 4) {
                        vm.errorMsgUpload = $translate.instant('AddRequest_Upload_InvalidType');
                        vm.invalidExtension = true;
                    }
                }
            });
        } else {
            vm.errorMsgUpload = $translate.instant('AddRequest_Upload_Required');
            vm.invalidExtension = true;
        }
    };

    vm.onDownload = function (item) {
        uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
            .success(function (response) {
                var file = new Blob([response], { type: item.FileType });
                FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
            });
    };

    vm.delete = function () {
        uploadDataService.deleteDocument({ FileName: vm.deleteFile.FileNameServer, UploadedDate: vm.deleteFile.UploadedDate })
            .success(function (response) {
                if (response.ErrorType === 0) {
                    var index = vm.documents.indexOf(vm.deleteFile);
                    vm.documents.splice(index, 1);
                }
            });
    };

    vm.save = function (type) {
        if (vm.request.Description === '' || vm.request.Description == undefined) {
            $('.mce-panel').addClass('parsley-error');
            $('.mce-content-body').addClass('parsley-error');
            $('#parsley-id-13').remove();
            $('#tiny').append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('Required') + "</li></ul>");
            vm.invalidDate = true;                     
        }else{            
            var regex = /(<([^>]+)>)/ig;
            var result = vm.request.Description.replace(regex, "");
            if (result.replace(/&nbsp;/g, '').length > 4000) {
                $('.mce-panel').addClass('parsley-error');
                $('.mce-content-body').addClass('parsley-error');                
                $('#parsley-id-13').remove();
                $('#tiny').append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('MaxLength') + "</li></ul>");
                vm.invalidDate = true;
                return;
            }
        }
        var result = initialScriptService.validateFutureDate(vm.request.ExpectedCompleteDate);
        if (!initialScriptService.validateForm(true) || initialScriptService.removeHiddenText(vm.request.Description) === '') {
            if (result === 1) {
                vm.invalidDate = false;
            } else if (result === 2) {
                vm.expectedDateMsg = $translate.instant('AddRequest_InvalidDate');
                vm.invalidDate = true;
            } else {
                vm.expectedDateMsg = $translate.instant('AddRequest_InvalidExpectedDate');
                vm.invalidDate = true;
            }
        }
        else {
            if (vm.request.ExpectedCompleteDate) {
                if (result === 1) {
                    vm.invalidDate = false;
                    if (vm.serviceId) {
                        vm.request.ServiceId = vm.serviceId;
                    }

                    vm.request.Description = initialScriptService.removeHiddenText(vm.request.Description);
                    vm.request.CreateType = type;
                    vm.request.Files = [];
                    for (var i = 0; i < vm.documents.length; i++) {
                        vm.request.Files.push({
                            FileName: vm.documents[i].FileName,
                            FileNameServer: vm.documents[i].FileNameServer,
                            Extension: vm.documents[i].Extension,
                            FileType: vm.documents[i].FileType,
                            Length: vm.documents[i].Length,
                            UploadedDate: vm.documents[i].UploadedDate
                        });
                    }
                    if (!vm.requestId || vm.isCopy) {
                        requestDataService.createRequest(vm.request).success(function (response) {
                            if (response.ErrorType !== 0) {
                                vm.message = $translate.instant(response.ErrorDescription);
                                vm.isShowResponse = true;
                            } else {
                                if ($routeParams.parentid) {
                                    $location.url("/processrequest?requestid=" + $routeParams.parentid);
                                }
                                else {
                                    $location.url("/managerequest");
                                }
                            }
                        });
                    } else {
                        requestDataService.updateRequest(vm.request).success(function (response) {
                            if (response.ErrorType !== 0) {
                                vm.message = $translate.instant(response.ErrorDescription);
                                vm.isShowResponse = true;
                            } else {
                                if ($routeParams.parentid) {
                                    $location.url("/processrequest?requestid=" + $routeParams.parentid);
                                }
                                else {
                                    $location.url("/managerequest");
                                }
                            }
                        });
                    }

                } else if (result === 2) {
                    vm.expectedDateMsg = $translate.instant('AddRequest_InvalidDate');
                    vm.invalidDate = true;
                } else {
                    vm.expectedDateMsg = $translate.instant('AddRequest_InvalidExpectedDate');
                    vm.invalidDate = true;
                }
            } else {
                if (result === 2) {
                    vm.expectedDateMsg = $translate.instant('AddRequest_InvalidDate');
                    vm.invalidDate = true;
                } else {
                    vm.expectedDateMsg = $translate.instant('AddRequest_InvalidExpectedDate');
                    vm.invalidDate = true;
                }
            }
        }
    };




    vm.deleleRequest = function () {
        var deleteRequest = {
            RequestId: vm.requestId
        };
        requestDataService.deleleRequest(deleteRequest).success(function (response) {
            if (response.ErrorType === 0) {
                $location.url("/managerequest");
            } else {
                vm.message = $translate.instant(response.ErrorDescription);
            }
        });
    };

    vm.serviceChange = function () {
        var serviceId;
        vm.isShowWarning = false;
        vm.isShowResponse = false;
        angular.forEach(vm.services, function (value, key) {
            if (value.RequestTypeId === vm.requestTypeId) {
                serviceId = value.Id;
            }
        });
        if (serviceId == null) {
            vm.message = $translate.instant('AddRequest_InvalidService');
            vm.processDescription = "";
            vm.isShowResponse = true;
        } else {
            angular.forEach(vm.services, function (value, key) {
                if (value.Id === serviceId) {
                    if (!value.IsProvidedByCurrentCompany) {
                        vm.warningMsg = $translate.instant('AddRequest_NotProvidedByCurrentCompany');
                        vm.providedCompany = value.ProvidedCompany.join(', ');
                        vm.isShowWarning = true;
                    } else if (!value.IsActiveService) {
                        vm.warningMsg = $translate.instant('AddRequest_InvalidService');
                        vm.isShowWarning = true;
                    } else {
                        vm.request.RequestName = value.Name.concat(' - ', $scope.fullName);
                        vm.processDescription = value.Description;
                        initialScriptService.onChangeSingleInstantSearch();
                        vm.request.ServiceId = serviceId;
                        vm.request.Description = value.RequestTemplate;
                        vm.message = '';
                    }
                }
            });
        }
    };

    vm.confirmBack = function () {
        if (vm.requestId) {
            vm.request.Description = initialScriptService.removeHiddenText(vm.request.Description);
            if (JSON.stringify(vm.rootObject) !== JSON.stringify(vm.request)) {
                $('.confirmation-back').modal();
            } else {
                vm.back();
            }
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        if ($routeParams.parentid) {
            $location.url("/processrequest?requestid=" + $routeParams.parentid);
        }
        else {
            $location.url("/managerequest?keysearch=true");
        }
    };

    vm.onDatePickerChange = function () {
        if ($('.date-picker').val() !== '') {
            $('.date-picker').removeClass('parsley-error');
            $('#parsley-id-11').remove();
        }
        var result = initialScriptService.validateFutureDate(vm.request.ExpectedCompleteDate);
        if (result === 2) {
            vm.expectedDateMsg = $translate.instant('AddRequest_InvalidDate');
            vm.invalidDate = true;
        } else if (result === 0) {
            vm.expectedDateMsg = $translate.instant('AddRequest_InvalidExpectedDate');
            vm.invalidDate = true;
        } else {
            vm.invalidDate = false;
        }
        
    };

}
module.exports = ctrl;