﻿'use strict';

var ctrl = function followrequest($scope, $q, $routeParams, $translate, $location, $filter, initialScriptService, routerDataService, dataTableService, DTColumnDefBuilder, uploadDataService, requestDataService, FileSaver, masterDataService, requestStatusConstant, securityDataService) {
    var vm = this;
    vm.loading = true;    
    vm.dtOptions = dataTableService.getDataTableOptions(3, 'desc');
    vm.dtColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4)
    ];
    $.fn.dataTable.moment($scope.dateTimeMomment);
    vm.dtFileOptions = dataTableService.getDataTableOptions();
    vm.dtFileColumns = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3)
    ];
    
    vm.requestId = $routeParams.requestid;
    vm.confirmResult = {
        IsCompleted: true,
        RequestId: vm.requestId,
        Rating: 0,
        SurveyTemplate: ""
    };
    $q.all([
        requestDataService.getApprovalLevels(vm.requestId),
        requestDataService.getProcessInfo(vm.requestId),
        requestDataService.getConfirmResult(vm.requestId),
        masterDataService.getKeyObjects('RequestStatus'),
        securityDataService.getPermission('FollowRequest'),
        initialScriptService.set()
    ]).then(function (results) {
        vm.permission = results[4].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

        if (results[0].data.ApprovalLevels == null) {
            $location.url("/notfound");
            return;
        }
        vm.approvalLevels = results[0].data.ApprovalLevels;
        vm.processInfo = results[1].data;
        vm.confirmResult = results[2].data.Confirmation == null ? vm.confirmResult : results[2].data.Confirmation;
        vm.compareSurvey = vm.confirmResult.SurveyTemplate;
        vm.confirmResult.RequestId = vm.requestId;        
        $(".stars").starrr({
            rating: vm.confirmResult.Rating
        });
        
        for (var i = 0; i < vm.approvalLevels.length; i++) {
            vm.approvalLevels[i].Status = vm.approvalLevels[i].Status === 'AP' ? $translate.instant('RequestStatus_Approved') : $translate.instant('RequestStatus_Rejected');
        }
        vm.KeyObjects = results[3].data.KeyObjects;
        vm.processCompleted = parseInt($filter('filter')(vm.KeyObjects, { Key: requestStatusConstant.ProcessCompleted })[0].Value);
        vm.completed = parseInt($filter('filter')(vm.KeyObjects, { Key: requestStatusConstant.Completed })[1].Value);
        vm.rejectedProcess = parseInt($filter('filter')(vm.KeyObjects, { Key: requestStatusConstant.RejectedProcess })[0].Value);      
        vm.loading = false;
    });

    vm.onDownload = function (item) {
        uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
            .success(function (response) {
                var file = new Blob([response], { type: item.FileType });
                FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
            });
    };

    vm.copy = function () {
        $location.url('/addrequest?requestid=' + vm.requestId + '&&action=copy');
    };

    vm.back = function () {
        if ($routeParams.parentview) {
            $location.url('/' + $routeParams.parentview + '?keysearch=true');
        }
        else{
            $location.url('/managerequest?keysearch=true');
        }
    };

    vm.send = function () {        
        if (!vm.confirmResult.IsCompleted && !initialScriptService.validateForm() || vm.confirmResult.SurveyTemplate === '' || vm.confirmResult.SurveyTemplate == vm.compareSurvey) {            
            $('.mce-panel').addClass('parsley-error');
            $('.mce-content-body').addClass('parsley-error');
            $('#parsley-id-13').remove();
            $('#tiny').append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('SurveyRequired') + "</li></ul>");
            return;
        }
        var request = {};        
        request.Confirmation = vm.confirmResult;
        requestDataService.confirmResult(request).success(function (response) {
            if (response.ErrorType === 0) {
                $location.url('/managerequest');
            }
        });
    };

    $('.stars').on('starrr:change', function (e, value) {
        vm.confirmResult.Rating = value;
        $('.stars-count').html(value);
    });
};

module.exports = ctrl;

