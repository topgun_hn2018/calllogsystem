﻿'use strict';

var ctrl = function sysconfiguration($scope, $q, $location, $translate, initialScriptService, sysConfigurationDataService, securityDataService) {
    var vm = this;
    vm.loading = true;
    vm.SystemParameters = [];
    vm.isShowSuccessMsg = false;
    vm.isShowErrorMsg = false;
    vm.errorMessage = "";
    vm.SampleTimeFormat = "";
    vm.Moment = null;
    vm.datetimeFormant = "";
    $q.all([
       sysConfigurationDataService.getSystemConfiguration(),
       initialScriptService.set(),
       securityDataService.getPermission('SysConfiguration')
    ]).then(function (results) {
        vm.permission = results[2].data.Actions;
        if (!vm.permission.View) {
            $location.url('/accessdenied');
        }

        var sysparams = results[0].data.SystemParameters;
        vm.SystemParameters = [];
        var param;
        for (var i = 0; i < sysparams.length; i++) {
            if (/^\+?\d+$/.test(sysparams[i].Value)) {
                param = {
                    Id: sysparams[i].Id,
                    Key: sysparams[i].Key,
                    Value: sysparams[i].Value,
                    IntValue: parseInt(sysparams[i].Value),
                    BoolValue: null,
                    IsActive: sysparams[i].IsActive
                }
            } else if (sysparams[i].Value == 'true' || sysparams[i].Value == 'false') {
                param = {
                    Id: sysparams[i].Id,
                    Key: sysparams[i].Key,
                    Value: sysparams[i].Value,
                    IntValue: null,
                    BoolValue: sysparams[i].Value == 'true',
                    IsActive: sysparams[i].IsActive
                }
            } else {
                param = {
                    Id: sysparams[i].Id,
                    Key: sysparams[i].Key,
                    Value: sysparams[i].Value,
                    IntValue: null,
                    BoolValue: null,
                    IsActive: sysparams[i].IsActive
                }
            }
            if (sysparams[i].Key == 'DATETIME_FORMAT') vm.datetimeFormant = sysparams[i].Value;
            vm.SystemParameters.push(param);
        }
        vm.TakeSampleDateTime(vm.datetimeFormant);
        //var date = new Date();
        //vm.Moment = moment([date.getFullYear(), date.getMonth(), date.getDay(), date.getHours(), date.getMinutes(), 50, 125]);
        //vm.SampleTimeFormat = vm.Moment.format(vm.TimeFormat);

        $('.parsley-errors-list').css('display', 'none !important');

        vm.rootObject = angular.copy(vm.SystemParameters);        
    });

    vm.confirmBack = function () {
        if (angular.toJson(vm.rootObject) != angular.toJson(vm.SystemParameters)) {
            $('.confirmation-back').modal();
        } else {
            vm.back();
        }
    };

    vm.back = function () {
        $location.url('/home');
    };

    vm.Save = function() {
        if (!vm.validateForm() || vm.invalidDateTime || !initialScriptService.validateForm()) return;
        for (var i = 0; i < vm.SystemParameters.length; i++) {
            if (vm.SystemParameters[i].IntValue != null) {
                vm.SystemParameters[i].Value = vm.SystemParameters[i].IntValue.toString();
            }
            if (vm.SystemParameters[i].BoolValue != null) {
                vm.SystemParameters[i].Value = vm.SystemParameters[i].BoolValue.toString();
            }
        }
        sysConfigurationDataService.saveSystemConfiguration(vm.SystemParameters).success(function(response) {
            if (response.ErrorType == '0') {
                vm.isShowSuccessMsg = true;
            } else {
                vm.isShowErrorMsg = true;
                vm.errorMessage = $translate.instant(response.ErrorDescription);
            }
        });
        setInterval(vm.hideMessages, 5000);
    };

    vm.validateForm = function() {
        var valid = true;
        for (var i = 0; i < vm.SystemParameters.length; i++) {
            if (vm.SystemParameters[i].IntValue != null) {
                if (vm.SystemParameters[i].IntValue.toString().indexOf('.') >= 0) {
                    vm.isShowErrorMsg = true;
                    vm.errorMessage += $translate.instant('SystemConfiguration_' + vm.SystemParameters[i].Key + '_ErrorMsg');
                    valid = false;
                }
            }
            if (vm.SystemParameters[i].Key == 'DECIMAL_PLACE' && (vm.SystemParameters[i].Value < 0 || vm.SystemParameters[i].Value > 9)) {
                vm.isShowErrorMsg = true;
                vm.errorMessage += $translate.instant('SystemConfiguration_' + vm.SystemParameters[i].Key + '_ErrorMsg');
                valid = false;
            }
        }
        return valid;
    };
    vm.TakeSampleDateTime = function (format) {
        var formatArr = format.split(' ');
        var formated = formatArr[0].toUpperCase();
        if (formatArr.length > 1) {
            formated += ' ' + formatArr[1];
        }
        var date = new Date();
        var temp = moment([date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0, 0]);
        if (temp.format(formated) == 'Invalid Date') {
            vm.invalidDateTime = true;
            vm.SampleTimeFormat = $translate.instant('AddRequest_InvalidDate');
            $('#datetimeFormat').addClass('parsley-error');
        } else {
            vm.invalidDateTime = false;
            $('#datetimeFormat').removeClass('parsley-error');
            vm.SampleTimeFormat = temp.format(formated);
        }
    };
    vm.hideMessages = function() {
        vm.isShowSuccessMsg = false;
        vm.isShowErrorMsg = false;
    };
    
    vm.loading = false;
};

module.exports = ctrl;