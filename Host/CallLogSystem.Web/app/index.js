﻿'use strict';

// Create the module and define its dependencies.
var app = angular.module('CallLogApp', [
    //'matchmedia-ng',
    'ngSanitize',
    'pascalprecht.translate',
    'ngRoute',
    'ngCookies',
    'ngStorage',
    'ngFileSaver',
    'ngDatatables',
    'datatables.scroller',
    //'ngDatepicker',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'gm.datepickerMultiSelect',
    'ui.tinymce'
]);
app.constant('uiDatetimePickerConfig', {
    dateFormat: 'dd-MM-yyyy',
    defaultTime: '17:30:00',
    html5Types: {
        date: 'dd-MM-yyyy',
        'datetime-local': 'dd-MM-yyyyTHH:mm:ss.sss',
        'month': 'MM-yyyy'
    },
    initialPicker: 'date',
    reOpenDefault: false,
    enableDate: true,
    enableTime: true,
    buttonBar: {
        show: true,
        now: {
            show: true,
            text: 'Hiện tại'
        },
        today: {
            show: true,
            text: 'Ngày hôm nay'
        },
        clear: {
            show: true,
            text: 'Xóa'
        },
        date: {
            show: true,
            text: 'Ngày'
        },
        time: {
            show: true,
            text: 'Giờ'
        },
        close: {
            show: true,
            text: 'Đóng'
        }
    },
    closeOnDateSelection: true,
    closeOnTimeNow: true,
    appendToBody: false,
    altInputFormats: [],
    ngModelOptions: {},
    saveAs: false,
    readAs: false
});
// config always runs before the services are ready.
// basically the first thing our module does.
app.config([
    '$routeProvider', '$translateProvider', '$httpProvider',
    function ($routeProvider, $translateProvider, $httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.interceptors.push('ptHttpInterceptor');

        $translateProvider
            .useStaticFilesLoader({
                prefix: 'content/languages/',
                suffix: '.json'
            })
            .preferredLanguage('vi-VN')
            .useSanitizeValueStrategy('escapeParameters');

        $routeProvider
            .when('/error',
            {
                templateUrl: 'app/common/shared/error.html'
            })
            .when('/download',
            {
                templateUrl: 'app/common/shared/download.html',
                controller: 'download as vm'
            })
            .when('/accessdenied',
            {
                templateUrl: 'app/common/shared/access-denied.html'
            })
            .when('/badrequest',
            {
                templateUrl: 'app/common/shared/bad-request.html'
            })
            .when('/login',
            {
                templateUrl: 'app/business-logic/authentication/login.html',
                controller: 'login as vm'
            })
            .when('/',
            {
                templateUrl: 'app/business-logic/home/home.html',
                controller: 'home as vm'
            })
             .when('/home',
            {
                templateUrl: 'app/business-logic/home/home.html',
                controller: 'home as vm'
            })
            .when('/addrequest',
            {
                templateUrl: 'app/business-logic/my-request/add-request.html',
                controller: 'addrequest as vm'
            })
            .when('/managerequest',
            {
                templateUrl: 'app/business-logic/my-request/manage-request.html',
                controller: 'managerequest as vm'
            })
            .when('/followrequest',
            {
                templateUrl: 'app/business-logic/my-request/follow-request.html',
                controller: 'followrequest as vm'
            })
            .when('/manageapprovalrequest',
            {
                templateUrl: 'app/business-logic/approve-request/manage-approval-request.html',
                controller: 'manageapprovalrequest as vm'
            })
            .when('/approverequest',
            {
                templateUrl: 'app/business-logic/approve-request/approve-request.html',
                controller: 'approverequest as vm'
            })
            .when('/manageassignrequest',
            {
                templateUrl: 'app/business-logic/assign-request/manage-assign-request.html',
                controller: 'manageassignrequest as vm'
            })
            .when('/assignrequest',
            {
                templateUrl: 'app/business-logic/assign-request/assign-request.html',
                controller: 'assignrequest as vm'
            })
            .when('/manageprocess',
            {
                templateUrl: 'app/business-logic/process-request/manage-process.html',
                controller: 'manageprocess as vm'
            })
            .when('/processrequest',
            {
                templateUrl: 'app/business-logic/process-request/process-request.html',
                controller: 'processrequest as vm'
            })
            .when('/managerelatedrequest',
            {
                templateUrl: 'app/business-logic/related-request/manage-related.html',
                controller: 'managerelatedrequest as vm'
            })
            .when('/addrequesttype',
            {
                templateUrl: 'app/business-logic/request-configuration/add-request-type.html',
                controller: 'addrequesttype as vm'
            })
            .when('/managerequesttype',
            {
                templateUrl: 'app/business-logic/request-configuration/manage-request-type.html',
                controller: 'managerequesttype as vm'
            })
            .when('/addrequestgroup',
            {
                templateUrl: 'app/business-logic/request-group/add-request-group.html',
                controller: 'addrequestgroup as vm'
            })
            .when('/managerequestgroup',
            {
                templateUrl: 'app/business-logic/request-group/manage-request-group.html',
                controller: 'managerequestgroup as vm'
            })
            .when('/manageservice',
            {
                templateUrl: 'app/business-logic/service-configuration/manage-service.html',
                controller: 'manageservice as vm'
            })
            .when('/addservice',
            {
                templateUrl: 'app/business-logic/service-configuration/add-service.html',
                controller: 'addservice as vm'
            })
            .when('/managerole',
            {
                templateUrl: 'app/business-logic/role-permission/manage-role.html',
                controller: 'managerole as vm'
            })
            .when('/addrole',
            {
                templateUrl: 'app/business-logic/role-permission/add-role.html',
                controller: 'addrole as vm'
            })
            .when('/managecompany',
            {
                templateUrl: 'app/business-logic/company-configuration/manage-company.html',
                controller: 'managecompany as vm'
            })
            .when('/addcompany',
            {
                templateUrl: 'app/business-logic/company-configuration/add-company.html',
                controller: 'addcompany as vm'
            })
            .when('/managedepartment',
            {
                templateUrl: 'app/business-logic/department-configuration/manage-department.html',
                controller: 'managedepartment as vm'
            })
            .when('/adddepartment',
            {
                templateUrl: 'app/business-logic/department-configuration/add-department.html',
                controller: 'adddepartment as vm'
            })
            .when('/managegroup',
            {
                templateUrl: 'app/business-logic/group-configuration/manage-group.html',
                controller: 'managegroup as vm'
            })
            .when('/addgroup',
            {
                templateUrl: 'app/business-logic/group-configuration/add-group.html',
                controller: 'addgroup as vm'
            })
            .when('/manageaccount',
            {
                templateUrl: 'app/business-logic/account-management/manage-account.html',                
                controller: 'manageaccount as vm'
            }).when('/addaccount',
            {
                templateUrl: 'app/business-logic/account-management/add-account.html',
                controller: 'addaccount as vm'
            }).when('/addroleaccount',
            {
                templateUrl: 'app/business-logic/account-management/add-role-account.html',
                controller: 'addroleaccount as vm'
            }).when('/manageemailtemplate',
            {
                templateUrl: 'app/business-logic/email-management/manage-emailtemplate.html',
                controller: 'manageemailtemplate as vm'
            }).when('/addemailtemplate',
            {
                templateUrl: 'app/business-logic/email-management/add-emailtemplate.html',
                controller: 'addemailtemplate as vm'
            }).when('/sysconfiguration',
            {
                templateUrl: 'app/business-logic/sys-configuration/sys-configuration.html',
                controller: 'sysconfiguration as vm'
            }).when('/report',
            {
                templateUrl: 'app/business-logic/report/report.html',
                controller: 'report as vm'
            }).when('/reportassignee',
            {
                templateUrl: 'app/business-logic/report/report-assignee.html',
                controller: 'reportassignee as vm'
            })
            .when('/manageworkingtime',
            {
                templateUrl: 'app/business-logic/working-time/manage-working-time.html',
                controller: 'manageworkingtime as vm'
            })
            .when('/editworkingtime',
            {
                templateUrl: 'app/business-logic/working-time/edit-working-time.html',
                controller: 'editworkingtime as vm'
            })
            .when('/searchrequest',
            {
                templateUrl: 'app/business-logic/search-request/search-request.html',
                controller: 'searchrequest as vm'
            })
            .when('/viewrequest',
            {
                templateUrl: 'app/business-logic/search-request/view-request.html',
                controller: 'viewrequest as vm'
            })
            .otherwise({
                templateUrl: 'app/common/shared/not-found.html'
            });
    }
]);
//register constant
var moment = require('moment');
app.constant('moment', moment);
var X2JS = require('x2js');
app.constant('X2JS', X2JS);
app.constant('eventTypeConstant', {
    CreateRequest: 'CreateRequest',
    UpdateRequest: 'UpdateRequest',
    DeleteRequest: 'DeleteRequest',
    ApprovedRequestLevel1: 'ApprovedRequestLevel1',
    ApprovedRequestLevel2: 'ApprovedRequestLevel2',
    ApprovedRequestLevel3: 'ApprovedRequestLevel3',
    ApprovedRequestLevel4: 'ApprovedRequestLevel4',
    ApprovedRequestLevel5: 'ApprovedRequestLevel5',
    RejectedRequestLevel1: 'RejectedRequestLevel1',
    RejectedRequestLevel2: 'RejectedRequestLevel2',
    RejectedRequestLevel3: 'RejectedRequestLevel3',
    RejectedRequestLevel4: 'RejectedRequestLevel4',
    RejectedRequestLevel5: 'RejectedRequestLevel5',
    AssignRequest: 'AssignRequest',
    StartProcess: 'StartProcess',
    CompletedProcess: 'CompletedProcess',
    RejectRequest: 'RejectRequest',
    ConfirmResult: 'ConfirmResult',
    UpdateProcess: 'UpdateProcess',
    Comment: 'Comment',
    Reopen: 'Reopen'
});

app.constant('requestStatusConstant', {
    Draft: 'Draft',
    WaitingForApproval: 'WaitingForApproval',
    RejectedApproval: 'RejectedApproval',    
    WaitingForAssigned: 'WaitingForAssigned',
    WaitingForProcess: 'WaitingForProcess',
    Processing: 'Processing',
    ProcessCompleted: 'ProcessCompleted',
    RejectedProcess: 'RejectedProcess',
    Completed: 'Completed',
    UpdateProcess: 'UpdateProcess'
});

app.constant('workingTimeConstant', {
    OffAllDay: 'all-day',
    OffMorning: 'morning',
    OffAfternoon: 'afternoon'
});

//register provider here
app.provider('globalConfig', ['X2JS', require('./common/common-services/global-config')]);

//factory register
app.factory('pathService', ['globalConfig', require('./common/common-services/path-service')]);
app.factory('cookieService', ['$window', '$cookies', require('./common/common-services/cookie-extension')]);
app.factory('jwtService', ['$window', 'cookieService', 'globalConfig', require('./common/common-services/jwt-service')]);
app.factory('ptHttpInterceptor', ['$q', '$location', 'cookieService', 'jwtService', require('./common/common-services/http-interceptor')]);
app.factory('dataTableService', ['DTOptionsBuilder', 'globalConfig', require('./common/common-services/data-table-service')]);
app.factory('initialScriptService', ['$translate', '$timeout', require('./common/common-services/initial-script-service')]);
app.factory('routerDataService', [require('./common/common-services/router-data-service')]);
app.factory('conditionSearchService', ['DTOptionsBuilder', 'globalConfig', require('./common/common-services/condition-search-service')]);

//services register
app.factory('authenticationDataService', ['pathService', '$http', require('./common/services/authentication-data-service')]);
app.factory('requestDataService', ['pathService', '$http', require('./common/services/request-data-service')]);
app.factory('uploadDataService', ['pathService', '$http', require('./common/services/upload-document-data-service')]);
app.factory('roleDataService', ['pathService', '$http', require('./common/services/role-data-service')]);
app.factory('emailDataService', ['pathService', '$http', require('./common/services/email-data-service')]);
app.factory('requestConfigurationDataService', ['pathService', '$http', require('./common/services/request-configuration-data-service')]);
app.factory('serviceConfigurationDataService', ['pathService', '$http', require('./common/services/services-configuration-data-service')]);
app.factory('partyConfigurationDataService', ['pathService', '$http', require('./common/services/party-configuration-data-service')]);
app.factory('accountDataService', ['pathService', '$http', require('./common/services/account-data-service')]);
app.factory('requestGroupDataService', ['pathService', '$http', require('./common/services/request-group-data-service')]);
app.factory('sysConfigurationDataService', ['pathService', '$http', require('./common/services/system-configuration-data-service')]);
app.factory('masterDataService', ['pathService', '$http', require('./common/services/master-data-service')]);
app.factory('reportDataService', ['pathService', '$http', require('./common/services/report-data-service')]);
app.factory('workingTimeDataService', ['pathService', '$http', require('./common/services/working-time-data-service')]);
app.factory('securityDataService', ['pathService', '$http', require('./common/services/security-data-service')]);


// DIRECTIVE
app.directive('requestinfor', require('./common/directives/request-info'));
app.directive('comment', require('./common/directives/comment'));

//CONTROLLER
app.controller('main', ['$rootScope', '$window', '$scope', '$filter', '$location', '$translate', '$timeout', '$http', 'cookieService', 'jwtService', 'uiDatetimePickerConfig', 'authenticationDataService', 'sysConfigurationDataService', 'securityDataService', 'pathService', 'masterDataService', 'globalConfig', require('./common/shared/main-controller')]);
app.controller('download', ['$routeParams', 'FileSaver', 'uploadDataService', require('./common/shared/download-controller')]);
app.controller('login', ['$location', '$translate', 'authenticationDataService', 'cookieService', 'jwtService', require('./business-logic/authentication/login-controller')]);
app.controller('home', ['$scope', '$timeout', '$q', '$translate', '$location', '$filter', 'initialScriptService', 'requestGroupDataService', 'requestConfigurationDataService', 'requestDataService', 'securityDataService', 'masterDataService', 'requestStatusConstant', require('./business-logic/home/home-controller')]);
app.controller('addrequest', ['$scope', '$q', '$translate', '$filter', '$routeParams', '$location', 'dataTableService', 'DTColumnDefBuilder', 'uploadDataService', 'initialScriptService', 'FileSaver', 'serviceConfigurationDataService', 'accountDataService', 'requestDataService', 'routerDataService', 'uiDatetimePickerConfig', 'securityDataService', 'workingTimeDataService', require('./business-logic/my-request/add-request-controller')]);
app.controller('managerequest', ['$routeParams','$scope', '$q', '$location', '$translate', '$filter', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'requestDataService', 'serviceConfigurationDataService', 'uiDatetimePickerConfig', 'securityDataService', 'requestStatusConstant', 'masterDataService','conditionSearchService', require('./business-logic/my-request/manage-request-controller')]);
app.controller('followrequest', ['$scope', '$q', '$routeParams', '$translate', '$location', '$filter', 'initialScriptService', 'routerDataService', 'dataTableService', 'DTColumnDefBuilder', 'uploadDataService', 'requestDataService', 'FileSaver', 'masterDataService', 'requestStatusConstant', 'securityDataService', require('./business-logic/my-request/follow-request-controller')]);
app.controller('manageapprovalrequest', ['$routeParams', '$scope', '$q', '$location', '$translate', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'serviceConfigurationDataService', 'accountDataService', 'requestDataService', 'uiDatetimePickerConfig', 'securityDataService', 'masterDataService', 'requestStatusConstant', 'conditionSearchService', require('./business-logic/approve-request/manage-approval-request-controller')]);
app.controller('approverequest', ['$scope','$timeout', '$q', '$translate', '$routeParams', '$location', 'FileSaver', 'uploadDataService', 'DTColumnDefBuilder', 'dataTableService', 'requestDataService', 'securityDataService', require('./business-logic/approve-request/approve-request-controller')]);
app.controller('manageassignrequest', ['$routeParams','$scope', '$q', '$location', '$translate', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'requestDataService', 'serviceConfigurationDataService', 'accountDataService', 'uiDatetimePickerConfig', 'securityDataService', 'requestStatusConstant', 'conditionSearchService', require('./business-logic/assign-request/manage-assign-request-controller')]);
app.controller('assignrequest', ['$scope', '$q', '$location', '$routeParams', '$translate', 'requestDataService', 'initialScriptService', 'securityDataService', require('./business-logic/assign-request/assign-request-controller')]);

app.controller('manageprocess', ['$scope', '$q', '$filter', '$translate', '$routeParams', '$location', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'serviceConfigurationDataService', 'accountDataService', 'requestDataService', 'uiDatetimePickerConfig', 'masterDataService', 'requestStatusConstant', 'securityDataService', 'conditionSearchService', require('./business-logic/process-request/manage-process-controller')]);
app.controller('processrequest', ['$scope','$timeout', '$q', '$filter', '$routeParams', '$translate', '$location', 'FileSaver', 'DTColumnDefBuilder', 'uploadDataService', 'dataTableService', 'requestDataService', 'initialScriptService', 'masterDataService', 'requestStatusConstant', 'eventTypeConstant', 'securityDataService', require('./business-logic/process-request/process-request-controller')]);
app.controller('managerelatedrequest', ['$routeParams','$scope', '$q', '$location', '$translate', '$filter', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'requestDataService', 'serviceConfigurationDataService', 'uiDatetimePickerConfig', 'masterDataService', 'requestStatusConstant', 'securityDataService','conditionSearchService', require('./business-logic/related-request/manage-related-controller')]);
app.controller('managerequesttype', ['$routeParams', '$scope', '$q', '$translate', '$location', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'requestConfigurationDataService', 'requestGroupDataService', 'securityDataService', 'conditionSearchService', require('./business-logic/request-configuration/manage-request-type-controller')]);
app.controller('managerequestgroup', ['$routeParams','$scope', '$q', '$translate', '$location', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'requestGroupDataService', 'securityDataService','conditionSearchService', require('./business-logic/request-group/manage-request-group-controller')]);
app.controller('addrequestgroup', ['$scope', '$q', '$translate', '$location', '$routeParams', 'uploadDataService', 'initialScriptService', 'requestGroupDataService', 'securityDataService', require('./business-logic/request-group/add-request-group-controller')]);
app.controller( 'managerole', ['$routeParams','$scope', '$q', '$location', '$translate', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'roleDataService', 'securityDataService', 'conditionSearchService', require('./business-logic/role-permission/manage-role-controller')]);
app.controller('addrole', ['$scope', '$q', '$location','$translate', '$routeParams', 'initialScriptService', 'roleDataService', 'securityDataService', require('./business-logic/role-permission/add-role-controller')]);
app.controller('addrequesttype', ['$scope', '$q', '$location','$translate', '$routeParams', 'dataTableService', 'initialScriptService', 'requestConfigurationDataService', 'requestGroupDataService', 'securityDataService', require('./business-logic/request-configuration/add-request-type-controller')]);
app.controller('manageservice', ['$scope', '$q', '$routeParams', '$location', '$translate', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'serviceConfigurationDataService', 'requestConfigurationDataService', 'partyConfigurationDataService', 'securityDataService','conditionSearchService', require('./business-logic/service-configuration/manage-service-controller')]);
app.controller('addservice', ['$scope', '$q', '$location', '$routeParams', '$filter', '$translate', 'initialScriptService', 'serviceConfigurationDataService', 'requestConfigurationDataService', 'partyConfigurationDataService', 'securityDataService', require('./business-logic/service-configuration/add-service-controller')]);
app.controller('managegroup', ['$scope', '$window', '$q', '$routeParams', '$translate', '$location', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'partyConfigurationDataService', 'securityDataService', 'conditionSearchService', require('./business-logic/group-configuration/manage-group-controller')]);
app.controller('addgroup', ['$scope', '$q', '$translate', '$location', '$routeParams','$timeout', '$filter', 'dataTableService', 'initialScriptService', 'partyConfigurationDataService', 'securityDataService', require('./business-logic/group-configuration/add-group-controller')]);
app.controller('manageaccount', ['$scope', '$q', '$location', '$translate', '$routeParams', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'accountDataService', 'securityDataService', 'DTColumnBuilder', 'DTOptionsBuilder', '$compile', 'conditionSearchService', require('./business-logic/account-management/manage-account-controller')]);
app.controller('addaccount', ['$scope','$timeout', '$q', '$location', '$translate', '$routeParams', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'accountDataService', 'securityDataService', 'securityDataService', require('./business-logic/account-management/add-account-controller')]);
app.controller('addroleaccount', ['$scope', '$q', '$location', '$routeParams', 'initialScriptService', 'accountDataService', 'roleDataService', 'partyConfigurationDataService', 'securityDataService', '$translate', require('./business-logic/account-management/add-role-account-controller')]);
app.controller('managecompany', ['$routeParams','$scope', '$q', '$translate', '$location', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'partyConfigurationDataService', 'securityDataService','conditionSearchService', require('./business-logic/company-configuration/manage-company-controller')]);
app.controller('addcompany', ['$scope', '$q', '$translate', '$location', '$routeParams', 'dataTableService', 'initialScriptService', 'partyConfigurationDataService', 'securityDataService', require('./business-logic/company-configuration/add-company-controller')]);
app.controller('managedepartment', ['$scope', '$q', '$routeParams', '$translate', '$location', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'partyConfigurationDataService', 'securityDataService','conditionSearchService', require('./business-logic/department-configuration/manage-department-controller')]);
app.controller('adddepartment', ['$scope', '$q', '$translate', '$location', '$routeParams', '$timeout', 'dataTableService', 'initialScriptService', 'partyConfigurationDataService', 'securityDataService', require('./business-logic/department-configuration/add-department-controller')]);
app.controller('report', ['$scope', '$q', '$filter', '$translate', '$location', 'requestDataService', 'dataTableService', 'serviceConfigurationDataService', 'initialScriptService', 'accountDataService', 'reportDataService', 'uploadDataService', 'FileSaver', 'masterDataService', 'requestStatusConstant', 'securityDataService', 'uiDatetimePickerConfig', 'DTColumnBuilder', 'DTOptionsBuilder', '$compile', require('./business-logic/report/report-controller')]);
app.controller('reportassignee', ['$scope', '$q', '$filter', '$translate', '$location', 'requestDataService', 'dataTableService', 'serviceConfigurationDataService', 'initialScriptService', 'accountDataService', 'reportDataService', 'uploadDataService', 'FileSaver', 'masterDataService', 'requestStatusConstant', 'securityDataService', 'uiDatetimePickerConfig', 'DTColumnBuilder', 'DTOptionsBuilder', '$compile', require('./business-logic/report/report-assignee-controller')]);
app.controller('manageworkingtime', ['$scope', '$q', '$translate', '$location', 'workingTimeConstant', 'partyConfigurationDataService', 'workingTimeDataService', 'initialScriptService', 'securityDataService', require('./business-logic/working-time/manage-working-time-controller')]);
app.controller('editworkingtime', ['$scope', '$q', '$translate', '$location', '$routeParams', 'workingTimeConstant', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'workingTimeDataService', 'securityDataService', require('./business-logic/working-time/edit-working-time-controller')]);
app.controller('manageemailtemplate', ['$routeParams','$scope', '$q', '$location', '$translate', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'emailDataService', 'requestConfigurationDataService', 'securityDataService','conditionSearchService', require('./business-logic/email-management/manage-emailtemplate-controller')]);
app.controller('addemailtemplate', ['$scope', '$q', '$location', '$translate', '$routeParams', 'initialScriptService', 'emailDataService', 'requestConfigurationDataService', 'accountDataService', 'securityDataService', require('./business-logic/email-management/add-emailtemplate-controller')]);
app.controller('sysconfiguration', ['$scope', '$q', '$location', '$translate', 'initialScriptService', 'sysConfigurationDataService', 'securityDataService', require('./business-logic/sys-configuration/sys-configuration-controller')]);
app.controller('searchrequest', ['$routeParams', '$scope', '$q', '$location', '$translate', '$filter', 'dataTableService', 'DTColumnDefBuilder', 'initialScriptService', 'requestDataService', 'serviceConfigurationDataService', 'uiDatetimePickerConfig', 'securityDataService', 'requestStatusConstant', 'masterDataService', 'conditionSearchService', require('./business-logic/search-request/search-request-controller')]);
app.controller('viewrequest', ['$scope', '$q', '$routeParams', '$translate', '$location', '$filter', 'initialScriptService', 'routerDataService', 'dataTableService', 'DTColumnDefBuilder', 'uploadDataService', 'requestDataService', 'FileSaver', 'masterDataService', 'requestStatusConstant', 'securityDataService', require('./business-logic/search-request/view-request-controller')]);

module.exports = app;