﻿'use strict';

require('jquery');
require('angular');
require('angularSanitize');
require('angularCookies');
require('ngStorage');
require('angularRoute');
require('angularTranslate');
require('angularTranslateStaticFileLoader');
require('angularFileSaver');

var appModule = require('./index');

angular.element(document).ready(function() {
    angular.bootstrap(document, [appModule.name]);
});

