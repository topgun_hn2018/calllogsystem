﻿var initialScriptServiceFactory = function ($translate, $timeout) {
    var service = {};

    service.set = function () {
        $('.date-picker-0').hide();
        $('.date-picker-1').hide();
        $('.date-picker-mgs').hide();

        //Validate textarea
        $('#editor').bind('DOMNodeInserted DOMNodeRemoved', function () {
            if ($('#editor').html() !== '') {
                $('#editor').removeClass('parsley-error');
                $('#parsley-id-13').remove();
            }
            var regex = /(<([^>]+)>)/ig;
            var result = $('#editor').html().replace(regex, "");
            if (result.replace(/&nbsp;/g, '').length < 4000) {
                $('#editor').removeClass('parsley-error');
                $('#parsley-id-13').remove();
            }
        });        

        $('#email-editor').bind('DOMNodeInserted DOMNodeRemoved', function () {
            if ($('#email-editor').html() !== '') {
                $('#email-editor').removeClass('parsley-error');
                $('#parsley-id-13').remove();
            }
        });
        //setup for textare
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                'Times New Roman', 'Verdana'
            ],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
                container: 'body'
            });
            $('.dropdown-menu input').click(function () {
                return false;
            })
              .change(function () {
                  $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
              })
              .keydown('esc', function () {
                  this.value = '';
                  $(this).change();
              });

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this),
                  target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });

            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();

                $('.voiceBtn').css('position', 'absolute').offset({
                    top: editorOffset.top,
                    left: editorOffset.left + $('#editor').innerWidth() - 35
                });
            } else {
                $('.voiceBtn').hide();
            }
        }
        initToolbarBootstrapBindings();
        $('#editor').wysiwyg({});                                       
        $('#email-editor').wysiwyg({});

        //setup single select instant 
        $(".select2_single").select2();
        $(".select2_group").select2();
        $(".select2_multiple").select2();

        //$('.select2_multiple:required').change(function() {
        //    if ($(this).val() !== '? undefined:undefined ?' && $(this).val() != null) {
        //        $(this).parent().find('.select2-selection--multiple').removeClass('parsley-error');
        //        $(this).parent().find('ul:nth-child(2)').remove();
        //    } else {
        //        $(this).parent().find('.select2-selection--multiple').addClass('parsley-error');
        //    }
        //});
    };

    service.stringToDate = function (stringDate) {
        var validDate = stringDate.split("-");
        return new Date(validDate[2], validDate[1] - 1, validDate[0]);
    }

    service.dateToString = function (date) {
        date = date.split('T')[0];
        var validDate = date.split("-");
        return validDate[2] + '-' + validDate[1] + '-' + validDate[0];
    }

    service.checkDate = function (date) {
        if (date === null) {
            return true;
        }
        if (Object.prototype.toString.call(service.stringToDate(date)) === "[object Date]") {
            return true;
        }
        return false;
    };

    service.validatePastDate = function (dateFormat) {
        var isValid = true;
        var classNo = '.date-picker-';
        $('.date-picker').each(function (i, object) {
            var date = $(this).val();
            if (date !== '') {
                if (moment(date, dateFormat).isValid()) {
                    $(classNo + i).hide();
                    $(this).removeClass('input-error'); 
                } else {
                    $(this).addClass('input-error');
                    $(classNo + i).show();
                    isValid = false;
                }
            }
        });
        return isValid;
    }

    service.validateFutureDate = function (date) {
        if ($('#form').parsley().isValid()) {
            if (date !== '' && date !== null && date != undefined) {
                //var validDate = date.split("-");
                if (date < new Date()) {
                    $('.date-picker').addClass('input-error');
                    return 0;
                } else {
                    $('.date-picker').removeClass('input-error');
                    return 1;
                }
            } else {
                $('.date-picker').addClass('input-error');
                return 2;
            }
        }
        return 1;
    }

    service.onChangeSingleInstantSearch = function () {
        $('.select2-selection--single').removeClass('parsley-error');
        $('#parsley-id-9').remove();

        $('#requestName').removeClass('parsley-error');
        $('#parsley-id-7').remove();
    }
    service.removeValidateSelect = function(id) {
       if($('#'+id).parent().find("li.parsley-required").length === 1)
        {
           $('.select2-selection--single').removeClass('parsley-error');
           $('#' + id).css('border', '1px solid #ccc');
           $('#parsley-id-9').remove();
        }
    }
    service.validateForm = function(isValidateTextarea, isEmail) {
        var isValid = true;
        
        $('select:required').each(function(i, object) {
            if($(this).val() === '? undefined:undefined ?' || $(this).val() == null || $(this).val() === '?') {
                $(this).parent().find('.select2-selection--multiple').addClass('parsley-error');
                isValid = false;
            }
        });

        $('#DDL select').each(function(i, object) {
            if ($(this).val() === '? undefined:undefined ?' || $(this).val() === "? string: ?" || $(this).val() === "? object:null ?" || $(this).val() === '?') {
                $('.select2-selection--single').addClass('parsley-error');
                $(this).css('border', '1px solid #E85445');
                if ($(this).parent().find("li.parsley-required").length === 0)
                    $(this).parent().append('<ul class="parsley-errors-list filled" id="parsley-id-9"><li class="parsley-required">' + $translate.instant('Required') + '</li></ul>');
                isValid = false;
            }
        });
        $('#form').parsley().validate();
        return $('#form').parsley().isValid() ? isValid : false;
    };

    service.validateTinymceWysiwyg = function (htmlValue, WysiwygId) {
        var isValid = true;
        if (htmlValue === '' || htmlValue == undefined) {
            $(WysiwygId +' .mce-panel').addClass('parsley-error');
            $(WysiwygId +' .mce-content-body').addClass('parsley-error');
            $(WysiwygId +' #parsley-id-13').remove();
            $(WysiwygId).append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('Required') + "</li></ul>");
            isValid = false;
        } else {
            var regex = /(<([^>]+)>)/ig;
            var result = htmlValue.replace(regex, "");
            if (result.replace(/&nbsp;/g, '').length > 4000) {
                $(WysiwygId + ' .mce-panel').addClass('parsley-error');
                $(WysiwygId + ' .mce-content-body').addClass('parsley-error');
                $(WysiwygId + ' #parsley-id-13').remove();
                $(WysiwygId).append("<ul class='parsley-errors-list filled' id='parsley-id-13'><li class='parsley-required'>" + $translate.instant('MaxLength') + "</li></ul>");
                isValid = false;
            }
        }
        return isValid;
    }

    service.removeHiddenText = function (source) {
        var startText = '<!--[if';
        var endText = '<![endif]-->';
        var startIndex = source.indexOf(startText);
        var lastIndex = source.indexOf(endText) + endText.length;
        while (startIndex !== -1) {
            var hiddenText = source.substring(startIndex, lastIndex);
            source = source.replace(hiddenText, '');
            startIndex = source.indexOf(startText);
            lastIndex = source.indexOf(endText) + endText.length;
        }
        source = source.replace('<!--', '').replace('-->', '');        
        return source;
    };

    service.getAccount = function (source) {
        angular.forEach(source, function (item, key) {
            item.Account = item.FullName + ' - ' + item.UserName;
        });
        return source;
    };

    service.convertUsersArr = function(source, selectedValues) {
        var selectedStringValues = [];

        //check object is an array
        if (Object.prototype.toString.call(selectedValues) === '[object Array]') {
            angular.forEach(selectedValues, function(item, index) {
                selectedStringValues.push(item.toString());
            });
        } else if (selectedValues != undefined) {
            selectedStringValues.push(selectedValues.toString());
        }

        $('#users-ddl').select2({ data: source });
        $timeout(function() {
            $('#users-ddl').val(selectedStringValues).trigger('change');
        }, 100);
        return selectedStringValues;
    };

    return service;
};

module.exports = initialScriptServiceFactory;