﻿var dataTableServiceFactory = function (DTOptionsBuilder, globalConfig) {
    var service = {};
    var pageSize = parseInt(globalConfig.pageSize);
   
    service.getDataTableOptions = function (columnNumber, orderType) {
        if (!columnNumber) {
            columnNumber = 0;
        }
        if (!orderType) {
            orderType = 'desc';
        }
        return DTOptionsBuilder.newOptions()
            .withOption('order', [columnNumber, orderType])            
            .withOption('sScrollX', '100%')
            .withOption('scrollX', true)
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('pageLength', pageSize);
    };
    service.getDataTableScrollOptions = function (columnNumber, orderType,height) {
        if (!columnNumber) {
            columnNumber = 0;
        }
        if (!height) {
            height = 0;
        }
        if (!orderType) {
            orderType = 'desc';
        }
        return DTOptionsBuilder.newOptions()
            .withOption('order', [columnNumber, orderType])
            .withOption('scrollY', height)
            .withOption('sScrollX', '100%')
            .withOption('bLengthChange', false)
            .withOption('searching', false)
            .withOption('info', false)
             .withOption('paging', true)
            .withOption('pageLength', pageSize);
    };
   
    return service;
};

module.exports = dataTableServiceFactory;