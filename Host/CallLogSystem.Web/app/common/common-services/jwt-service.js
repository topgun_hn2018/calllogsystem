﻿var jwtServiceFactory = function ($window, cookieService, globalConfig) {
    var service = {};
    var parseJwt = function (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        //$window.btoa(JSON.stringify(JSON.parse($window.atob(base64)))).slice(0, -2);
        return JSON.parse($window.atob(base64));
    };

    service.isAuthed = function() {
        var expirationTime = cookieService.getCookie('exp');
        if (expirationTime) {
            if (Math.round(new Date().getTime() / 1000) <= parseInt(expirationTime)) {
                return 'AC';
            } else {                
                return 'IN';
            }

        } else {
            return 'FA';
        }
    };

    service.createClientAccessToken = function (token) {
        var params = parseJwt(token);
        cookieService.createCookie('accessToken', token);
        //cookieService.createCookie('FullName', params.FullName);
        //cookieService.createCookie('UserRoleId', params.UserRoleId);
        //cookieService.createCookie('CompanyId', params.CompanyId);
        //cookieService.createCookie('ComapanyName', params.ComapanyName);
        //cookieService.createCookie('DepartmentId', params.DepartmentId);
        //cookieService.createCookie('DepartmentName', params.DepartmentName);
        cookieService.createCookie('exp', params.exp);
    };

    var getToken = function () {
        var url = globalConfig.logintUrl;
        $window.location.href = url;
    };

    service.authenticate = function () {
        getToken();
    };

    return service;
};
module.exports = jwtServiceFactory;