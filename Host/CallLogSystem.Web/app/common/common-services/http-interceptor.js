﻿var interceptor = function ($q, $location, cookieService, jwtService) {
    return {
        // optional method
        'request': function (config) {
            $(".modal-backdrop.fade.in").remove();
            $('body').removeClass('modal-open');
            var accessToken = cookieService.getCookie("accessToken");
            var authorizationHeader = "Bearer " + accessToken;
            config.headers.authorization = authorizationHeader;
            return config;
        },

        // optional method
        'requestError': function (rejection) {
            //if (canRecover(rejection)) {
            //    return responseOrNewPromise
            //}
            return $q.reject(rejection);
        },

        // optional method
        'response': function (response) {
            if (response.data.AccessToken) {
                jwtService.createClientAccessToken(response.data.AccessToken);
            }
            return response;
        },

        // optional method
        'responseError': function (rejection) {
            //if (canRecover(rejection)) {
            //    return responseOrNewPromise
            //}
            var status = rejection.status;
            if (status === 401) {
                //Handle not login case
                $location.url("/login");
            } else if (status === 403) {
                $location.url("/accessdenied");
            } else if (status === 440) {
                $location.url("/login");
            } else if (status === 400) {
                $location.url("/badrequest");
            } else {
                var accessToken = cookieService.getCookie("accessToken");
                if (accessToken == undefined) {
                    $location.url("/login");
                } else {
                    $location.url("/error");
                }

            }
            return $q.reject(rejection);
        }
    };
};

module.exports = interceptor;