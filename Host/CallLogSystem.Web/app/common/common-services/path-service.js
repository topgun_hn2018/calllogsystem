﻿var pathServiceFactory = function (globalConfig) {
    var service = {};
    service.getApiPath = function (path) {      
        var host = globalConfig.webApiUrl.replace(/\/$/, "");
        if (path.charAt(0) !== "/") {
            path = "/" + path;
        }        
        return host + path;
    };
    service.getPTApiPath = function (path) {
        var host = globalConfig.webPTApiUrl.replace(/\/$/, "");
        if (path.charAt(0) !== "/") {
            path = "/" + path;
        }
        return host + path;
    };
    service.combinePath = function(baseUrl, path) {
        baseUrl = baseUrl.replace(/\/$/, "");
        if (path.charAt(0) !== "/") {
            path = "/" + path;
        }
        return baseUrl + path;
    };

    service.getbasePath = function(url) {
        var startPos = url.indexOf('/') + 1;
        if (startPos <= 0) {
            return url;
        }
        var endPos = url.indexOf('/', startPos);
        return endPos > 0 ? url.substring(startPos, endPos) : url.substring(startPos);
    };
    
    return service;
};

module.exports = pathServiceFactory;