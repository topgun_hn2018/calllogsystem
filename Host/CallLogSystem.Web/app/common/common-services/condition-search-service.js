﻿var conditionSearchServiceFactory = function (DTOptionsBuilder, globalConfig) {
    var service = {
        status:"",
        Account: {
            UserName: "",
            FullName: "",
            status: "-1"
        },
        //
        Role: {
            Name: "",
            IsActive: "",
        },
        //
        managerequesttype: {
            CategoryId: "",
            Status: "",
            RequestTypeName: "",
        },
        //
        manageservice: {
            RequestTypeId: "",
            CompanyId: "",
            DepartmentId: "",
            GroupId: "",
            IsActive: "",
        },
        //
        managecompany: {
            PartyName: "",
            PartyCode: "",
            status: "",
        },
        //
        managedepartment: {
            PartyName: "",
            CompanySearch: "",
            PartyCode:"",
            IsActive: "",
            Manager: "",
            Reporter:"",
        },
        //
        managegroup: {
            PartyName: "",
            PartyCode: "",
            CompanySearch:[],
            DepartmentSearch:[],
            IsActive: "",
            GroupUserLeaderId:"",
            GroupUserMemberId:"",
        },
        //
        managerequestgroup :{
            Name: "",
            IsActive:""
        },
        //
        manageemailtemplate: {
            Name: "",
            Subject: "",
            IsActive: "-1",
            StatusToId: "-1"
        },
        //
        managerequest :{
            ServiceId: [-1],
            RequestNo: "",
            RequestName: "",
            RequestStatusId: [-1],
            ToDate: null,
            FromDate: null,
            CompletedStatusId: 1
        },
        // phê duyệt yêu cầu
        manageapprovalrequest: {
            ServiceId: [-1],
            RequestNo: "",
            RequestName: "",
            RequestStatusId: [-1],
            RequesterId: "-1",
            ToDate: null,
            FromDate: null,
        },
        // phân công xử lý
        manageassignrequest: {
            ServiceId: [-1],
            RequestNo: "",
            RequestName: "",
            RequestStatusId: [],
            RequesterId: [],
            ToDate: null,
            FromDate: null,
        },
        //xử lý yêu cầu
        manageprocess: {
            ServiceId: [-1],
            RequestNo: "",
            RequestName: "",
            RequestStatusId: [],
            CreateDateTo: null,
            CreateDate: null,
            RequesterId: [-1]
        },
        // yêu cầu liên quan 
       managerelatedrequest :{
           ServiceId: [-1],
           RequestNo: "",
           RequestName: "",
           RequestStatusId: [-1],
           ToDate: null,
           FromDate: null,
           CompletedStatusId: 1,
           FilterBy: -1
        // tra cứu yêu cầu
       },
       searchrequest :{
           RequestNo: "",
         },
        viewfrequentlyquestion:{
            QuestionTypeId: "-1",
            QuestionName: "",
            IsActive:true,
          },
        managefrequentlyquestion:{
                QuestionTypeId: "-1",
                QuestionName: "",
                IsActive :false,
                }
        
       
    };

    return service;
   
};

module.exports = conditionSearchServiceFactory;