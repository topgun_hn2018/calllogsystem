﻿var globalConfigFactory = function (X2JS) {
    var globalConfig = {};
    
    this.$get = function() {
        var xmlConfig = jQuery.ajax({
            type: 'GET', url: 'config.xml', cache: false, async: false, contentType: 'text/xml', dataType: 'xml'
        });
        if (xmlConfig.status === 200) {
            var x2js = new X2JS();
            var jsonConfig = x2js.xml_str2json(xmlConfig.responseText);

            angular.extend(globalConfig, angular.fromJson(jsonConfig.globalConfig));
        } else {
            alert("Cannot load global config file");
        }
        return globalConfig;
    };
};

module.exports = globalConfigFactory;