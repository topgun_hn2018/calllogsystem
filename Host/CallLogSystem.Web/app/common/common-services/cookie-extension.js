﻿var cookieServiceFactory = function ($window, $cookies) {
    var service = {};
    service.createCookie = function (name, value) {
        //minutes = minutes || 60;
        //var expireDate = new Date;
        //expireDate.setMinutes(expireDate.getMinutes() + minutes);
        //$cookies.put(name, value, { 'expires': expireDate });
        $cookies.put(name, value);
    };

    service.createObjectCookie = function (name, value) {
        $cookies.put(name, JSON.stringify(value));
    };

    service.getCookie = function (cookieName) {
        return $cookies.get(cookieName);
    };

    service.clearCookie = function () {
        var cookies = $cookies.getAll();
        angular.forEach(cookies, function (v, k) {
            $cookies.remove(k);
        });
    };

    service.clearCookieName = function (cookieName) {
        $cookies.remove(cookieName);
    };

    return service;
};
module.exports = cookieServiceFactory;