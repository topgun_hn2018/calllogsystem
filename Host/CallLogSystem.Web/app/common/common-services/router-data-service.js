﻿var routerDataServiceFactory = function () {
    var service = {};
    var routingData = {};
    service.set = function (data) {
        routingData = data;
    };

    service.get = function () {
        var data = routingData;
        routingData = {};
        return data;
    };

    return service;
};

module.exports = routerDataServiceFactory;