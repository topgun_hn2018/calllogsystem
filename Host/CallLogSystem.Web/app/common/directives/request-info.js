﻿'use strict';

var requestinfor = function requestinfor() {
    return {
        restrict: 'E',
        templateUrl: './app/common/directives/request-info.html',
        controller: [
            '$scope', '$q', '$routeParams', '$location', 'dataTableService', 'DTColumnDefBuilder', 'uploadDataService', 'requestDataService', 'FileSaver', 'requestStatusConstant','$sce',
            function ($scope, $q, $routeParams, $location, dataTableService, DTColumnDefBuilder, uploadDataService, requestDataService, FileSaver, requestStatusConstant, $sce) {
                var vm = this;
                vm.loading = true;
                vm.dtOptions = dataTableService.getDataTableOptions();
                vm.dtColumns = [
                    DTColumnDefBuilder.newColumnDef(0),
                    DTColumnDefBuilder.newColumnDef(1),
                    DTColumnDefBuilder.newColumnDef(2),
                    DTColumnDefBuilder.newColumnDef(3)
                ];
                var requestId = $routeParams.requestid;
                $scope.isReloadProgressBar = false;
                vm.count = 0;
                $q.all([
                    requestDataService.getRequestById(requestId)
                ]).then(function (results) {
                    vm.request = results[0].data.Request;
                    vm.request.Description = $sce.trustAsHtml(vm.request.Description);
                    vm.requesterDepartment = vm.request.DepartmentName.join(', ');
                    var offset = new Date().getTimezoneOffset();
                    vm.request.ExpectedCompleteDate = new Date(new Date(vm.request.ExpectedCompleteDate).getTime() + 60000 * offset);
                    if (vm.request == null) {
                        $location.url('/notfound');
                        return;
                    }
                    vm.documents = vm.request.Files == null ? [] : vm.request.Files;
                    vm.rejectedProcess = $scope.filter($scope.keyObjects, requestStatusConstant.RejectedProcess);
                    vm.rejectedApproval = $scope.filter($scope.keyObjects, requestStatusConstant.RejectedApproval);
                    vm.setColor = function (step) {
                        if (vm.request) {
                            if (vm.request.RequestStatusId === vm.rejectedProcess) {
                                if (vm.request.AssignId == null) {
                                    if (step > $scope.filter($scope.keyObjects, requestStatusConstant.WaitingForAssigned)) {
                                        return 'disabled';
                                    } else {
                                        return 'done';
                                    }
                                } else {
                                    if (step > vm.rejectedProcess) {
                                        return 'disabled';
                                    } else {
                                        return 'done';
                                    }
                                }
                            }
                            if (step === vm.request.RequestStatusId) {
                                if (step === $scope.filter($scope.keyObjects, requestStatusConstant.Completed) ||
                                    step === $scope.filter($scope.keyObjects, requestStatusConstant.ProcessCompleted) ||
                                    step === vm.rejectedApproval) {
                                    return 'done';
                                }
                                return 'selected';
                            } else if (step < vm.request.RequestStatusId) {
                                return 'done';
                            }
                            return 'disabled';
                        }
                        return 'disabled';
                    };
                    vm.loading = false;
                });

                vm.onDownload = function (item) {
                    uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
                        .success(function (response) {
                            var file = new Blob([response], { type: item.FileType });
                            FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
                        });
                };

                 $scope.$watch("isReloadProgressBar", function () {
                     if ($scope.isReloadProgressBar) {
                         requestDataService.getRequestById(requestId).success(function(response) {
                             vm.request = response.Request;
                         });
                     }
                 });

            }],
        controllerAs: "requestinfor"
    };
};

module.exports = requestinfor;