﻿'use strict';

var comment = function comment() {
    return {
        restrict: 'E',
        templateUrl: './app/common/directives/comment.html',
        controller: [
            '$scope', '$q', '$routeParams', '$translate', '$filter', 'dataTableService', 'DTColumnDefBuilder', 'uploadDataService', 'requestDataService', 'FileSaver', 'cookieService', 'masterDataService', 'eventTypeConstant',
    function ($scope, $q, $routeParams, $translate, $filter, dataTableService, DTColumnDefBuilder, uploadDataService, requestDataService, FileSaver, cookieService, masterDataService, eventTypeConstant) {
                var vm = this;
                vm.loading = true;
                vm.dtOptions = dataTableService.getDataTableOptions();
                vm.dtColumns = [
                    DTColumnDefBuilder.newColumnDef(0).notSortable(),
                    DTColumnDefBuilder.newColumnDef(1).notSortable(),
                    DTColumnDefBuilder.newColumnDef(2).notSortable(),
                    DTColumnDefBuilder.newColumnDef(3).notSortable()
                ];

                vm.documents = [];
                vm.requestId = $routeParams.requestid;
                vm.request = {
                    RequestId: vm.requestId,
                    Content: ''
                }
                $q.all([
                    requestDataService.getHistories(vm.requestId),
                    masterDataService.getKeyObjects('EventTypes')
                ]).then(function (result) {
                    vm.requestComment = result[0].data.Histories;
                    vm.isFollowRequest = result[0].data.IsSubscriber;
                    for (var i = 0; i < vm.requestComment.length; i++) {
                        vm.requestComment[i].Id = i;
                    }
                    vm.eventTypes = result[1].data.KeyObjects;
                });

                $scope.$watch("isReloadProgressBar", function () {
                    if ($scope.isReloadProgressBar) {
                        requestDataService.getHistories(vm.requestId).success(function (response) {
                            vm.requestComment = response.Histories;
                            vm.isFollowRequest = response.IsSubscriber;
                            for (var i = 0; i < vm.requestComment.length; i++) {
                                vm.requestComment[i].Id = i;
                            }
                        });
                    }
                });

                $scope.uploadedFile = function (element) {
                    $scope.$apply(function ($scope) {
                        if (element.files[0].size > $scope.maxLength) {
                            vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MaxSize') +$scope.maxSize + 'MB';
                            vm.invalidExtension = true;
                        } else if (element.files[0].size === 0) {
                            vm.errorMsgUpload = $translate.instant('AddRequest_Upload_MinSize');
                            vm.invalidExtension = true;
                        } else {
                            vm.invalidExtension = false;
                            var fileUpload = element.files[0];
                            var data = new FormData();
                            data.append("File", fileUpload);
                            vm.uploadFile = data;
                        }
                    });
                };

                vm.uploadDocument = function () {
                    if (vm.uploadFile) {
                        uploadDataService.uploadDocument(vm.uploadFile).success(function (response) {
                            if (response.IsSuccess) {
                                vm.invalidExtension = false;
                                $('#comment-file').val(''); 
                                delete vm.uploadFile;
                                vm.documents = [];
                                vm.documents.push(response);
                            } else {
                                if (response.ErrorType === 4) {
                                    vm.errorMsgUpload = $translate.instant('AddRequest_Upload_InvalidType');
                                    vm.invalidExtension = true;
                                }
                            }
                        });
                    } else {
                        vm.errorMsgUpload = $translate.instant('AddRequest_Upload_Required');
                        vm.invalidExtension = true;
                    }
                };

                vm.onDownload = function (item) {
                    uploadDataService.downloadDocument({ FileName: item.FileNameServer, UploadedDate: item.UploadedDate, fileType: item.FileType })
                        .success(function (response) {
                            var file = new Blob([response], { type: item.FileType });
                            FileSaver.saveAs(file, item.FileName + '.' + item.Extension.trim());
                        });
                };
                vm.delete = function () {
                    uploadDataService.deleteDocument({ FileName: vm.documents[0].FileNameServer, UploadedDate: vm.documents[0].UploadedDate })
                        .success(function (response) {
                            if (response.ErrorType === 0) {
                                vm.documents = [];
                            }
                        });
                };

                vm.addComment = function () {
                    if (vm.request.Content === '' || vm.request.Content === null) {
                        $('#editor').addClass('parsley-error');
                        vm.isShowMsg = true;
                    } else {
                        if (vm.documents.length > 0) {
                            vm.request.File = {
                                FileName: vm.documents[0].FileName,
                                FileNameServer: vm.documents[0].FileNameServer,
                                Extension: vm.documents[0].Extension,
                                FileType: vm.documents[0].FileType,
                                Length: vm.documents[0].Length,
                                UploadedDate: vm.documents[0].UploadedDate
                            };
                        }
                        requestDataService.createComment(vm.request).success(function (response) {
                            if (response.ErrorType === 0) {
                                var keyValue;
                                angular.forEach(vm.eventTypes, function (item, index) {
                                    if (item.Key === eventTypeConstant.Comment) {
                                        keyValue = item;
                                    }
                                });
                                var index = vm.requestComment.length;
                                var fullName = (JSON.parse(cookieService.getCookie('UserProfile'))).FullName;
                                vm.requestComment.push({
                                    Id: index + 1,
                                    EventType: parseInt(keyValue.Value),
                                    UserName: fullName,
                                    Files: vm.request.File == undefined ? []:[vm.request.File],
                                    Content: vm.request.Content,
                                    CreatedDate: response.CreatedDate
                                });
                                vm.request = {
                                    RequestId: vm.requestId,
                                    Content: ''
                                }
                                vm.documents = [];
                                $('#comment-file').val('');
                            }
                        });
                    }
                };

                vm.onTextareChange = function () {
                    if (vm.request.Content === '' || vm.request.Content === null) {
                        $('#editor').addClass('parsley-error');
                        vm.isShowMsg = true;
                    } else {
                        $('#editor').removeClass('parsley-error');
                        vm.isShowMsg = false;
                    }
                };

                vm.followRequest = function (action) {
                    switch (action) {
                        case 'Y':
                            break;
                        case 'N':
                            vm.isFollowRequest = !vm.isFollowRequest;
                            break;
                    }
                    requestDataService.followRequest({ RequestId: vm.requestId, IsFollow: vm.isFollowRequest }).success(function (response) {
                        if (response.ErrorType !== 0) {
                            vm.isFollowRequest = !vm.isFollowRequest;
                        }
                    });
                };

                vm.getCommentType = function (event) {
                    var keyValue = {};
                    angular.forEach(vm.eventTypes, function(item, index) {
                        if (item.Value === event.EventType.toString()) {
                            keyValue = item;
                        }
                    });
                    if (keyValue !== null && keyValue !== undefined) {
                        switch (keyValue.Key) {
                            case eventTypeConstant.CreateRequest:
                                return $translate.instant('CreateRequest_Title');
                            case eventTypeConstant.ApprovedRequestLevel1:
                            case eventTypeConstant.ApprovedRequestLevel2:
                            case eventTypeConstant.ApprovedRequestLevel3:
                            case eventTypeConstant.ApprovedRequestLevel4:
                            case eventTypeConstant.ApprovedRequestLevel5:
                                return $translate.instant('ApprovedRequest_Title');
                            case eventTypeConstant.RejectedRequestLevel1:
                            case eventTypeConstant.RejectedRequestLevel2:
                            case eventTypeConstant.RejectedRequestLevel3:
                            case eventTypeConstant.RejectedRequestLevel4:
                            case eventTypeConstant.RejectedRequestLevel5:
                                return $translate.instant('RejectedRequest_Title');
                            case eventTypeConstant.AssignRequest:
                                return $translate.instant('AssignRequest_Title');
                            case eventTypeConstant.StartProcess:
                                return $translate.instant('StartProcess_Title');
                            case eventTypeConstant.CompletedProcess:
                                return $translate.instant('CompletedProcess_Title');
                            case eventTypeConstant.RejectRequest:
                                return $translate.instant('RejectProcess_Title');
                            case eventTypeConstant.ConfirmResult:
                                return $translate.instant('Comment_ConfirmResutl');
                            case eventTypeConstant.Comment:
                                return $translate.instant('Comment_Title');
                            case eventTypeConstant.Reopen:
                                return $translate.instant('Comment_Reopen');
                            case eventTypeConstant.UpdateProcess:
                                return $translate.instant('UpdateProcess_Title');
                            default:
                                return '';
                        }
                    }
                    return '';
                };
            }],
        controllerAs: "comment"
    };
};

module.exports = comment;