﻿var partyConfigurationDataService = function(pathService, $http) {

    var service = {};
    service.getManageParties = function (data) {
        var url = pathService.getApiPath('/api/partyconfiguration/SearchParty');
        return $http.post(url,data);
    };

    service.SearchDepartment = function (data) {
        var url = pathService.getApiPath('/api/partyconfiguration/SearchDepartment');
        return $http.post(url, data);
    };
    service.SearchGroup = function (data) {
        var url = pathService.getApiPath('/api/partyconfiguration/SearchGroup');
        return $http.post(url, data);
    };
  
    service.getPartyById = function (data) {
        var url = pathService.getApiPath('/api/partyconfiguration/GetPartyById');
        return $http.post(url, data);
    };

    service.saveParty = function (data) {
        var url = pathService.getApiPath('/api/partyconfiguration/SaveParty');
        return $http.post(url, data, {
        });
    };
    
    service.getAllCompanies = function () {
        var url = pathService.getApiPath('/api/partyconfiguration/GetAllCompanies');
        return $http.get(url);
    }

    service.getActiveCompanies = function () {
        var url = pathService.getApiPath('/api/partyconfiguration/getactivecompanies');
        return $http.get(url);
    }

    service.getAllUsers = function () {
        var url = pathService.getApiPath('api/partyconfiguration/getallpartyusers');
        return $http.get(url);
    };

    service.getAllDepartmentUserManagers = function () {
        var url = pathService.getApiPath('api/partyconfiguration/GetAllDepartmentUserManagers');
        return $http.get(url);
    };

    service.getAllDepartmentUserReporters = function () {
        var url = pathService.getApiPath('api/partyconfiguration/GetAllDepartmentUserReporters');
        return $http.get(url);
    };

    service.getDepartments = function (data) {
        var url = pathService.getApiPath('api/partyconfiguration/getDepartments');
        return $http.post(url,data);
    };

    service.getDepartmentByCompany = function (data) {
        var url = pathService.getApiPath('api/partyconfiguration/getdepartmentbycompany');
        return $http.post(url, data);
    };

    service.getGroupByDepartment = function (data) {
        var url = pathService.getApiPath('api/partyconfiguration/getgroupbydepartment');
        return $http.post(url, data);
    };

    service.getAllGroupUserLeaders = function () {
        var url = pathService.getApiPath('api/partyconfiguration/GetAllGroupUserLeaders');
        return $http.get(url);
    };

    service.getAllGroupUserMembers = function () {
        var url = pathService.getApiPath('api/partyconfiguration/GetAllGroupUserMembers');
        return $http.get(url);
    };

    return service;
};

module.exports = partyConfigurationDataService;