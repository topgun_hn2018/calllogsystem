﻿var masterDataService = function (pathService, $http) {

    var service = {};

    service.getKeyObjects = function (data) {
        var url = pathService.getApiPath('/api/masterdata/getlistkeyobjects?tableName=' + data);
        return $http.get(url);
    }

    service.getKeyObject = function (tableName, key) {
        var url = pathService.getApiPath('/api/masterdata/getkeyobject?tableName=' + tableName + '&key=' + key);
        return $http.get(url);
    }
   
    return service;
};

module.exports = masterDataService;