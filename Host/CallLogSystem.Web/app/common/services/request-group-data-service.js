﻿var requestGroupDataService = function (pathService, $http) {

    var service = {};
    service.searchCategories = function (data) {
        var url = pathService.getApiPath('/api/requestgroupmanagement/searchCategories');
        return $http.post(url, data);
    };

    service.getAllCategory = function () {
        var url = pathService.getApiPath('/api/requestgroupmanagement/getallcategory');
        return $http.get(url);
    };

    service.getAllCategoryActive = function () {
        var url = pathService.getApiPath('/api/requestgroupmanagement/GetAllCategoriesActive');
        return $http.get(url);
    };

    service.saveCategory = function (data) {
        var url = pathService.getApiPath('/api/requestgroupmanagement/saveCategory');
        return $http.post(url, data);
    };
    
    service.getCategoryById = function (data) {
        var url = pathService.getApiPath('/api/requestgroupmanagement/getCategoryById');
        return $http.post(url, data);
    };
    
    return service;
};

module.exports = requestGroupDataService;