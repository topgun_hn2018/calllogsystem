﻿var emailService = function (pathService, $http) {

    var service = {};

    service.searchEmailTemplate = function (data) {
        var url = pathService.getApiPath('/api/emailtemplate/searchemailtemplate');
        return $http.post(url, { Name: data.Name, Subject: data.Subject, StatusToId: data.StatusToId, IsActive: data.IsActive });
    }

    service.saveEmailTemplate =function(data) {
        var url = pathService.getApiPath('/api/emailtemplate/saveemailtemplate');
        var response = $http.post(url, { EmailTemplate: data });
        return response;
    }
    service.getEmailTemplateById = function (emailTemplateId) {
        var url = pathService.getApiPath('/api/emailtemplate/getemailtemplatebyid');
        return $http.post(url, { Id: emailTemplateId });
    }
    service.preSendEmail=function(data) {
        var url = pathService.getApiPath('/api/emailtemplate/presendemailtemplate');
        return $http.post(url, { EmailTemplate: data });
    }

    return service;
};

module.exports = emailService;