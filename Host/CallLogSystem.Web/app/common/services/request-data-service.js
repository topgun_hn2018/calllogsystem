﻿var requestDataServiceFactory = function (pathService, $http) {
    var service = {};

    service.getRelatedRequest = function () {
        var url = pathService.getApiPath('api/request/getrelatedrequest');
        return $http.get(url);
    };

    service.createRequest = function (data) {
        var url = pathService.getApiPath('api/request/createrequest');
        return $http.post(url, data);
    };

    service.updateRequest = function (data) {
        var url = pathService.getApiPath('api/request/updaterequest');
        return $http.post(url, data);
    };

    service.search = function (data) {
        var url = pathService.getApiPath('api/request/search');
        return $http.post(url, data);
    };

    service.searchRequest = function (data) {
        var url = pathService.getApiPath('api/request/searchrequest');
        return $http.post(url, data);
    };

    service.searchApprovalRequest = function (data) {
        var url = pathService.getApiPath('api/request/searchapprovalrequest');
        return $http.post(url, data);
    };

    service.getRequestById = function (data) {
        var url = pathService.getApiPath('api/request/getrequestbyid?requestId=' + data);
        return $http.get(url);
    };

    service.getApprovalLevels = function (data) {
        var url = pathService.getApiPath('api/request/getapprovallevels?requestId=' + data);
        return $http.get(url);
    };

    service.deleleRequest = function (data) {
        var url = pathService.getApiPath('api/request/deleteRequest');
        return $http.post(url, data);
    };

    service.createComment = function (data) {
        var url = pathService.getApiPath('api/request/createcomment');
        return $http.post(url, data);
    };

    service.getHistories = function (data) {
        var url = pathService.getApiPath('api/request/gethistories?requestId=' + data);
        return $http.get(url);
    };

    service.approveRequest = function (data) {
        var url = pathService.getApiPath('api/request/approverequest');
        return $http.post(url, data);
    };

    service.getApproval = function (data) {
        var url = pathService.getApiPath('api/request/getapproval?requestId=' + data);
        return $http.get(url);
    };

    service.getRequestAssign = function (data) {
        var url = pathService.getApiPath('api/request/getrequestassign?requestId=' + data);
        return $http.get(url);
    };

    service.searchAssignRequest = function (data) {
        var url = pathService.getApiPath('api/request/searchassignrequests');
        return $http.post(url, data);
    };

    service.getGroupsByRequest = function (data) {
        var url = pathService.getApiPath('api/request/getgroupsbyrequest?requestId=' + data);
        return $http.get(url);
    };

    service.getUsersByRequest = function (requestId, groupId) {
        var url = pathService.getApiPath('api/request/getusersbyrequest?requestId=' + requestId + '&groupId=' + groupId);
        return $http.get(url);
    };

    service.assignRequest = function (data) {
        var url = pathService.getApiPath('api/request/assignrequest');
        return $http.post(url, data);
    };

    service.getProcessRequestData = function (data) {
        var url = pathService.getApiPath('api/request/getprocessrequest');
        return $http.post(url, data);
    };

    service.processRequest = function (data) {
        var url = pathService.getApiPath('api/request/processrequest');
        return $http.post(url, data);
    };

    service.getChildRequest = function (data) {
        var url = pathService.getApiPath('api/request/getchildrequest?requestId=' + data);
        return $http.get(url);
    };

    service.getRequestToProcess = function(data) {
        var url = pathService.getApiPath('api/request/getrequesttoprocess');
        return $http.post(url, data);
    }

    service.followRequest = function (data) {
        var url = pathService.getApiPath('api/request/followRequest');
        return $http.post(url, data);
    };

    service.getProcessInfo = function (data) {
        var url = pathService.getApiPath('api/request/getprocessinfo?requestId=' + data);
        return $http.get(url);
    };

    service.confirmResult = function (data) {
        var url = pathService.getApiPath('api/request/confirmresult');
        return $http.post(url, data);
    };

    service.getConfirmResult = function (data) {
        var url = pathService.getApiPath('api/request/getconfirmresult?requestId=' + data);
        return $http.get(url);
    };

    service.searchRelatedRequest = function (data) {
        var url = pathService.getApiPath('api/request/getrelatedrequest');
        return $http.post(url, data);
    }

    return service;
};

module.exports = requestDataServiceFactory;