﻿var permissionService = function (pathService, $http) {

    var service = {};

    service.getPermission = function (data) {
        var url = pathService.getApiPath('/api/security/getpermission?functionKey=' + data);
        return $http.get(url);
    }

    service.getPermissionAccessing = function () {
        var url = pathService.getApiPath('/api/security/getpermissionaccessing');
        return $http.get(url);
    }
    
    return service;
};

module.exports = permissionService;