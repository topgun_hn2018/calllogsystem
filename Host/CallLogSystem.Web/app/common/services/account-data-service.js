﻿var accountService = function (pathService, $http) {

    var service = {};
    service.getAllUsers = function () {
        var url = pathService.getApiPath('api/accountmanagement/getallusers');
        return $http.get(url);
    };

    service.searchAccount = function (data) {
        var url = pathService.getApiPath('/api/accountmanagement/searchaccount');
        return $http.get(url, { params: data });
    }
    service.getAccountById = function (accountId) {
        var url = pathService.getApiPath('/api/accountmanagement/getaccountbyid');
        return $http.post(url, { Id: accountId });
    }
    service.getAliasById = function (aliasId) {
        var url = pathService.getApiPath('/api/accountmanagement/getaliasbyid');
        return $http.post(url, { Id: aliasId });
    }
    service.saveAccount = function (account) {
        var url = pathService.getApiPath('/api/accountmanagement/SaveAccount');
        var response = $http.post(url, { Account: account });
        return response;
    }
    service.saveAlias = function (alias) {
        var url = pathService.getApiPath('/api/accountmanagement/SaveAlias');
        var response = $http.post(url, { Alias: alias });
        return response;
    }
    service.deleteAlias = function(userId, aliasId)
    {
        var url = pathService.getApiPath('/api/accountmanagement/DeleteAlias');
        var response = $http.post(url, { Id: aliasId, AccountId: userId });
        return response;
    }
    return service;
};

module.exports = accountService;