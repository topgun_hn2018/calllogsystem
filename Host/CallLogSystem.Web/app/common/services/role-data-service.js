﻿var roleService = function (pathService, $http) {

    var service = {};

    service.searchRole = function (data) {
        var url = pathService.getApiPath('/api/rolepermission/searchrole');
        return $http.post(url, { Name: data.Name, IsActive: data.IsActive });
    }
    service.getRoleById = function (roleId) {
        var url = pathService.getApiPath('/api/rolepermission/getrolebyid');
        return $http.post(url, { Id: roleId });
    }
    service.getAllPermission = function () {
        var url = pathService.getApiPath('/api/rolepermission/GetAllPermission');
        return $http.get(url);
    }

    service.getAllRoles = function () {
        var url = pathService.getApiPath('/api/rolepermission/getallroles');
        return $http.get(url);
    }

    service.saveRole = function (role) {
        var url = pathService.getApiPath('/api/rolepermission/SaveRole');
        var response = $http.post(url, { Role: role });
        return response;
    }
    return service;
};

module.exports = roleService;