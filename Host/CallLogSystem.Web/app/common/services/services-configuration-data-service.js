﻿var serviceConfigurationDataService = function(pathService, $http) {

    var service = {};
    service.getAllServices = function () {
        var url = pathService.getApiPath('/api/serviceconfiguration/getallservices');
        return $http.get(url);
    };

    service.saveService = function (data) {
        var url = pathService.getApiPath('/api/serviceconfiguration/saveservice');
        return $http.post(url, data);
    };

    service.searchService = function (data) {
        var url = pathService.getApiPath('/api/serviceconfiguration/searchservice');
        return $http.post(url, data);
    };

    service.getServiceById = function (data) {
        var url = pathService.getApiPath('/api/serviceconfiguration/getservicebyid?serviceId=' + data);
        return $http.get(url);
    };
    return service;
};

module.exports = serviceConfigurationDataService;