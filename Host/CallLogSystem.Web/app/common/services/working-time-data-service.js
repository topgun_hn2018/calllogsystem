﻿var workingTimeDataService = function(pathService, $http) {

    var service = {};

    service.getWorkingTime = function (data) {
        var url = pathService.getApiPath('/api/workingTime/getWorkingTime');
        return $http.post(url, data);
    };

    service.getHoliday = function (data) {
        var url = pathService.getApiPath('/api/workingTime/getHoliday');
        return $http.post(url, data);
    };

    service.saveWorkingTime = function (data) {
        var url = pathService.getApiPath('/api/workingTime/saveWorkingTime');
        return $http.post(url, data);
    };

    service.saveHoliday = function (data) {
        var url = pathService.getApiPath('/api/workingTime/saveHoliday');
        return $http.post(url, data);
    };
 
    return service;
};

module.exports = workingTimeDataService;