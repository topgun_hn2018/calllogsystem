﻿var reportDataService = function (pathService, $http) {

    var service = {};

    service.GetCompanyByUser = function (data) {
        var url = pathService.getApiPath('/api/report/GetCompanyByUser');
        return $http.post(url, data);
    };

    service.GetDepartmentByCompany = function (data) {
        var url = pathService.getApiPath('/api/report/GetDepartmentByCompany');
        return $http.post(url, data);
    };

    service.GetGroupByDepartment = function (data) {
        var url = pathService.getApiPath('/api/report/GetGroupByDepartment');
        return $http.post(url, data);
    };

    service.GetReport = function(data) {
        var url = pathService.getApiPath('/api/report/GetReport');
        return $http.post(url, data);
    }

    service.GetReportAssignee = function (data) {
        var url = pathService.getApiPath('/api/report/GetReportAssignee');
        return $http.post(url, data);
    }

    service.GetUserReport = function(data) {
        var url = pathService.getApiPath('/api/report/GetUserReport');
        return $http.post(url, data);
    }

    return service;
};

module.exports = reportDataService;