﻿var requestConfigurationDataService = function(pathService, $http) {

    var service = {};
    service.getAllRequestTypes = function () {
        var url = pathService.getApiPath('/api/requestconfiguration/getallrequesttypes');
        return $http.get(url);
    };
    service.searchRequestType = function (data) {
        var url = pathService.getApiPath('/api/requestconfiguration/searchrequesttype');
        return $http.post(url,data);
    };
   
    service.getRequestTypeById = function (data) {
        var url = pathService.getApiPath('/api/requestconfiguration/getRequestTypeById');
        return $http.post(url, data);
    };

    service.saveRequestType = function (data) {
        var url = pathService.getApiPath('/api/requestconfiguration/saveRequestType');
        return $http.post(url, data);
    };
    
    service.getRequestStatus = function () {
        var url = pathService.getApiPath('/api/requestconfiguration/getrequeststatus');
        return $http.get(url);
    }
    return service;
};

module.exports = requestConfigurationDataService;