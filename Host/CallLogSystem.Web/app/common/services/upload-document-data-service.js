﻿var uploadDataFactory = function(pathService, $http) {

    var service = {};
    service.uploadDocument = function (data) {
        var url = pathService.getApiPath('/api/uploadDocument/upload');
        return $http.post(url, data ,{headers: {'Content-Type': undefined}
        });
    };
    service.uploadCategory = function (data) {
        var url = pathService.getApiPath('/api/uploadDocument/uploadcate');
        return $http.post(url, data ,{headers: {'Content-Type': undefined}
        });
    };
    service.deleteDocument = function (data) {
        var url = pathService.getApiPath('/api/uploadDocument/delete');
        return $http.get(url, { params: { FileName: data.FileName, UploadedDate: data.UploadedDate } });
    };
    service.downloadDocument = function (data) {
        var url = pathService.getApiPath('/api/uploadDocument/get');
        return $http({
            method: 'GET',
            cache: false,
            params: data,
            url: url,
            responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'application/octet-stream'
            }
        });
    };
    return service;
};

module.exports = uploadDataFactory;