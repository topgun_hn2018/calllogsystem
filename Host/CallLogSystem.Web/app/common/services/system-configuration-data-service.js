﻿var sysConfigurationService = function (pathService, $http) {

    var service = {};

    service.getSystemConfiguration = function () {
        var url = pathService.getApiPath('/api/sysconfiguration/getsystemconfiguration');
        return $http.get(url);
    }
    
    service.saveSystemConfiguration = function (sysparams) {
        var url = pathService.getApiPath('/api/sysconfiguration/savesystemconfiguration');
        var response = $http.post(url, { SystemParameters: sysparams });
        return response;
    }
    return service;
};

module.exports = sysConfigurationService;