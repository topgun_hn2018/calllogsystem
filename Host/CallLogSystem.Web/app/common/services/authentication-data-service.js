﻿var authenticationDataFactory = function (pathService, $http) {
    var service = {};

    service.login = function (data) {
        var url = pathService.getApiPath('api/authentication/logon');
        return $http.post(url, data);
    };

    service.switchSite = function (data) {
        var url = pathService.getApiPath('api/authentication/switchsite');
        return $http.post(url, data);
    };
    return service;
};

module.exports = authenticationDataFactory;