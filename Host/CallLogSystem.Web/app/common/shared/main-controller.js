﻿'use strict';

var ctrl = function ($rootScope, $window, $scope, $filter, $location, $translate, $timeout, $http, cookieService, jwtService, uiDatetimePickerConfig, authenticationDataService, sysConfigurationDataService, securityDataService, pathService, masterDataService, globalConfig) {
    var vm = this;
    vm.initiation = true;
    vm.showMenu = false;
    //Page size
    $scope.pageSize = parseInt(globalConfig.pageSize);

    //Loading indicator
    vm.loader = $('body').loadingIndicator({
        useImage: false
    }).data("loadingIndicator");
    $scope.isLoading = function() {
        return $http.pendingRequests.length > 0;
    };
    $scope.$watch($scope.isLoading, function(loading) {
        if (loading) {
            vm.loader.show();
            var method = $http.pendingRequests[0].url.split('/api')[1];
            if (method !== '/uploadDocument/upload') {
                window.scroll(0, 0);
            }
        } else {
            $timeout(function() {
                vm.loader.hide();
            }, 5);
        }
        $('.nav-sm ul .nav.child_menu').css('display', 'none');
        $('.nav-sm #sidebar-menu ul li').removeClass('active');
    });

    //Config date picker for working time
    uiDatetimePickerConfig.enableTime = false;
    uiDatetimePickerConfig.date = new Date().toISOString();
    $scope.minDate = new Date(new Date().getFullYear(), 0, 0);
    $scope.maxDate = new Date(new Date().getFullYear(), 0, 0);

    //Language
    $scope.languageKeys = [];
    var languageKeyCookie = localStorage.LanguageKey;
    if (languageKeyCookie) {
        vm.defaultLanguageKeys = languageKeyCookie;
    } else {
        vm.defaultLanguageKeys = 'vi-VN';
    }
    $translate.use(vm.defaultLanguageKeys);
    if (globalConfig.LanguageKeys) {
        var languageKeys = globalConfig.LanguageKeys.split('|');
        for (var j = 0; j < languageKeys.length; j++) {
            var language = languageKeys[j].split(':');
            if (language.length === 2) {
                $scope.languageKeys.push({
                    Key: language[0],
                    Name: language[1]
                });
            }
        }
    } else {
        $scope.languageKeys.push({
            Key: 'vi-VN',
            Name: 'Tiếng Việt'
        });
    }

    //Max length
    $scope.maxLength = parseInt(globalConfig.maxLength);
    $scope.maxSize = (parseInt(globalConfig.maxLength) / (1024 * 1024)).toFixed(2);
    $scope.tinymceOptions = {
        setup: function (e) {
            e.on('change', function () {
                $('.mce-panel').removeClass('parsley-error');
                $('#parsley-id-13').remove();
            });
        },
        theme: 'modern',
        entity_encoding : "raw",
        height: "250",
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        //toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        //toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        paste_retain_style_properties: "all",
        image_advtab: true,
        menubar: false,
        statusbar: false,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    };

    $scope.tinymceOptionsSetup = {
        setup: function (e) {
            e.on('change', function () {
                $('.mce-panel').removeClass('parsley-error');
                $('#parsley-id-13').remove();
            });
        },
        height: "250",
        theme: 'modern',
        entity_encoding: "raw",
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        paste_retain_style_properties: "all",
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',

        image_advtab: true,        
        statusbar: false,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    };

    //Check token
    vm.checkToken = function (nextPath) {
        if (nextPath !== '/download') {
            vm.isAuthed = jwtService.isAuthed();
            var accessToken = cookieService.getCookie("accessToken");
            if ((!accessToken && vm.isAuthed !== 'AC') || vm.isAuthed === 'IN') {
                vm.showMenu = false;
                jwtService.authenticate();
                return false;
            } else {
                vm.sites = [];
                vm.userProfile = JSON.parse(cookieService.getCookie('UserProfile'));
                $scope.fullName = vm.userProfile.FullName;
                for (var i = 0; i < vm.userProfile.UserInfo.length; i++) {
                    var site = {
                        CompanyId: vm.userProfile.UserInfo[i].CompanyId,
                        SiteName: vm.userProfile.UserInfo[i].CompanyName
                    };
                    vm.sites.push(site);
                }
                vm.defaultSite = ($filter('filter')(vm.userProfile.UserInfo, { IsDefault: true })[0].CompanyId).toString();
                $scope.defaultSite = vm.defaultSite;
                if ($scope.nextUrl != null && $scope.nextPath !== '/login' && $scope.nextPath !== '' &&
                    $scope.nextPath !== '/sessiontimeout' && $scope.nextPath !== '/error') {
                    location.href = $scope.nextUrl;
                    $scope.nextUrl = null;
                }
                //Get permission accessing
                securityDataService.getPermissionAccessing().success(function(response) {
                    $scope.permissionAccessing = response.PermissionAccessing;
                });

                //Config date time format
                sysConfigurationDataService.getSystemConfiguration().success(function(response) {
                    vm.systemParameters = response.SystemParameters;
                    vm.dateTimeFormat = $filter('filter')(vm.systemParameters, { Key: 'DATETIME_FORMAT' })[0].Value.toLowerCase().split(' ');
                    vm.is24hFormat = $filter('filter')(vm.systemParameters, {
                        Key: '24H_FORMAT'
                    })[0].Value;
                    $scope.timeFormat = vm.is24hFormat === 'true' ? vm.dateTimeFormat[1].replace('hh', 'HH') : vm.dateTimeFormat[1];
                    $scope.dateFormat = vm.dateTimeFormat[0].replace('mm', 'MM');
                    $scope.dateTimeFormat = $scope.dateFormat + ' ' + $scope.timeFormat;
                    $scope.dateTimeMomment = $scope.dateFormat.toUpperCase() + ' ' + $scope.timeFormat;
                    uiDatetimePickerConfig.dateFormat = vm.dateTimeFormat[0].replace('mm', 'MM');
                });

                masterDataService.getKeyObjects('RequestStatus').success(function(response) {
                    $scope.keyObjects = response.KeyObjects;
                });
                $("body").css("background", "none");
                
                vm.showMenu = true;
            }
        }
        return true;
    };
    $scope.nextUrl = window.location.href;
    var nextPath = window.location.href.split('#');
    if (nextPath.length > 1) {
        $scope.nextPath = nextPath[1].split('?')[0];
    } else {
        $scope.nextPath = '';
    }
    vm.checkToken($scope.nextPath);

    //Handle route change
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        var isAuthed = vm.checkToken($scope.nextPath);
        if (!isAuthed) {
            vm.showMenu = false;
            $location.url('/login');
        }
        if (next && !isAuthed) {
            if (next.originalPath) {
                var nextPath = next.originalPath;
                if (nextPath !== '/login' && nextPath !== '/download') {
                    vm.checkToken(nextPath);
                } else {
                    vm.showMenu = false;
                }
            } 
        }
    });
    $rootScope.$on('$routeChangeSuccess', function(event, next, current) {
        if (next && next.originalPath) {
            var nextPath = next.originalPath.split('/')[1];

            //If exist next path
            if ($('.side-menu li a[href="#' + nextPath + '"]').length > 0) {
                // Select item
                $('.side-menu li a').each(function(i, object) {
                    if (object.href.split('#')[1] === nextPath) {
                        if ($(this).parent().parent().is('.child_menu')) {
                            $(this).parent().parent().parent().addClass('active');
                            $(this).parent().parent().slideDown();
                        } else {
                            $('#sidebar-menu').find('li ul').slideUp();
                        }
                        $(this).parent().addClass('active');
                    } else {
                        $(this).parent().removeClass('active');
                    }
                });
            }
        }
    });


    vm.logout = function () {
        $('.menu_section ul').find('li.active').removeClass('active');
        $('#sidebar-menu').find('li ul').slideUp();
        vm.showMenu = false;

        //reset authentication
        vm.isAuthed = false;
        cookieService.clearCookie();
    };

    vm.switchSite = function () {
        authenticationDataService.switchSite({ NewCompanyId: vm.defaultSite }).success(function (response) {
            if (response.ErrorType === 0) {
                cookieService.createObjectCookie('UserProfile', response.UserProfile);
                if (window.location.hash === '#/home') {
                    location.reload();
                } else {
                    $location.url('/home');
                }

            }
        });
    };

    vm.switchLanguage = function () {
        localStorage.setItem('LanguageKey', vm.defaultLanguageKeys);
        $translate.use(vm.defaultLanguageKeys);
    };

    $scope.filter = function (source, value) {
        var result = "";
        angular.forEach(source, function (item, key) {
            if (item.Key === value) {
                result = item.Value;
            }
        });
        return parseInt(result);
    };

    vm.initiation = false;
};

module.exports = ctrl;