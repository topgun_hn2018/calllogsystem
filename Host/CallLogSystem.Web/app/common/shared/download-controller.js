﻿'use strict';

var ctrl = function ($routeParams, FileSaver, uploadDataService) {
    var vm = this;
    var createdDate = $routeParams.createdDate;
    var fileNameServer = $routeParams.filemameserver;
    var fileName = $routeParams.filename;
    var fileType = $routeParams.fileType;
    var extension = $routeParams.extension;
    if (fileType && fileName && createdDate && extension && fileNameServer) {
        uploadDataService.downloadDocument({ FileName: fileNameServer, UploadedDate: createdDate, fileType: fileType })
            .success(function(response) {
                var file = new Blob([response], { type: fileType });
                FileSaver.saveAs(file, fileName + '.' + extension);
            });
    }

};

module.exports = ctrl;