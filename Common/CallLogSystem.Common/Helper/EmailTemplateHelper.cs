﻿namespace CallLogSystem.Common.Helper
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using Constant;
    using Data.DataAccess;
    using log4net;
    using Model;
    using NVelocity;
    using NVelocity.App;

    public class EmailTemplateHelper
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public EmailTemplateHelper()
        {
            Velocity.Init();
        }

        /// <summary>
        /// Replaces the token.
        /// </summary>
        /// <param name="emailTemplate">The email template.</param>
        /// <param name="emailTokenValues">The email token values.</param>
        /// <param name="systemConfig">The system configuration.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public bool ReplaceToken(ref string emailTemplate, EmailTokenValues emailTokenValues, List<SystemParameter> systemConfig, string name)
        {
            try
            {
                string dateTimeFormat = this.GetSystemConfigValue(systemConfig);
                //remove html tag inside the token
                this.GetValidTemplate(ref emailTemplate);

                var velocityContext = new VelocityContext();
                var sb = new StringBuilder();

                foreach (var token in EmailTokenConstant.Tokens)
                {
                    object value = this.GetValue(emailTokenValues, dateTimeFormat, token) ?? string.Empty;
                    velocityContext.Put(token, value);
                }

                Velocity.Evaluate(
                    velocityContext,
                    new StringWriter(sb),
                    name,
                    new StringReader(emailTemplate));
                emailTemplate = sb.ToString();
                return true;
            }
            catch (ArgumentNullException ex)
            {
                this._log.ErrorFormat("Exception when replace token key: {0}", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Gets the valid template.
        /// </summary>
        /// <param name="emailTemplate">The template.</param>
        /// <returns></returns>
        private void GetValidTemplate(ref string emailTemplate)
        {
            MatchCollection coll = Regex.Matches(emailTemplate, @"\$<(.*?)>");
            //Replace html tags inside tokens
            for (int i = 0; i < coll.Count; i++)
            {
                string validToken = string.Concat(coll[i].Value.Replace("$", string.Empty), "$");
                emailTemplate = emailTemplate.Replace(coll[i].Value, validToken);
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="emailTokenValues">The email token values.</param>
        /// <param name="dateTimeFormat">The date time format.</param>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        private object GetValue(EmailTokenValues emailTokenValues, string dateTimeFormat, string token)
        {
            PropertyInfo prop = emailTokenValues.GetType().GetProperty(token);
            if (prop == null)
            {
                return string.Empty;
            }

            if (prop.PropertyType == typeof(DateTime))
            {
                DateTime value = (DateTime)prop.GetValue(emailTokenValues, null);
                return value.ToString(dateTimeFormat);
            }
            return prop.GetValue(emailTokenValues, null);
        }

        /// <summary>
        /// Gets the system configuration value.
        /// </summary>
        /// <param name="systemConfig">The system configuration.</param>
        private string GetSystemConfigValue(List<SystemParameter> systemConfig)
        {
            string dateTimeFormatvalue = string.Empty;
            SystemParameter dateTimeFormat = systemConfig.FirstOrDefault(s => s.Key == SystemParameterConstant.DateTimeFormat);
            var timeFormat = systemConfig.FirstOrDefault(s => s.Key == SystemParameterConstant.Format24H);
            if (dateTimeFormat != null && timeFormat != null)
            {
                var regex = new Regex(Regex.Escape("mm"));
                string datetimeFormatString = regex.Replace(dateTimeFormat.Value.ToLower(), "MM", 1);
                dateTimeFormatvalue = timeFormat.Value == "true" ? datetimeFormatString.Replace("hh", "HH") : datetimeFormatString;
            }
            return dateTimeFormatvalue;
        }

    }
}