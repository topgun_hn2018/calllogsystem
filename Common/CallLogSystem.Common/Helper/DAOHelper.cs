﻿namespace CallLogSystem.Common.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class DAOHelper
    {
        /// <summary>
        /// Lists the identifier wraper.
        /// </summary>
        /// <param name="listIds">The list ids.</param>
        /// <returns></returns>
        public static DataTable ListIdWraper(IEnumerable<int> listIds)
        {
            var listIdTable = new DataTable("ListIdTable");
            listIdTable.Columns.Add("Id", Type.GetType("System.Int32"));
            foreach (var id in listIds)
            {
                if (id > -1)
                {
                    var dataRow = listIdTable.NewRow();
                    dataRow[0] = id;
                    listIdTable.Rows.Add(dataRow);
                }
                else
                {
                    return null;
                }
            }
            return listIdTable;
        }

    }
}
