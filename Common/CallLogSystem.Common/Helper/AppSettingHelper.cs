﻿namespace CallLogSystem.Common.Helper
{
    using System;
    using Constant;
    using Utilites;

    /// <summary>
    /// App Setting Helper
    /// </summary>
    public static class AppSettingHelper
    {
        public static string GetDocumentPath(DateTime? updatedDate = null)
        {
            var dateTime = updatedDate ?? DateTime.Now;
            var path = ConfigUtilities.GetStringKey(ConfigurationConstant.DocumentPath, "");
            return string.Format("{0}/{1}/{2}/{3}", path,
                dateTime.Year, dateTime.Month, dateTime.Day);
        }
    }
}