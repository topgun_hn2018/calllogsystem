﻿namespace CallLogSystem.Common.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;
    using System.Reflection;
    using System.Text;
    using Constant;
    using ContractBase;
    using Enum;
    using log4net;
    using Utilites;

    /// <summary>
    /// Using to send email
    /// Author: Lam Tran
    /// Date: 1/Nov/2016
    /// </summary>
    public static class Emailer
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly bool UsingDefaultReceiverEmail;
        private static readonly string DefaultReceiverEmail;
        private const string ForwardEmailHeaderFormat = "<i>Email này sẽ được gửi cho: {0}</i><br /><br />";
        private static readonly string SmtpEmailAccount = ConfigUtilities.GetStringKey(ConfigurationConstant.SmtpEmailAccount, "");
        //private static readonly string SmtpSentFrom = ConfigUtilities.GetStringKey(ConfigurationConstant.SmtpSentFrom, "");
        private static readonly string SmtpPassword = ConfigUtilities.GetStringKey(ConfigurationConstant.SmtpPassword, "");
        private static readonly string SmtpServer = ConfigUtilities.GetStringKey(ConfigurationConstant.SmtpServer, "");
        private static readonly int SmtpPort = ConfigUtilities.GetIntKey(ConfigurationConstant.SmtpPort, 25);
        private static readonly bool SmtpSsl = ConfigUtilities.GetBoolKey(ConfigurationConstant.SmtpSSL, false);
        static Emailer()
        {
            UsingDefaultReceiverEmail = ConfigUtilities.GetBoolKey(ConfigurationConstant.UsingDefaultReceiverEmail, false);
            DefaultReceiverEmail = ConfigUtilities.GetStringKey(ConfigurationConstant.DefaultReceiverEmail, null);

            if (UsingDefaultReceiverEmail && !ValidateEmailAddress(DefaultReceiverEmail))
                UsingDefaultReceiverEmail = false;
        }

        /// <summary>
        /// Send an email without attach files
        /// </summary>
        /// <param name="fromAddress">Email address of sender</param>
        /// <param name="toAddress">Email address of receivers</param>
        /// <param name="ccAddress"></param>
        /// <param name="subject">subject of the email</param>
        /// <param name="message">content of the email</param>        
        /// <returns>True: if send email successful, false: if send email fail</returns>
        public static BaseResponse SendEmail(string fromAddress, IEnumerable<string> toAddress,IEnumerable<string> ccAddress , string subject, string message)
        {
            return SendEmailNow(fromAddress, toAddress,ccAddress,subject, message);
        }

        /// <summary>
        /// Send and email with attach files
        /// </summary>
        /// <param name="fromAddress">Email address of sender</param>
        /// <param name="toAddress">Email address of receivers</param>
        /// <param name="ccAddress"></param>
        /// <param name="subject">subject of the email</param>
        /// <param name="message">content of the email</param>
        /// <param name="attachFiles">Attach files</param>
        /// <returns>True: if send email successful, false: if send email fail</returns>
        public static BaseResponse SendEmailWithAttachments(string fromAddress, IEnumerable<string> toAddress, IEnumerable<string> ccAddress, string subject, string message
                                                 , params string[] attachFiles)
        {
            return SendEmailNow(fromAddress, toAddress,ccAddress, subject, message, attachFiles);
        }

        /// <summary>
        /// Send an email to receivers with attach files if needed
        /// </summary>
        /// <param name="fromAddress">Email address of sender</param>
        /// <param name="toAddress">Email address of receivers</param>
        /// <param name="ccAddress"></param>
        /// <param name="subject">subject of the email</param>
        /// <param name="message">content of the email</param>
        /// <param name="attachFiles">Attach files</param>
        /// <returns>True: if send email successful, false: if send email fail</returns>
        private static BaseResponse SendEmailNow(string fromAddress, IEnumerable<string> toAddress, IEnumerable<string> ccAddress, string subject, string message
                                             , params string[] attachFiles)
        {
            var feedback = new BaseResponse();
            return feedback;
            //Config for using default receiver email


            var toEmailAddress = new List<string>();//Config for using default receiver email
            var ccEmailAddress = new List<string>();//Config for using default receiver email
            if (!UsingDefaultReceiverEmail)
            {
                toEmailAddress.AddRange(toAddress);
                ccEmailAddress.AddRange(ccAddress);
            }
            else
            {
                toEmailAddress.Add(DefaultReceiverEmail);
                message = string.Format(ForwardEmailHeaderFormat, ConvertSeriesAddress(toAddress) + ',' + ConvertSeriesAddress(ccAddress)) + message;
            }
            // Validate sender and receiver email addresses););
            if (!ValidateEmailAddress(fromAddress))
            {
                feedback.ErrorType = ErrorType.Error;
                feedback.ErrorDescription = "UC12-ERROR-02";
                return feedback;
            }

            if (!ValidateEmailAddress(toEmailAddress))
            {
                feedback.ErrorType = ErrorType.Error;
                feedback.ErrorDescription = "UC12-ERROR-01";
                return feedback;
            }

            using (var mailMessage = new MailMessage())
            {
                // Credentials are necessary if the server requires the client 
                // to authenticate before it will send e-mail on the client's behalf.
                // smtp.UseDefaultCredentials = true;
                // smtp.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                // Check we have email addresses
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(SmtpEmailAccount, SmtpPassword),
                    Host = SmtpServer,
                    Port = SmtpPort,
                    EnableSsl = SmtpSsl
                };

                // Build Email
                mailMessage.IsBodyHtml = true;
                mailMessage.From = new MailAddress(fromAddress);
                foreach (var addReceiver in toEmailAddress)
                {
                    mailMessage.To.Add(addReceiver);
                }
                foreach (var ccReceiver in ccEmailAddress)
                {
                    mailMessage.CC.Add(ccReceiver);
                }
                mailMessage.Body = message;
                mailMessage.Subject = subject;

                // Add Attachments
                foreach (var attachFile in attachFiles)
                {
                    var attachment = new Attachment(attachFile);
                    mailMessage.Attachments.Add(attachment);
                }

                try
                {
                    smtpClient.Send(mailMessage);
                }
                catch (SmtpException ex)
                {
                    Log.ErrorFormat("Actor:{0} \n {1}", "Emailer", ex.Message);
                    feedback.ErrorType = ErrorType.Error;
                    feedback.ErrorDescription = ex.ToString();
                }
                return feedback;
            }  
            
        }
        /// <summary>
        /// Validate and email address
        /// It must be follow these rules:
        /// Has only one @ character
        /// Has at least 3 chars after the @
        /// Domain portion contains at least one dot
        /// Dot can't be before or immediately after the @ character
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>True: If valid, False: If not</returns>
        private static bool ValidateEmailAddress(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                return false;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(emailAddress, "^[-A-Za-z0-9_@.]+$"))
            {
                return false;
            }

            // Search for the @ char
            int i = emailAddress.IndexOf("@", StringComparison.Ordinal);

            // There must be at least 3 chars after the @
            if (i <= 0 || i >= emailAddress.Length - 3)
            {
                return false;
            }

            // Ensure there is only one @
            if (emailAddress.IndexOf("@", i + 1, StringComparison.Ordinal) > 0)
            {
                return false;
            }
                

            // Check the domain portion contains at least one dot
            int j = emailAddress.LastIndexOf(".", StringComparison.Ordinal);

            // It can't be before or immediately after the @ character
            if (j < 0 || j <= i + 1)
            {
                return false;
            }

            // EmailAddress is validated
            return true;

        }

        /// <summary>
        /// Validate list email address
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns>true: If all of email address are valid, false: If one of email address is invalid</returns>
        private static bool ValidateEmailAddress(IEnumerable<string> emailAddress)
        {
            foreach (var email in emailAddress)
            {
                // ReShaper suggest declare variable in here
                bool retValue = ValidateEmailAddress(email);
                if (!retValue)
                {
                    return false;
                }
                    
            }
            return true;
        }
        private static string ConvertSeriesAddress(IEnumerable<string> address)
        {
            if (address == null) return null;

            var sb = new StringBuilder();
            foreach (var addr in address)
            {
                sb.Append(addr + ", ");
            }

            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - 2, 2);
            }

            return sb.ToString();
        }
    }
}