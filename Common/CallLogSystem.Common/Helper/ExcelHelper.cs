﻿namespace CallLogSystem.Common.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Reflection;
    using System.Web.Hosting;
    using log4net;
    using OfficeOpenXml;

    public static class ExcelHelper
    {
        private const string TemplateName = "/template.xlsx";

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Export data to DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this List<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type columnType = pi.PropertyType;
                if ((columnType.IsGenericType))
                {
                    columnType = columnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, columnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        /// Copy file
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        private static string CopyFile(string source, string dest)
        {
            var result = string.Empty;
            try
            {
                // Overwrites existing files
                File.Copy(source, dest, true);
            }
            catch (IOException ex)
            {
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Auto gen name of excel
        /// </summary>
        /// <returns></returns>
        private static string NameOfExcel()
        {
            var datetime = DateTime.Now;
            return datetime.ToString("yyyyMMddHHmmss");
        }
        
        
        /// <summary>
        /// Export to Excel EPP
        /// </summary>
        /// <param name="tbl"></param>
        /// <returns></returns>
        public static string ExportToExcelEpp(DataTable tbl)
        {
            string name = "report" + NameOfExcel();
            string newFileName = AppSettingHelper.GetDocumentPath(DateTime.Now) + '/' + name + ".xlsx";
            string fileName = "fileName";
            string templateFileName = HostingEnvironment.MapPath(@"~/App_Data/" + TemplateName);
            if (!Directory.Exists(AppSettingHelper.GetDocumentPath(DateTime.Now)))
            {
                Directory.CreateDirectory(AppSettingHelper.GetDocumentPath(DateTime.Now));
            }

            if (CopyFile(templateFileName, newFileName) == string.Empty)
            {
                fileName = newFileName;
            }
            if (templateFileName != null)
            {
                var templateFile = new FileInfo(templateFileName);
                using (ExcelPackage excelPackage = new ExcelPackage(templateFile))
                {
                    //Create the worksheet
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets["Report"];

                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells[2,1,tbl.Rows.Count+1,61].LoadFromDataTable(tbl, false);
                
                    var newFile = new FileInfo(fileName);
               
                    excelPackage.SaveAs(newFile);
                    return name;
                }
            }
            return string.Empty;
        }
    }
}