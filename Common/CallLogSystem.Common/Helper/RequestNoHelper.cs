﻿namespace CallLogSystem.Common.Helper
{
    using System;

    public static class RequestNumberHelper
    {
        /// <summary>
        /// Generates the request no.
        /// </summary>
        /// <param name="companyCode">The company code.</param>
        /// <param name="currentRequestNo">The current request no.</param>
        /// <returns></returns>
        public static string GenerateRequestNo(string companyCode, string currentRequestNo)
        {
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month > 9 ? DateTime.Now.Month.ToString() : "0" + DateTime.Now.Month.ToString();
            if (string.IsNullOrEmpty(currentRequestNo) || string.IsNullOrWhiteSpace(currentRequestNo))
            {
                return string.Format("{0}{1}{2}{3}", companyCode,
                    year.Substring(year.Length - 2), month, "0001");
            }

            int currentRequestNumber = int.Parse(currentRequestNo.Substring(currentRequestNo.Length - 4));
            int requestNumber = CompareDate(currentRequestNo) ? currentRequestNumber + 1 : 0;
            string requetNo = string.Format("{0}{1}{2}{3}", companyCode,
                year.Substring(year.Length - 2), month, GetRequestNumber(requestNumber));
            return requetNo;
        }

        /// <summary>
        /// Gets the request number.
        /// </summary>
        /// <param name="requestNumber">The request number.</param>
        /// <returns></returns>
        private static string GetRequestNumber(int requestNumber)
        {
            if (requestNumber == 0)
            {
                requestNumber++;
            }
            var requestNo = requestNumber.ToString();
            string prefix = "";
            switch (requestNo.Length)
            {
                case 1:
                    prefix = "000";
                    break;
                case 2:
                    prefix = "00";
                    break;
                case 3:
                    prefix = "0";
                    break;
                case 4:
                    prefix = "";
                    break;

            }
            return string.Format("{0}{1}", prefix, requestNo);
        }

        /// <summary>
        /// Compares the date.
        /// </summary>
        /// <param name="currenRequestNo">The curren request no.</param>
        /// <returns></returns>
        private static bool CompareDate(string currenRequestNo)
        {
            int requestMonth = int.Parse(currenRequestNo.Substring(currenRequestNo.Length - 6, 2));
            int currentMonth = DateTime.Now.Month;
            if (requestMonth == currentMonth)
            {
                return true;
            }
            return false;
        }
    }
}