﻿namespace CallLogSystem.Common.Helper
{
    using System.Security.Cryptography;
    using System.Text;

    public static class MD5CryptoService
    {
        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public static string HashPassword(string password)
        {
            using (MD5 md5 = new MD5CryptoServiceProvider())
            {
                //compute hash from the bytes of text
                md5.ComputeHash(Encoding.ASCII.GetBytes(password));

                //get hash result after compute it
                byte[] result = md5.Hash;

                StringBuilder strBuilder = new StringBuilder();
                foreach (byte t in result)
                {
                    //change it into 2 hexadecimal digits
                    //for each byte
                    strBuilder.Append(t.ToString("x2"));
                }
                return strBuilder.ToString();
            }
        }
    }
}