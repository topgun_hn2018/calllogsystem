﻿namespace CallLogSystem.Common.Singleton
{
    using System.Text;

    public static class EncryptionHelper
    {
        private const string SymatricKeyClearTextValue =
            "VGhlZml0bmVzc2ZpcnN0c3l0ZW1zZWNyZXRrZXlnZW5lcmF0ZW9uNXBtMDZtMTIwNDIwMTVhbmR0aGlzc2hvdWxkdXNlZm9yZ2VuZXJhdGV0aGVqYXNvbndlYnRva2Vu";

        public static byte[] SymatricKey
        {
            get { return Encoding.UTF8.GetBytes(SymatricKeyClearTextValue); }
        }
    }
}