﻿namespace CallLogSystem.Common.Singleton
{
    using System.IdentityModel.Tokens;
    using Constant;
    using Utilites;

    public static class JwtValidationParameters
    {
        /// <summary>
        /// Token Parameter
        /// </summary>
        public static TokenValidationParameters Parameters
        {
            get
            {
                return new TokenValidationParameters
                {
                    ValidIssuer = ConfigUtilities.GetStringKey(ConfigurationConstant.WebApiUrl, ""),
                    ValidAudience = ConfigUtilities.GetStringKey(ConfigurationConstant.WebtUrl, ""),
                    IssuerSigningKey = new InMemorySymmetricSecurityKey(EncryptionHelper.SymatricKey)
                }; 
            }
        }
    }
}