﻿namespace CallLogSystem.Common.Enum
{
    /// <summary>
    /// Validation Token Result
    /// </summary>
    public enum ValidationTokenResult
    {
        Valid,
        Expired,
        InvalidSignature,
        Invalid
    }
}