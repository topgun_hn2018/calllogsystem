﻿namespace CallLogSystem.Common.Enum
{
    /// <summary>
    /// Error Type
    /// </summary>
    public enum PartyType
    {
        All ,
        Com ,
        Dep ,
        Grp
    }
}