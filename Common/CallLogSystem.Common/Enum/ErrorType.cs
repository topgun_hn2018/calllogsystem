﻿namespace CallLogSystem.Common.Enum
{
    /// <summary>
    /// Error Type
    /// </summary>
    public enum ErrorType
    {
        NoError = 0,
        Error = 1,
        NotFound = 2,
        Warning = 3,
        InvalidType = 4,
        InvalidRequest = 5
    }
}