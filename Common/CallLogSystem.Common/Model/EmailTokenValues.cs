﻿namespace CallLogSystem.Common.Model
{
    using System;
    using System.Collections.Generic;

    public class EmailTokenValues
    {
        public string RequestNo { get; set; }
        public string RequestType { get; set; }
        public string RequestName { get; set; }
        public string RequestStatus { get; set; }
        public string ParentsRequest { get; set; }
        public string ChildrenRequests { get; set; }
        public string RequestDescription { get; set; }
        public DateTime ExpectedCompleteDate { get; set; }
        public string RequestNote { get; set; }
        public string RequesterName { get; set; }
        public DateTime RequestCreatedDate { get; set; }
        public string ApprovingApprover { get; set; }
        public DateTime ApprovingCreatedDate { get; set; }
        public string ApprovingStatus { get; set; }
        public string ApprovingNote { get; set; }
        public string Assigner { get; set; }
        public DateTime AssignTime { get; set; }
        public string AssignNote { get; set; }
        public string Assignee { get; set; }
        public string ProcessStatus { get; set; }
        public DateTime ProcessStartTime { get; set; }
        public DateTime ProcessEndTime { get; set; }
        public string ProcessDescription { get; set; } 
        public int Rating { get; set; }
        public string CommentContent { get; set; }
        public string CommentOwner { get; set; }
        public DateTime CommentCreatedDate { get; set; }
        public string AttachFileUrl { get; set; }
        public string FollowRequestUrl { get; set; }
        public string ApproveRequestUrl { get; set; }
        public string AssignRequestUrl { get; set; }
        public string ProcessRequestUrl { get; set; }
        public string RatingRequestUrl { get; set; } 
        public string CurrentOwner { get; set; }
        public string RequesterCompany{ get; set; }
        public List<string> RequesterDepartmentList{ get; set; }
        public string RequesterDepartment{ get; set; }
        public List<string> RequesterGroupList { get; set; } 
        public string RequesterGroup{ get; set; } 
    }
}