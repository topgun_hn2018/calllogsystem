﻿namespace CallLogSystem.Common.ContractBase
{
    using System;
    using System.ServiceModel;
    using System.Web;

    [MessageContract]
    public class UploadBaseRequest
    {
        public UploadBaseRequest()
        {
            if (HttpContext.Current != null)
            {
                var op = HttpContext.Current.Items["UserId"];
                if (op != null)
                {
                    this.UserId = Int32.Parse(op.ToString());
                }
            }

        }

        /// <summary>s
        /// Gets or sets the user id.
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        [MessageHeader]
        public int UserId { get; set; }
    }
}