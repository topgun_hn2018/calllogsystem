﻿namespace CallLogSystem.Common.ContractBase
{
    using System;
    using System.Web;

    /// <summary>
    /// Base Request
    /// </summary>
    public class BaseRequest
    {
        protected BaseRequest()
        {

            if (HttpContext.Current != null)
            {
                var userId = HttpContext.Current.Items["UserId"];
                if (userId != null)
                {
                    this.UserId = Int32.Parse(userId.ToString());
                }
                var companyId = HttpContext.Current.Items["CompanyId"];
                if (companyId != null)
                {
                    this.CompanyId = Int32.Parse(companyId.ToString());
                }
                var userAliasId = HttpContext.Current.Items["UserAliasId"];
                if (userAliasId != null)
                {
                    this.UserAliasId = Int32.Parse(userAliasId.ToString());
                }
            }
        }

        /// <summary>s
        /// Gets or sets the user id
        /// </summary>
        /// <value>
        /// The user id.
        /// </value>
        public int UserId { get; set; }


        /// <summary>
        /// Gets or sets the companyid.
        /// </summary>
        /// <value>
        /// The CompanyId.
        /// </value>
        public int CompanyId { get; set; }

        /// <summary>
        /// Gets or sets the user alias identifier.
        /// </summary>
        /// <value>
        /// The user alias identifier.
        /// </value>
        public int UserAliasId { get; set; }
    }
}