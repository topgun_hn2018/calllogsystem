﻿namespace CallLogSystem.Common.ContractBase
{
    using System.ServiceModel;
    using Enum;

    [MessageContract]
    public class UploadBaseResponse : BaseResponse
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        [MessageHeader]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets the error description.
        /// </summary>
        /// <value>
        /// The error description.
        /// </value>
        [MessageHeader]
        public string ErrorDescription { get; set; }

        /// <summary>
        /// Gets or sets the error type.
        /// </summary>
        /// <value>
        /// The error type.
        /// </value>
        [MessageHeader]
        public ErrorType ErrorType { get; set; }
    }
}