﻿namespace CallLogSystem.Common.ContractBase
{
    using System.Web;
    using Enum;

    /// <summary>
    /// Base Response
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Gets or sets the error description.
        /// </summary>
        /// <value>
        /// The error description.
        /// </value>
        public string ErrorDescription { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public ErrorType ErrorType { get; set; }


        /// <summary>
        /// Gets access token
        /// </summary>
        public string AccessToken
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    object accessToken = HttpContext.Current.Items["AccessToken"];
                    if (accessToken != null)
                    {
                        return accessToken.ToString();
                    }
                }
                return string.Empty;
            }
        }
    }
}