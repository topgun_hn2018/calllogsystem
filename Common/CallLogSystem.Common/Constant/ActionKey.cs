﻿namespace CallLogSystem.Common.Constant
{
    public static class ActionKey
    {
        public const string Edit = "Edit";
        public const string Add = "Add";
        public const string DeleteFile = "DeleteFile";
        public const string Upload = "Upload";
        public const string Delete = "Delete";
        public const string View = "View";
        public const string Download = "Download";
    }
}