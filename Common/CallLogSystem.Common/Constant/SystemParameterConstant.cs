﻿namespace CallLogSystem.Common.Constant
{
    public static class SystemParameterConstant
    {
        public const string DateTimeFormat = "DATETIME_FORMAT";
        public const string Format24H = "24H_FORMAT";
        public const string ReviewDeadLine = "REVIEW_DEADLINE";
        public const string PasswordSalt = "PASSWORD_SALT";
        public const string DecimalPlace = "DECIMAL_PLACE";
    }
}