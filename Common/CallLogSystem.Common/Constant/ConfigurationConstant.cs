﻿namespace CallLogSystem.Common.Constant
{
    public static class ConfigurationConstant
    {
        public const string WebtUrl = "WebtUrl";
        public const string WebApiUrl = "WebApiUrl";
        public const string LogOnUrl = "LogOnUrl";
        public const string DownloadUrl = "DownloadUrl";
        public const string CloseRequestAutomatically = "CloseRequestAutomatically";
        public const string TokenExpirationTime = "TokenExpirationTime";
        public const string DocumentPath = "DocumentPath";
        public const string FileExtension = "FileExtension";
        public const string SmtpEmailAccount = "SmtpEmailAccount";
        public const string SmtpSentFrom = "SmtpSentFrom";
        public const string SmtpPassword = "SmtpPassword";
        public const string SmtpServer = "SmtpServer";
        public const string SmtpPort = "SmtpPort";
        public const string SmtpSSL = "SmtpSSL";
        public const string UsingDefaultReceiverEmail = "UsingDefaultReceiverEmail";
        public const string DefaultReceiverEmail = "DefaultReceiverEmail";
    }
}