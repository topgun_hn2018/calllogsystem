﻿namespace CallLogSystem.Common.Constant
{
    using System.Collections.Generic;

    public static class EmailTokenConstant
    {
        public static readonly List<string> Tokens = new List<string>
         {
            "RequestNo",
            "RequestType",
            "RequestName",
            "RequestStatus",
            "ParentsRequest",
            "ChildrenRequests",
            "RequestDescription",
            "ExpectedCompleteDate",
            "RequestNote",
            "RequesterName",
            "RequestCreatedDate",

            "ApprovalApprover",
            "ApprovalCreatedDate",
            "ApprovalStatus",
            "ApprovalNote",

            "Assigner",
            "AssignTime",
            "AssignNote",

            "Assignee",
            "ProcessStatus",
            "ProcessStartTime",
            "ProcessEndTime",
            "ProcessDescription",

            "Rating",

            "CommentContent",
            "CommentOwner",
            "CommentCreatedDate",

            "AttachFileUrl",
            "FollowRequestUrl",
            "ApproveRequestUrl",
            "AssignRequestUrl",
            "ProcessRequestUrl",
            "RatingRequestUrl",

            "CurrentOwner",

            "RequesterCompany",
            "RequesterDepartment",
            "RequesterGroup"
         };
    }
}