﻿namespace CallLogSystem.Common.Constant
{
    public static class EmailHyperlink
    {
        public const string Href = "<a href='{0}{1}{2}'>đây</a>";
        public const string DownloadHref = "<a href='{0}{1}'>{2}</a>";
        public const string FollowRequest = "#/followrequest?requestid=";
        public const string ApproveRequest = "#/approverequest?requestid=";
        public const string AssignRequest = "#/assignrequest?requestid=";
        public const string ProcessRequest = "#/processrequest?requestid=";
        public const string Download = "#/download?filemameserver={0}&&filename={1}&&fileType={2}&&extension={3}&&createdDate={4}";
        public const string Space = "&nbsp;";
        public const string Comma = ",";
        public const string Break = "</br>";
    }
}