﻿namespace CallLogSystem.Common.Constant
{
    using System.Collections.Generic;

    public static class TokenConstant
    {
        public static readonly Dictionary<string, string> TokenValues = new Dictionary<string, string>
        {
            {"UserId", "UserId"},
            {"FullName", "FullName"},
            {"UserAliasId", "UserAliasId"},
            {"CompanyId", "CompanyId"},
            {"CompanyName", "CompanyName"},
            {"iss", "Issuer"},
            {"aud", "Audience"},
            {"exp", "ExpirationTime"},
            {"nbf", "NotBefore"}
        }; 
    }
}