﻿namespace CallLogSystem.Common.Utilites
{
    using System.Configuration;

    public static class ConfigUtilities
    {
        public static string GetStringKey(string keyName, string defaultValue)
        {
            var value = ConfigurationManager.AppSettings[keyName];
            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        public static bool GetBoolKey(string keyName, bool defaultValue)
        {
            var value = ConfigurationManager.AppSettings[keyName];
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            bool returnValue;

            if (bool.TryParse(value, out returnValue))
                return returnValue;

            return defaultValue;
        }

        public static int GetIntKey(string keyName, int defaultValue)
        {
            int returnValue;
            return int.TryParse(ConfigurationManager.AppSettings[keyName], out returnValue) ? returnValue : defaultValue;
        }

        public static string GetConnectionString(string key, string defaultConnectionString)
        {
            var value = ConfigurationManager.ConnectionStrings[key].ConnectionString;
            return string.IsNullOrEmpty(value) ? defaultConnectionString : value;
        }
    }
}
