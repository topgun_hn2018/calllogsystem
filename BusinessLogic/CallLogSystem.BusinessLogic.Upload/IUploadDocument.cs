﻿namespace CallLogSystem.BusinessLogic.UploadDocument
{
    using Common.ContractBase;
    using Contract.Request;
    using Contract.Response;

    /// <summary>
    /// Uploads Document
    /// </summary>
    public interface IUploadDocument
    {
        /// <summary>
        /// Saves Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UploadDocumentResponse SaveDocument(UploadDocumentRequest request);

        /// <summary>
        /// Removes Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UploadBaseResponse RemoveDocument(RemoveDocumentRequest request);

        /// <summary>
        /// Downloads Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DownloadDocumentResponse DownloadDocument(DownloadDocumentRequest request);
    }
}
