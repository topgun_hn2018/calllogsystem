﻿namespace CallLogSystem.BusinessLogic.UploadDocument
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Common.ContractBase;
    using Common.Enum;
    using Common.Helper;
    using Contract.Request;
    using Contract.Response;
    using Helper;

    /// <summary>
    /// Uploads Document
    /// </summary>
    public class UploadDocument:IUploadDocument
    {
        /// <summary>
        /// Saves Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UploadDocumentResponse SaveDocument(UploadDocumentRequest request)
        {
            if (request == null)
            {
                return null;
            }

            //Check file extension
            var extension = request.FileNameServer.Split('.').Last();
            if (!FileExtensionHelper.CheckFileExtension(extension))
            {
                return new UploadDocumentResponse
                {
                    IsSuccess = false,
                    ErrorType = ErrorType.InvalidType
                };
            }
            var path = AppSettingHelper.GetDocumentPath();

            if (request.IsCategory)
            {
                path = HttpContext.Current.Server.MapPath("/content/image");
            }
            var fileName = string.Format("{0}.{1}", Path.GetRandomFileName(), extension);
            var fullPath = string.Format("{0}/{1}", path, fileName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            using (var streamWriter = File.Create(fullPath, (int)request.Length))
            {
                // Initialize the bytes array with the stream length and then fill it with data
                var bytesInStream = new byte[request.Length];
                // Use write method to write to the file specified above
                streamWriter.Write(request.ByteStream, 0, bytesInStream.Length);
            }
            var response = new UploadDocumentResponse
            {
                FileName = Path.GetFileName(request.FileNameServer.Substring(0, request.FileNameServer.Length - extension.Length - 1)),
                Extension = extension,
                Length = request.Length,
                FileType = request.FileType,
                FileNameServer = fileName,
                UploadedBy = request.UserId,
                UploadedDate = DateTime.Now,
                IsSuccess = true
            };
            return response;
        }

        /// <summary>
        /// Removes Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UploadBaseResponse RemoveDocument(RemoveDocumentRequest request)
        {
            var path = AppSettingHelper.GetDocumentPath(request.UploadedDate);
            var fullPath = string.Format("{0}/{1}", path, request.FileName);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
            return new UploadBaseResponse();
        }

        /// <summary>
        /// Downloads Document
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DownloadDocumentResponse DownloadDocument(DownloadDocumentRequest request)
        {
            string path = AppSettingHelper.GetDocumentPath(request.UploadedDate);
            var fullPath = string.Format("{0}/{1}", path, request.FileName);
            if (File.Exists(fullPath))
            {
                using (var stream = File.Open(fullPath, FileMode.Open, FileAccess.Read))
                {
                    var fileBytes = new byte[stream.Length];
                    while (stream.Read(fileBytes, 0, fileBytes.Length) > 0)
                    {
                        return new DownloadDocumentResponse
                        {
                            ByteStream = fileBytes,
                            FileName = request.FileName,
                            Length = fileBytes.Length,
                            ErrorType = ErrorType.NoError
                        };
                    }
                    
                }
            }
            return new DownloadDocumentResponse { ErrorType = ErrorType.Error };
        }
    }
}