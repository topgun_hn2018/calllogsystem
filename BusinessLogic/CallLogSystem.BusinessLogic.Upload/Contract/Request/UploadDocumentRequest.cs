﻿namespace CallLogSystem.BusinessLogic.UploadDocument.Contract.Request
{
    using System.ServiceModel;
    using Common.ContractBase;

    [MessageContract]
    public class UploadDocumentRequest:UploadBaseRequest
    {
        [MessageHeader(MustUnderstand = true)]
        public string FileNameServer { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public long Length { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public string FileType { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public byte[] ByteStream { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public int FilePosition { get; set; }

        [MessageHeader(MustUnderstand = true)]
        public bool IsCategory { get; set; }
    }
}