﻿namespace CallLogSystem.BusinessLogic.UploadDocument.Contract.Request
{
    using System;
    using System.ServiceModel;
    using Common.ContractBase;

    [MessageContract]
    public class DownloadDocumentRequest : UploadBaseRequest
    {
        [MessageHeader]
        public string FileName { get; set; }

        [MessageHeader]
        public DateTime UploadedDate { get; set; }

        [MessageHeader]
        public string FileType { get; set; }
    }
}