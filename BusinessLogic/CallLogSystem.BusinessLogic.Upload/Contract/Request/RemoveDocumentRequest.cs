﻿namespace CallLogSystem.BusinessLogic.UploadDocument.Contract.Request
{
    using System;
    using System.ServiceModel;
    using Common.ContractBase;

    public class RemoveDocumentRequest : UploadBaseRequest
    {
        [MessageHeader]
        public string FileName { get; set; }

        [MessageHeader]
        public DateTime UploadedDate { get; set; }
    }
}