﻿namespace CallLogSystem.BusinessLogic.UploadDocument.Contract.Response
{
    using System.ServiceModel;
    using Common.ContractBase;

    [MessageContract]
    public class DownloadDocumentResponse : UploadBaseResponse
    {
        [MessageBodyMember]
        public string FileName { get; set; }

        [MessageBodyMember]
        public long Length { get; set; }

        [MessageBodyMember]
        public byte[] ByteStream { get; set; }
    }
}