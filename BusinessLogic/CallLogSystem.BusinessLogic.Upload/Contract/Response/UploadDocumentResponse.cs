﻿namespace CallLogSystem.BusinessLogic.UploadDocument.Contract.Response
{
    using System;
    using System.ServiceModel;
    using Common.ContractBase;

    [MessageContract]
    public class UploadDocumentResponse:UploadBaseResponse
    {
        [MessageBodyMember]
        public int DocumentId { get; set; }
        [MessageBodyMember]
        public string FileNameServer { get; set; }

        [MessageBodyMember]
        public string FileName { get; set; }

        [MessageBodyMember]
        public string Extension { get; set; }

        [MessageBodyMember]
        public string FileType { get; set; }

        [MessageBodyMember]
        public long Length { get; set; }

        [MessageBodyMember]
        public string UploadedByName { get; set; }

        [MessageBodyMember]
        public int UploadedBy { get; set; }

        [MessageBodyMember]
        public DateTime UploadedDate { get; set; }

        public string Size
        {
            get
            {
                if (this.Length > 1024 * 1024)
                {
                    return string.Format("{0} MB", Math.Round((double)this.Length / 1024 / 1024, 1));
                }
                if (this.Length > 1024)
                {
                    return string.Format("{0} KB", Math.Round((double)this.Length / 1024, 1));
                }
                return string.Format("{0} Byte", this.Length);
            }
        }
    }
}