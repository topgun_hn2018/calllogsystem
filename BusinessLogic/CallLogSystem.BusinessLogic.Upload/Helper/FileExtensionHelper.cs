﻿namespace CallLogSystem.BusinessLogic.UploadDocument.Helper
{
    using System.Linq;
    using Common.Constant;
    using Common.Utilites;

    public static class FileExtensionHelper
    {
        public static bool CheckFileExtension(string input)
        {
            string fileExtension = ConfigUtilities.GetStringKey(ConfigurationConstant.FileExtension, "");
            return fileExtension.Split(',').ToList().Any(s => s.Trim().ToLower() == input.ToLower());
        }
    }
}