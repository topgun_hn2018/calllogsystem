﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection;
    using Common.Enum;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using log4net;

    public class RequestConfiguration : IRequestConfiguration
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public RequestConfiguration(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets all request types.
        /// </summary>
        /// <returns></returns>
        public GetAllRequestTypeResponse GetAllRequestTypes()
        {
            List<RequestTypeModel> requestTypes = this._unitOfWork.Repo<RequestType>().TableNoTracking.Where(s => s.IsActive).Select(s => new RequestTypeModel
            {
                Id = s.Id,
                Name = s.Name,
                TAT = s.DefaultTAT
            }).AsNoTracking().ToList();
            return new GetAllRequestTypeResponse
            {
                RequestTypes = requestTypes.OrderBy(s => s.Name).ToList()
            };
        }

        public GetAllRequestStatusResponse GetAllRequestStatus()
        {
            var requestStatus = this._unitOfWork.Repo<RequestStatus>().TableNoTracking.Where(s => s.IsActive && s.Id != 1).Select(s => new RequestStatusModel
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
            return new GetAllRequestStatusResponse
            {
                RequestStatus = requestStatus
            };
        }
        public SearchRequestTypeResponse SearchRequestType(SearchRequestTypeRequest request)
        {
            if (request.RequestTypeName == null)
                request.RequestTypeName = string.Empty;
            var requestTypes =
                this._unitOfWork.Repo<RequestType>().TableNoTracking.Where(
                    s => (s.IsActive == request.Status || request.Status == null) && (s.Category.Id == request.CategoryId || request.CategoryId == -1) && s.Name.Contains(request.RequestTypeName)).Select(s => new RequestTypeModel
                    {
                        Id = s.Id,
                        Name = s.Name,
                        CategoryName = s.Category.Name,
                        TAT = s.DefaultTAT,
                        ProcessBySite = s.ProcessBySite,
                        IsActive = s.IsActive
                    }).AsNoTracking().ToList();
            return new SearchRequestTypeResponse
            {
                RequestTypes = requestTypes
            };
        }

        public GetRequestTypeByIdResponse GetRequestTypeById(GetByIdRequest request)
        {
            var requestType =
                this._unitOfWork.Repo<RequestType>().TableNoTracking.Where(x => x.Id == request.Id).Select(s => new RequestTypeModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    CategoryId = s.CategoryId,
                    CategoryName = s.Category.Name,
                    TAT = s.DefaultTAT,
                    ProcessBySite = s.ProcessBySite,
                    IsActive = s.IsActive,
                    Order = s.Order
                }).AsNoTracking().FirstOrDefault();
            return new GetRequestTypeByIdResponse
            {
                RequestType = requestType
            };
        }

        public SaveResponse SaveRequestType(SaveRequestTypeRequest request)
        {
            var req = this._unitOfWork.Repo<RequestType>().Table.FirstOrDefault(s => s.Name == request.RequestType.Name.Trim() && s.Id != request.RequestType.Id);
            if (req == null) {
                RequestType requestType;
                if (request.RequestType.Id > 0)
                {
                    requestType = this._unitOfWork.Repo<RequestType>().Table.First(r => r.Id == request.RequestType.Id);
                    requestType.IsActive = request.RequestType.IsActive;
                    requestType.Name = request.RequestType.Name;
                    requestType.DefaultTAT = request.RequestType.TAT;
                    requestType.ProcessBySite = request.RequestType.ProcessBySite;
                    requestType.CategoryId = request.RequestType.CategoryId;
                    requestType.UpdatedBy = request.UserId;
                    requestType.UpdatedDate = DateTime.Now;
                    requestType.Order = request.RequestType.Order;
                    this._unitOfWork.Repo<RequestType>().Update(requestType);
                }
                else
                {
                    requestType = new RequestType
                    {
                        Id = request.RequestType.Id,
                        Name = request.RequestType.Name,
                        IsActive = request.RequestType.IsActive,
                        DefaultTAT = request.RequestType.TAT,
                        ProcessBySite = request.RequestType.ProcessBySite,
                        CategoryId = request.RequestType.CategoryId,
                        CreatedBy = request.UserId,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = request.UserId,
                        UpdatedDate = DateTime.Now,
                        Order = request.RequestType.Order
                    };
                    this._unitOfWork.Repo<RequestType>().Insert(requestType);
                }
                
                this._unitOfWork.CommitAsync();
                return new SaveResponse
                {
                    ErrorType = ErrorType.NoError,
                    ErrorDescription = ""
                };
            }

            this._log.ErrorFormat("Actor:{0} \n {1}", request.UserId, "Requesttype exist!");
            return new SaveResponse
            {
                ErrorType = ErrorType.Warning,
                ErrorDescription = "Requesttype exist!"
            };
        }
    }
}