﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Caching;
    using Common.Constant;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;

    public class Security : ISecurity
    {
        private readonly IUnitOfWork _unitOfWork;

        public Security(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets the permission by function.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetPermissionResponse GetPermissionByFunction(GetPermissionRequest request)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddHours(1.0)
            };

            List<UserPermissionModel> permissionNames = new List<UserPermissionModel>();
            if (cache.Contains(request.FunctionKey))
            {
                //Get data from the cache
                permissionNames = (List<UserPermissionModel>)cache.Get(request.FunctionKey);
            }
            else
            {
                var function = this._unitOfWork.Repo<Function>().TableNoTracking
                    .FirstOrDefault(s => s.Key == request.FunctionKey);
                if (function != null)
                {
                    //Get permissions from user role
                    IQueryable<Roles_Permissions> rolePermissions = this._unitOfWork.Repo<Roles_Permissions>().TableNoTracking
                        .Where(s => s.Permission.FunctionId == function.Id);
                    foreach (var rolesPermission in rolePermissions)
                    {
                        permissionNames.AddRange(rolesPermission.Role.UserAlias_Roles.Select(s => new UserPermissionModel
                        {
                            UserId = s.UserAlias.UserId,
                            CompanyId = s.UserAlias.CompanyId,
                            ActionKey = rolesPermission.Permission.Action.Key
                        }));
                    }

                    //Get permissions from user add-on
                    var permissions = this._unitOfWork.Repo<Permission>().TableNoTracking
                        .Where(s => s.FunctionId == function.Id).ToList();
                    foreach (var permission in permissions)
                    {
                        permissionNames.AddRange(permission.Addons.Select(s => new UserPermissionModel
                        {
                            UserId = s.UserAlias.UserId,
                            CompanyId = s.UserAlias.CompanyId,
                            ActionKey = permission.Action.Key
                        }));
                    }
                }
                
                cache.Add(request.FunctionKey, permissionNames.Distinct().ToList(), cacheItemPolicy);
            }

            //Get permission of current user
            IEnumerable<string> userPermissions =
                permissionNames.Where(s => s.CompanyId == request.CompanyId && s.UserId == request.UserId)
                    .Select(s => s.ActionKey);

            //Set value for new instance
            ActionModel actions = new ActionModel();
            foreach (var permission in userPermissions)
            {
                var prop = typeof(ActionModel).GetProperty(permission);
                if (prop != null)
                {
                    prop.SetValue(actions, true, null);
                }
            }

            return new GetPermissionResponse
            {
                Actions = actions
            };
        }

        /// <summary>
        /// Gets the permission accessing.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetPermissionAccessingResponse GetPermissionAccessing(GetPermissionAccessingRequest request)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddHours(1.0)
            };
            List<UserPermissionModel> permissionNames = new List<UserPermissionModel>();
            if (cache.Contains(PermissionKey.PermissionAccessing))
            {
                //Get data from the cache
                permissionNames = (List<UserPermissionModel>) cache.Get(PermissionKey.PermissionAccessing);
            }

            else
            {
                using (var dbContext = new CallLogEntities())
                {
                    //Get permissions from user role
                    permissionNames.AddRange((from userAlias in dbContext.UserAliases
                                              join aliasRolese in dbContext.UserAlias_Roles on userAlias.Id equals aliasRolese.UserAliasId
                                              join role in dbContext.Roles on aliasRolese.RoleId equals role.Id
                                              join rolesPermissionse in dbContext.Roles_Permissions on role.Id equals rolesPermissionse.RoleId
                                              join permission in dbContext.Permissions on rolesPermissionse.PermissonId equals permission.Id
                                              join action in dbContext.Actions on permission.ActionId equals action.Id
                                              join function in dbContext.Functions on permission.FunctionId equals function.Id
                                              where action.Key == ActionKey.View
                                              select new UserPermissionModel
                                              {
                                                  UserId = userAlias.UserId,
                                                  CompanyId = userAlias.CompanyId,
                                                  FuntionKey = function.Key
                                              }).Distinct().ToList());

                    //Get permissions from user add-on
                    permissionNames.AddRange((from userAlias in dbContext.UserAliases
                                              join addon in dbContext.Addons on userAlias.Id equals addon.UserAliasId
                                              join permission in dbContext.Permissions on addon.PermissionId equals permission.Id
                                              join action in dbContext.Actions on permission.ActionId equals action.Id
                                              join function in dbContext.Functions on permission.FunctionId equals function.Id
                                              where action.Key == ActionKey.View
                                              select new UserPermissionModel
                                              {
                                                  UserId = userAlias.UserId,
                                                  CompanyId = userAlias.CompanyId,
                                                  FuntionKey = function.Key
                                              }).Distinct().ToList());
                    cache.Add(PermissionKey.PermissionAccessing, permissionNames, cacheItemPolicy);
                }
            }
            
            //Get permission of current user
            
            IEnumerable<string> userPermissions =
                permissionNames.Where(s => s.CompanyId == request.CompanyId && s.UserId == request.UserId)
                    .Select(s => s.FuntionKey);
            
            //Set value for new instance
            PermissionAccessingModel permissions = new PermissionAccessingModel();
            foreach (var function in userPermissions)
            {
                var prop = typeof(PermissionAccessingModel).GetProperty(function);
                if (prop != null)
                {
                    prop.SetValue(permissions, true, null);
                }
            }

            return new GetPermissionAccessingResponse
            {
                PermissionAccessing = permissions
            };
        }
    }
}