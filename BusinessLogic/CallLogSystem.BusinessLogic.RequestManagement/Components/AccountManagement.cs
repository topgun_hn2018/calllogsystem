﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Linq;
    using Common.Constant;
    using Common.Enum;
    using Common.Helper;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using log4net;

    public class AccountManagement : IAccountManagement
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public AccountManagement(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns></returns>
        public GetAllUserResponse GetAllUsers(GetAllUserRequest request)
        {
            var users = this._unitOfWork.Repo<User>().TableNoTracking.Where(s => s.IsActive).Select(s => new UserAccountModel
            {
                id = s.Id,
                text = s.UserName + " - " + s.FullName,
            }).ToList();

            return new GetAllUserResponse
            {
                Users = users.OrderBy(s => s.text).ToList()
            };
        }

        public SaveAccountResponse SaveAccount(SaveAccountRequest request)
        {
            var uniqueUser = this._unitOfWork.Repo<User>().TableNoTracking.FirstOrDefault(u => u.UserName == request.Account.UserName && u.Id != request.Account.UserId);
            if (uniqueUser != null)
            {
                return new SaveAccountResponse
                {
                    ErrorDescription = "EXISTED_ACCOUNT",
                    ErrorType = ErrorType.Error
                };
            }

            var salt = "";
            //Get password key
            var passwordSalt = this._unitOfWork.Repo<SystemParameter>().TableNoTracking.FirstOrDefault(s => s.Key == SystemParameterConstant.PasswordSalt);
            if (passwordSalt != null)
            {
                salt = passwordSalt.Value;
            }

            var crrUser = this._unitOfWork.Repo<User>().TableNoTracking.FirstOrDefault(s => s.Id == request.Account.UserId);
            var user = new User
            {
                Id = request.Account.UserId,
                IsActive = request.Account.IsActive,
                UserName = request.Account.UserName,
                FullName = request.Account.FullName,
                Email = request.Account.Email,
                MobileNumber = request.Account.Mobile,
                Password = MD5CryptoService.HashPassword(request.Account.Password + salt),
                CreatedBy = request.UserId,
                CreatedDate = DateTime.Now,
                UpdatedBy = request.UserId,
                UpdatedDate = DateTime.Now
            };
            if (crrUser != null && request.Account.Password == "")
            {
                user.Password = crrUser.Password;
            }
            this._unitOfWork.Repo<User>().Insert(user);
            //Update default role
            if (request.Account.DefaultAliasId > 0 && user.Id > 0)
            {
                var listAlias = this._unitOfWork.Repo<UserAlias>().Table.Where(s => s.UserId == user.Id);
                foreach (var item in listAlias)
                {
                    item.IsDefault = item.Id == request.Account.DefaultAliasId;
                    this._unitOfWork.Repo<UserAlias>().Update(item);
                }
            }
            this._unitOfWork.CommitAsync();
            return new SaveAccountResponse
            {
                UserId = user.Id,
                ErrorDescription = "",
                ErrorType = ErrorType.NoError
            };
        }

        public SaveAliasResponse SaveAlias(SaveAliasRequest request)
        {
            var checkExistedAlias =
                this._unitOfWork.Repo<UserAlias>().TableNoTracking.FirstOrDefault(
                    a => a.CompanyId == request.AliasModel.CompanyId && a.UserId == request.AliasModel.UserId);
            if (request.AliasModel.Id > 0 || checkExistedAlias == null)
            {
                this.AddOrUpdateAlias(request.AliasModel);
            }
            else
            {
                //Get existing addons and alias roles
                var lstAddons = this._unitOfWork.Repo<Addon>().TableNoTracking.Where(ad => ad.UserAliasId == checkExistedAlias.Id).ToList();
                var lstAliasRoles = this._unitOfWork.Repo<UserAlias_Roles>().TableNoTracking.Where(ar => ar.UserAliasId == checkExistedAlias.Id).ToList();
                //Add addons to existed list
                request.AliasModel.Addons.ForEach(a =>
                {
                    var checkExistedAddon = lstAddons.FirstOrDefault(l => l.Id == a.Id);
                    if (checkExistedAddon == null)
                    {
                        lstAddons.Add(new Addon
                        {
                            UserAliasId = checkExistedAlias.Id,
                            PermissionId = a.PermissionId
                        });
                    }
                });
                //Add roles to existed list
                request.AliasModel.Roles.ForEach(r =>
                {
                    var checkExistedRole = lstAliasRoles.FirstOrDefault(l => l.RoleId == r.Id);
                    if (checkExistedRole == null) lstAliasRoles.Add(new UserAlias_Roles
                    {
                        RoleId = r.Id,
                        UserAliasId = checkExistedAlias.Id
                    });
                });
                //Create new model from existed Alias
                var alias = new AliasModel
                {
                    Id = checkExistedAlias.Id,
                    UserId = checkExistedAlias.UserId,
                    CompanyId = checkExistedAlias.CompanyId,
                    Roles = lstAliasRoles.Select(a => new RoleModel
                    {
                        Id = a.RoleId
                    }).ToList(),
                    Addons = lstAddons.Select(a => new Contract.Model.AddonModel
                    {
                        PermissionId = a.PermissionId
                    }).ToList()
                };
                this.AddOrUpdateAlias(alias);
            }

            this._unitOfWork.CommitAsync();
            return new SaveAliasResponse
            {
                ErrorDescription = "",
                ErrorType = ErrorType.NoError
            };
        }

        private void AddOrUpdateAlias(AliasModel aliasModel)
        {
            //Update master
            var dbAlias = new UserAlias
            {
                Id = aliasModel.Id,
                UserId = aliasModel.UserId,
                CompanyId = aliasModel.CompanyId,
                IsDefault = aliasModel.IsDefault
            };
            this._unitOfWork.Repo<UserAlias>().Insert(dbAlias);
            //Remove childs
            if (aliasModel.Id > 0)
            {
                this._unitOfWork.Repo<Addon>().Delete(this._unitOfWork.Repo<Addon>().TableNoTracking.Where(a => a.UserAliasId == aliasModel.Id));
                this._unitOfWork.Repo<UserAlias_Roles>().Delete(this._unitOfWork.Repo<UserAlias_Roles>().TableNoTracking.Where(a => a.UserAliasId == aliasModel.Id));
            }

            //Add Alias Roles
            aliasModel.Roles.ForEach(r =>
            {
                this._unitOfWork.Repo<UserAlias_Roles>().Insert(new UserAlias_Roles
                {
                    UserAliasId = dbAlias.Id,
                    RoleId = r.Id
                });
            });
            //Add Addons
            aliasModel.Addons.ForEach(a =>
            {
                this._unitOfWork.Repo<Addon>().Insert(new Addon
                {
                    UserAliasId = dbAlias.Id,
                    PermissionId = a.PermissionId
                });
            });
            this._unitOfWork.CommitAsync();
        }

        public SearchAccountResponse SearchAccount(SearchAccountRequest request)
        {
            IQueryable<UserModel> result = this._unitOfWork.Repo<User>().TableNoTracking.Where(user =>
                    (string.IsNullOrEmpty(request.UserName) || user.UserName.Contains(request.UserName)) &&
                    (string.IsNullOrEmpty(request.FullName) || user.FullName.Contains(request.FullName)) &&
                    (request.IsActive == null || user.IsActive == request.IsActive)).OrderBy(user => user.Id)
                .Select(user => new UserModel
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    FullName = user.FullName,
                    WorkArea = user.UserAliases.FirstOrDefault(s => s.IsDefault).Company.Name,
                    Status = user.IsActive ? "SearchFrom_AccountStatus_On" : "SearchFrom_AccountStatus_Off"
                });
            return new SearchAccountResponse
            {
                Accounts = result
            };
        }

        public GetAccountByIdResponse GetAccountById(GetAccountByIdRequest request)
        {
            var account = this._unitOfWork.Repo<User>().TableNoTracking.FirstOrDefault(a => a.Id == request.Id);
            if (account != null)
            {
                var roles = new List<RoleModel>();
                var aliases = new List<AliasModel>();
                foreach (var userAlias in account.UserAliases)
                {
                    roles.AddRange(userAlias.UserAlias_Roles.Select(s => new RoleModel
                    {
                        Id = s.RoleId,
                        AliasId = s.UserAliasId,
                        Name = s.Role.Name,
                        IsDefault = s.UserAlias.IsDefault,
                        CompanyName = s.UserAlias.Company.Name
                    }));

                    aliases.AddRange(userAlias.UserAlias_Roles.Select(s => new AliasModel
                    {
                        Id = s.UserAliasId,
                        CompanyId = s.UserAlias.CompanyId,
                        CompanyName = s.UserAlias.Company.Name,
                        IsDefault = s.UserAlias.IsDefault,
                        Roles = userAlias.UserAlias_Roles.Where(a => a.UserAliasId == s.UserAliasId).Select(a => new RoleModel
                        {
                            Id = a.RoleId,
                            Name = a.Role.Name
                        }).ToList()
                    }));
                }

                AliasModel defaultAlias = aliases.FirstOrDefault(a => a.IsDefault);
                var response = new GetAccountByIdResponse
                {
                    Account = new UserModel
                    {
                        UserId = account.Id,
                        IsActive = account.IsActive,
                        FullName = account.FullName,
                        UserName = account.UserName,
                        Email = account.Email,
                        Mobile = account.MobileNumber,
                        Password = account.Password,
                        Roles = roles,
                        RoleId = -1,
                        DefaultAliasId = defaultAlias != null ? defaultAlias.Id : -1,
                        Aliases = aliases,
                        WorkArea = ""
                    }
                };

                return response;
            }
            return null;
        }

        public GetAliasByIdResponse GetAliasById(GetAliasByIdRequest request)
        {
            var alias = this._unitOfWork.Repo<UserAlias>().TableNoTracking.FirstOrDefault(a => a.Id == request.Id);
            if (alias != null)
            {

                var roles = this._unitOfWork.Repo<UserAlias_Roles>().TableNoTracking.Where(t => t.UserAliasId == alias.Id).Select(r => new RoleModel
                {
                    Id = r.RoleId
                }).ToList();

                var addons = this._unitOfWork.Repo<Addon>().TableNoTracking.Where(a => a.UserAliasId == alias.Id).Select(r => new AddonModel
                {
                    PermissionId = r.PermissionId
                }).ToList();
                var reponse = new GetAliasByIdResponse
                {
                    Alias = new AliasModel
                    {
                        Id = alias.Id,
                        CompanyId = alias.CompanyId,
                        Roles = roles,
                        Addons = addons,
                        IsDefault = alias.IsDefault
                    },
                    ErrorDescription = "",
                    ErrorType = ErrorType.NoError
                };
                return reponse;
            }
            return null;
        }

        public DeleteAliasResponse DeleteAlias(DeleteAliasRequest request)
        {
            this._unitOfWork.Repo<UserAlias_Roles>().Delete(this._unitOfWork.Repo<UserAlias_Roles>().Table
                .Where(ar => ar.UserAliasId == request.Id));
            this._unitOfWork.Repo<Addon>()
                .Delete(this._unitOfWork.Repo<Addon>().Table.Where(ad => ad.UserAliasId == request.Id));
            this._unitOfWork.Repo<UserAlias>()
                .Delete(this._unitOfWork.Repo<UserAlias>().TableNoTracking.First(a => a.Id == request.Id));

            //Reload roles
            var account = this._unitOfWork.Repo<User>().TableNoTracking.FirstOrDefault(a => a.Id == request.Id);
            var roles = new List<RoleModel>();
            var aliases = new List<AliasModel>();
            foreach (var userAlias in account.UserAliases)
            {
                roles.AddRange(userAlias.UserAlias_Roles.Select(s => new RoleModel
                {
                    Id = s.RoleId,
                    AliasId = s.UserAliasId,
                    Name = s.Role.Name,
                    IsDefault = s.UserAlias.IsDefault,
                    CompanyName = s.UserAlias.Company.Name
                }));

                aliases.AddRange(userAlias.UserAlias_Roles.Select(s => new AliasModel
                {
                    Id = s.UserAliasId,
                    CompanyId = s.UserAlias.CompanyId,
                    CompanyName = s.UserAlias.Company.Name,
                    IsDefault = s.UserAlias.IsDefault,
                    Roles = userAlias.UserAlias_Roles.Where(a => a.UserAliasId == s.UserAliasId).Select(a =>
                        new RoleModel
                        {
                            Id = a.RoleId,
                            Name = a.Role.Name
                        }).ToList()
                }));
            }

            return new DeleteAliasResponse
            {
                Roles = roles,
                Aliases = aliases,
                ErrorDescription = "",
                ErrorType = ErrorType.NoError
            };
        }
    }
}