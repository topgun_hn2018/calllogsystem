﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Web.Management;
    using Common.Extension;
    using Common.Helper;
    using Common.Utilites;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using log4net;

    public class Report : IReport
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public Report(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get companies by user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetCompanyByUserResponse GetCompanyByUser(GetCompanyByUserRequest request)
        {
            //get company with user'role is leader of group
            IQueryable<int> companies1 = this._unitOfWork.Repo<Group_User>().TableNoTracking
                .Where(s => s.UserId == request.UserId && s.IsLeader && s.Group.IsActive &&
                            s.Group.Department.IsActive && s.Group.Department.Company.IsActive)
                .Select(s => s.Group.Department.CompanyId).Distinct();

            //get company with user'role is Manager of Departments
            IQueryable<int> companies2 = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                    s.UserId == request.UserId &&
                    s.Department.IsActive &&
                    s.User.IsActive &&
                    s.Department.Company.IsActive &&
                    (s.IsManager || s.IsReporter))
                .Select(s => s.UserId).Distinct();

            List<CompanyModel> companies = (from c in this._unitOfWork.Repo<Company>().TableNoTracking
                             where (companies1.Any(s => s.Equals(c.Id)) || companies2.Any(s => s.Equals(c.Id)))
                             select new CompanyModel
                             {
                                 CompanyId = c.Id,
                                 CompanyName = c.Name
                             }).Distinct().OrderBy(s => s.CompanyName).ToList();

            return new GetCompanyByUserResponse
            {
                Companies = companies
            };
        }

        /// <summary>
        /// Get department by list company id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetDepartmentByCompanyResponse GetDepartmentByCompany(GetDepartmentByCompanyRequest request)
        {
            //Get all list company ID which User is leader of group
            IQueryable<int> listComp1 = this._unitOfWork.Repo<Group_User>().TableNoTracking
                .Where(s => s.UserId == request.UserId && s.IsLeader && s.Group.IsActive &&
                            s.Group.Department.IsActive && s.Group.Department.Company.IsActive)
                .Select(s => s.Group.Department.CompanyId).Distinct();

            //Get all list company ID which User is manager or reporter of department
            IQueryable<int> listComp2 = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                    s.UserId == request.UserId &&
                    s.Department.IsActive &&
                    s.User.IsActive &&
                    s.Department.Company.IsActive &&
                    (s.IsManager || s.IsReporter))
                .Select(s => s.UserId).Distinct();

            //Join to get all company which user have role report
            List<int> allCompanies = listComp1.Concat(listComp2).ToList(); 

            //Get list company ID which User is manager of BGD department
            IQueryable<int> listCompanyBGD = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                    s.UserId == request.UserId &&
                    s.Department.IsActive &&
                    s.User.IsActive &&
                    s.Department.Company.IsActive &&
                    s.Department.IsBOD &&
                    (s.IsManager || s.IsReporter))
                .Select(s => s.UserId).Distinct();

            //Get all department which User is manager or reporter
            IQueryable<int> departments1 = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                allCompanies.Any(a => a == s.Department.CompanyId) &&
                (!request.CompaniesId.Any() || request.CompaniesId.Any(a => a == -1) ||
                 request.CompaniesId.Any(a => a == s.Department.CompanyId)) &&
                ((s.IsManager || s.IsReporter) && s.UserId == request.UserId ||
                 listCompanyBGD.Any(a => a == s.Department.CompanyId)) &&
                s.Department.Company.IsActive && s.Department.IsActive).Select(s => s.DepartmentId).Distinct();

            //
            IQueryable<int> departments2 = this._unitOfWork.Repo<Group_User>().TableNoTracking.Where(s =>
                    allCompanies.Any(a => a == s.Group.Department.CompanyId) &&
                    (!request.CompaniesId.Any() || request.CompaniesId.Any(a => a == -1) ||
                     request.CompaniesId.Any(a => a == s.Group.Department.CompanyId)) &&
                    s.IsLeader && s.UserId == request.UserId &&
                    s.Group.Department.Company.IsActive && s.Group.Department.IsActive)
                .Select(s => s.Group.Department.CompanyId);


            var departments = (from d in this._unitOfWork.Repo<Department>().TableNoTracking
                              where (departments1.Any(s => s.Equals(d.Id)) || departments2.Any(s => s.Equals(d.Id)))
                             select new DepartmentModel
                             {
                                 DepartmentId = d.Id,
                                 DepartmentName = d.Name
                             }).Distinct().ToList();

            return new GetDepartmentByCompanyResponse
            {
                Departments = departments.OrderBy(s => s.DepartmentName).ToList()
            };
        }

        /// <summary>
        /// Get group by department
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetGroupByDepartmentResponse GetGroupByDepartment(GetGroupByDepartmentRequest request)
        {
            //Get all company which User is manager of BGD department
            IQueryable<int> listCompanyBGD = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                s.UserId == request.UserId &&
                s.User.IsActive &&
                s.Department.IsBOD &&
                (s.IsManager || s.IsReporter) &&
                s.Department.IsActive &&
                s.Department.Company.IsActive
            ).Select(s => s.Department.CompanyId);

            //Get all department which User is manager or is reporter
            IQueryable<int> depReport = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                (s.IsReporter && s.UserId == request.UserId) || (s.IsManager && s.UserId == request.UserId) ||
                listCompanyBGD.Any(a => a == s.Department.CompanyId) && s.Department.IsActive &&
                s.Department.Company.IsActive).Select(s => s.DepartmentId).Distinct();

            //Get all groups which is User is leader
            var groups = this._unitOfWork.Repo<Group_User>().TableNoTracking.Where(s =>
                s.Group.IsActive && s.Group.Department.IsActive && s.Group.Department.Company.IsActive &&
                (s.IsLeader && s.UserId == request.UserId) || depReport.Any(a => a == s.Group.DepartmentId) &&
                (!request.CompaniesId.Any() || request.CompaniesId.Any(a => a.Equals(-1)) ||
                 request.CompaniesId.Any(a => a == s.Group.Department.CompanyId)) &&
                (!request.DepartmentIds.Any() || request.DepartmentIds.Any(a => a.Equals(-1)) ||
                 request.DepartmentIds.Any(a => a == s.Group.DepartmentId))).Select(s => new GroupModel
            {
                GroupId = s.Id,
                GroupName = s.Group.Name,
                DepartmentId = s.Group.DepartmentId
            }).Distinct().ToList();

            return new GetGroupByDepartmentResponse
            {
                Groups = groups
            };
        }

        /// <summary>
        /// Get reports
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetReportResponse GetReport(GetReportRequest request)
        {
            DateTime requestFromDate = request.RequestFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.RequestFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime requestToDate = request.RequestToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.RequestToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            DateTime approveFromDate = request.ApproveFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.ApproveFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime approveToDate = request.ApproveToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.ApproveToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            DateTime assignFromDate = request.AssignFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.AssignFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime assignToDate = request.AssignToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.AssignToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            DateTime handleFromDate = request.HandleFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.HandleFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime handleToDate = request.HandleToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.HandleToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            var fileName = "";
            var reports = new List<Contract.Model.ReportModel>();
            using (var conn = new SqlConnection(ConfigUtilities.GetConnectionString("CallLogEntities", null)))
            {
                conn.Open();
                var cmd = new SqlCommand("dbo.GetDataForReport", conn)
                {
                    CommandType = CommandType.StoredProcedure,
                };
                cmd.Parameters.AddWithValue("@UserId", request.UserId);
                cmd.Parameters.AddWithValue("@requestFrom", requestFromDate);
                cmd.Parameters.AddWithValue("@requestTo", requestToDate);
                cmd.Parameters.AddWithValue("@approveFrom", approveFromDate);
                cmd.Parameters.AddWithValue("@approveTo", approveToDate);
                cmd.Parameters.AddWithValue("@assignFrom", assignFromDate);
                cmd.Parameters.AddWithValue("@assignTo", assignToDate);
                cmd.Parameters.AddWithValue("@handleFrom", handleFromDate);
                cmd.Parameters.AddWithValue("@handleTo", handleToDate);
                cmd.Parameters.AddWithValue("@completeStatus", request.CompletedStatusId);
                cmd.Parameters.AddWithValue("@requestNo", request.RequestNo);
                cmd.Parameters.AddWithValue("@requestName", request.RequestName);
                cmd.Parameters.AddWithValue("@requestCompleteStatus", request.CompletedStatusId);

                var listCompaniesTable = DAOHelper.ListIdWraper(request.CompanyIds);
                var listCompaniesParam = cmd.Parameters.AddWithValue("@ListCompaniesParam", listCompaniesTable);
                listCompaniesParam.SqlDbType = SqlDbType.Structured;
                listCompaniesParam.TypeName = "_ListId";
                var listDeparmentTable = DAOHelper.ListIdWraper(request.DepartmentIds);
                var listDepartmentParam = cmd.Parameters.AddWithValue("@ListDeparmentParam", listDeparmentTable);
                listDepartmentParam.SqlDbType = SqlDbType.Structured;
                listDepartmentParam.TypeName = "_ListId";
                var listGropTable = DAOHelper.ListIdWraper(request.GroupIds);
                var listGropParam = cmd.Parameters.AddWithValue("@ListGroupParam", listGropTable);
                listGropParam.SqlDbType = SqlDbType.Structured;
                listGropParam.TypeName = "_ListId";
                var listServiceTable = DAOHelper.ListIdWraper(request.ServiceId);
                var listServiceParam = cmd.Parameters.AddWithValue("@ListServiceParam", listServiceTable);
                listServiceParam.TypeName = "_ListId";
                listServiceParam.SqlDbType = SqlDbType.Structured;
                var listRequestStatusTable = DAOHelper.ListIdWraper(request.RequestStatusId);
                var listRequestStatusParam = cmd.Parameters.AddWithValue("@ListRequestStatusParam", listRequestStatusTable);
                listRequestStatusParam.SqlDbType = SqlDbType.Structured;
                listRequestStatusParam.TypeName = "_ListId";
                var listRequesterTable = DAOHelper.ListIdWraper(request.RequesterIds);
                var listRequesterParam = cmd.Parameters.AddWithValue("@ListRequesterParam", listRequesterTable);
                listRequesterParam.SqlDbType = SqlDbType.Structured;
                listRequesterParam.TypeName = "_ListId";
                var listApproverTable = DAOHelper.ListIdWraper(request.ApproverIds);
                var listApproverParam = cmd.Parameters.AddWithValue("@ListApproverParam", listApproverTable);
                listApproverParam.SqlDbType = SqlDbType.Structured;
                listApproverParam.TypeName = "_ListId";
                var listAssignTable = DAOHelper.ListIdWraper(request.AssignerIds);
                var listAssignParam = cmd.Parameters.AddWithValue("@ListAssignParam", listAssignTable);
                listAssignParam.SqlDbType = SqlDbType.Structured;
                listAssignParam.TypeName = "_ListId";
                var listHandleTable = DAOHelper.ListIdWraper(request.HandlerIds);
                var listHandleParam = cmd.Parameters.AddWithValue("@ListHandleParam", listHandleTable);
                listHandleParam.SqlDbType = SqlDbType.Structured;
                listHandleParam.TypeName = "_ListId";
                try
                {
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {                        
                        var reportData = new Contract.Model.ReportModel()
                        {
                            Index = Convert.ToInt32(reader["RowNo"]),
                            RequestName = reader["RequestName"].ToString(),
                            RequestNo = request.IsDownload ? reader["RequestNo"].ToString() : "<a target=\"_blank\" href =\"#viewrequest?requestid=" + reader["Id"].ToString() + "\">" + reader["RequestNo"].ToString() + "</ a >",
                            ServiceName = reader["ServiceName"].ToString(),
                            RequestStatus = reader["RequestStatus"].ToString(),
                            RequesterFullName = reader["Requester"].ToString(),
                            RequesterCompanyCode = reader["CompanyCode"].ToString(),
                            RequesterDepartmentCode = reader["DeparmentCode"].ToString(),
                            CreateRequestTime = Convert.ToDateTime(reader["CreatedDate"]),
                            ParentRequestNo = reader["ParentRequestNo"].ToString(),
                            RequestChildList = reader["RequestChildList"].ToString(),
                            RequestNote =
                               Regex.Replace(Regex.Replace(Regex.Replace(reader["RequestNote"].ToString(),
                               "<.*?>|&.*?;", string.Empty), @"</?span( [^>]*|/)?>", string.Empty), @"</?td( [^>]*|/)?>", string.Empty),
                            ExpectedCompleteDate = Convert.ToDateTime(reader["ExpectedDate"]),
                            IsCompleted = reader["IsCompleted"].ToString(),
                            ApproverLv1 = reader["ApproverLv1"].ToString(),
                            ApprovedDate1 =
                                reader["ApproveLv1Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv1Date"]
                                    : null,
                            ApproveNote1 = reader["ApproveLv1Note"].ToString(),
                            ApproveStatus1 = reader["ApproveLv1Status"].ToString(),
                            ApproverLv2 = reader["ApproverLv2"].ToString(),
                            ApproveStatus2 = reader["ApproveLv2Status"].ToString(),
                            ApprovedDate2 =
                                reader["ApproveLv2Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv2Date"]
                                    : null,
                            ApproveNote2 = reader["ApproveLv2Note"].ToString(),

                            ApproverLv3 = reader["ApproverLv3"].ToString(),
                            ApproveStatus3 = reader["ApproveLv3Status"].ToString(),
                            ApprovedDate3 =
                                reader["ApproveLv3Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv3Date"]
                                    : null,
                            ApproveNote3 = reader["ApproveLv3Note"].ToString(),

                            ApproverLv4 = reader["ApproverLv4"].ToString(),
                            ApproveStatus4 = reader["ApproveLv4Status"].ToString(),
                            ApprovedDate4 =
                                reader["ApproveLv4Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv4Date"]
                                    : null,
                            ApproveNote4 = reader["ApproveLv4Note"].ToString(),

                            ApproverLv5 = reader["ApproverLv5"].ToString(),
                            ApprovedDate5 =
                                reader["ApproveLv5Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv5Date"]
                                    : null,
                            ApproveNote5 = reader["ApproveLv5Note"].ToString(),
                            ApproveStatus5 = reader["ApproveLv5Status"].ToString(),
                            GroupHandleCode = reader["GroupHandleCode"].ToString(),
                            AssignerName = reader["AssignerName"].ToString(),
                            AssignDate =
                                reader["AssignDate"] != DBNull.Value ? (DateTime?)reader["AssignDate"] : null,
                            AssignNote = reader["AssignNote"].ToString(),
                            ProcessStartTime =
                                reader["ProcessStartTime"] != DBNull.Value ? (DateTime?)reader["ProcessStartTime"] : null,
                            ProcessEndTime =
                            reader["ProcessEndTime"] != DBNull.Value ? (DateTime?)reader["ProcessEndTime"] : null,
                            HandlerName = reader["HandlerName"].ToString(),
                            HandleDescription =
                             Regex.Replace(Regex.Replace(Regex.Replace(reader["HandlerDescription"].ToString(),
                               "<.*?>|&.*?;", string.Empty), @"</?span( [^>]*|/)?>", string.Empty), @"</?td( [^>]*|/)?>", string.Empty),                            
                            HandleNote = reader["HandlerNote"].ToString(),
                            HandleConfirmStatus = reader["HandleStatus"].ToString(),
                            //HandleDateTime =
                            //    reader["HandleDate"] != DBNull.Value ? (DateTime?) reader["HandleDate"] : null,
                            Rating = Convert.ToInt32(reader["Rating"]),
                            NoteIncomplete = reader["ConfirmNote"].ToString(),
                            TATDefault =
                                reader["TATDefault"] != DBNull.Value ? Convert.ToDouble(reader["TATDefault"]) : 0,
                            TAT = reader["TAT"] != DBNull.Value ? Convert.ToDouble(reader["TAT"]) : 0,
                            Expired = reader["IsExpired"].ToString(),
                            Duration1 =
                                reader["DurationLv1"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv1"]) : 0,
                            RealDuration1 =
                                reader["RealDurationLv1"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv1"])
                                    : 0,
                            Duration2 =
                                reader["DurationLv2"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv2"]) : 0,
                            RealDuration2 =
                                reader["RealDurationLv2"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv2"])
                                    : 0,
                            Duration3 =
                                reader["DurationLv3"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv3"]) : 0,
                            RealDuration3 =
                                reader["RealDurationLv3"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv3"])
                                    : 0,
                            Duration4 =
                                reader["DurationLv4"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv4"]) : 0,
                            RealDuration4 =
                                reader["RealDurationLv4"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv4"])
                                    : 0,
                            Duration5 =
                                reader["DurationLv5"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv5"]) : 0,
                            RealDuration5 =
                                reader["RealDurationLv5"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv5"])
                                    : 0,

                            AssignDuration =
                                reader["AssignDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["AssignDuration"])
                                    : 0,
                            AssignRealDuration =
                                reader["AssignRealDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["AssignRealDuration"])
                                    : 0,

                            HandleDuration =
                                reader["HandleDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["HandleDuration"])
                                    : 0,
                            HandleRealDuration =
                                reader["HandleRealDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["HandleRealDuration"])
                                    : 0,
                            SurveyContent =
                               Regex.Replace(Regex.Replace(Regex.Replace(reader["SurveyContent"].ToString(),
                               "<.*?>|&.*?;", string.Empty), @"</?span( [^>]*|/)?>", string.Empty), @"</?td( [^>]*|/)?>", string.Empty)
                        };
                        reports.Add(reportData);
                    }

                }
                catch (SqlExecutionException seException)
                {
                    this._log.ErrorFormat("Actor:{0} \n {1}", request.UserId, seException.Message);
                }
            }
            if (request.IsDownload)
            {
                fileName = ExcelHelper.ExportToExcelEpp(reports.ToDataTable());

            }
            return new GetReportResponse
            {
                Reports = reports,
                FileNameServer = fileName,
                UploadedDate = DateTime.Now,
                FileType = "xlsx"
            };
        }

        /// <summary>
        /// Get reports
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetReportResponse GetReportAssignee(GetReportRequest request)
        {
            DateTime requestFromDate = request.RequestFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.RequestFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime requestToDate = request.RequestToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.RequestToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            DateTime approveFromDate = request.ApproveFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.ApproveFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime approveToDate = request.ApproveToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.ApproveToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            DateTime assignFromDate = request.AssignFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.AssignFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime assignToDate = request.AssignToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.AssignToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            DateTime handleFromDate = request.HandleFromDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.HandleFromDate.ToLocalTime().ChangeTime(0, 0, 0, 0);
            DateTime handleToDate = request.HandleToDate == DateTime.MinValue ? new DateTime(1900, 1, 1) : request.HandleToDate.ToLocalTime().ChangeTime(0, 0, 0, 0).AddDays(1);
            var fileName = "";
            var reports = new List<Contract.Model.ReportModel>();
            using (var conn = new SqlConnection(ConfigUtilities.GetConnectionString("CallLogEntities", null)))
            {
                conn.Open();
                var cmd = new SqlCommand("dbo.GetDataForReportAssignee", conn)
                {
                    CommandType = CommandType.StoredProcedure,
                };
                cmd.Parameters.AddWithValue("@UserId", request.UserId);
                cmd.Parameters.AddWithValue("@requestFrom", requestFromDate);
                cmd.Parameters.AddWithValue("@requestTo", requestToDate);
                cmd.Parameters.AddWithValue("@approveFrom", approveFromDate);
                cmd.Parameters.AddWithValue("@approveTo", approveToDate);
                cmd.Parameters.AddWithValue("@assignFrom", assignFromDate);
                cmd.Parameters.AddWithValue("@assignTo", assignToDate);
                cmd.Parameters.AddWithValue("@handleFrom", handleFromDate);
                cmd.Parameters.AddWithValue("@handleTo", handleToDate);
                cmd.Parameters.AddWithValue("@completeStatus", request.CompletedStatusId);
                cmd.Parameters.AddWithValue("@requestNo", request.RequestNo);
                cmd.Parameters.AddWithValue("@requestName", request.RequestName);
                cmd.Parameters.AddWithValue("@requestCompleteStatus", request.CompletedStatusId);

                var listCompaniesTable = DAOHelper.ListIdWraper(request.CompanyIds);
                var listCompaniesParam = cmd.Parameters.AddWithValue("@ListCompaniesParam", listCompaniesTable);
                listCompaniesParam.SqlDbType = SqlDbType.Structured;
                listCompaniesParam.TypeName = "_ListId";
                var listDeparmentTable = DAOHelper.ListIdWraper(request.DepartmentIds);
                var listDepartmentParam = cmd.Parameters.AddWithValue("@ListDeparmentParam", listDeparmentTable);
                listDepartmentParam.SqlDbType = SqlDbType.Structured;
                listDepartmentParam.TypeName = "_ListId";
                var listGropTable = DAOHelper.ListIdWraper(request.GroupIds);
                var listGropParam = cmd.Parameters.AddWithValue("@ListGroupParam", listGropTable);
                listGropParam.SqlDbType = SqlDbType.Structured;
                listGropParam.TypeName = "_ListId";
                var listServiceTable = DAOHelper.ListIdWraper(request.ServiceId);
                var listServiceParam = cmd.Parameters.AddWithValue("@ListServiceParam", listServiceTable);
                listServiceParam.TypeName = "_ListId";
                listServiceParam.SqlDbType = SqlDbType.Structured;
                var listRequestStatusTable = DAOHelper.ListIdWraper(request.RequestStatusId);
                var listRequestStatusParam = cmd.Parameters.AddWithValue("@ListRequestStatusParam", listRequestStatusTable);
                listRequestStatusParam.SqlDbType = SqlDbType.Structured;
                listRequestStatusParam.TypeName = "_ListId";
                var listRequesterTable = DAOHelper.ListIdWraper(request.RequesterIds);
                var listRequesterParam = cmd.Parameters.AddWithValue("@ListRequesterParam", listRequesterTable);
                listRequesterParam.SqlDbType = SqlDbType.Structured;
                listRequesterParam.TypeName = "_ListId";
                var listApproverTable = DAOHelper.ListIdWraper(request.ApproverIds);
                var listApproverParam = cmd.Parameters.AddWithValue("@ListApproverParam", listApproverTable);
                listApproverParam.SqlDbType = SqlDbType.Structured;
                listApproverParam.TypeName = "_ListId";
                var listAssignTable = DAOHelper.ListIdWraper(request.AssignerIds);
                var listAssignParam = cmd.Parameters.AddWithValue("@ListAssignParam", listAssignTable);
                listAssignParam.SqlDbType = SqlDbType.Structured;
                listAssignParam.TypeName = "_ListId";
                var listHandleTable = DAOHelper.ListIdWraper(request.HandlerIds);
                var listHandleParam = cmd.Parameters.AddWithValue("@ListHandleParam", listHandleTable);
                listHandleParam.SqlDbType = SqlDbType.Structured;
                listHandleParam.TypeName = "_ListId";
                try
                {
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {

                        var reportData = new Contract.Model.ReportModel()
                        {
                            Index = Convert.ToInt32(reader["RowNo"]),
                            RequestName = reader["RequestName"].ToString(),                                                        
                            RequestNo = request.IsDownload? reader["RequestNo"].ToString(): "<a target=\"_blank\" href =\"#viewrequest?requestid=" + reader["Id"].ToString() + "\">" + reader["RequestNo"].ToString() + "</ a >",
                            ServiceName = reader["ServiceName"].ToString(),
                            RequestStatus = reader["RequestStatus"].ToString(),
                            RequesterFullName = reader["Requester"].ToString(),
                            RequesterCompanyCode = reader["CompanyCode"].ToString(),
                            RequesterDepartmentCode = reader["DeparmentCode"].ToString(),
                            CreateRequestTime = Convert.ToDateTime(reader["CreatedDate"]),
                            ParentRequestNo = reader["ParentRequestNo"].ToString(),
                            RequestChildList = reader["RequestChildList"].ToString(),
                            RequestNote =
                               Regex.Replace(Regex.Replace(Regex.Replace(reader["RequestNote"].ToString(),
                               "<.*?>|&.*?;", string.Empty), @"</?span( [^>]*|/)?>", string.Empty), @"</?td( [^>]*|/)?>", string.Empty),
                            ExpectedCompleteDate = Convert.ToDateTime(reader["ExpectedDate"]),
                            IsCompleted = reader["IsCompleted"].ToString(),
                            ApproverLv1 = reader["ApproverLv1"].ToString(),
                            ApprovedDate1 =
                                reader["ApproveLv1Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv1Date"]
                                    : null,
                            ApproveNote1 = reader["ApproveLv1Note"].ToString(),
                            ApproveStatus1 = reader["ApproveLv1Status"].ToString(),
                            ApproverLv2 = reader["ApproverLv2"].ToString(),
                            ApproveStatus2 = reader["ApproveLv2Status"].ToString(),
                            ApprovedDate2 =
                                reader["ApproveLv2Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv2Date"]
                                    : null,
                            ApproveNote2 = reader["ApproveLv2Note"].ToString(),

                            ApproverLv3 = reader["ApproverLv3"].ToString(),
                            ApproveStatus3 = reader["ApproveLv3Status"].ToString(),
                            ApprovedDate3 =
                                reader["ApproveLv3Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv3Date"]
                                    : null,
                            ApproveNote3 = reader["ApproveLv3Note"].ToString(),

                            ApproverLv4 = reader["ApproverLv4"].ToString(),
                            ApproveStatus4 = reader["ApproveLv4Status"].ToString(),
                            ApprovedDate4 =
                                reader["ApproveLv4Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv4Date"]
                                    : null,
                            ApproveNote4 = reader["ApproveLv4Note"].ToString(),

                            ApproverLv5 = reader["ApproverLv5"].ToString(),
                            ApprovedDate5 =
                                reader["ApproveLv5Date"] != DBNull.Value
                                    ? (DateTime?)reader["ApproveLv5Date"]
                                    : null,
                            ApproveNote5 = reader["ApproveLv5Note"].ToString(),
                            ApproveStatus5 = reader["ApproveLv5Status"].ToString(),
                            GroupHandleCode = reader["GroupHandleCode"].ToString(),
                            AssignerName = reader["AssignerName"].ToString(),
                            AssignDate =
                                reader["AssignDate"] != DBNull.Value ? (DateTime?)reader["AssignDate"] : null,
                            AssignNote = reader["AssignNote"].ToString(),
                            ProcessStartTime =
                                reader["ProcessStartTime"] != DBNull.Value ? (DateTime?)reader["ProcessStartTime"] : null,
                            ProcessEndTime =
                            reader["ProcessEndTime"] != DBNull.Value ? (DateTime?)reader["ProcessEndTime"] : null,
                            HandlerName = reader["HandlerName"].ToString(),
                            HandleDescription =
                             Regex.Replace(Regex.Replace(Regex.Replace(reader["HandlerDescription"].ToString(),
                               "<.*?>|&.*?;", string.Empty), @"</?span( [^>]*|/)?>", string.Empty), @"</?td( [^>]*|/)?>", string.Empty),
                            HandleNote = reader["HandlerNote"].ToString(),
                            HandleConfirmStatus = reader["HandleStatus"].ToString(),
                            //HandleDateTime =
                            //    reader["HandleDate"] != DBNull.Value ? (DateTime?) reader["HandleDate"] : null,
                            Rating = Convert.ToInt32(reader["Rating"]),
                            NoteIncomplete = reader["ConfirmNote"].ToString(),
                            TATDefault =
                                reader["TATDefault"] != DBNull.Value ? Convert.ToDouble(reader["TATDefault"]) : 0,
                            TAT = reader["TAT"] != DBNull.Value ? Convert.ToDouble(reader["TAT"]) : 0,
                            Expired = reader["IsExpired"].ToString(),
                            Duration1 =
                                reader["DurationLv1"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv1"]) : 0,
                            RealDuration1 =
                                reader["RealDurationLv1"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv1"])
                                    : 0,
                            Duration2 =
                                reader["DurationLv2"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv2"]) : 0,
                            RealDuration2 =
                                reader["RealDurationLv2"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv2"])
                                    : 0,
                            Duration3 =
                                reader["DurationLv3"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv3"]) : 0,
                            RealDuration3 =
                                reader["RealDurationLv3"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv3"])
                                    : 0,
                            Duration4 =
                                reader["DurationLv4"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv4"]) : 0,
                            RealDuration4 =
                                reader["RealDurationLv4"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv4"])
                                    : 0,
                            Duration5 =
                                reader["DurationLv5"] != DBNull.Value ? Convert.ToDouble(reader["DurationLv5"]) : 0,
                            RealDuration5 =
                                reader["RealDurationLv5"] != DBNull.Value
                                    ? Convert.ToDouble(reader["RealDurationLv5"])
                                    : 0,

                            AssignDuration =
                                reader["AssignDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["AssignDuration"])
                                    : 0,
                            AssignRealDuration =
                                reader["AssignRealDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["AssignRealDuration"])
                                    : 0,

                            HandleDuration =
                                reader["HandleDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["HandleDuration"])
                                    : 0,
                            HandleRealDuration =
                                reader["HandleRealDuration"] != DBNull.Value
                                    ? Convert.ToDouble(reader["HandleRealDuration"])
                                    : 0,
                            SurveyContent =
                               Regex.Replace(Regex.Replace(Regex.Replace(reader["SurveyContent"].ToString(),
                               "<.*?>|&.*?;", string.Empty), @"</?span( [^>]*|/)?>", string.Empty), @"</?td( [^>]*|/)?>", string.Empty)                       
                        };
                        reports.Add(reportData);
                    }

                }
                catch (SqlExecutionException seException)
                {
                    this._log.ErrorFormat("Actor:{0} \n {1}", request.UserId, seException.Message);
                }
            }
            if (request.IsDownload)
            {
                fileName = ExcelHelper.ExportToExcelEpp(reports.ToDataTable());

            }
            return new GetReportResponse
            {
                Reports = reports,
                FileNameServer = fileName,
                UploadedDate = DateTime.Now,
                FileType = "xlsx"
            };
        }

        /// <summary>
        /// Get users to report.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUserReportResponse GetUserReport(GetAllUserRequest request)
        {
            //Get all company is manager of BGD department
            IQueryable<int> listCompanyBGD = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                s.UserId == request.UserId && s.Department.IsBOD && (s.IsManager || s.IsReporter) &&
                s.Department.IsActive && s.Department.Company.IsActive).Select(s => s.Department.CompanyId);

            //Get all department User have report role or is manager
            IQueryable<int> depReport = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s =>
                (s.IsReporter && s.UserId == request.UserId) || (s.IsManager && s.UserId == request.UserId) ||
                listCompanyBGD.Any(a => a == s.Department.CompanyId) && s.Department.IsActive &&
                s.Department.Company.IsActive).Select(s => s.DepartmentId).Distinct();

            //Get all groups User is leader
            IQueryable<int> groups = this._unitOfWork.Repo<Group_User>().TableNoTracking.Where(s =>
                    s.Group.IsActive && s.Group.Department.IsActive && s.Group.Department.Company.IsActive
                    && (s.IsLeader && s.UserId == request.UserId) || depReport.Any(a => a == s.Group.DepartmentId))
                .Select(s => s.GroupId).Distinct();

            //get user by all group by role report
            var requestusers = this._unitOfWork.Repo<Group_User>().TableNoTracking
                .Where(s => s.User.IsActive && s.Group.IsActive && s.Group.Department.IsActive &&
                            groups.Any(a => a == s.GroupId)).Select(s => new UserModel
                {
                    UserId = s.User.Id,
                    UserName = s.User.UserName,
                    FullName = s.User.FullName
                }).Distinct().ToList();

            //get all department manager
            var approveusers = this._unitOfWork.Repo<Department_User>().TableNoTracking
                .Where(s => s.User.IsActive && s.Department.IsActive && s.IsManager).Select(s => new UserModel
                {
                    UserId = s.User.Id,
                    UserName = s.User.UserName,
                    FullName = s.User.FullName
                }).Distinct().ToList();

            //get all leader of group
            var assignusers = this._unitOfWork.Repo<Group_User>().TableNoTracking
                .Where(s => s.User.IsActive && s.Group.IsActive && s.Group.Department.IsActive && s.IsLeader).Select(
                    s => new UserModel
                    {
                        UserId = s.User.Id,
                        UserName = s.User.UserName,
                        FullName = s.User.FullName
                    }).Distinct().ToList();

            //get all user in system
            var handleusers = (from u in this._unitOfWork.Repo<User>().TableNoTracking
                               where u.IsActive
                               select new UserModel
                               {
                                   UserId = u.Id,
                                   UserName = u.UserName,
                                   FullName = u.FullName
                               }).Distinct().ToList();

            return new GetUserReportResponse
            {
                RequestUsers = requestusers,
                ApproveUsers = approveusers,
                AssignUsers = assignusers,
                HandleUsers = handleusers
            };
        }
    }
}