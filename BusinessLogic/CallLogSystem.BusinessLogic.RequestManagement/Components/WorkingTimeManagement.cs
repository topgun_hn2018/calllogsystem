﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Common.Enum;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;

    public class WorkingTimeManagement : IWorkingTimeManagement
    {
        private readonly IUnitOfWork _unitOfWork;

        public WorkingTimeManagement(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public SaveResponse SaveWorkingTime(SaveWorkingTimeRequest request)
        {
            WorkingTime workingTime = this._unitOfWork.Repo<WorkingTime>().Table.FirstOrDefault(r => r.CompanyId == request.WorkingTime.CompanyId && r.Year == request.WorkingTime.Year);
            if (workingTime != null)
            {
                workingTime.CompanyId = request.WorkingTime.CompanyId;
                workingTime.HourPerDay = request.WorkingTime.HourPerDay;
                workingTime.Time1 = request.WorkingTime.Time1;
                workingTime.Time2 = request.WorkingTime.Time2;
                workingTime.Time3 = request.WorkingTime.Time3;
                workingTime.Time4 = request.WorkingTime.Time4;
                workingTime.Year = request.WorkingTime.Year;
                this._unitOfWork.Repo<WorkingTime>().Update(workingTime);
            }
            else
            {
                workingTime = new WorkingTime
                {
                    Id = request.WorkingTime.Id,
                    CompanyId = request.WorkingTime.CompanyId,
                    HourPerDay = request.WorkingTime.HourPerDay,
                    Time1 = request.WorkingTime.Time1,
                    Time2 = request.WorkingTime.Time2,
                    Time3 = request.WorkingTime.Time3,
                    Time4 = request.WorkingTime.Time4,
                    Year = request.WorkingTime.Year
                };
                this._unitOfWork.Repo<WorkingTime>().Insert(workingTime);
            }
            
            this._unitOfWork.CommitAsync();
            return new SaveResponse
            {
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public SaveResponse SaveHoliday(SaveHolidayRequest request)
        {
            var holiday = request.Holydays.Select(h => new Holiday
            {
                CompanyId = request.SelectedCompanyId,
                HolidayTypeId = h.HolidayTypeId,
                Note = h.Note,
                Date = DateTime.ParseExact(h.StrDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            }).ToList();
            this._unitOfWork.Repo<Holiday>().Delete(this._unitOfWork.Repo<Holiday>().Table.Where(x => x.CompanyId == request.SelectedCompanyId && x.Date.Year == request.Year && x.Date.Month == request.Month));
            this._unitOfWork.Repo<Holiday>().Insert(holiday);
            this._unitOfWork.CommitAsync();
            return new SaveResponse
            {
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public GetHolidayResponse GetHoliday(GetHolidayRequest request)
        {
            return new GetHolidayResponse
            {
                Holiday =
                    this._unitOfWork.Repo<Holiday>().TableNoTracking.Where(
                        x =>
                            x.CompanyId == request.SelectCompanyId && x.Date.Year == request.Year &&
                            (request.Month == null || x.Date.Month == request.Month)).Select(s => new Contract.Model.HolidayModel
                            {
                                CompanyId = s.CompanyId,
                                Date = s.Date,
                                Note = s.Note,
                                HolidayTypeId = s.HolidayTypeId
                            }).ToList(),
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }
        public GetWorkingTimeResponse GetWorkingTime(GetWorkingTimeRequest request)
        {
            return new GetWorkingTimeResponse
            {
                WorkingTime =
                    this._unitOfWork.Repo<WorkingTime>().TableNoTracking.Where(x => x.CompanyId == request.SelectCompanyId && x.Year == request.Year)
                        .Select(s => new Contract.Model.WorkingTime
                        {
                            CompanyId = s.CompanyId,
                            Id = s.Id,
                            HourPerDay = s.HourPerDay,
                            Year = s.Year,
                            Time1 = s.Time1,
                            Time2 = s.Time2,
                            Time3 = s.Time3,
                            Time4 = s.Time4
                        }).FirstOrDefault(),
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }
    }
}