﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection;
    using Common.Enum;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using log4net;
    using Company = Data.DataAccess.Company;
    using Department = Data.DataAccess.Department;
    using Group = Data.DataAccess.Group;

    public class PartyManagement : IPartyManagement
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public PartyManagement(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public SearchPartyResponse SearchGroup(SearchGroupRequest request)
        {
            var result = new List<PartyModel>();
            if (string.IsNullOrEmpty(request.PartyCode))
            {
                request.PartyCode = string.Empty;
            }

            if (string.IsNullOrEmpty(request.PartyName))
            {
                request.PartyName = string.Empty;
            }

            if (request.CompanySearch == null)
            {
                request.CompanySearch = new List<int>();
            }

            if (request.DepartmentSearch == null)
            {
                request.DepartmentSearch = new List<int>();
            }

            List<PartyModel> groups =
                this._unitOfWork.Repo<Group>().TableNoTracking.Where(x =>
                    (request.IsActive == null || x.IsActive == request.IsActive)
                    && (x.Group_User.Any(gu => gu.UserId == request.GroupUserLeaderId && gu.IsLeader) ||
                        request.GroupUserLeaderId == 0)
                    && (x.Group_User.Any(gu => gu.UserId == request.GroupUserMemberId && !gu.IsLeader) ||
                        request.GroupUserMemberId == 0)
                    && x.GroupCode.Contains(request.PartyCode) &&
                    (!request.CompanySearch.Any() || request.CompanySearch.Contains(-1) ||
                     request.CompanySearch.Contains(x.Department.CompanyId))
                    && (request.DepartmentSearch.Contains(x.DepartmentId) || request.DepartmentSearch.Contains(-1) ||
                        !request.DepartmentSearch.Any()) && x.Name.Contains(request.PartyName)
                ).Select(s => new PartyModel
                {
                    PartyCode = s.GroupCode,
                    PartyId = s.Id,
                    PartyName = s.Name,
                    DepartmentName = s.Department.Name,
                    CompanyName = s.Department.Company.Name,
                    IsActive = s.IsActive
                }).Distinct().ToList();
            result.AddRange(groups);

            return new SearchPartyResponse
            {
                Parties = result
            };
        }


        public SearchPartyResponse SearchParty(SearchPartyRequest request)
        {
            var result = new List<PartyModel>();
            if (string.IsNullOrEmpty(request.PartyCode)) request.PartyCode = string.Empty;
            if (string.IsNullOrEmpty(request.PartyName)) request.PartyName = string.Empty;
            if (request.PartyTypeId == PartyType.All.ToString() || request.PartyTypeId == PartyType.Com.ToString())
            {
                var companies =
                    this._unitOfWork.Repo<Company>().TableNoTracking.Where(x =>
                        (x.IsActive == request.IsActive || request.IsActive == null) &&
                        x.CompanyCode.Contains(request.PartyCode)
                        && x.Name.Contains(request.PartyName)).Select(s => new PartyModel
                    {
                        PartyCode = s.CompanyCode,
                        PartyId = s.Id,
                        PartyName = s.Name,
                        IsActive = s.IsActive
                    }).ToList();
                result.AddRange(companies);
            }
            return new SearchPartyResponse
            {
                Parties = result
            };
        }

        public GetPartyResponse GetParty(GetPartyRequest request)
        {
            var company = new PartyModel { IsActive = true };
            var department = new Departments { IsActive = true };
            var group = new Groups { IsActive = true };
            if (request.PartyTypeId == PartyType.Com.ToString())
            {
                company = this._unitOfWork.Repo<Company>().TableNoTracking.Where(x => x.Id == request.PartyId).Select(s => new PartyModel
                {
                    PartyCode = s.CompanyCode,
                    PartyId = s.Id,
                    PartyName = s.Name,
                    IsActive = s.IsActive
                }).FirstOrDefault();
            }
            if (request.PartyTypeId == PartyType.Dep.ToString())
            {
                if (request.PartyId != 0)
                {
                    department = this._unitOfWork.Repo<Department>().TableNoTracking.Where(x => x.Id == request.PartyId).Select(s => new Departments
                    {
                        PartyCode = s.DepartmentCode,
                        PartyId = s.Id,
                        CompanyId = s.CompanyId,
                        PartyName = s.Name,
                        IsActive = s.IsActive,
                        IsBOD = s.IsBOD
                    }).FirstOrDefault();
                    var departmentManagements = this._unitOfWork.Repo<Department_User>().TableNoTracking
                        .Where(s => s.IsManager && s.DepartmentId == request.PartyId)
                        .Select(s => new DepartmentUserModel
                        {
                            Id = s.Id,
                            DepartmentId = s.DepartmentId,
                            UserId = s.UserId,
                            Name = s.User.UserName,
                        }).ToList();

                    var departmentReporters =
                        this._unitOfWork.Repo<Department_User>().TableNoTracking
                            .Where(s => s.IsReporter && s.DepartmentId == request.PartyId)
                            .Select(s => new DepartmentUserModel
                            {
                                Id = s.Id,
                                DepartmentId = s.DepartmentId,
                                UserId = s.UserId,
                                Name = s.User.UserName,
                            }).ToList();
                    if (department != null)
                    {
                        department.DepartmentManagements = departmentManagements;
                        department.DepartmentReporters = departmentReporters;
                    }
                }
            }
            if (request.PartyTypeId == PartyType.Grp.ToString())
            {
                if (request.PartyId != 0)
                {
                    group = this._unitOfWork.Repo<Group>().TableNoTracking.Where(s => s.Id == request.PartyId).Select(
                        s => new Groups
                        {
                            PartyCode = s.GroupCode,
                            PartyId = s.Id,
                            DepartmentId = s.DepartmentId,
                            CompanyId = s.Department.CompanyId,
                            PartyName = s.Name,
                            IsActive = s.IsActive
                        }).FirstOrDefault();

                    var groupLeaders =
                        this._unitOfWork.Repo<Group_User>().TableNoTracking
                            .Where(s => s.IsLeader && s.User.IsActive && s.GroupId == request.PartyId)
                            .Select(s => new GroupUserModel
                            {
                                Id = s.Id,
                                GroupId = s.GroupId,
                                UserId = s.UserId,
                                Name = s.User.UserName,
                            }).ToList();
                    var groupUsers = this._unitOfWork.Repo<Group_User>().TableNoTracking
                        .Where(s => !s.IsLeader && s.User.IsActive && s.GroupId == request.PartyId)
                        .Select(s => new GroupUserModel
                        {
                            Id = s.Id,
                            GroupId = s.GroupId,
                            UserId = s.UserId,
                            Name = s.User.UserName,
                        }).ToList();
                    if (group != null)
                    {
                        group.GroupLeaders = groupLeaders;
                        group.GroupUsers = groupUsers;
                    }
                }
            }
            return new GetPartyResponse
            {
                Company = company,
                Department = department,
                Group = group
            };
        }

        public GetAllCompanyResponse GetAllCompanies()
        {
            var lstCompanies = this._unitOfWork.Repo<Company>().TableNoTracking.Select(s => new PartyItemModel
            {
                Id = s.Id,
                Name = s.Name,
                IsActive = s.IsActive,
                IsShow = s.IsActive
            }).AsNoTracking().OrderBy(o => o.Name).ToList();
            return new GetAllCompanyResponse
            {
                Companies = lstCompanies,
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public GetAllCompanyResponse GetActiveCompanies()
        {
            var lstCompanies = this._unitOfWork.Repo<Company>().TableNoTracking.Where(t => t.IsActive).Select(s => new PartyItemModel
            {
                Id = s.Id,
                Name = s.Name,
                IsActive = s.IsActive,
                IsShow = s.IsActive
            }).AsNoTracking().OrderBy(o => o.Name).ToList();
            return new GetAllCompanyResponse
            {
                Companies = lstCompanies,
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public GetAllUserResponse GetAllPartyUsers()
        {
            var users = this._unitOfWork.Repo<User>().TableNoTracking.Where(s => s.IsActive).Select(s => new UserAccountModel
            {
                id = s.Id,
                text = s.UserName + " - " + s.FullName,
            }).AsNoTracking().ToList();

            return new GetAllUserResponse
            {
                Users = users.OrderBy(s => s.text).ToList()
            };
        }

        public GetAllUserResponse GetAllDepartmentUserManagers()
        {
            var users = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s=>s.IsManager).Select(s => new UserAccountModel
            {
                id = s.UserId,
                text = s.User.UserName + " - " + s.User.FullName,
            }).AsNoTracking().Distinct().ToList();

            return new GetAllUserResponse
            {
                Users = users.OrderBy(s => s.text).ToList()
            };
        }

        public GetAllUserResponse GetAllDepartmentUserReporters()
        {
            var users = this._unitOfWork.Repo<Department_User>().TableNoTracking.Where(s => s.IsReporter).Select(s => new UserAccountModel
            {
                id = s.UserId,
                text = s.User.UserName + " - " + s.User.FullName,
            }).AsNoTracking().Distinct().ToList();

            return new GetAllUserResponse
            {
                Users = users.OrderBy(s => s.text).ToList()
            };
        }

        public SaveResponse SaveParty(SavePartyRequest request)
        {
            if (request.PartyType == PartyType.Com.ToString())
            {
                Company company;

                if (request.Company.PartyId > 0)
                {
                    company = this._unitOfWork.Repo<Company>().TableNoTracking.First(r => r.Id == request.Company.PartyId);
                    company.IsActive = request.Company.IsActive;
                    company.Name = request.Company.PartyName;
                    company.CompanyCode = request.Company.PartyCode;
                    company.UpdatedBy = request.UserId;
                    company.UpdatedDate = DateTime.Now;
                    this._unitOfWork.Repo<Company>().Update(company);
                }
                else
                {
                    company = new Company()
                    {
                        Id = request.Company.PartyId,
                        Name = request.Company.PartyName,
                        CompanyCode = request.Company.PartyCode,
                        IsActive = request.Company.IsActive,
                        CurrentRequestNo = string.Empty,
                        CreatedBy = request.UserId,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = request.UserId,
                        UpdatedDate = DateTime.Now,

                    };
                    this._unitOfWork.Repo<Company>().Insert(company);
                }

            }
            if (request.PartyType == PartyType.Dep.ToString())
            {
                Department department;
                var departmentUsers = request.Department.SelectedManagements.Select(s => new Department_User
                {
                    DepartmentId = request.Department.PartyId,
                    UserId = s,
                    IsManager = true
                }).ToList();
                foreach (var reporter in request.Department.SelectedReporters)
                {
                    var departmentUser = departmentUsers.FirstOrDefault(x => x.UserId == reporter);
                    if (departmentUser != null)
                    {
                        departmentUser.IsReporter = true;
                    }
                    else
                    {
                        departmentUsers.Add(new Department_User
                        {
                            DepartmentId = request.Department.PartyId,
                            UserId = reporter,
                            IsManager = false,
                            IsReporter = true
                        });
                    }
                }
                if (request.Department.PartyId > 0)
                {
                    department = this._unitOfWork.Repo<Department>().Table.First(r => r.Id == request.Department.PartyId);
                    department.IsActive = request.Department.IsActive;
                    department.Name = request.Department.PartyName;
                    department.CompanyId = request.Department.CompanyId;
                    department.DepartmentCode = request.Department.PartyCode;
                    department.IsBOD = request.Department.IsBOD;
                    department.UpdatedBy = request.UserId;
                    department.UpdatedDate = DateTime.Now;
                    this._unitOfWork.Repo<Department_User>().Delete(this._unitOfWork.Repo<Department_User>().Table.Where(u => u.DepartmentId == request.Department.PartyId));
                    this._unitOfWork.Repo<Department_User>().Insert(departmentUsers);
                    this._unitOfWork.Repo<Department>().Update(department);
                }
                else
                {
                    department = new Department()
                    {
                        Id = request.Department.PartyId,
                        Name = request.Department.PartyName,
                        DepartmentCode = request.Department.PartyCode,
                        CompanyId = request.Department.CompanyId,
                        Department_User = departmentUsers,
                        IsActive = request.Department.IsActive,
                        IsBOD = request.Department.IsBOD,
                        CreatedBy = request.UserId,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = request.UserId,
                        UpdatedDate = DateTime.Now
                    };
                    this._unitOfWork.Repo<Department>().Insert(department);
                }
                
            }
            if (request.PartyType == PartyType.Grp.ToString())
            {
                Group group;
                var groupUsers = request.Group.SelectedGroupUsers.Select(s => new Group_User
                {
                    GroupId = request.Group.PartyId,
                    UserId = s,
                    IsLeader = false
                }).ToList();
                if (request.Group.SelectedGroupLeaders != null)
                    groupUsers.AddRange(request.Group.SelectedGroupLeaders.Select(s => new Group_User
                    {
                        GroupId = request.Group.PartyId,
                        UserId = s,
                        IsLeader = true
                    }).ToList());
                if (request.Group.PartyId > 0)
                {
                    group = this._unitOfWork.Repo<Group>().Table.First(r => r.Id == request.Group.PartyId);
                    group.IsActive = request.Group.IsActive;
                    group.Name = request.Group.PartyName;
                    group.GroupCode = request.Group.PartyCode;
                    group.DepartmentId = request.Group.DepartmentId;
                    group.UpdatedBy = request.UserId;
                    group.UpdatedDate = DateTime.Now;
                    this._unitOfWork.Repo<Group_User>().Delete(this._unitOfWork.Repo<Group_User>().Table.Where(u => u.GroupId == request.Group.PartyId));
                    this._unitOfWork.Repo<Group_User>().Insert(groupUsers);
                    this._unitOfWork.Repo<Group>().Update(group);
                }
                else
                {
                    group = new Group
                    {
                        Id = request.Group.PartyId,
                        Name = request.Group.PartyName,
                        GroupCode = request.Group.PartyCode,
                        DepartmentId = request.Group.DepartmentId,
                        Group_User = groupUsers,
                        IsActive = request.Group.IsActive,
                        CreatedBy = request.UserId,
                        CreatedDate = DateTime.Now,
                        UpdatedBy = request.UserId,
                        UpdatedDate = DateTime.Now
                    };
                    this._unitOfWork.Repo<Group>().Insert(group);
                }
                
            }
            this._unitOfWork.CommitAsync();
            return new SaveResponse
            {
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        /// <summary>
        /// Gets the department by company.
        /// </summary>
        /// <param name="companyIds">The company ids.</param>
        /// <returns></returns>
        public GetDepartmentByCompanyResponse GetDepartmentByCompany(List<int> companyIds)
        {
            if (companyIds == null || !companyIds.Any())
            {
                return new GetDepartmentByCompanyResponse
                {
                    Departments = new List<DepartmentModel>()
                };
            }
            List<DepartmentModel> departments =
                this._unitOfWork.Repo<Department>().TableNoTracking
                    .Where(s => s.IsActive && companyIds.Any(a => a.Equals(s.CompanyId))).Select(s =>
                        new DepartmentModel
                        {
                            CompanyId = s.CompanyId,
                            DepartmentId = s.Id,
                            DepartmentName = s.Name
                        }).Distinct().ToList();
            return new GetDepartmentByCompanyResponse
            {
                Departments = departments.OrderBy(s => s.DepartmentName).ToList()
            };
        }

        /// <summary>
        /// Gets the group by department.
        /// </summary>
        /// <param name="departmentIds">The department identifier.</param>
        /// <returns></returns>
        public GetGroupByDepartmentResponse GetGroupByDepartment(List<int> departmentIds)
        {
            if (departmentIds == null || !departmentIds.Any())
            {
                return new GetGroupByDepartmentResponse
                {
                    Groups = new List<GroupModel>()
                };
            }
            List<GroupModel> groups =
                this._unitOfWork.Repo<Group>().TableNoTracking
                    .Where(s => s.IsActive && departmentIds.Any(a => a.Equals(s.DepartmentId)))
                    .Select(s => new GroupModel
                    {
                        DepartmentId = s.DepartmentId,
                        GroupId = s.Id,
                        GroupName = s.Name
                    }).Distinct().ToList();
            return new GetGroupByDepartmentResponse
            {
                Groups = groups.OrderBy(s => s.GroupName).ToList()
            };
        }

        public SearchPartyResponse SearchDepartment(SearchDepartmentRequest request)
        {
            var result = new List<PartyModel>();
            if (string.IsNullOrEmpty(request.PartyCode))
            {
                request.PartyCode = string.Empty;
            }

            if (string.IsNullOrEmpty(request.PartyName))
            {
                request.PartyName = string.Empty;
            }

            var companies =
                this._unitOfWork.Repo<Department>().TableNoTracking.Where(x =>
                    (x.IsActive == request.IsActive || request.IsActive == null)
                    && (x.Department_User.Any(du => du.UserId == request.Manager && du.IsManager) ||
                        request.Manager == 0)
                    && (x.Department_User.Any(du => du.UserId == request.Reporter && du.IsReporter) ||
                        request.Reporter == 0)
                    && x.DepartmentCode.Contains(request.PartyCode) &&
                    (request.CompanySearch == -1 || x.CompanyId == request.CompanySearch)
                    && x.Name.Contains(request.PartyName)).Select(s => new PartyModel
                {
                    PartyCode = s.DepartmentCode,
                    PartyId = s.Id,
                    PartyName = s.Name,
                    CompanyName = s.Company.Name,
                    IsActive = s.IsActive
                }).ToList();
            result.AddRange(companies);

            return new SearchPartyResponse
            {
                Parties = result
            };
        }
        public GetDepartmentsResponse GetDepartments(GetDepartmentsRequest request)
        {
            if (request.CompanyDepartmentId == null) request.CompanyDepartmentId = new List<int>();
            var departments = this._unitOfWork.Repo<Department>().TableNoTracking.Where(s =>
                s.IsActive && (request.CompanyDepartmentId.Contains(s.CompanyId)
                               || request.CompanyDepartmentId.Count == 0 ||
                               request.CompanyDepartmentId.Contains(-1))).Select(s => new PartyItemModel
            {
                Id = s.Id,
                Name = s.Name
            }).AsNoTracking().OrderBy(o => o.Name).ToList();
            return new GetDepartmentsResponse
            {
                Departments = departments,
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public GetAllUserResponse GetAllGroupUserLeaders()
        {
            var users = this._unitOfWork.Repo<Group_User>().TableNoTracking.Where(s => s.IsLeader).Select(s => new UserAccountModel
            {
                id = s.UserId,
                text = s.User.UserName + " - " + s.User.FullName,
            }).AsNoTracking().Distinct().ToList();

            return new GetAllUserResponse
            {
                Users = users.OrderBy(s => s.text).ToList()
            };
        }

        public GetAllUserResponse GetAllGroupUserMembers()
        {
            var users = this._unitOfWork.Repo<Group_User>().TableNoTracking.Where(s => !s.IsLeader).Select(s => new UserAccountModel
            {
                id = s.UserId,
                text = s.User.UserName + " - " + s.User.FullName,
            }).AsNoTracking().Distinct().ToList();

            return new GetAllUserResponse
            {
                Users = users.OrderBy(s => s.text).ToList()
            };
        }
    }
}