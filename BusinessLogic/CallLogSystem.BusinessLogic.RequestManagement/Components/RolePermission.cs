﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Reflection;
    using Common.Enum;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Interfaces;
    using log4net;
    using System.Linq;
    using Contract.Model;
    using Data.DataAccess.UoW;

    public class RolePermission : IRolePermission
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;

        public RolePermission(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public SaveRoleResponse SaveRole(SaveRoleRequest request)
        {
            var req = this._unitOfWork.Repo<Role>().Table.FirstOrDefault(s => s.Name == request.Role.Name.Trim() && s.Id != request.Role.Id);
            if (req == null)
            {
                var role = new Role
                {
                    Id = request.Role.Id,
                    Name = request.Role.Name,
                    IsActive = request.Role.IsActive,
                    CreatedBy = request.UserId,
                    CreatedDate = DateTime.Now,
                    UpdatedBy = request.UserId,
                    LastUpdate = DateTime.Now
                };

                if (request.Role.Id > 0)
                {
                    role = this._unitOfWork.Repo<Role>().Table.First(r => r.Id == request.Role.Id);
                    role.IsActive = request.Role.IsActive;
                    role.Name = request.Role.Name;
                    role.UpdatedBy = request.UserId;
                    role.LastUpdate = DateTime.Now;
                    this._unitOfWork.Repo<Roles_Permissions>().Delete(this._unitOfWork.Repo<Roles_Permissions>().Table.Where(p => p.RoleId == request.Role.Id));
                }

                this._unitOfWork.Repo<Role>().Insert(role);
                this._unitOfWork.CommitAsync();

                request.Role.Permissions.ForEach(p =>
                {
                    this._unitOfWork.Repo<Roles_Permissions>().Insert(new Roles_Permissions
                    {
                        RoleId = role.Id,
                        PermissonId = p.FunctionActionId
                    });
                });

                this._unitOfWork.CommitAsync();
                return new SaveRoleResponse
                {
                    ErrorType = ErrorType.NoError,
                    ErrorDescription = ""
                };
            }

            this._log.ErrorFormat("Actor:{0} \n {1}", request.UserId, "Role exist!");
            return new SaveRoleResponse
            {
                ErrorType = ErrorType.Warning,
                ErrorDescription = "Role exist!"
            };
        }

        public SearchRoleResponse SearchRole(SearchRoleRequest request)
        {
            var roles = this._unitOfWork.Repo<Role>().TableNoTracking.Select(r => new Contract.Model.RoleModel
            {
                Id = r.Id,
                Name = r.Name,
                IsActive = r.IsActive
            })
                .Where(
                    t =>
                        t.Name.Contains(request.Name) &&
                        (request.IsActive == -1 || t.IsActive == (request.IsActive == 1 ? true : false)))
                .OrderBy(o => o.Name)
                .ToList();

            return new SearchRoleResponse
            {
                Roles = roles,
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public SearchPermissionResponse GetAllPermission()
        {
            var lstPermission = this._unitOfWork.Repo<Permission>().TableNoTracking.Select(fa => new PermissionModel
            {
                FunctionActionId = fa.Id,
                Name = fa.Function.Name + " - " + fa.Action.Name
            }).OrderBy(p => p.Name).ToList();
            return new SearchPermissionResponse
            {
                Permissions = lstPermission,
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public GetRoleByIdResponse GetRoleById(GetRoleByIdRequest request)
        {
            var role = this._unitOfWork.Repo<Role>().TableNoTracking.FirstOrDefault(n => n.Id == request.Id);
            if (role != null)
                return new GetRoleByIdResponse
                {
                    Role = new RoleModel
                    {
                        Id = role.Id,
                        Name = role.Name,
                        IsActive = role.IsActive,
                        Permissions =
                            role.Roles_Permissions.Select(r => new PermissionModel
                            {
                                Id = r.Id,
                                RoleId = r.RoleId,
                                FunctionActionId = r.PermissonId,
                                Name = r.Permission.Function.Name + " - " + r.Permission.Action.Name
                            }).ToList()
                    },
                    ErrorDescription = "",
                    ErrorType = ErrorType.NoError
                };
            return null;
        }
    }
}