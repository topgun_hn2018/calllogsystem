﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Common.Constant;
    using Common.ContractBase;
    using Common.Enum;
    using Common.Helper;
    using Common.Utilites;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using log4net;
    using EmailInformTo = Data.DataAccess.EmailInformTo;

    public class EmailManagement : IEmailManagement
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;
        public EmailManagement(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public SearchEmailTemplateResponse SearchEmailTemplate(SearchEmailTemplateRequest request)
        {
            var emailTemplate = this._unitOfWork.Repo<EmailTemplate>().TableNoTracking.Where(
                e =>
                    e.Name.Contains(request.Name) && e.Subject.Contains(request.Subject) &&
                    (request.IsActive == -1 || e.IsActive == request.IsActive > 0 ? true : false)
                    && (request.StatusToId == -1 || e.StatusToId == request.StatusToId)).OrderBy(s => s.Name).ToList();

            var response = new SearchEmailTemplateResponse
            {
                EmailTemplates = new List<EmailTemplateModel>()
            };
            emailTemplate.ForEach(e =>
            {
                var template = new EmailTemplateModel
                {
                    Id = e.Id,
                    Name = e.Name,
                    IsActive = e.IsActive,
                    Subject = e.Subject
                };
                var requestStatus = this._unitOfWork.Repo<RequestStatus>().TableNoTracking.FirstOrDefault(s => s.Id == e.StatusToId);
                if (requestStatus != null)
                {
                    template.StatusToName = requestStatus.Name;
                }
                response.EmailTemplates.Add(template);
            });
            return response;
        }

        public SaveEmailTemplateResponse SaveEmailTemplate(SaveEmailTemplateRequest request)
        {
            var emailTemplate = new EmailTemplate
            {
                Id = request.EmailTemplate.Id,
                Name = request.EmailTemplate.Name,
                Subject = request.EmailTemplate.Subject,
                StatusToId = request.EmailTemplate.StatusToId,
                Content = request.EmailTemplate.Content,
                IsActive = request.EmailTemplate.IsActive,
                CreatedDate = request.EmailTemplate.CreatedDate,
                UpdatedBy = request.UserId,
                UpdatedDate = DateTime.UtcNow
            };

            if (request.EmailTemplate.Id > 0)
            {
                this._unitOfWork.Repo<EmailInformTo>().Delete(
                    this._unitOfWork.Repo<EmailInformTo>().Table.Where(p => p.EmailTemplateId == request.EmailTemplate.Id));
            }
            else
            {
                emailTemplate.CreatedBy = request.UserId;
                emailTemplate.CreatedDate = DateTime.UtcNow;
            }

            this._unitOfWork.Repo<EmailTemplate>().Insert(emailTemplate);
            request.EmailTemplate.EmailCCs.ForEach(p =>
            {
                this._unitOfWork.Repo<EmailInformTo>().Insert(new EmailInformTo
                {
                    UserId = p.UserId,
                    EmailTemplateId = emailTemplate.Id
                });
            });

            this._unitOfWork.CommitAsync();
            return new SaveEmailTemplateResponse
            {
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }

        public GetEmailTemplateByIdResponse GetEmailTemplateById(GetEmailTemplateByIdRequest request)
        {
            var emailTemplate = this._unitOfWork.Repo<EmailTemplate>().TableNoTracking.FirstOrDefault(n => n.Id == request.Id);
            if (emailTemplate != null)
            {
                return new GetEmailTemplateByIdResponse
                {
                    EmailTemplate = new EmailTemplateModel
                    {
                        Id = emailTemplate.Id,
                        Name = emailTemplate.Name,
                        IsActive = emailTemplate.IsActive,
                        Subject = emailTemplate.Subject,
                        Content = emailTemplate.Content,
                        StatusToId = emailTemplate.StatusToId,
                        CreatedDate = emailTemplate.CreatedDate,
                        EmailCCs = emailTemplate.EmailInformToes.Select(a => new EmailInformToModel
                        {
                            Id = a.Id,
                            UserId = a.UserId,
                            EmailTemplateId = request.Id
                        }).ToList()
                    },
                    ErrorDescription = "",
                    ErrorType = ErrorType.NoError
                };
            }
                
            return new GetEmailTemplateByIdResponse
            {
                EmailTemplate = new EmailTemplateModel { }
            };
        }

        public PreSendEmailTemplateResponse PreSendEmailTemplate(PreSendEmailTemplateRequest request)
        {
            List<string> toList = new List<string> { request.EmailTemplate.EmailSendTo };
            List<string> ccList = new List<string>();
            var emailSender = ConfigUtilities.GetStringKey(ConfigurationConstant.SmtpSentFrom, "test@fdev.local");
            var feedback = Emailer.SendEmail(emailSender, toList, ccList, request.EmailTemplate.Subject,
                request.EmailTemplate.Content);

            return new PreSendEmailTemplateResponse
            {
                ErrorDescription = feedback.ErrorDescription,
                ErrorType = feedback.ErrorType
            };
        }

        public BaseResponse SendEmailAfterCloseRequestAutomatically(int requestId)
        {
            //var response = this._commonMethods.SendEmail(requestId, Constant.RequestStatus.Completed);
            //return response;
            return new BaseResponse();
        }
    }
}