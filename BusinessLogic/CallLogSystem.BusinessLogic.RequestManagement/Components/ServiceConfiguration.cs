﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Common.ContractBase;
    using Common.Enum;
    using Common.Extension;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using log4net;

    public class ServiceConfiguration : IServiceConfiguration
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUnitOfWork _unitOfWork;
        public ServiceConfiguration(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }



        /// <summary>
        /// Gets all services
        /// </summary>
        /// <returns></returns>
        public GetAllServiceResponse GetAllServices(GetAllServiceRequest request)
        {
            var services = this._unitOfWork.Repo<Service>().TableNoTracking.Where(s => s.RequestType.IsActive).Select(
                s => new ServiceModel
                {
                    RequestTypeId = s.RequestType.Id,
                    Name = s.RequestType.Name,
                    Id = s.Id,
                    Description = s.Description,
                    RequestTemplate = s.RequestTemplate,
                    IsActiveService = s.IsActive,
                    IsProvidedByCurrentCompany =
                        (!s.RequestType.ProcessBySite || (s.Group_Service.Any(c => c.CompanyId == request.CompanyId)))
                }).ToList().DistinctBy(s => s.Name).ToList();

            foreach (var service in services)
            {
                var companyIds = this._unitOfWork.Repo<Group_Service>().TableNoTracking
                    .Where(s => s.ServiceId == service.Id).Select(s => s.CompanyId).ToList();

                service.ProvidedCompany = this._unitOfWork.Repo<Company>().TableNoTracking
                    .Where(c => companyIds.Contains(c.Id))
                    .Select(c => c.Name)
                    .ToList();
            }

            return new GetAllServiceResponse
            {
                Services = services
            };
        }


        /// <summary>
        /// Searches the service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SearchServiceResponse SearchService(SearchServiceRequest request)
        {
            bool isActive = request.IsActive == 0;
            if (request.IsActive == 1)
            {
                isActive = false;
            }

            var searchResult = (from service in this._unitOfWork.Repo<Service>().TableNoTracking 
                                join groupService in this._unitOfWork.Repo<Group_Service>().TableNoTracking on service.Id equals groupService.ServiceId
                                join company in this._unitOfWork.Repo<Company>().TableNoTracking on groupService.CompanyId equals company.Id
                                join department in this._unitOfWork.Repo<Department>().TableNoTracking on groupService.DepartmentId equals department.Id
                                join @group in this._unitOfWork.Repo<Group>().TableNoTracking on groupService.GroupId equals @group.Id
                                where
                                    (request.RequestTypeId == -1 || service.RequestType.Id == request.RequestTypeId) &&
                                    (request.IsActive == -1 || service.RequestType.IsActive == isActive) &&
                                    (request.CompanyId == -1 || groupService.CompanyId == request.CompanyId) &&
                                    (request.DepartmentId == -1 || groupService.DepartmentId == request.DepartmentId) &&
                                    (request.GroupId == -1 || groupService.GroupId == request.GroupId)
                                select new ServiceSearch
                                {
                                    ServiceId = service.Id,
                                    CompanyName = company.Name,
                                    DepartmentName = department.Name,
                                    GroupName = @group.Name,
                                    RequestTypeName = service.RequestType.Name,
                                    IsActive = service.IsActive
                                }).ToList();
            return new SearchServiceResponse
            {
                Services = searchResult
            };
        }

        /// <summary>
        /// Gets the service by identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns></returns>
        public GetServiceByIdResponse GetServiceById(int serviceId)
        {
            GetServiceByIdResponse service =
                this._unitOfWork.Repo<Service>().TableNoTracking
                    .Where(s => s.Id == serviceId)
                    .Select(s => new GetServiceByIdResponse
                    {
                        RequestTypeId = s.RequestTypeId,
                        Description = s.Description,
                        IsApprovedByManager = s.IsApprovedByManager,
                        IsActive = s.IsActive,
                        TAT = s.TAT,
                        CompanyIds = s.Group_Service.Select(a => a.CompanyId).ToList(),
                        DepartmentIds = s.Group_Service.Select(a => a.DepartmentId).ToList(),
                        GroupIds = s.Group_Service.Select(a => a.GroupId).ToList(),
                        ApprovalLevel2 = s.ApprovalLevels.Where(a => a.ApprovalLevelType == 2).Select(a => a.DepartmentId).ToList(),
                        ApprovalLevel3 = s.ApprovalLevels.Where(a => a.ApprovalLevelType == 3).Select(a => a.DepartmentId).ToList(),
                        ApprovalLevel4 = s.ApprovalLevels.Where(a => a.ApprovalLevelType == 4).Select(a => a.DepartmentId).ToList(),
                        ApprovalLevel5 = s.ApprovalLevels.Where(a => a.ApprovalLevelType == 5).Select(a => a.DepartmentId).ToList(),
                        RequestTemplate = s.RequestTemplate,    
                        SurveyTemplate = s.SurveyTemplate                    
                    }).FirstOrDefault();

            if (service == null)
            {
                return new GetServiceByIdResponse
                {
                    ErrorType = ErrorType.Error
                };
            }
            return service;
        }

        /// <summary>
        /// Saves the service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveServiceResponse SaveService(SaveServiceRequest request)
        {
            if (request.ServiceId == 0)
            {
                //BaseResponse validationResult = this._commonMethods.ValidateService(request);
                //if (validationResult.ErrorType == ErrorType.Error)
                //{
                //    return new SaveServiceResponse
                //    {
                //        ErrorType = ErrorType.Error,
                //        ErrorDescription = validationResult.ErrorDescription
                //    };
                //}
            }
            double? tat = request.TAT ??
                          this._unitOfWork.Repo<RequestType>().Table.Where(s => s.Id == request.RequestTypeId)
                              .Select(s => s.DefaultTAT)
                              .First();
            var service = new Data.DataAccess.Service
            {
                Id = request.ServiceId == 0 ? 0 : request.ServiceId,
                RequestTypeId = request.RequestTypeId,
                Description = request.Description,
                RequestTemplate = request.RequestTemplate,
                SurveyTemplate = request.SurveyTemplate,
                IsActive = request.IsActive,
                IsApprovedByManager = request.IsApprovedByManager,
                TAT = tat,
                CreatedDate = DateTime.Now,
                CreatedBy = request.UserId,
                UpdatedDate = DateTime.Now,
                UpdatedBy = request.UserId
            };
            this._unitOfWork.Repo<Service>().Insert(service);
            this._unitOfWork.CommitAsync();
            foreach (var groupId in request.GroupIds)
            {
                GroupServiceModel groupService = this._unitOfWork.Repo<Group_Service>().Table
                    .Where(s => s.GroupId == groupId).Select(s => new GroupServiceModel
                    {
                        CompanyId = s.Group.Department.CompanyId,
                        DepartmentId = s.Group.DepartmentId,
                        GroupId = groupId
                    }).FirstOrDefault();
                if (groupService != null)
                {
                    if (request.ServiceId != 0)
                    {
                        this._unitOfWork.Repo<Group_Service>().Delete(this._unitOfWork.Repo<Group_Service>().Table.Where(s => s.ServiceId == service.Id));
                    }
                    this._unitOfWork.Repo<ApprovalLevel>().Delete(this._unitOfWork.Repo<ApprovalLevel>().Table.Where(s => s.ServiceId == service.Id));
                    //this._commonMethods.CreateApprovalLevel(service.Id, request.ApprovalLevel2.Distinct().ToList(), 2);
                    //this._commonMethods.CreateApprovalLevel(service.Id, request.ApprovalLevel3.Distinct().ToList(), 3);
                    //this._commonMethods.CreateApprovalLevel(service.Id, request.ApprovalLevel4.Distinct().ToList(), 4);
                    //this._commonMethods.CreateApprovalLevel(service.Id, request.ApprovalLevel5.Distinct().ToList(), 5);

                    this._unitOfWork.Repo<Group_Service>().Insert(new Group_Service
                    {
                        ServiceId = service.Id,
                        CompanyId = groupService.CompanyId,
                        DepartmentId = groupService.DepartmentId,
                        GroupId = groupService.GroupId
                    });
                }
            }
            this._unitOfWork.CommitAsync();
            return new SaveServiceResponse();
        }
    }
}