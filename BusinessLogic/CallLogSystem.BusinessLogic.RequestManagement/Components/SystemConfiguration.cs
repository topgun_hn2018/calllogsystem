﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Linq;
    using Common.Enum;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;

    public class SystemConfiguration : ISystemConfiguration
    {
        private readonly IUnitOfWork _unitOfWork;

        public SystemConfiguration(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public GetSystemConfigurationResponse GetSystemConfiguration()
        {
            var sysparams = this._unitOfWork.Repo<SystemParameter>().TableNoTracking.Select(a => new SystemParameterModel
            {
                Id = a.Id,
                Key = a.Key,
                Value = a.Value,
                IsActive = a.IsActive
            }).ToList();
            return new GetSystemConfigurationResponse
            {
                SystemParameters = sysparams
            };
        }

        public SaveSystemConfigurationResponse SaveSystemConfiguration(SaveSystemConfigurationRequest request)
        {
            request.SystemParameters.ForEach(s =>
            {
                var sysparam = new Data.DataAccess.SystemParameter
                {
                    Id = s.Id,
                    Key = s.Key,
                    IsActive = s.IsActive,
                    Value = s.Value,
                    UpdatedBy = request.UserId,
                    UpdatedDate = DateTime.Now
                };
                this._unitOfWork.Repo<SystemParameter>().Insert(sysparam);
            });

            this._unitOfWork.CommitAsync();
            return new SaveSystemConfigurationResponse
            {
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }
    }
}