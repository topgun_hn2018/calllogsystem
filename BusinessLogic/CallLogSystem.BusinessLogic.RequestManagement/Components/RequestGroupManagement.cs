﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Components
{
    using System;
    using System.Linq;
    using System.Web;
    using Common.Enum;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;
    using Interfaces;
    using File = System.IO.File;

    public class RequestGroupManagement : IRequestGroupManagement
    {
        private readonly IUnitOfWork _unitOfWork;

        public RequestGroupManagement(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get all Category
        /// </summary>
        /// <returns></returns>
        public SearchCategoriesResponse GetAllCategory()
        {
            var listCategories = this._unitOfWork.Repo<Category>().TableNoTracking.Select(c => new CategoryModel
            {
                Id = c.Id,
                Name = c.Name,
                OrderNum = c.Order,
                AvatarSource = c.AvatarSource,
                RequestTypes =
                    c.RequestTypes.Select(n => new RequestTypeModel
                    {
                        Id = n.Id,
                        Name = n.Name
                    }).ToList()
            }).OrderBy(r => r.OrderNum).ToList();

            foreach (CategoryModel t in listCategories)
            {
                if (t.AvatarSource != null)
                {
                    t.AvatarSource = File.Exists(HttpContext.Current.Server.MapPath("/content/image" + '/' + t.AvatarSource)) ? string.Format("{0}/{1}", "/content/image", t.AvatarSource) : null;
                }
                else
                {
                    t.AvatarSource = null;
                }
            }

            return new SearchCategoriesResponse
            {
                Categories = listCategories
            };
        }

        public SearchCategoriesResponse SearchCategories(SearchCategoriesRequest request)
        {
            if (string.IsNullOrEmpty(request.Name)) request.Name = string.Empty;
            var listCategories = this._unitOfWork.Repo<Category>().TableNoTracking.Where(x=>x.Name.Contains(request.Name)&&( x.IsActive==request.IsActive||request.IsActive==null)).Select(c => new CategoryModel
            {
                Id = c.Id,
                Name = c.Name,
                OrderNum = c.Order,
                IsActive = c.IsActive
            }).OrderBy(x => x.OrderNum).ToList();

            return new SearchCategoriesResponse()
            {
                Categories = listCategories
            };
        }

        /// <summary>
        /// Get all Category active
        /// </summary>
        /// <returns></returns>
        public SearchCategoriesResponse GetAllCategoryActive(SearchCategoriesRequest request)
        {
            var listCategories = this._unitOfWork.Repo<Category>().TableNoTracking.Where(s => s.IsActive).Select(c => new CategoryModel
            {
                Id = c.Id,
                Name = c.Name,
                OrderNum = c.Order,
                AvatarSource = c.AvatarSource,
                RequestTypes =
                    c.RequestTypes.Where(t => t.IsActive && t.CategoryId == c.Id).Select(n => new RequestTypeModel
                    {
                        Id = n.Id,                        
                        Name = n.Order != null ? n.Order + " " + n.Name : n.Name
                    }).OrderBy(s => s.Name).ToList()
            }).OrderBy(r => r.OrderNum).ToList();            

            foreach (CategoryModel t in listCategories)
            {
                if (t.AvatarSource != null)
                {
                    t.AvatarSource = File.Exists(HttpContext.Current.Server.MapPath("/content/image" + '/' + t.AvatarSource)) ? string.Format("{0}/{1}", "/content/image", t.AvatarSource) : null;
                }
                else
                {
                    t.AvatarSource = null;
                }
            }

            return new SearchCategoriesResponse()
            {
                Categories = listCategories
            };
        }

        public GetCategoryByIdResponse GetCategoryById(GetByIdRequest request)
        {
            return new GetCategoryByIdResponse
            {
                Category = this._unitOfWork.Repo<Category>().TableNoTracking.Where(x => x.Id == request.Id).Select(c => new CategoryModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    AvatarSource = c.AvatarSource,
                    IsActive = c.IsActive,
                    OrderNum = c.Order
                }).FirstOrDefault(),
                ErrorDescription = string.Empty,
                ErrorType = ErrorType.NoError
            };
        }

        public SaveResponse SaveCategory(SaveCategoryRequest request)
        {
            Category category;
            if (request.Category.Id > 0)
            {
                category = this._unitOfWork.Repo<Category>().Table.First(r => r.Id == request.Category.Id);
                if (category.AvatarSource != request.Category.AvatarSource)
                    category.AvatarUpdatedDate = DateTime.Now;
                category.IsActive = request.Category.IsActive;
                category.Name = request.Category.Name;
                category.Order = request.Category.OrderNum;
                category.AvatarSource = request.Category.AvatarSource;
                category.UpdatedBy = request.UserId;
                category.UpdatedDate = DateTime.Now;
                this._unitOfWork.Repo<Category>().Update(category);
            }
            else
            {
                category = new Category
                {
                    Id = request.Category.Id,
                    Name = request.Category.Name,
                    IsActive = request.Category.IsActive,
                    Order = request.Category.OrderNum,
                    AvatarSource = request.Category.AvatarSource,
                    CreatedBy = request.UserId,
                    CreatedDate = DateTime.Now,
                    AvatarUpdatedDate = DateTime.Now,
                    UpdatedBy = request.UserId,
                    UpdatedDate = DateTime.Now
                };
                this._unitOfWork.Repo<Category>().Insert(category);
            }
            
            this._unitOfWork.CommitAsync();
            return new SaveResponse
            {
                ErrorType = ErrorType.NoError,
                ErrorDescription = ""
            };
        }
    }     
}