﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Caching;
    using Contract.Model;
    using Data.DataAccess;

    public static class KeyObjectUtilities<T>
    {
        /// <summary>
        /// Gets the list key objects.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        public static List<KeyObjectModel> GetListKeyObjects(string tableName)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddHours(1.0)
            };
            var result = new List<KeyObjectModel>();
            if (string.IsNullOrEmpty(tableName))
            {
                return result;
            }
            if (cache.Contains(tableName))
            {
                //Get data from the cache
                return (List<KeyObjectModel>) cache.Get(tableName);
            }
            using (var db = new CallLogEntities())
            {
                // Store data in the cache    
                var cacheKey = db.KeyObjects.Where(s => s.TableName == tableName).Select(s => new KeyObjectModel
                {
                    Key = s.Key,
                    Value = s.Value
                }).ToList();
                cache.Add(tableName, cacheKey, cacheItemPolicy);
                return cacheKey;
            }

        }


        /// <summary>
        /// Gets the key object.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static T GetKeyObject(string tableName, string key)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddHours(1.0)
            };
            if (string.IsNullOrEmpty(tableName) || string.IsNullOrEmpty(key))
            {
                return default(T);
            }
            if (cache.Contains(string.Concat(tableName, key)))
            {
                //Get data from the cache
                return (T) cache.Get(string.Concat(tableName, key));
            }

            using (var db = new CallLogEntities())
            {
                object value =
                    db.KeyObjects.Where(s => s.TableName == tableName && s.Key == key)
                        .Select(s => s.Value)
                        .FirstOrDefault();
                if (value != null)
                {
                    // Store data in the cache   
                    cache.Add(string.Concat(tableName, key), (T) Convert.ChangeType(value, typeof (T)), cacheItemPolicy);
                }
                return (T) Convert.ChangeType(value, typeof (T));
            }
        }

    }
}