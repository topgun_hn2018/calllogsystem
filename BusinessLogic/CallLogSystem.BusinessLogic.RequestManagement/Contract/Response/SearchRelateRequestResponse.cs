﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SearchRelateRequestResponse:BaseResponse
    {
        public List<Model.RequestModel> Requests { get; set; }
        public List<Model.RequestModel> Filters { get; set; }
    }
}
