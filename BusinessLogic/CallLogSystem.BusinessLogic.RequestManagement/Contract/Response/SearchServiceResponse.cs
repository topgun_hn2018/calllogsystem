﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchServiceResponse:BaseResponse
    {
        public List<ServiceSearch> Services { get; set; }
    }
}