﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchPartyResponse : BaseResponse
    {
        public List<PartyModel> Parties { get; set; }
    }
}
