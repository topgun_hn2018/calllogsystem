﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetChildRequestResponse:BaseResponse
    {
        public List<Model.RequestModel> Requests { get; set; }
    }
}
