﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;

    public class GetCategoryByIdResponse: BaseResponse
    {
        public Model.CategoryModel  Category { get; set; }
    }
}
