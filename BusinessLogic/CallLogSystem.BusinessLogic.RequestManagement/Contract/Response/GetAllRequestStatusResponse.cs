﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllRequestStatusResponse:BaseResponse
    {
        public List<RequestStatusModel> RequestStatus { get; set; }
    }
}