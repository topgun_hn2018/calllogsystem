﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchRoleResponse:BaseResponse
    {
        public List<RoleModel> Roles { get; set; }
    }
}
