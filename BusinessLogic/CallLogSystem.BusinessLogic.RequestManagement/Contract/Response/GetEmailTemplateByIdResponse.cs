﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetEmailTemplateByIdResponse : BaseResponse
    {
        public EmailTemplateModel EmailTemplate { get; set; }
    }
}
