﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchQuestionResponse : BaseResponse
    {
        public List<QuestionModel> Questions { get; set; }
    }
}
