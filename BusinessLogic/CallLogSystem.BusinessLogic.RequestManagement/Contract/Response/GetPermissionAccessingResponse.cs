﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetPermissionAccessingResponse:BaseResponse
    {
        public PermissionAccessingModel PermissionAccessing { get; set; }
    }
}