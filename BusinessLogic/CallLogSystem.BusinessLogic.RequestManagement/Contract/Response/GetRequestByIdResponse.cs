﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;

    public class GetRequestByIdResponse:BaseResponse
    {
        public Model.RequestModel Request { get; set; } 
    }
}