﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetDepartmentByUserResponse:BaseResponse
    {
        public List<Model.DepartmentModel> Departments { get; set; }
    }
}
