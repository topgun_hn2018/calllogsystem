﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SearchCategoriesResponse: BaseResponse
    {
        public List<Model.CategoryModel> Categories { get; set; }
    }
}
