﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class DeleteAliasResponse : BaseResponse
    {
        public List<RoleModel> Roles { get; set; }
        public List<AliasModel> Aliases { get; set; } 
    }
}
