﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;

    public class GetRequestTypeByIdResponse : BaseResponse
    {
        public Model.RequestTypeModel RequestType { get; set; }
    }
}
