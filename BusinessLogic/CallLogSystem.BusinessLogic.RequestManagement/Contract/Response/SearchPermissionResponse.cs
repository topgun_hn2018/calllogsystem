﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchPermissionResponse : BaseResponse
    {
        public List<PermissionModel> Permissions { get; set; }
    }
}
