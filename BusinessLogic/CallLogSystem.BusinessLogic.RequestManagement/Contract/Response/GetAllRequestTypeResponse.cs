﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllRequestTypeResponse:BaseResponse
    {
        public List<RequestTypeModel> RequestTypes { get; set; }
    }
}