﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetServiceByIdResponse:BaseResponse
    {
        public int RequestTypeId { get; set; }
        public List<int> CompanyIds { get; set; }
        public List<int> DepartmentIds { get; set; }
        public List<int> GroupIds { get; set; }
        public bool IsApprovedByManager { get; set; }
        public List<int> ApprovalLevel2 { get; set; }
        public List<int> ApprovalLevel3 { get; set; }
        public List<int> ApprovalLevel4 { get; set; }
        public List<int> ApprovalLevel5 { get; set; }
        public string Description { get; set; }
        public double? TAT { get; set; }
        public bool IsActive { get; set; }
        public string RequestTemplate { get; set; }
        public string SurveyTemplate { get; set; }
    }
}