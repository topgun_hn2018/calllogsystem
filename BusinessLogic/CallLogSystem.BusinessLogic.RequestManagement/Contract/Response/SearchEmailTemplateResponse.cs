﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchEmailTemplateResponse : BaseResponse
    {
        public List<EmailTemplateModel> EmailTemplates { get; set; }
    }
}
