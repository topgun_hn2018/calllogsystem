﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SearchResponse:BaseResponse
    {
        public List<Model.RequestModel> Requests { get; set; }
    }
}