﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetSystemConfigurationResponse : BaseResponse
    {
        public List<SystemParameterModel> SystemParameters { get; set; } 
    }
}