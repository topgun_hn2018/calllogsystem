﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetGroupByDepartmentResponse:BaseResponse
    {
        public List<GroupModel> Groups { get; set; }
    }
}