﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllFunctionResponse : BaseResponse
    {
        public List<FunctionModel> Functions { get; set; }
    }
}
