﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;

    public class GetRequestAssignResponse:BaseResponse
    {
        public string ProcessDescription { get; set; }
        public string Note { get; set; }
        public bool IsWaittingForAssign { get; set; }
        public bool IsWaittingForProcess { get; set; }
        public bool IsCompleted { get; set; }
    }
}