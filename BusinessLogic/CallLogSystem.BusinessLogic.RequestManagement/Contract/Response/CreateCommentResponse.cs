﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System;
    using Common.ContractBase;

    public class CreateCommentResponse:BaseResponse
    {
        public string UserName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}