﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllQuestionTypesResponse : BaseResponse
    {
        public List<QuestionTypeModel> QuestionTypes { get; set; }
    }
}
