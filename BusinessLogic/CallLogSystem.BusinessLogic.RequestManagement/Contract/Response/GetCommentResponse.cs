﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetCommentResponse:BaseResponse
    {
        public List<HistoryModel> Histories { get; set; }
        public bool IsSubscriber { get; set; }
    }
}