﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetPermissionResponse:BaseResponse
    {
        public ActionModel Actions { get; set; }
    }
}