﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SearchRequestTypeResponse : BaseResponse
    {
        public List<Model.RequestTypeModel> RequestTypes { get; set; }
    }
}
