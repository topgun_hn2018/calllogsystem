﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;

    public class GetQuestionsByIdResponse : BaseResponse
    {
        public Model.QuestionModel question { get; set; } 
    }
}