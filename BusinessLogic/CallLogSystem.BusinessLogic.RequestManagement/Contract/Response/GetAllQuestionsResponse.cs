﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllQuestionsResponse : BaseResponse
    {
        public List<QuestionModel> Questions { get; set; }
    }
}
