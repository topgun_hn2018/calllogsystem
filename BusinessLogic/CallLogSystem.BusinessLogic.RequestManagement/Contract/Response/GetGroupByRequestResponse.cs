﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetGroupByRequestResponse:BaseResponse
    {
        public List<GroupByRequest> Groups { get; set; }
    }
}