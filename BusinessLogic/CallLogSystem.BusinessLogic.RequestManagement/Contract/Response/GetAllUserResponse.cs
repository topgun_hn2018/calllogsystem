﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllUserResponse:BaseResponse
    {
        public List<UserAccountModel> Users { get; set; }
    }
}