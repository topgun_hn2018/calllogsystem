﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetWorkingTimeResponse:BaseResponse
    {
        public WorkingTime WorkingTime { get; set; }
    }
}