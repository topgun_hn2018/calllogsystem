﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllCompanyResponse : BaseResponse
    {
        public List<PartyItemModel> Companies { get; set; }
    }
}
