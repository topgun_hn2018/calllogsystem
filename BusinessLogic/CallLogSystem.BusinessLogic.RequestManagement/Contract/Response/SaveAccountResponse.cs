﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;

    public class SaveAccountResponse : BaseResponse
    {
        public int UserId { get; set; }
    }
}
