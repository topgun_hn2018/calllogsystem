﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetUserReportResponse:BaseResponse
    {
        public List<UserModel> RequestUsers { get; set; }
        public List<UserModel> ApproveUsers { get; set; }
        public List<UserModel> AssignUsers { get; set; }
        public List<UserModel> HandleUsers { get; set; }
    }
}
