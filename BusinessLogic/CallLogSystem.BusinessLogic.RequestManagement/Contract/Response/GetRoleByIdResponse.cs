﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetRoleByIdResponse : BaseResponse
    {
        public RoleModel Role { get; set; }
    }
}
