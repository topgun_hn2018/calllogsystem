﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetApprovalLevelResponse:BaseResponse
    {
        public List<ApprovalLevelModel> ApprovalLevels { get; set; }
        public int CurrentApprovalLevel { get; set; }
    }
}