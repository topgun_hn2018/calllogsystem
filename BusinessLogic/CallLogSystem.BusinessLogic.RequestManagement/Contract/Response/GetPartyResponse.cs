﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetPartyResponse : BaseResponse
    {
        public PartyModel Company { get; set; }
        public Departments Department { get; set; }
        public Groups Group { get; set; }
    }
}
