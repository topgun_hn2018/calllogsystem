﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System;
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetReportResponse:BaseResponse
    {
        public List<Model.ReportModel> Reports { get; set; }

        public string FileNameServer { get; set; }

        public DateTime UploadedDate { get; set; }

        public string FileType { get; set; }
    }
}
