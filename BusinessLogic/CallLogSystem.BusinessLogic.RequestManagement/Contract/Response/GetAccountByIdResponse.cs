﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetAccountByIdResponse : BaseResponse
    {
        public UserModel Account { get; set; }
    }
}
