﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetHolidayResponse:BaseResponse
    {
        public List<HolidayModel> Holiday { get; set; }
    }
}