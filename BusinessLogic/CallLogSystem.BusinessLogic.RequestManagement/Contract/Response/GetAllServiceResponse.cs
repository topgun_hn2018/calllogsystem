﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetAllServiceResponse:BaseResponse
    {
        public List<ServiceModel> Services { get; set; }
    }
}