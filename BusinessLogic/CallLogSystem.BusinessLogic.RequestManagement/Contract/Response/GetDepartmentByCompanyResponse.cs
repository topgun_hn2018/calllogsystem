﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetDepartmentByCompanyResponse:BaseResponse
    {
        public List<DepartmentModel> Departments { get; set; }
    }
}