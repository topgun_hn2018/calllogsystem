﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SearchAccountResponse : BaseResponse
    {
        public IEnumerable<UserModel> Accounts { get; set; }
    }
}
