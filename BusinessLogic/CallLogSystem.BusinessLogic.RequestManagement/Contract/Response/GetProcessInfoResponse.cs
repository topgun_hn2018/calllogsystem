﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetProcessInfoResponse:BaseResponse
    {
        public string Description { get; set; }
        public List<Model.RequestModel> Requests { get; set; }
        public string Note { get; set; }
        public string LeaderName { get; set; }
        public string Assignee { get; set; }
    }
}