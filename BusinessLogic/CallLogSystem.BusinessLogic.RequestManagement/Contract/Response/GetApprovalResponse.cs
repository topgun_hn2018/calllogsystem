﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetApprovalResponse:BaseResponse
    {
        public string Note{ get; set; }
        public List<FileModel> Files{ get; set; }
        public bool IsApproved { get; set; }
    }
}