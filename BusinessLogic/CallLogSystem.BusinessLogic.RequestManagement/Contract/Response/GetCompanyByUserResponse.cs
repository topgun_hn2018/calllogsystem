﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetCompanyByUserResponse:BaseResponse
    {
        public List<Model.CompanyModel> Companies { get; set; }
    }
}
