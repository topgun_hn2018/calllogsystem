﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class GetAliasByIdResponse : BaseResponse
    {
        public AliasModel Alias { get; set; }
    }
}
