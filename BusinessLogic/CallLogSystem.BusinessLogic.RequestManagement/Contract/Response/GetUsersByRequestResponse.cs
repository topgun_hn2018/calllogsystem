﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class GetUsersByRequestResponse:BaseResponse
    {
        public List<UserModel> Users { get; set; }    
    }
}