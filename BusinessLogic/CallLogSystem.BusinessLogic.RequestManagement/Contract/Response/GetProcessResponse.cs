﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Response
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetProcessResponse:BaseResponse
    {
        public List<Model.RequestModel> Requests { get; set; }
    }
}