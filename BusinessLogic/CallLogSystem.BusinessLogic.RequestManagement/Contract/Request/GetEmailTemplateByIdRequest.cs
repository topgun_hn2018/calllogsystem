﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetEmailTemplateByIdRequest : BaseRequest
    {
        public int Id { get; set; }
    }
}
