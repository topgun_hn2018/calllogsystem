﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchPartyRequest:BaseRequest
    {
        public string PartyTypeId { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public bool? IsActive { get; set; }
    }
}