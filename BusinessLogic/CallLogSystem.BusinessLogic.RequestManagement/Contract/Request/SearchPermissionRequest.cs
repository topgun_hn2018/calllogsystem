﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchPermissionRequest:BaseRequest
    {
        public int? RoleId { get; set; }
    }
}
