﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class SaveRoleRequest:BaseRequest
    {
        public RoleModel Role { get; set; }
    }
}
