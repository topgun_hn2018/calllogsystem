﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchRequestTypeRequest : BaseRequest
    {
        public string RequestTypeName { get; set; }
        public int? CategoryId { get; set; }
        public bool? Status { get; set; }

    }
}