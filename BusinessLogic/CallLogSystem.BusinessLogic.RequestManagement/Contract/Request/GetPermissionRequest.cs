﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetPermissionRequest:BaseRequest
    {
        public string FunctionKey { get; set; }
    }
}