﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class CreateCommentRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public string Content { get; set; }
        public FileModel File { get; set; }
    }
}