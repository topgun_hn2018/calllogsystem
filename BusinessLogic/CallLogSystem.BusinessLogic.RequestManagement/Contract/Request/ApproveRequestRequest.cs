﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class ApproveRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public string Note { get; set; }
        public bool IsApproval { get; set; }
        public List<FileModel> Files { get; set; }
    }
}