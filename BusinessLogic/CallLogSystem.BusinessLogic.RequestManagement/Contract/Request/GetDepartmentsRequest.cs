﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetDepartmentsRequest :BaseRequest
    {
        public List<int> CompanyDepartmentId { get; set; }

    }
}
