﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetRequestAssignRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}