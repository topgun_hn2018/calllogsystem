﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class SavePartyRequest:BaseRequest
    {  
        public string PartyType { get; set; }
        public CompanyObject Company { get; set; }
        public DepartmentObject Department { get; set; }
        public GroupObject Group { get; set; }
    }
}
