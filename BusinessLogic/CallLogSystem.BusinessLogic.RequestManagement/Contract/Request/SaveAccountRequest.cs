﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class SaveAccountRequest : BaseRequest
    {
        public UserModel Account { get; set; }
    }
}
