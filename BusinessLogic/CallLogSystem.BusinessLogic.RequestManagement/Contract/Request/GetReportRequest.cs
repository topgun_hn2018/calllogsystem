﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System;
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetReportRequest:BaseRequest
    {
        public string RequestNo { get; set; }
        public string RequestName { get; set; }
        public IEnumerable<int> ServiceId { get; set; }
        public IEnumerable<int> CompanyIds { get; set; }
        public IEnumerable<int> DepartmentIds { get; set; }
        public IEnumerable<int> GroupIds { get; set; }
        public IEnumerable<int> RequestStatusId { get; set; }
        public int CompletedStatusId { get; set; }

        public IEnumerable<int> RequesterIds { get; set; }
        public DateTime RequestToDate { get; set; }
        public DateTime RequestFromDate { get; set; }

        public IEnumerable<int> ApproverIds { get; set; }
        public DateTime ApproveToDate { get; set; }
        public DateTime ApproveFromDate { get; set; }

        public IEnumerable<int> AssignerIds { get; set; }
        public DateTime AssignToDate { get; set; }
        public DateTime AssignFromDate { get; set; }

        public IEnumerable<int> HandlerIds { get; set; }
        public DateTime HandleToDate { get; set; }
        public DateTime HandleFromDate { get; set; }

        public bool IsDownload { get; set; }
    }
}
