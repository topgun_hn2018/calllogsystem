﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SearchGroupRequest:BaseRequest
    {
        public List<int> CompanySearch { get; set; }
        public List<int> DepartmentSearch { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public bool? IsActive { get; set; }
        public int GroupUserLeaderId { get; set; }
        public int GroupUserMemberId { get; set; }
    }
}