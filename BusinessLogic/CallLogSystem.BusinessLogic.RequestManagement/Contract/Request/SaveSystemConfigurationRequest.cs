﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class SaveSystemConfigurationRequest : BaseRequest
    {
        public List<SystemParameterModel> SystemParameters { get; set; }
    }
}
