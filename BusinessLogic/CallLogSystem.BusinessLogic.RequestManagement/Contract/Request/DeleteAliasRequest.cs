﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class DeleteAliasRequest : BaseRequest
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
    }
}
