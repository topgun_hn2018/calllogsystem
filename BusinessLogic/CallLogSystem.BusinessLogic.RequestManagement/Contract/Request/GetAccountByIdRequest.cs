﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetAccountByIdRequest : BaseRequest
    {
        public int Id { get; set; }
    }
}
