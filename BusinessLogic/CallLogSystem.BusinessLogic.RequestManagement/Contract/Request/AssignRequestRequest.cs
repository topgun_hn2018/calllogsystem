﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class AssignRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public int GroupId { get; set; }
        public int AssigneeId { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public bool IsCancel { get; set; }
    }
}