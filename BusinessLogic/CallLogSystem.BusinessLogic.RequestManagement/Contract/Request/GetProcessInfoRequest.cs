﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetProcessInfoRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}