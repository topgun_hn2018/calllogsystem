﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchQuestionsRequest : BaseRequest
    {
        public int QuestionTypeId { get; set; }
        public string QuestionName { get; set; }
        public bool IsActive { get; set; }

    }
}