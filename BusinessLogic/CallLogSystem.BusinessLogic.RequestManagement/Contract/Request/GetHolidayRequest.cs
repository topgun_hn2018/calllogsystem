﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetHolidayRequest:BaseRequest
    {
        public int SelectCompanyId { get; set; }
        public int Year { get; set; }
        public int? Month { get; set; }
    }
}