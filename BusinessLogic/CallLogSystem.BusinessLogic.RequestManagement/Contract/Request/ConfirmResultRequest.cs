﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class ConfirmResultRequest:BaseRequest
    {
        public Confirmation Confirmation { get; set; }
    }
}