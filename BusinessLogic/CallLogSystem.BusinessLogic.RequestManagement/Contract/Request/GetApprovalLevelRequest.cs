﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetApprovalLevelRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}