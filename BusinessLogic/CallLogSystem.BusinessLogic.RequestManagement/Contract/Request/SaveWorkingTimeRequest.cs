﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SaveWorkingTimeRequest : BaseRequest
    {
        public Model.WorkingTime WorkingTime { get; set; }
    }
}
