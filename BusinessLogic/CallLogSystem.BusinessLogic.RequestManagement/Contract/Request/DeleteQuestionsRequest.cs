﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class DeleteQuestionsRequest : BaseRequest
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
    }
}
