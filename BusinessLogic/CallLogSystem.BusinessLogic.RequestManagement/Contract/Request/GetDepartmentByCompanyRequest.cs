﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetDepartmentByCompanyRequest:BaseRequest
    {
        public List<int> CompaniesId { get; set; }
    }
}
