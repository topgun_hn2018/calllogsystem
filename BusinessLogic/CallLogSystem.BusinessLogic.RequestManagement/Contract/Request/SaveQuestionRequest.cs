﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class SaveQuestionRequest : BaseRequest
    {
        public QuestionModel Questions { get; set; }
    }
}
