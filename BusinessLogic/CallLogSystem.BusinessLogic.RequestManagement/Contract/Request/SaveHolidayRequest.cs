﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SaveHolidayRequest : BaseRequest
    {
        public int SelectedCompanyId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public List<Model.HolidayModel> Holydays { get; set; }
    }
}
