﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class ProcessRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int StatusId { get; set; }
    }
}
