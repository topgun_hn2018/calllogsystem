﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetConfirmResultRequest:BaseRequest
    {
        public int RequestId { get; set; } 
    }
}