﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchAccountRequest : BaseRequest
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public bool? IsActive { get; set; }
    }
}
