﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetGroupByDepartmentRequest:BaseRequest
    {
        public List<int> DepartmentIds { get; set; }
        public List<int> CompaniesId { get; set; }
    }
}
