﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetHistoriesRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}