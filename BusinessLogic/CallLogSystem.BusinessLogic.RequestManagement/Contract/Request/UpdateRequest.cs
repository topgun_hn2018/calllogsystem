﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System;
    using System.Collections.Generic;
    using Common.ContractBase;
    using Model;

    public class UpdateRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public string CreateType { get; set; }
        public int ServiceId { get; set; }
        public string RequestName { get; set; }
        public int? ParentId { get; set; }
        public string Description { get; set; }
        public DateTime ExpectedCompleteDate { get; set; }
        public List<int> InformTos { get; set; }

        public List<FileModel> Files { get; set; }
    }
}