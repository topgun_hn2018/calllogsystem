﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetGroupByRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}