﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class FollowRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public bool IsFollow { get; set; }
    }
}