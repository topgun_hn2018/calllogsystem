﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class PreSendEmailTemplateRequest : BaseRequest
    {
        public EmailTemplateModel EmailTemplate { get; set; }
    }
}
