﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetByIdRequest:BaseRequest
    {
        public int Id { get; set; }
    }
}