﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchRoleRequest:BaseRequest
    {
        public string Name { get; set; }
        public int IsActive { get; set; }
    }
}
