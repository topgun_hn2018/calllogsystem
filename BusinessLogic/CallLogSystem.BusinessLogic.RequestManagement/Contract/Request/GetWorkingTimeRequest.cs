﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetWorkingTimeRequest:BaseRequest
    {
        public int SelectCompanyId { get; set; }
        public int Year { get; set; }
    }
}