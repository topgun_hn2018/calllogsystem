﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System;
    using System.Collections.Generic;
    using Common.ContractBase;

    public class SearchRequest:BaseRequest
    {
        public List<int> ServiceId { get; set; }
        public string RequestNo { get; set; }
        public string RequestName { get; set; }
        public List<int> RequestStatusId { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public int RequesterId { get; set; }
        public int CompletedStatusId { get; set; }

    }
}