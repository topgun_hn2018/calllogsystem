﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class SaveEmailTemplateRequest : BaseRequest
    {
        public EmailTemplateModel EmailTemplate { get; set; }
    }
}
