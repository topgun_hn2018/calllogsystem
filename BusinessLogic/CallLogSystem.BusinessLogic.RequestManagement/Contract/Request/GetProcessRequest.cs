﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using System;
    using System.Collections.Generic;
    using Common.ContractBase;

    public class GetProcessRequest:BaseRequest
    {
        public List<int> ServiceId { get; set; }
        public string RequestNo { get; set; }
        public string RequestName { get; set; }
        public List<int> RequestStatusId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CreateDateTo { get; set; }
        public List<int> RequesterId { get; set; }
    }
}
