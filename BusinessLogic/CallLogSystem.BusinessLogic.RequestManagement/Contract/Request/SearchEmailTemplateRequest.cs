﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchEmailTemplateRequest : BaseRequest
    {
        public string Name { get; set; }
        public int IsActive { get; set; }
        public string Subject { get; set; }
        public int StatusToId { get; set; }
    }
}
