﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchCategoriesRequest : BaseRequest
    {
        public string Name { get; set; }
        public bool? IsActive { get; set; }

    }
}