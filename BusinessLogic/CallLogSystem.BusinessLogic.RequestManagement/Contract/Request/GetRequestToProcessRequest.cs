﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetRequestToProcessRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}
