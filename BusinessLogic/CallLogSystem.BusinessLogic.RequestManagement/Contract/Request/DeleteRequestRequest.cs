﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class DeleteRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}