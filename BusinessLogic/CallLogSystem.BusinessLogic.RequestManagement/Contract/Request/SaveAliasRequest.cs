﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;
    using Model;

    public class SaveAliasRequest : BaseRequest
    {
        public AliasModel AliasModel { get; set; }
    }
}
