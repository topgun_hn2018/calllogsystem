﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetChildRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}
