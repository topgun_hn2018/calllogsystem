﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetAliasByIdRequest : BaseRequest
    {
        public int Id { get; set; }
    }
}
