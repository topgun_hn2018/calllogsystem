﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetUsersByRequestRequest:BaseRequest
    {
        public int RequestId { get; set; }
        public int GroupId { get; set; }
    }
}