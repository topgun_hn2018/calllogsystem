﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SaveRequestTypeRequest : BaseRequest
    {
        public Model.RequestTypeModel RequestType { get; set; }
    }
}
