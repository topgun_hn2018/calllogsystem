﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetRoleByIdRequest : BaseRequest
    {
        public int Id { get; set; }
    }
}
