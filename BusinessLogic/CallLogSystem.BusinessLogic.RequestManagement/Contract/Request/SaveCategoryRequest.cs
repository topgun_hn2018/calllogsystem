﻿
namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SaveCategoryRequest : BaseRequest
    {
        public Model.CategoryModel Category { get; set; }
    }
}
