﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchServiceRequest:BaseRequest
    {
        public int RequestTypeId { get; set; }
        public int IsActive { get; set; }
        public int CompanyId { get; set; }
        public int DepartmentId { get; set; }
        public int GroupId { get; set; }
    }
}