﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class SearchDepartmentRequest:BaseRequest
    {
        public int? CompanySearch { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public bool? IsActive { get; set; }
        public int Manager { get; set; }
        public int Reporter { get; set; }
    }
}