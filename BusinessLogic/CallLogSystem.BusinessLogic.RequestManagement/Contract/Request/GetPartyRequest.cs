﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetPartyRequest:BaseRequest
    {
        public string PartyTypeId { get; set; }
        public int PartyId { get; set; }
    }
}