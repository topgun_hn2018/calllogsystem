﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Request
{
    using Common.ContractBase;

    public class GetApprovalRequest:BaseRequest
    {
        public int RequestId { get; set; }
    }
}