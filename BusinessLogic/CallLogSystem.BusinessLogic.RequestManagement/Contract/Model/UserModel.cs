﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string WorkArea { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public int RoleId { get; set; }
        public int DefaultAliasId { get; set; }
        public List<RoleModel> Roles { get; set; }
        public List<AliasModel> Aliases { get; set; } 
        public bool IsActive { get; set; }
        public string Status { get; set; }
    }
}