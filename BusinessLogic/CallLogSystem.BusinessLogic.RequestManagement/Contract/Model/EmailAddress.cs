﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class EmailAddress
    {
        public List<UserEmailAddressModel> SendTo { get; set; }
        public List<UserEmailAddressModel> CCs { get; set; } 
    }
}