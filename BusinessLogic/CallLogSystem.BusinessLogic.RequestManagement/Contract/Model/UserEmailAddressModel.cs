﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class UserEmailAddressModel
    {
        public string FullName { get; set; } 
        public string Email { get; set; } 
        public string OwnerName { get; set; } 
    }
}