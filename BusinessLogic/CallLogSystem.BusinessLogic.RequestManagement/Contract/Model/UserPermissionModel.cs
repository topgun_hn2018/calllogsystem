﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class UserPermissionModel
    {
        public string ActionKey { get; set; } 
        public string FuntionKey { get; set; } 
        public int UserId { get; set; } 
        public int CompanyId { get; set; } 
    }
}