﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class RequestStatusModel
    {
        public int Id { get; set; } 
        public string Name { get; set; } 
    }
}