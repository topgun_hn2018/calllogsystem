﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class PartyModel
    {
        public int PartyId { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }        
        public bool IsActive { get; set; }        
    }
}