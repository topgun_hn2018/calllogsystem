﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class ServiceModel
    {
        public int? Id { get; set; }
        public int RequestTypeId { get; set; }
        public string Name { get; set; }
        public bool? IsActiveService { get; set; }
        public string Description { get; set; }
        public bool IsProvidedByCurrentCompany { get; set; }
        public List<string> ProvidedCompany { get; set; }
        public string RequestTemplate { get; set; }
    }
}