﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class EmailInformToModel
    {
        public int Id { get; set; }
        public int EmailTemplateId { get; set; }
        public int UserId { get; set; }
    }
}
