﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class AddonModel
    {
        public int Id { get; set; }
        public int AliasId { get; set; }
        public int PermissionId { get; set; }
    }
}
