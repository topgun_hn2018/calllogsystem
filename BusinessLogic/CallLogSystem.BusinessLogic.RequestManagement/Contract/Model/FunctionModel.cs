﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public  class FunctionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
