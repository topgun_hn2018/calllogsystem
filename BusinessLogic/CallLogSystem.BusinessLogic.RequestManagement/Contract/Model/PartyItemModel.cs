﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class PartyItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  bool IsActive { get; set; }
        public bool IsShow { get; set; }

    }
}