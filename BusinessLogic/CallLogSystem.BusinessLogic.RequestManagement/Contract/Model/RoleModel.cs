﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class RoleModel
    {
        public int Id { get; set; }
        public int AliasId { get; set; }
        public string Name { get; set; }
        public List<PermissionModel> Permissions { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDefault { get; set; }
        public string CompanyName { get; set; }
    }
}
