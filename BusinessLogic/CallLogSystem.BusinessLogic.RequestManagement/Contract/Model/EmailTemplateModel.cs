﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;
    using System.Collections.Generic;

    public class EmailTemplateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Subject { get; set; }
        public string StatusToName { get; set; }
        public int StatusToId { get; set; }
        public string Content { get; set; }
        public string EmailSendTo { get; set; }
        public List<EmailInformToModel> EmailCCs { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
