﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class QuestionTypeModel
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    }
}