﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;
    using System.Collections.Generic;

    public class ApprovalLevelModel
    {
        public int HistoryId { get; set; }
        public int Level { get; set; }
        public string UserName { get; set; }
        public DateTime ApprovalDateTime { get; set; }
        public string Status  { get; set; }
        public string Note { get; set; }
        public string Description { get; set; }
        public List<FileModel> Files { get; set; }
        public double TAT { get; set; }
        public double RealTAT { get; set; }
    }
}