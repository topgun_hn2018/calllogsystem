﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class CompanyModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}
