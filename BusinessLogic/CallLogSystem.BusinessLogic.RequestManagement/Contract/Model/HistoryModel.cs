﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;
    using System.Collections.Generic;

    public class HistoryModel
    {
        public int HistoryId { get; set; }
        public int EventType { get; set; }
        public int CurrentStatus { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public List<FileModel> Files { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserName { get; set; }
    }
}