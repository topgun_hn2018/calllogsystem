﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class GroupByRequest
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public bool IsProcessing { get; set; }
    }
}