﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public int QuestionTypeId { get; set; }
        public string QuestionName { get; set; }
        public string Answer { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
        
        //
        public string QuestionsTypeName { get; set; }
    }
}