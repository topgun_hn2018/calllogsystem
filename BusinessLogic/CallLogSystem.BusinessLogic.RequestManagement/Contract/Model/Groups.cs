﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class Groups:PartyModel
    {
        public int DepartmentId { get; set; }
        public List<GroupUserModel> GroupUsers { get; set; }
        public int CompanyId { get; set; }
        public  List<GroupUserModel> GroupLeaders { get; set; }
    }
}