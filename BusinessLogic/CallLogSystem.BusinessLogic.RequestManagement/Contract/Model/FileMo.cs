﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;

    public class FileModel
    {
        public string FileName { get; set; }
        public string FileNameServer { get; set; }
        public string Extension { get; set; }
        public double Length { get; set; }
        public DateTime UploadedDate { get; set; }
        public string FileType { get; set; }

        public string Size
        {
            get
            {
                if (this.Length > 1024 * 1024)
                {
                    return string.Format("{0} MB", Math.Round((double)this.Length / 1024 / 1024, 1));
                }
                if (this.Length > 1024)
                {
                    return string.Format("{0} KB", Math.Round((double)this.Length / 1024, 1));
                }
                return string.Format("{0} Byte", this.Length);
            }
        }
    }
}