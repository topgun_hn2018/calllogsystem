﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;

    public class HolidayModel
    {
        public int CompanyId { get; set; }
        public int HolidayTypeId { get; set; }
        public DateTime Date { get; set; }
        public string StrDate { get; set; }
        public string Note { get; set; }
    }
}
