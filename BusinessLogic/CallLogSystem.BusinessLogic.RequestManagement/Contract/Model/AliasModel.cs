﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class AliasModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public bool IsDefault { get; set; }
        public List<RoleModel> Roles { get; set; }
        public List<AddonModel> Addons { get; set; }

    }
}