﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;

    public class EmailApprovalLevel
    {
        public string AprrovalLevel { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
    }
}