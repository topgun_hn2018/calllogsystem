﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class KeyObjectModel
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}