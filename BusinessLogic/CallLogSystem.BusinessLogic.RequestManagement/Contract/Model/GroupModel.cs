﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class GroupModel
    {
        public int DepartmentId { get; set; } 
        public int GroupId { get; set; } 
        public string GroupName { get; set; } 
    }
}