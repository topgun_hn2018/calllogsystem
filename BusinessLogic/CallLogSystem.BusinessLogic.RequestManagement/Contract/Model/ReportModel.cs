﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;

    public class ReportModel
    {
        public int Index { get; set; }
        public string RequestNo { get; set; }
        public string ServiceName { get; set; }
        public string RequestName { get; set; }
        public string RequestStatus { get; set; }
        public string RequesterFullName { get; set; }
        public string RequesterCompanyCode { get; set; }
        public string RequesterDepartmentCode { get; set; }
        public DateTime? CreateRequestTime { get; set; }
        public string ParentRequestNo { get; set; }
        public string RequestChildList { get; set; }
        public string RequestNote { get; set; }
        public DateTime? ExpectedCompleteDate { get; set; }

        public string IsCompleted { get; set; }
        public string ApproverLv1 { get; set; }
        public string ApproveStatus1 { get; set; }
        public DateTime? ApprovedDate1 { get; set; }
        
        public string ApproveNote1 { get; set; }

        public string ApproverLv2 { get; set; }
        public string ApproveStatus2 { get; set; }
        public DateTime? ApprovedDate2 { get; set; }
        
        public string ApproveNote2 { get; set; }

        public string ApproverLv3 { get; set; }
        public string ApproveStatus3 { get; set; }
        public DateTime? ApprovedDate3 { get; set; }
       
        public string ApproveNote3 { get; set; }

        public string ApproverLv4 { get; set; }
        public string ApproveStatus4 { get; set; }
        public DateTime? ApprovedDate4 { get; set; }
        
        public string ApproveNote4 { get; set; }

        public string ApproverLv5 { get; set; }
        public string ApproveStatus5 { get; set; }
        public DateTime? ApprovedDate5 { get; set; }
        
        public string ApproveNote5 { get; set; }

        public string GroupHandleCode { get; set; }
        public string AssignerName { get; set; }
        public DateTime? AssignDate { get; set; }
        public string AssignNote { get; set; }

        public DateTime? ProcessStartTime { get; set; }
        public DateTime? ProcessEndTime { get; set; }
        public string HandlerName { get; set; }
        public string HandleDescription { get; set; }
        public string HandleNote { get; set; }
        //public DateTime? HandleDateTime { get; set; }
        public string HandleConfirmStatus { get; set; }
        public int Rating { get; set; }
        public string NoteIncomplete { get; set; }
        public double? TATDefault { get; set; }
        public double TAT { get; set; }
        public string Expired { get; set; }
        
        public double Duration1 { get; set; }
        public double RealDuration1 { get; set; }
        public double Duration2 { get; set; }
        public double RealDuration2 { get; set; }
        public double Duration3 { get; set; }
        public double RealDuration3 { get; set; }
        public double Duration4 { get; set; }
        public double RealDuration4 { get; set; }
        public double Duration5 { get; set; }
        public double RealDuration5 { get; set; }
        public double AssignDuration { get; set; }
        public double AssignRealDuration { get; set; }
        public double HandleDuration { get; set; }
        public double HandleRealDuration { get; set; }
        public string SurveyContent { get; set; }
    }
}
