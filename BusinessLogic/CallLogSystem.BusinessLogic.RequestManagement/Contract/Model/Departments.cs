﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class Departments: PartyModel
    {
        public int CompanyId { get; set; }
        public bool IsBOD { get; set; }
        public List<DepartmentUserModel> DepartmentManagements { get; set; }
        public List<DepartmentUserModel> DepartmentReporters { get; set; }
    }
}