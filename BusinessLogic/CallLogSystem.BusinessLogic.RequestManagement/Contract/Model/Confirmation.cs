﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class Confirmation
    {
        public int RequestId { get; set; }
        public int RequestStatusId { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsRequester { get; set; }
        public int Rating { get; set; }
        public string Note { get; set; }
        public string SurveyTemplate { get; set; }
    }
}