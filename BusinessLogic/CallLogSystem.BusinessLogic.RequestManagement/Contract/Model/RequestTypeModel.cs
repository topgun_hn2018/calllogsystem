﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class RequestTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public double? TAT { get; set; }
        public bool ProcessBySite { get; set; }
        public bool IsActive { get; set; }
        public string Order { get; set; }        
    }
}