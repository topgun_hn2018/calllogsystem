﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public  class CompanyObject
    {
        public int PartyId { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public bool IsActive { get; set; }
    }
}
