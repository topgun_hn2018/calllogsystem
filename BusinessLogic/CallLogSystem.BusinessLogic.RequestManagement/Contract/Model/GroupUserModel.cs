﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class GroupUserModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }
}