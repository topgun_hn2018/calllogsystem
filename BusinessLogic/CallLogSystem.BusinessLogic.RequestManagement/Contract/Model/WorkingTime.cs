﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class WorkingTime{
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int Time1 { get; set; }
        public int Time2 { get; set; }
        public int Time3 { get; set; }
        public int Time4 { get; set; }
        public double HourPerDay { get; set; }
        public int Year { get; set; }
    }
}
