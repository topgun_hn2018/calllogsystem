﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class ServiceSearch
    {
        public int ServiceId { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string GroupName { get; set; }
        public string RequestTypeName { get; set; }
        public bool IsActive { get; set; } 
    }
}