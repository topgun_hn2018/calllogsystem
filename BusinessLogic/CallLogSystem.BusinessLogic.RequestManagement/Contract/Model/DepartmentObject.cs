﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class DepartmentObject
    {
        public int PartyId { get; set; }
        public int CompanyId { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public List<int> SelectedManagements { get; set; }
        public List<int> SelectedReporters { get; set; }
        public bool IsActive { get; set; }
        public bool IsBOD { get; set; }
    }
}
