﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class GroupServiceModel
    {
        public int CompanyId { get; set; } 
        public int DepartmentId { get; set; } 
        public int GroupId { get; set; } 
    }
}