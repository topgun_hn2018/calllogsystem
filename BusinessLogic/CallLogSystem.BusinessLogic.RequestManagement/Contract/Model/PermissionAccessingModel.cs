﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class PermissionAccessingModel
    {
        public bool Home { get; set; }
        public bool AddRequest { get; set; }
        public bool ManageRequest { get; set; }
        public bool ManageApprovalRequest { get; set; }
        public bool ManageAssignRequest { get; set; }
        public bool ManageProcess { get; set; }
        public bool ManageRelatedRequest { get; set; }
        public bool Report { get; set; }
        public bool ManageAccount { get; set; }
        public bool ManageRole { get; set; }
        public bool ManageWorkingTime { get; set; }
        public bool ManageRequestType { get; set; }
        public bool ManageService { get; set; }
        public bool ManageCompany { get; set; }
        public bool ManageDepartment { get; set; }
        public bool ManageGroup { get; set; }
        public bool ManageRequestGroup { get; set; }
        public bool ManageEmailTemplate { get; set; }
        public bool SysConfiguration { get; set; }
        public bool SearchRequest { get; set; }
        public bool ManageFrequentlyQuestion { get; set; }
        public bool FrequentlyAskedQuestions { get; set; }
    }
}