﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;
    using System.Collections.Generic;

    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<RequestTypeModel> RequestTypes { get; set; }
        public int OrderNum { get; set; }
        public string AvatarSource { get; set; }
        public DateTime? AvataCreateDate { get; set; }
        public bool IsActive { get; set; }
    }
}
