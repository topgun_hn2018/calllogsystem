﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class DepartmentUserModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int UserId { get; set; }
        public int DepartmentId { get; set; }
    }
}