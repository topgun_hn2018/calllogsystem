﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class ActionModel
    {
        public bool Edit { get; set; }
        public bool Add { get; set; }
        public bool Delete { get; set; }
        public bool DeleteFile { get; set; }
        public bool Upload { get; set; }
        public bool View { get; set; }
        public bool Download { get; set; }
    }
}