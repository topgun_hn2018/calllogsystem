﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class DepartmentModel
    {
        public int CompanyId { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; } 
    }
}