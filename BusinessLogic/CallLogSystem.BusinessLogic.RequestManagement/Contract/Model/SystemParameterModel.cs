﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class SystemParameterModel
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public bool? IsActive { get; set; }
    }
}
