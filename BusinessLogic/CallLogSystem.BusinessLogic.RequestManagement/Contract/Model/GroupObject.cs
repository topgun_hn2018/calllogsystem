﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System.Collections.Generic;

    public class GroupObject
    {
        public int PartyId { get; set; }
        public int DepartmentId { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public bool IsActive { get; set; }
        public List<int> SelectedGroupLeaders { get; set; }
        public List<int> SelectedGroupUsers { get; set; }
    }
}
