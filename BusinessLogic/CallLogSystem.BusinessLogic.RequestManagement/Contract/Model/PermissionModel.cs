﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    public class PermissionModel
    {
        public int Id { get; set; }
        public int FunctionActionId { get; set; }
        public int RoleId { get; set; }
        public string Name { get; set; }
    }
}
