﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;
    using System.Collections.Generic;

    public class RequestModel
    {
        public int RequestId { get; set; }
        public int ServiceId { get; set; }
        public int? AssignId { get; set; }
        public string ServiceDescription { get; set; }
        public int? ParentId { get; set; }
        public string RequestName { get; set; }
        public string Description { get; set; }
        public DateTime ExpectedCompleteDate { get; set; }
        public List<int> InformTos { get; set; }
        public List<FileModel> Files { get; set; }
        public string RequestNo { get; set; }
        public string RequestTypeName { get; set; }
        public int RequestTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string RequestStatusName { get; set; }
        public int RequestStatusId { get; set; }
        public string RequesterName { get; set; }
        public string CompanyCode { get; set; }
        public List<string> DepartmentName { get; set; }
        public List<string> RelatedRequest { get; set; }
    }
}