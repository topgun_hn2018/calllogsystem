﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Contract.Model
{
    using System;

    public class TATModel
    {
        public int HistoryId { get; set; }
        public int CompanyId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}