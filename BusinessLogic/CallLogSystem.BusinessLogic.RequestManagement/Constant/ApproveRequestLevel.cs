﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using System.Collections.Generic;

    public static class ApproveRequestLevel
    {
         public static readonly List<int> Levels = new List<int>
         {
             EventTypeConstant.ApprovedRequestLevel1,
             EventTypeConstant.ApprovedRequestLevel2,
             EventTypeConstant.ApprovedRequestLevel3,
             EventTypeConstant.ApprovedRequestLevel4,
             EventTypeConstant.ApprovedRequestLevel5,
             EventTypeConstant.RejectedRequestLevel1,
             EventTypeConstant.RejectedRequestLevel2,
             EventTypeConstant.RejectedRequestLevel3,
             EventTypeConstant.RejectedRequestLevel4,
             EventTypeConstant.RejectedRequestLevel5
         };
    }
}