﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using System.Collections.Generic;

    public static class ApprovalEventType
    {
        public static readonly Dictionary<int, int> ApprovalLevelLookup = new Dictionary<int, int>
        {
            {1, EventTypeConstant.ApprovedRequestLevel1},
            {2, EventTypeConstant.ApprovedRequestLevel2},
            {3, EventTypeConstant.ApprovedRequestLevel3},
            {4, EventTypeConstant.ApprovedRequestLevel4},
            {5, EventTypeConstant.ApprovedRequestLevel5}
        };
       
        public static readonly List<int> ApprovalLevel = new List<int>
        {
            EventTypeConstant.ApprovedRequestLevel1,
            EventTypeConstant.ApprovedRequestLevel2,
            EventTypeConstant.ApprovedRequestLevel3,
            EventTypeConstant.ApprovedRequestLevel4,
            EventTypeConstant.ApprovedRequestLevel5
        };
    }
}