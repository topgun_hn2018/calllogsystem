﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using System.Collections.Generic;

    public static class CompletedStatusConstant
    {
         public static readonly List<int> Completed = new List<int>
         {
            RequestStatusConstant.RejectedApproval,
            RequestStatusConstant.RejectedProcess,
            RequestStatusConstant.Completed,
            RequestStatusConstant.ProcessCompleted
         };

         public static readonly List<int> InCompleted = new List<int>
         {
            RequestStatusConstant.WaitingForApproval,
            RequestStatusConstant.WaitingForAssign,
            RequestStatusConstant.WaitingForProcess,
            RequestStatusConstant.Processing
         };

         public static readonly List<int> InCompletedStatus = new List<int>
         {
            RequestStatusConstant.WaitingForApproval,
            RequestStatusConstant.WaitingForAssign,
            RequestStatusConstant.WaitingForProcess,
            RequestStatusConstant.Processing,
            RequestStatusConstant.Draft
         };
    }
}