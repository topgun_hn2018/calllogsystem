﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    public static class Key
    {
        //EventType
        public const string CreateRequest = "CreateRequest";
        public const string DeleteRequest = "DeleteRequest";
        public const string ApprovedRequestLevel1 = "ApprovedRequestLevel1";
        public const string ApprovedRequestLevel2 = "ApprovedRequestLevel2";
        public const string ApprovedRequestLevel3 = "ApprovedRequestLevel3";
        public const string ApprovedRequestLevel4 = "ApprovedRequestLevel4";
        public const string ApprovedRequestLevel5 = "ApprovedRequestLevel5";
        public const string RejectedRequestLevel1 = "RejectedRequestLevel1";
        public const string RejectedRequestLevel2 = "RejectedRequestLevel2";
        public const string RejectedRequestLevel3 = "RejectedRequestLevel3";
        public const string RejectedRequestLevel4 = "RejectedRequestLevel4";
        public const string RejectedRequestLevel5 = "RejectedRequestLevel5";
        public const string AssignRequest = "AssignRequest";
        public const string StartProcess = "StartProcess";
        public const string CompletedProcess = "CompletedProcess";
        public const string RejectRequest = "RejectRequest";
        public const string ConfirmResult = "ConfirmResult";
        public const string Comment = "Comment";
        public const string Reopen = "Reopen";
        public const string UpdateProcess = "UpdateProcess"; 

        //RequestStatus
        public const string Draft = "Draft";
        public const string WaitingForApproval = "WaitingForApproval";
        public const string RejectedApproval = "RejectedApproval";
        public const string WaitingForAssign = "WaitingForAssigned";
        public const string WaitingForProcess = "WaitingForProcess";
        public const string Processing = "Processing";
        public const string ProcessCompleted = "ProcessCompleted";
        public const string RejectedProcess = "RejectedProcess";
        public const string Completed = "Completed"; 
    }
}