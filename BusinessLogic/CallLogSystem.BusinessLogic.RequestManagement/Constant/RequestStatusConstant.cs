﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using Utilities;

    public static class RequestStatusConstant
    {
        public static int Draft
        {
            get {return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.Draft);}
        }

        public static int WaitingForApproval
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.WaitingForApproval); }
        }

        public static int RejectedApproval
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.RejectedApproval); }
        }

        public static int WaitingForAssign
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.WaitingForAssign); }
        }

        public static int WaitingForProcess
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.WaitingForProcess); }
        }

        public static int Processing
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.Processing); }
        }

        public static int ProcessCompleted
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.ProcessCompleted); }
        }

        public static int RejectedProcess
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.RejectedProcess); }
        }

        public static int Completed
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.Completed); }
        }

        public static int Comment
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.RequestStatus, Key.Comment); }
        }

    }
}