﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using System.Collections.Generic;

    public static class RejectEventType
    {
        public static readonly Dictionary<int, int> ApprovalLevelLookup = new Dictionary<int, int>()
        {
            {1, EventTypeConstant.RejectedRequestLevel1},
            {2, EventTypeConstant.RejectedRequestLevel2},
            {3, EventTypeConstant.RejectedRequestLevel3},
            {4, EventTypeConstant.RejectedRequestLevel4},
            {5, EventTypeConstant.RejectedRequestLevel5}
        };
        
    }
}