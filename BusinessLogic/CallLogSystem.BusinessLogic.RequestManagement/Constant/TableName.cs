﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    public static class TableName
    {
        public const string EventTypes = "EventTypes";
        public const string RequestStatus = "RequestStatus";
    }
}