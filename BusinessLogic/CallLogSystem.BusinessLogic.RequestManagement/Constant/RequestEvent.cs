﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using System.Collections.Generic;

    public static class RequestEvent
    {
        public static readonly Dictionary<string, List<int>> ApproveEventLookup = new Dictionary<string, List<int>>()
        {
            {Key.ApprovedRequestLevel1, new List<int> { EventTypeConstant.ApprovedRequestLevel1, EventTypeConstant.RejectedRequestLevel1 } },
            {Key.ApprovedRequestLevel2, new List<int> { EventTypeConstant.ApprovedRequestLevel2, EventTypeConstant.RejectedRequestLevel2 } },
            {Key.ApprovedRequestLevel3, new List<int> { EventTypeConstant.ApprovedRequestLevel3, EventTypeConstant.RejectedRequestLevel3 } },
            {Key.ApprovedRequestLevel4, new List<int> { EventTypeConstant.ApprovedRequestLevel4, EventTypeConstant.RejectedRequestLevel4 } },
            {Key.ApprovedRequestLevel5, new List<int> { EventTypeConstant.ApprovedRequestLevel5, EventTypeConstant.RejectedRequestLevel5 } },
            {Key.AssignRequest, new List<int> { EventTypeConstant.AssignRequest, EventTypeConstant.RejectRequest} },
            {Key.ProcessCompleted, new List<int>
            {
                EventTypeConstant.StartProcess,
                EventTypeConstant.CompletedProcess,
                EventTypeConstant.RejectRequest
            }},
            {Key.Reopen, new List<int>{EventTypeConstant.Reopen}},
            {Key.Completed, new List<int>
            {
                EventTypeConstant.CompletedProcess,
                EventTypeConstant.RejectRequest,
                EventTypeConstant.RejectedRequestLevel1,
                EventTypeConstant.RejectedRequestLevel2,
                EventTypeConstant.RejectedRequestLevel3,
                EventTypeConstant.RejectedRequestLevel4,
                EventTypeConstant.RejectedRequestLevel5,
                EventTypeConstant.DeleteRequest
            }}
        };
    }
}
