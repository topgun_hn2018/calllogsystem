﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    using System.Collections.Generic;
    using Utilities;

    public static class EventTypeConstant
    {
        public static int CreateRequest
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.CreateRequest); }
        }

        public static int DeleteRequest
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.DeleteRequest); }
        }

        public static int ApprovedRequestLevel1
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.ApprovedRequestLevel1); }
        }

        public static int ApprovedRequestLevel2
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.ApprovedRequestLevel2); }
        }

        public static int ApprovedRequestLevel3
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.ApprovedRequestLevel3); }
        }

        public static int ApprovedRequestLevel4
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.ApprovedRequestLevel4); }
        }

        public static int ApprovedRequestLevel5
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.ApprovedRequestLevel5); }
        }

        public static int RejectedRequestLevel1
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.RejectedRequestLevel1); }
        }

        public static int RejectedRequestLevel2
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.RejectedRequestLevel2); }
        }

        public static int RejectedRequestLevel3
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.RejectedRequestLevel3); }
        }

        public static int RejectedRequestLevel4
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.RejectedRequestLevel4); }
        }

        public static int RejectedRequestLevel5
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.RejectedRequestLevel5); }
        }

        public static int AssignRequest
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.AssignRequest); }
        }

        public static int StartProcess
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.StartProcess); }
        }

        public static int CompletedProcess
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.CompletedProcess); }
        }

        public static int RejectRequest
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.RejectRequest); }
        }

        public static int ConfirmResult
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.ConfirmResult); }
        }

        public static int Comment
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.Comment); }
        }

        public static int Reopen
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.Reopen); }
        }

        public static int UpdateProcess
        {
            get { return KeyObjectUtilities<int>.GetKeyObject(TableName.EventTypes, Key.UpdateProcess); }
        }

        public static readonly Dictionary<int, int> EventTypeLookup = new Dictionary<int, int>()
        {
            {ApprovedRequestLevel1, 1},
            {ApprovedRequestLevel2, 2},
            {ApprovedRequestLevel3, 3},
            {ApprovedRequestLevel4, 4},
            {ApprovedRequestLevel5, 5},
            {RejectedRequestLevel1, 1},
            {RejectedRequestLevel2, 2},
            {RejectedRequestLevel3, 3},
            {RejectedRequestLevel4, 4},
            {RejectedRequestLevel5, 5},
        };   
    }
}