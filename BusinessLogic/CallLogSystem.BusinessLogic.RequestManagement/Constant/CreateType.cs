﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Constant
{
    public static class CreateType
    {
        public const string Send = "SE";
        public const string Draft = "DR";
    }
}