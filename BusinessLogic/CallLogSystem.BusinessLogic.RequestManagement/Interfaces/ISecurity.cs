﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface ISecurity
    {
        /// <summary>
        /// Gets the permission by function.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        GetPermissionResponse GetPermissionByFunction(GetPermissionRequest request);


        /// <summary>
        /// Gets the permission accessing.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        GetPermissionAccessingResponse GetPermissionAccessing(GetPermissionAccessingRequest request);
    }
}