﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IRequestGroupManagement
    {
        SearchCategoriesResponse SearchCategories(SearchCategoriesRequest request);
        SearchCategoriesResponse GetAllCategoryActive(SearchCategoriesRequest request);
        GetCategoryByIdResponse GetCategoryById(GetByIdRequest request);
        SaveResponse SaveCategory(SaveCategoryRequest request);        
        SearchCategoriesResponse GetAllCategory();        
    }
}