﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using System.Collections.Generic;
    using Contract.Request;
    using Contract.Response;

    public interface IPartyManagement
    {
        SearchPartyResponse SearchGroup(SearchGroupRequest request);
        SearchPartyResponse SearchDepartment(SearchDepartmentRequest request);
        GetDepartmentsResponse GetDepartments(GetDepartmentsRequest request);

        /// <summary>
        /// Gets the department by company.
        /// </summary>
        /// <param name="companyIds">The company ids.</param>
        /// <returns></returns>
        GetDepartmentByCompanyResponse GetDepartmentByCompany(List<int> companyIds);

        /// <summary>
        /// Gets the group by department.
        /// </summary>
        /// <param name="departmentIds">The department identifier.</param>
        /// <returns></returns>
        GetGroupByDepartmentResponse GetGroupByDepartment(List<int> departmentIds);
        SearchPartyResponse SearchParty(SearchPartyRequest request);


        GetPartyResponse GetParty(GetPartyRequest request);

        GetAllCompanyResponse GetAllCompanies();
        GetAllCompanyResponse GetActiveCompanies();
        GetAllUserResponse GetAllPartyUsers();
        GetAllUserResponse GetAllDepartmentUserManagers();
        GetAllUserResponse GetAllDepartmentUserReporters();
        GetAllUserResponse GetAllGroupUserLeaders();
        GetAllUserResponse GetAllGroupUserMembers();

        SaveResponse SaveParty(SavePartyRequest request);
    }
}