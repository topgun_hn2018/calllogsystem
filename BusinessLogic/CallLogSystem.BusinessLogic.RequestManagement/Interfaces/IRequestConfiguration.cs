﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IRequestConfiguration
    {
        GetAllRequestStatusResponse GetAllRequestStatus();
        /// <summary>
        /// Gets all request types.
        /// </summary>
        /// <returns></returns>
        GetAllRequestTypeResponse GetAllRequestTypes();

        SearchRequestTypeResponse SearchRequestType(SearchRequestTypeRequest request);
        GetRequestTypeByIdResponse GetRequestTypeById(GetByIdRequest request);

        SaveResponse SaveRequestType(SaveRequestTypeRequest request);
    }
}