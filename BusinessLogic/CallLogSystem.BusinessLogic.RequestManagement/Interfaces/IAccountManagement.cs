﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IAccountManagement
    {
        /// <summary>
        /// Gets all users
        /// </summary>
        /// <returns></returns>
        GetAllUserResponse GetAllUsers(GetAllUserRequest request);
        SaveAccountResponse SaveAccount(SaveAccountRequest request);
        SaveAliasResponse SaveAlias(SaveAliasRequest request);
        SearchAccountResponse SearchAccount(SearchAccountRequest request);
        GetAccountByIdResponse GetAccountById(GetAccountByIdRequest request);
        GetAliasByIdResponse GetAliasById(GetAliasByIdRequest request);
        DeleteAliasResponse DeleteAlias(DeleteAliasRequest request);
    }
}