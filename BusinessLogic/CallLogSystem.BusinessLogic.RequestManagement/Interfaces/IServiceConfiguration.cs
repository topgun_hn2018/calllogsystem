﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IServiceConfiguration
    {

        /// <summary>
        /// Gets all services.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        GetAllServiceResponse GetAllServices(GetAllServiceRequest request);


        /// <summary>
        /// Searches the service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        SearchServiceResponse SearchService(SearchServiceRequest request);

        /// <summary>
        /// Saves the service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        SaveServiceResponse SaveService(SaveServiceRequest request);

        /// <summary>
        /// Gets the service by identifier.
        /// </summary>
        /// <param name="serviceId">The service identifier.</param>
        /// <returns></returns>
        GetServiceByIdResponse GetServiceById(int serviceId);
    }
}