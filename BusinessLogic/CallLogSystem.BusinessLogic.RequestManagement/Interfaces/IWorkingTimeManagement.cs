﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IWorkingTimeManagement
    {
        SaveResponse SaveWorkingTime(SaveWorkingTimeRequest request);

        SaveResponse SaveHoliday(SaveHolidayRequest request);
        GetWorkingTimeResponse GetWorkingTime(GetWorkingTimeRequest request);
        GetHolidayResponse GetHoliday(GetHolidayRequest request);
    }
}