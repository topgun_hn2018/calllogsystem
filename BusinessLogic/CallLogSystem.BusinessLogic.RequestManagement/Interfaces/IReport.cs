﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IReport
    {
        /// <summary>
        /// Get companies by user.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetCompanyByUserResponse GetCompanyByUser(GetCompanyByUserRequest request);

        /// <summary>
        /// Get department by user.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetDepartmentByCompanyResponse GetDepartmentByCompany(GetDepartmentByCompanyRequest request);

        /// <summary>
        /// Get group by department.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetGroupByDepartmentResponse GetGroupByDepartment(GetGroupByDepartmentRequest request);

        /// <summary>
        /// Get reports.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetReportResponse GetReport(GetReportRequest request);

        /// <summary>
        /// Get reports.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetReportResponse GetReportAssignee(GetReportRequest request);        

        /// <summary>
        /// Get users to report.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetUserReportResponse GetUserReport(GetAllUserRequest request); 
    }
}