﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Common.ContractBase;
    using Contract.Request;
    using Contract.Response;

    public interface IEmailManagement
    {
        SearchEmailTemplateResponse SearchEmailTemplate(SearchEmailTemplateRequest request);
        SaveEmailTemplateResponse SaveEmailTemplate(SaveEmailTemplateRequest request);
        GetEmailTemplateByIdResponse GetEmailTemplateById(GetEmailTemplateByIdRequest request);
        PreSendEmailTemplateResponse PreSendEmailTemplate(PreSendEmailTemplateRequest request);

        BaseResponse SendEmailAfterCloseRequestAutomatically(int requestId);
    }
}