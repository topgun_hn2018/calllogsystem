﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface IRolePermission
    {
        SaveRoleResponse SaveRole(SaveRoleRequest request);
        SearchRoleResponse SearchRole(SearchRoleRequest request);
        SearchPermissionResponse GetAllPermission();
        GetRoleByIdResponse GetRoleById(GetRoleByIdRequest request);
    }
}