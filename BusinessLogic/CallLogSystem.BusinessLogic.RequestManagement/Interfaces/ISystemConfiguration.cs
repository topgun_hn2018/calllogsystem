﻿namespace CallLogSystem.BusinessLogic.RequestManagement.Interfaces
{
    using Contract.Request;
    using Contract.Response;

    public interface ISystemConfiguration
    {
        GetSystemConfigurationResponse GetSystemConfiguration();
        SaveSystemConfigurationResponse SaveSystemConfiguration(SaveSystemConfigurationRequest request); 
    }
}