﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class LoginResponse:BaseResponse
    {
        public bool IsAuthenticated { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}