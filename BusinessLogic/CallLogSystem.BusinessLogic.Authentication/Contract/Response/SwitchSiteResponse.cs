﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Response
{
    using Common.ContractBase;
    using Model;

    public class SwitchSiteResponse:BaseResponse
    {
        public UserProfile UserProfile { get; set; }
    }
}