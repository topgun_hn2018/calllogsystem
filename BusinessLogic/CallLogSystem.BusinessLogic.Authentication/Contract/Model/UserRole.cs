﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Model
{
    public class UserInfo
    {
        public int UserAliasId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; } 
        public bool IsDefault { get; set; } 
    }
}