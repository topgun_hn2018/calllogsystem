﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Model
{
    public class TokenValues
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public int UserAliasId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Issuer { get; set; }  
        public string Audience { get; set; }
        public double ExpirationTime { get; set; }  
        public double NotBefore { get; set; }  
    }
}