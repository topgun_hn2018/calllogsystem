﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Model
{
    using System.Collections.Generic;

    public class UserProfile
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public List<UserInfo> UserInfo { get; set; }
    }
}