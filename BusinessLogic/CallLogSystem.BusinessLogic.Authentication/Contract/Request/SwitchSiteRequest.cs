﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Request
{
    using Common.ContractBase;

    public class SwitchSiteRequest:BaseRequest
    {
        public int NewCompanyId { get; set; } 
    }
}