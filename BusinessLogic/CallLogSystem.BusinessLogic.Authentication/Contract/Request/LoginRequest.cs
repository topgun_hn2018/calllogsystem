﻿namespace CallLogSystem.BusinessLogic.Authentication.Contract.Request
{
    public class LoginRequest
    {
        public string LoginId { get; set; } 
        public string Password { get; set; } 
    }
}