﻿namespace CallLogSystem.BusinessLogic.Authentication
{
    using Contract.Request;
    using Contract.Response;

    public interface IAuthenticationBusinessLogic
    {
        /// <summary>
        /// Logins
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        LoginResponse LogOn(LoginRequest request);

        /// <summary>
        /// Switches the site.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        SwitchSiteResponse SwitchSite(SwitchSiteRequest request);
    }
}