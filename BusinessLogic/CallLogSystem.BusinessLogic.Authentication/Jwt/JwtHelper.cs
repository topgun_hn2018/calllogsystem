﻿namespace CallLogSystem.BusinessLogic.Authentication.Jwt
{
    using System;
    using System.IdentityModel.Protocols.WSTrust;
    using System.IdentityModel.Tokens;
    using System.Linq;
    using System.Security.Claims;
    using Common.Constant;
    using Common.Singleton;
    using Common.Utilites;
    using Contract.Model;

    /// <summary>
    /// JWT Helper
    /// </summary>
    public static class JwtHelper
    {
        /// <summary>
        /// Generates JWT Token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GenerateToken(UserProfile user)
        {
            var defaultUserRole = user.UserInfo.FirstOrDefault(s => s.IsDefault);
            if (defaultUserRole == null)
            {
                return string.Empty;
            }
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                    {                        
                        new Claim("UserId", user.UserId.ToString()),
                        new Claim("FullName", user.FullName),
                        new Claim("UserAliasId", defaultUserRole.UserAliasId.ToString()),
                        new Claim("CompanyId", defaultUserRole.CompanyId.ToString()),
                        new Claim("CompanyName", defaultUserRole.CompanyName)
                    }),
                AppliesToAddress = ConfigUtilities.GetStringKey(ConfigurationConstant.WebtUrl, ""),
                TokenIssuerName = ConfigUtilities.GetStringKey(ConfigurationConstant.WebApiUrl, ""),
                SigningCredentials = new SigningCredentials(new
                    InMemorySymmetricSecurityKey(EncryptionHelper.SymatricKey),
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256"),
                Lifetime = new Lifetime(
                    DateTime.Now.ToLocalTime(), DateTime.Now.AddMinutes(ConfigUtilities.GetIntKey(ConfigurationConstant.TokenExpirationTime, 0)).ToLocalTime())
            };

            //Generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}