﻿namespace CallLogSystem.BusinessLogic.Authentication.Jwt
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Common.Constant;
    using Common.Enum;
    using Common.Singleton;
    using Contract.Model;

    /// <summary>
    /// JWT Validation
    /// </summary>
    public class JwtValidation
    {
        /// <summary>
        /// Validates Token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="tokenValues"></param>
        /// <returns></returns>
        public ValidationTokenResult ValidateToken(string accessToken, ref TokenValues tokenValues)
         {
             try
             {
                 SecurityToken validatedToken = this.GetValidatedToken(accessToken);
                 //Token is expired
                 if (validatedToken.ValidFrom.ToLocalTime() > DateTime.Now ||  validatedToken.ValidTo.ToLocalTime() < DateTime.Now)
                 {
                     return ValidationTokenResult.Expired;
                 }

                 //Check token values
                 tokenValues = this.ExtractToken(validatedToken.ToString());
                 if (tokenValues == null || tokenValues.GetType().GetProperties().Any(s => string.IsNullOrEmpty(s.ToString())))
                 {
                     return ValidationTokenResult.InvalidSignature;
                 }
                 return ValidationTokenResult.Valid;
                 
             }
             catch (SecurityTokenException ex)
             {
                 if (ex.Message.Split(':')[0] == "IDX10223")
                 {
                     return ValidationTokenResult.Expired;
                 }
                 return ValidationTokenResult.Invalid;
             }
         }

        /// <summary>
        /// Gets Validated Token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        private SecurityToken GetValidatedToken(string accessToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = JwtValidationParameters.Parameters;
            SecurityToken validatedToken;
            tokenHandler.ValidateToken(accessToken, validationParameters, out validatedToken);
            return validatedToken;
        }


        /// <summary>
        /// Extracts Token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        private TokenValues ExtractToken(string accessToken)
        {
            try
            {
                var tokenPart = Regex.Split(accessToken, "}.{");

                //Invalid access TokenConstant
                if (tokenPart.Length != 2)
                {
                    return null;
                }
                var claims = tokenPart[1].Remove(tokenPart[1].Length - 1).Split(',');
                Dictionary<string, string> tokenValues = new Dictionary<string, string>();
                foreach (var claim in claims)
                {
                    var items = claim.Replace("\"", "").Split(':');
                    tokenValues.Add(items[0], items.Length == 3 ? items[1] + items[2] : items[1]);
                }
                var result = this.ConvertClaimValues(tokenValues);
                return result;
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        /// <summary>
        /// Converts token values
        /// </summary>
        /// <param name="tokenValues"></param>
        /// <returns></returns>
        private TokenValues ConvertClaimValues(Dictionary<string, string> tokenValues)
        {
            TokenValues result = new TokenValues();
            foreach (var item in TokenConstant.TokenValues)
            {
                string value;
                tokenValues.TryGetValue(item.Key, out value);
                var property = typeof(TokenValues).GetProperty(item.Value);
                if (property != null)
                {
                    property.SetValue(result, Convert.ChangeType(value, property.PropertyType), null);
                }
            }
            return result;
        }
    }
}