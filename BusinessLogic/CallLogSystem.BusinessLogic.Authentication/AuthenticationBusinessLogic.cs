﻿namespace CallLogSystem.BusinessLogic.Authentication
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Caching;
    using Common.Constant;
    using Common.Enum;
    using Common.Helper;
    using Contract.Model;
    using Contract.Request;
    using Contract.Response;
    using Data.DataAccess;
    using Data.DataAccess.UoW;

    public class AuthenticationBusinessLogic:IAuthenticationBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public AuthenticationBusinessLogic(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Logins
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public LoginResponse LogOn(LoginRequest request)
        {
            if (request == null)
            {
                return new LoginResponse
                {
                    IsAuthenticated = false,
                    ErrorType = ErrorType.Error
                };
            }
            this.ClearPermissionCaching();
            //Get password key
            SystemParameter passwordSalt = this._unitOfWork.Repo<SystemParameter>().TableNoTracking.FirstOrDefault(s => s.Key == SystemParameterConstant.PasswordSalt);
            //if (request.LoginId.EndsWith(ConfigurationConstant.Email))
            //{
            //    request.LoginId = request.LoginId.Replace(ConfigurationConstant.Email, string.Empty);
            //}
            if (passwordSalt != null)
            {
                //Check UserName and Password with local account
                string password = MD5CryptoService.HashPassword(request.Password + passwordSalt.Value);
                var user =
                    this._unitOfWork.Repo<User>().TableNoTracking.FirstOrDefault(s => s.UserName.ToLower() == request.LoginId.ToLower() && s.Password == password && s.IsActive);
                if (user != null)
                {
                    ICollection<UserInfo> userInfo = this.GetUserInfo(user.Id);
                    if (userInfo == null)
                    {
                        return new LoginResponse
                        {
                            ErrorType = ErrorType.InvalidRequest,
                            IsAuthenticated = false,
                            ErrorDescription = "UC01-ERROR-01"
                        };
                    }
                    return new LoginResponse
                    {
                        UserProfile = new UserProfile
                        {
                            FullName = user.FullName,
                            UserId = user.Id,
                            UserInfo = userInfo.ToList()
                        },
                        IsAuthenticated = true
                    };
                }
            }
            
            return new LoginResponse
            {
                IsAuthenticated = false,
                ErrorType = ErrorType.Error
            }; 
        }

        /// <summary>
        /// Switches the site.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SwitchSiteResponse SwitchSite(SwitchSiteRequest request)
        {
            User user = this._unitOfWork.Repo<User>().TableNoTracking.FirstOrDefault(s => s.Id == request.UserId);
            if (user != null)
            {
                ICollection<UserInfo> userInfo = this.GetUserInfo(user.Id);
                if (userInfo != null && userInfo.Any(s => s.CompanyId == request.NewCompanyId))
                {
                    foreach (var item in userInfo)
                    {
                        item.IsDefault = item.CompanyId == request.NewCompanyId;
                    }
                    this.ClearPermissionCaching();
                    return new SwitchSiteResponse
                    {
                        UserProfile = new UserProfile
                        {
                            UserInfo = userInfo.ToList(),
                            FullName = user.FullName,
                            UserId = user.Id
                        }
                    };
                }
                return new SwitchSiteResponse
                {
                    ErrorType = ErrorType.Error
                };
            }
            return new SwitchSiteResponse
            {
                ErrorType = ErrorType.InvalidRequest
            };
            
        }

        #region private method

        /// <summary>
        /// Clears the permission caching.
        /// </summary>
        private void ClearPermissionCaching()
        {
            ObjectCache caches = MemoryCache.Default;
            //Clear accessing permission
            caches.Remove(PermissionKey.PermissionAccessing);

            //Clear permission on each function
            List<string> functionKeys = this._unitOfWork.Repo<Function>().TableNoTracking.Where(s => s.IsActive).Select(s => s.Key).ToList();
            foreach (var functionKey in functionKeys)
            {
                caches.Remove(functionKey);    
            }
        }

        /// <summary>
        /// Gets all roles of user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private ICollection<UserInfo> GetUserInfo(int userId)
        {
            var userAlias = this._unitOfWork.Repo<UserAlias>().TableNoTracking.Where(s => s.UserId == userId).ToList();
            var result = new List<UserInfo>();
            if (userAlias.All(s => !s.IsDefault))
            {
                return null;
            }
            foreach (var item in userAlias)
            {
                Company company = this._unitOfWork.Repo<Company>().TableNoTracking.First(s => s.Id == item.CompanyId);
                UserInfo userRole = new UserInfo
                {
                    UserAliasId = item.Id,
                    CompanyId = company.Id,
                    CompanyName = company.Name,
                    IsDefault = item.IsDefault
                };
                result.Add(userRole);
                
            }
            return result;
        }
        
        #endregion
    }
}